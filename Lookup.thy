(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 3 (Field and Method Lookups)\<close>

theory Lookup imports Expectation Statement begin

type_synonym 'a variable_declaration = "x \<times> 'a"

type_synonym 'C xT = "'C T variable_declaration"
type_synonym 'C xS = "'C S variable_declaration"

definition variable_name :: "'a variable_declaration \<Rightarrow> x" ("\<eta>")
  where
    [iff]: "\<eta> xD \<equiv> fst xD"

definition variable_type :: "'a variable_declaration \<Rightarrow> 'a" ("\<tau>")
  where
    [iff]: "\<tau> xS \<equiv> snd xS"

type_synonym 'a variable_declarations = "'a variable_declaration list"
type_synonym ('C, 'I) xV\<^sub>i = "('C, 'I) type variable_declarations"
type_synonym 'C xT\<^sub>i = "('C, k) xV\<^sub>i"
type_synonym 'C xS\<^sub>i = "('C, \<gamma>) xV\<^sub>i"

definition variable_names :: "('C, 'I) xV\<^sub>i \<Rightarrow> x\<^sub>i" ("\<eta>")
  where
    [iff]: "\<eta> xS\<^sub>i \<equiv> map fst xS\<^sub>i"

definition variable_types :: "('C, 'I) xV\<^sub>i \<Rightarrow> ('C, 'I) V\<^sub>i" ("\<tau>")
  where
    [iff]: "\<tau> xS\<^sub>i \<equiv> map snd xS\<^sub>i"

type_synonym ('C, 'I) method_signature = "'I \<times> ('C, 'I) xV\<^sub>i \<times> ('C, 'I) V \<times> ('C, 'I) xV\<^sub>i"
type_synonym 'C method_signature\<^sub>k = "('C, k) method_signature"
type_synonym 'C method_signature\<^sub>\<gamma> = "('C, \<gamma>) method_signature"

definition method_target:: "('C, 'I) method_signature \<Rightarrow> 'I"
  where [iff]: "method_target s \<equiv> fst s"

definition method_arguments:: "('C, 'I) method_signature \<Rightarrow> ('C, 'I) xV\<^sub>i"
  where [iff]: "method_arguments s \<equiv> fst (snd s)"

definition method_result:: "('C, 'I) method_signature \<Rightarrow> ('C, 'I) V" ("\<rho>")
  where [iff]: "method_result s \<equiv> fst (snd (snd s))"

definition method_locals:: "('C, 'I) method_signature \<Rightarrow> ('C, 'I) xV\<^sub>i"
  where [iff]: "method_locals s \<equiv> snd (snd (snd s))"

type_synonym ('C, 'I) constructor_signature = "('C, 'I) xV\<^sub>i \<times> ('C, 'I) xV\<^sub>i"

definition constructor_arguments:: "('C, 'I) constructor_signature \<Rightarrow> ('C, 'I) xV\<^sub>i"
  where [iff]: "constructor_arguments s \<equiv> fst s"

definition constructor_locals:: "('C, 'I) constructor_signature \<Rightarrow> ('C, 'I) xV\<^sub>i"
  where [iff]: "constructor_locals s \<equiv> snd s"

locale
  lookup = classes +
  constrains
    is_subclass :: "'C:: {equal, partial_order} \<Rightarrow> 'C \<Rightarrow> bool" and
    "class" :: "'C set" and
    "fields" :: "'C \<Rightarrow> f list" and
    "methods" :: "'C \<Rightarrow> m set"
  fixes
    fType :: "'C \<times> f \<rightharpoonup> 'C t" and
    mSig :: "'C \<times> m \<rightharpoonup> ('C, \<gamma>) method_signature" and
    cSig :: "'C \<rightharpoonup> ('C, \<gamma>) constructor_signature" and
    mBody :: "'C \<times> m \<rightharpoonup> 'C s" and
    cBody :: "'C \<rightharpoonup> 'C s"
  assumes
    fields_dom: "f \<in> set (fields C) \<longleftrightarrow> (C, f) \<in> dom fType" and
    fields_subclass: "f \<in> set (fields C) \<and> D \<sqsubseteq> C \<Longrightarrow> fType (D, f) = fType (C, f)" and
      \<comment> \<open>The report restricts use of @{term this} only as a target of an assignment.
         However, assignment is just one form of reattachment,
         another one is setting formal arguments to actual ones.
         This restriction is captured by additional predicates
         for method and constructor formal arguments and locals.\<close>
    method_no_this_in_arguments: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> this \<notin> set (\<eta> (method_arguments S\<^sub>m))" and
    method_no_this_in_locals: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> this \<notin> set (\<eta> (method_locals S\<^sub>m))" and
      \<comment> \<open>The report does not mention that the name of a special variable @{term res}
        should be different from argument and local variable names.\<close>
    method_no_result_in_arguments: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> res \<notin> set (\<eta> (method_arguments S\<^sub>m))" and
    method_no_result_in_locals: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> res \<notin> set (\<eta> (method_locals S\<^sub>m))" and
      \<comment> \<open>The report does not mention that all names of variables declared
         in a method or in a constructor should be distinct.
         Without this restriction, variable updates become ambiguous.\<close>
    method_distinct_variables: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> distinct (\<eta> (method_arguments S\<^sub>m) @ \<eta> (method_locals S\<^sub>m))" and
    method_result_expectation: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> vars [method_result S\<^sub>m] \<subseteq> vars [(C, method_target S\<^sub>m, !)] \<union> vars (\<tau> (method_arguments S\<^sub>m))" and
    method_local_expectation: "mSig (C, m) = Some S\<^sub>m \<Longrightarrow> vars (\<tau> (method_locals S\<^sub>m)) \<subseteq> vars [(C, method_target S\<^sub>m, !)] \<union> vars (\<tau> (method_arguments S\<^sub>m))" and
    methods_dom: "m \<in> methods C \<longleftrightarrow> (C, m) \<in> dom mSig" and
    methods_contravariant_target: "\<lbrakk>m \<in> methods C; D \<sqsubseteq> C\<rbrakk> \<Longrightarrow>
      method_target (the (mSig (C, m))) \<sqsubseteq> method_target (the (mSig (D, m)))" and
    methods_contravariant_arguments: "\<lbrakk>m \<in> methods C; D \<sqsubseteq> C\<rbrakk> \<Longrightarrow>
      \<tau> (method_arguments (the (mSig (C, m)))) \<sqsubseteq> \<tau> (method_arguments (the (mSig (D, m))))" and
    methods_covariant_result: "\<lbrakk>m \<in> methods C; D \<sqsubseteq> C\<rbrakk> \<Longrightarrow>
      method_result (the (mSig (D, m))) \<sqsubseteq> method_result (the (mSig (C, m)))" and
    constructor_no_this_in_arguments: "cSig C = Some S\<^sub>c \<Longrightarrow> this \<notin> set (\<eta> (constructor_arguments S\<^sub>c))" and
    constructor_no_this_in_locals: "cSig C = Some S\<^sub>c \<Longrightarrow> this \<notin> set (\<eta> (constructor_locals S\<^sub>c))" and
    constructor_distinct_variables: "cSig C = Some S\<^sub>c \<Longrightarrow> distinct (\<eta> (constructor_arguments S\<^sub>c) @ \<eta> (constructor_locals S\<^sub>c))" and
    constructor_local_expectation: "cSig C = Some S\<^sub>c \<Longrightarrow> vars (\<tau> (constructor_locals S\<^sub>c)) \<subseteq> vars (\<tau> (constructor_arguments S\<^sub>c))"

lemma (in lookup) target_vars:
  fixes
    C D :: 'C
  assumes
    M: "m \<in> methods C" and
    I: "D \<sqsubseteq> C"
  shows
    "vars [(D, method_target (the (mSig (D, m))), !)] \<subseteq>
    vars [(C, method_target (the (mSig (C, m))), !)]"
  unfolding vars_def using assms methods_contravariant_target var_le by (simp add: var1_def)

lemma (in lookup) arguments_vars:
  fixes
    C D :: 'C
  assumes
    M: "m \<in> methods C" and
    I: "D \<sqsubseteq> C"
  shows
    "vars (\<tau> (method_arguments (the (mSig (D, m))))) \<subseteq> vars (\<tau> (method_arguments (the (mSig (C, m)))))"
  using I M methods_contravariant_arguments vars_le by blast

lemma (in lookup) method_disjoint_variables:
  fixes
    C :: 'C
  assumes
    "mSig (C, m) = Some S\<^sub>m"
  shows
    "set (\<eta> (method_arguments S\<^sub>m)) \<inter> set (\<eta> (method_locals S\<^sub>m)) = {}"
  using assms method_distinct_variables by simp_all

lemma (in lookup) constructor_disjoint_variables:
  fixes
    C :: 'C
  assumes
    "cSig C = Some S\<^sub>c"
  shows
    "set (\<eta> (constructor_arguments S\<^sub>c)) \<inter> set (\<eta> (constructor_locals S\<^sub>c)) = {}"
  using assms constructor_distinct_variables by simp_all

end