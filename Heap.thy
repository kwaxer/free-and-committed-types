(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 8 (Heaps, Values, Allocation)\<close>

theory Heap imports Type begin

type_synonym 'a address = "'a set"
type_synonym 'a \<iota> = "'a address"

datatype 'a "value" =
  non_null (ref: "'a \<iota>") |
  null

type_synonym 'a v = "'a value"

type_synonym 'a h\<^sub>v = "'a \<iota> \<times> f \<rightharpoonup> 'a v"
type_synonym ('a, 'C) h\<^sub>c = "'a \<iota> \<rightharpoonup> 'C"
type_synonym ('a, 'C) h = "'a h\<^sub>v \<times> ('a, 'C) h\<^sub>c"

definition "\<chi>\<^sub>v" :: "('a, 'C) h \<Rightarrow> 'a h\<^sub>v"
  where
    "\<chi>\<^sub>v \<equiv> fst"
declare \<chi>\<^sub>v_def [iff]

lemma \<chi>\<^sub>v_conv [iff]: "\<chi>\<^sub>v (h\<^sub>v, h\<^sub>c) = h\<^sub>v"
  by simp

definition "\<chi>\<^sub>c" :: "('a, 'C) h \<Rightarrow> ('a, 'C) h\<^sub>c"
  where
    "\<chi>\<^sub>c \<equiv> snd"
declare \<chi>\<^sub>c_def [iff]

lemma \<chi>\<^sub>c_conv [iff]: "h\<^sub>c = \<chi>\<^sub>c (h\<^sub>v, h\<^sub>c)"
  by simp

type_synonym field_names = "f list"
type_synonym f\<^sub>i = field_names

fun f_upds :: "('a \<times> 'b \<Rightarrow> 'c) \<Rightarrow> 'a \<Rightarrow> 'b list \<Rightarrow> 'c \<Rightarrow> ('a \<times> 'b \<Rightarrow> 'c)"
  where
    "f_upds h a [] x = h" |
    "f_upds h a (f # fs) x = (f_upds h a fs x) ((a, f) := x)"

lemma f_upds_same_list: "(f_upds h a (fh @ f # ft) x) (a, f) = x"
  by (induction fh, simp_all)

lemma f_upds_same: "f \<in> set fs \<Longrightarrow> (f_upds h a fs x) (a, f) = x"
  using f_upds_same_list split_list by fastforce

lemma f_upds_idem_iff: "((f_upds h a fs x) = h) = (\<forall> f \<in> set fs. h (a, f) = x)"
  by (induction fs, simp, metis f_upds.simps(2) f_upds_same fun_upd_triv list.set_intros(1) list.set_intros(2))

lemma f_upds_other_1: "a \<noteq> b \<Longrightarrow> f_upds h a fs x (b, f) = h (b, f)"
  by (induction fs, simp_all)

definition f\<^sub>i_upds :: "'a h\<^sub>v \<Rightarrow> 'a \<iota> \<Rightarrow> f\<^sub>i \<Rightarrow> 'a h\<^sub>v" ("\<nu>")
  where
    "\<nu> h\<^sub>v \<iota> f\<^sub>i = f_upds h\<^sub>v \<iota> f\<^sub>i (Some value.null)"
declare f\<^sub>i_upds_def [simp]

lemma f\<^sub>i_upds_same_list: "(\<nu> h\<^sub>v \<iota> (fh @ f # ft)) (\<iota>, f) = Some value.null"
  by (simp add: f_upds_same_list)

lemma f\<^sub>i_upds_same: "f \<in> set fs \<Longrightarrow> (\<nu> h\<^sub>v \<iota> fs) (\<iota>, f) = Some value.null"
  using f_upds_same_list split_list by fastforce

lemma f\<^sub>i_upds_dom: "dom (\<nu> h\<^sub>v \<iota> fs) = dom h\<^sub>v \<union> {(a, n). a = \<iota> \<and> n \<in> set fs}"
proof (induction fs)
  case Nil
  show "dom (\<nu> h\<^sub>v \<iota> []) = dom h\<^sub>v \<union> {(a, n). a = \<iota> \<and> n \<in> set []}" by simp
next
  case (Cons f fs)
  then have "dom (\<nu> h\<^sub>v \<iota> (f # fs)) = dom h\<^sub>v \<union> {(a, n). a = \<iota> \<and> n \<in> set fs} \<union> {(\<iota>, f)}" by simp
  moreover have "{(a, n). a = \<iota> \<and> n \<in> set fs} \<union> {(\<iota>, f)} = {(a, n). a = \<iota> \<and> n \<in> set (f # fs)}" by auto
  ultimately show "dom (\<nu> h\<^sub>v \<iota> (f # fs)) = dom h\<^sub>v \<union> {(a, n). a = \<iota> \<and> n \<in> set (f # fs)}" by auto
qed

definition (in classes) wf_heap_image :: "('a, 'C) h \<Rightarrow> bool"
  where
    "wf_heap_image h \<equiv> \<forall> v. v \<in> image (\<chi>\<^sub>v h) (dom (\<chi>\<^sub>v h)) \<longrightarrow> v = Some null \<or> (\<exists> \<iota>. v = Some (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h))"

lemma (in classes) wf_heap_image_def2:
  "wf_heap_image h = (image (\<chi>\<^sub>v h) (dom (\<chi>\<^sub>v h)) \<subseteq> {Some null} \<union> {v. (\<exists> \<iota>. v = Some (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h))})"
  unfolding wf_heap_image_def
  by rule (rule, blast)+

definition unbounded_memory :: "('a \<rightharpoonup> 'b) \<Rightarrow> bool" where
  "unbounded_memory c \<equiv> \<forall> n. \<exists> xs. n < card (xs - dom c)"

lemma unbounded_memory_preservation:
  assumes
    "unbounded_memory h"
    "x \<notin> dom h \<or> y \<noteq> None"
  shows
    "unbounded_memory (h (x := y))"
  unfolding unbounded_memory_def
proof (rule allI, cases y)
  fix n
  case None
  with assms show "\<exists>xs. n < card (xs - dom (h(x := y)))" unfolding unbounded_memory_def by simp
next
  fix n
  case (Some v)
  obtain m :: nat where
    "n < m" by blast
  moreover from assms(1) obtain xs where
    "m < card (xs - dom h)" unfolding unbounded_memory_def by blast
  then have
    mle: "m \<le> card (xs - dom h) - 1" by (simp add: discrete)
  moreover from Some have
    D: "dom (h(x := y)) = insert x (dom h)" by simp
  have
    "card (xs - dom h) - 1 \<le> card (xs - dom (h(x := y)))"
  proof (cases "x \<in> dom h")
    case True
    with D show ?thesis by (metis diff_le_self insert_absorb)
  next
    case False
    with D show ?thesis
    proof (cases "x \<in> xs")
      case True
      with D show ?thesis
        by (metis Diff_insert card_gt_0_iff diff_card_le_card_Diff is_singleton_altdef
            is_singleton_def zero_less_one)
    next
      case False
      with D show ?thesis by simp
    qed
  qed
  ultimately show
    "\<exists>xs. n < card (xs - dom (h(x := y)))" using less_le_trans le_trans by blast
qed

lemma
  assumes
    "unbounded_memory h"
  obtains
    x
  where
    "x \<notin> dom h"
proof -
  from assms obtain xs where
    "1 < card (xs - dom h)" unfolding unbounded_memory_def by blast
  then have
    "xs - dom h \<noteq> {}" by fastforce 
  then show ?thesis using that by auto
qed

definition unbounded_heap :: "('a, 'b) h \<Rightarrow> bool" where
  "unbounded_heap h \<longleftrightarrow> unbounded_memory (\<chi>\<^sub>c h)"

locale heap = classes +
  constrains
    is_subclass :: "'C:: {equal, partial_order} \<Rightarrow> 'C \<Rightarrow> bool"
  fixes
    address :: "'a address" and
    alloc :: "('a, 'C) h \<Rightarrow> 'C \<Rightarrow> ('a, 'C) h \<times> 'a address"
  assumes
    finite_address: "finite address" and
(*    dom: "dom (h\<^sub>c) = {\<iota>. \<exists> f. (\<iota>, f) \<in> dom (h\<^sub>v)}" and*)
    alloc: "\<lbrakk>unbounded_heap h; alloc h C = (h', \<iota>) \<rbrakk> \<Longrightarrow>
      \<iota> \<notin> dom (\<chi>\<^sub>c h) \<and>
      \<chi>\<^sub>v h' = (\<nu> (\<chi>\<^sub>v h) \<iota> (fields C)) \<and>
      \<chi>\<^sub>c h' = (\<chi>\<^sub>c h) (\<iota> := Some C)"

lemma (in heap) alloc_type:
  "\<lbrakk>unbounded_heap h; alloc h C = (h', \<iota>'); \<iota> \<noteq> \<iota>'\<rbrakk> \<Longrightarrow> \<chi>\<^sub>c h' \<iota> = \<chi>\<^sub>c h \<iota>"
  using alloc by auto

lemma (in heap) alloc_type_preservation:
  assumes
    "unbounded_heap h" and
    "alloc h C = (h', \<iota>')" and
    "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
    "P (\<chi>\<^sub>c h \<iota>)"
  shows
    "P (\<chi>\<^sub>c h' \<iota>)"
  using assms alloc alloc_type by metis

lemma (in heap) alloc_value:
  "\<lbrakk>unbounded_heap h; alloc h C = (h', \<iota>'); \<iota> \<noteq> \<iota>'\<rbrakk> \<Longrightarrow> \<chi>\<^sub>v h' (\<iota>, f) = \<chi>\<^sub>v h (\<iota>, f)"
  by (cases "f = this", insert alloc, simp_all add: f_upds_other_1)

lemma (in heap) alloc_value_preservation:
  assumes
    "unbounded_heap h" and
    "alloc h C = (h', \<iota>')" and
    "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
    "P (\<chi>\<^sub>v h (\<iota>, f))"
  shows
    "P (\<chi>\<^sub>v h' (\<iota>, f))"
  using assms alloc alloc_value by metis

lemma (in heap) alloc_unbounded_heap:
  assumes
    "unbounded_heap h" and
    "alloc h C = (h', \<iota>')"
  shows
    "unbounded_heap h'"
  using assms alloc unfolding unbounded_heap_def using unbounded_memory_preservation by metis

definition (in classes) wf_heap_dom_weak :: "('a, 'C) h \<Rightarrow> bool"
  where
    "wf_heap_dom_weak h \<equiv> dom (\<chi>\<^sub>c h) = {\<iota>. \<exists> f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h)} \<union> {\<iota>. \<exists> C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}"

lemma (in heap) alloc_dom_weak:
  assumes
    D: "wf_heap_dom_weak h" and
    M: "unbounded_heap h" and
    A: "alloc h C = (h', \<iota>)"
  shows
    "wf_heap_dom_weak h'"
proof -
  from A M have
    I: "\<iota> \<notin> dom (\<chi>\<^sub>c h)" using alloc by simp
  then have
    N: "\<iota> \<notin> {\<iota>. \<exists> C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}" by blast 
  from A M have
    D': "dom (\<chi>\<^sub>c h') = dom (\<chi>\<^sub>c h) \<union> {\<iota>}" using alloc by simp
  from A M have
    D'': "dom (\<chi>\<^sub>v h') = dom (\<chi>\<^sub>v h) \<union> {(a, f). a = \<iota> \<and> f \<in> set (fields C)}"
      using alloc f\<^sub>i_upds_dom by metis
  from A M have
    C: "\<forall> \<iota> C. \<chi>\<^sub>c h \<iota> = Some C \<longrightarrow> \<chi>\<^sub>c h' \<iota> = Some C" using alloc alloc_type by (metis domI)
  then have
    S: "{\<iota>. \<exists>C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []} \<subseteq> {\<iota>. \<exists>C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []}" by blast
  from A M have
    C': "\<forall> \<iota>' C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> \<iota>' \<noteq> \<iota> \<longrightarrow> \<chi>\<^sub>c h \<iota>' = Some C" using alloc alloc_type by simp
  then have
    S': "{\<iota>'. \<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> \<iota>' \<noteq> \<iota> \<and> fields C = []} \<subseteq> {\<iota>. \<exists>C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}" by blast
  have
    E: "{\<iota>'. \<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> \<iota>' \<noteq> \<iota> \<and> fields C = []} =
        {\<iota>'. \<iota>' \<noteq> \<iota> \<and> (\<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> fields C = [])}" by auto
  also have
    "\<dots> = {\<iota>'. (\<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> fields C = [])} - {\<iota>}" by auto
  finally have
    I': "{\<iota>'. \<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> \<iota>' \<noteq> \<iota> \<and> fields C = []} =
        {\<iota>'. (\<exists>C. \<chi>\<^sub>c h' \<iota>' = Some C \<and> fields C = [])} - {\<iota>}" by auto
  from A have
    C: "\<forall> C. \<chi>\<^sub>c h' \<iota> = Some C \<longrightarrow> \<chi>\<^sub>c h' \<iota> = Some C" using alloc by blast
  show ?thesis
  proof (cases "fields C")
    case Nil
    with A M have
      "\<chi>\<^sub>v h' = \<chi>\<^sub>v h"  using alloc by simp
    with D D' have
      "dom (\<chi>\<^sub>c h') = {\<iota>. \<exists> f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h')} \<union> {\<iota>. \<exists> C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []} \<union> {\<iota>}"
      unfolding wf_heap_dom_weak_def by simp
    with E N S S' I' have
      "dom (\<chi>\<^sub>c h') = {\<iota>. \<exists> f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h')} \<union> {\<iota>. \<exists> C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []} \<union> {\<iota>}"
      by (metis (no_types, lifting) Diff_empty Un_Diff_cancel2 subset_Diff_insert subset_antisym sup_assoc)
    moreover from A M Nil have "\<iota> \<in> {\<iota>. \<exists> C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []}" using alloc by simp
    ultimately have
      "dom (\<chi>\<^sub>c h') = {\<iota>. \<exists> f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h')} \<union> {\<iota>. \<exists> C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []}" by auto
    then show ?thesis unfolding wf_heap_dom_weak_def by simp
  next
    case Cons
    with A M S have "{\<iota>. \<exists>C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []} = {\<iota>. \<exists> C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}"
      using alloc by auto
    with Cons D D' D'' show ?thesis by (auto simp add: wf_heap_dom_weak_def)
  qed
qed

definition (in classes) wf_heap_dom :: "('a, 'C) h \<Rightarrow> bool"
  where
    "wf_heap_dom h \<equiv> dom (\<chi>\<^sub>v h) = {(\<iota>, f). \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (\<chi>\<^sub>c h \<iota>)))}"

lemma (in classes) access_dom:
  assumes
    "wf_heap_dom h"
    "\<iota> \<in> dom (\<chi>\<^sub>c h)"
    "f \<in> set (fields (the (\<chi>\<^sub>c h \<iota>)))"
  shows
    "(\<iota>, f) \<in> dom (\<chi>\<^sub>v h)"
  using assms unfolding wf_heap_dom_def by simp

lemma (in heap) alloc_dom:
  assumes
    W: "wf_heap_dom h" and
    M: "unbounded_heap h" and
    A: "alloc h C = (h', \<iota>)"
  shows
    "wf_heap_dom h'"
proof -
  from A M have
    S: "dom (\<chi>\<^sub>c h') = dom (\<chi>\<^sub>c h) \<union> {\<iota>}" using alloc(1) by simp
  from A M have
    "\<chi>\<^sub>c h' \<iota> = Some C" using alloc by simp
  moreover from A M have
    "{(\<iota>, f). \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' \<iota>)))} = {(\<iota>, f). \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (\<chi>\<^sub>c h \<iota>)))}"
    using alloc alloc_type by (metis (no_types))
  moreover note W
  moreover from A M have
    "dom (\<chi>\<^sub>v h') = dom (\<chi>\<^sub>v h) \<union> {(x, f). x = \<iota> \<and> f \<in> set (fields C)}"
    using alloc f\<^sub>i_upds_dom by metis
  ultimately have
    "dom (\<chi>\<^sub>v h') = {(x, f). x \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' x)))} \<union>
      {(x, f). x \<in> {\<iota>} \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' x)))}" unfolding wf_heap_dom_def by auto
  also have
    "\<dots> = {(x, f). x \<in> dom (\<chi>\<^sub>c h) \<union> {\<iota>} \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' x)))}"
    by rule (rule, blast)+
  also from S have
    "\<dots> = {(x, f). x \<in> dom (\<chi>\<^sub>c h') \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' x)))}" using alloc by simp
  finally show ?thesis unfolding wf_heap_dom_def by simp
qed

lemma (in heap) wf_heap_dom_weak: "wf_heap_dom h' \<Longrightarrow> wf_heap_dom_weak h'"
  unfolding wf_heap_dom_def wf_heap_dom_weak_def
proof (rule; rule)
  fix x
  assume
    D: "dom (\<chi>\<^sub>v h') = {(\<iota>, f). \<iota> \<in> dom (\<chi>\<^sub>c h') \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' \<iota>)))}" and
    X: "x \<in> dom (\<chi>\<^sub>c h')"
  then obtain C where
    C: "\<chi>\<^sub>c h' x = Some C" by blast
  show "x \<in> {\<iota>. \<exists>f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h')} \<union> {\<iota>. \<exists>C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []}"
  proof (cases "fields C = []")
    case True
    with C show ?thesis by simp
  next
    case False
    with C obtain f where
      "f \<in> set (fields (the (\<chi>\<^sub>c h' x)))" by fastforce
    with D X show ?thesis by auto
  qed
next
  fix x
  assume
    "dom (\<chi>\<^sub>v h') = {(\<iota>, f). \<iota> \<in> dom (\<chi>\<^sub>c h') \<and> f \<in> set (fields (the (\<chi>\<^sub>c h' \<iota>)))}"
    "x \<in> {\<iota>. \<exists>f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h')} \<union> {\<iota>. \<exists>C. \<chi>\<^sub>c h' \<iota> = Some C \<and> fields C = []}"
  then show
    "x \<in> dom (\<chi>\<^sub>c h')" by auto
qed

lemma (in heap) alloc_null:
  assumes
    D: "wf_heap_dom_weak h" and
    M: "unbounded_heap h" and
    A: "alloc h C = (h', \<iota>)" and
    I: "(\<iota>, f) \<in> dom (\<chi>\<^sub>v h')"
  shows
    "\<chi>\<^sub>v h' (\<iota>, f) = Some null"
proof -
  from A M have
    "\<iota> \<notin> dom (\<chi>\<^sub>c h)" using alloc by simp
  then have
    N: "(\<iota>, f) \<notin> dom (\<chi>\<^sub>v h)" using D UnI1 unfolding wf_heap_dom_weak_def by blast
  from A M have
    F: "\<chi>\<^sub>v h' = (\<nu> (\<chi>\<^sub>v h) \<iota> (fields C))" using alloc by simp
  then have
    "dom (\<chi>\<^sub>v h') = dom (\<chi>\<^sub>v h) \<union> {(a, f). a = \<iota> \<and> f \<in> set (fields C)}" using f\<^sub>i_upds_dom by simp
  with I N have
    "f \<in> set (fields C)" by simp
  with F show ?thesis using f\<^sub>i_upds_same by simp
qed

lemma (in heap) alloc_null_fields:
  assumes
    "unbounded_heap h" and
    "alloc h C = (h', \<iota>)" and
    "f \<in> set (fields C)"
  shows
    "\<chi>\<^sub>v h' (\<iota>, f) = Some null"
  using assms alloc f\<^sub>i_upds_same by simp

lemma (in heap) alloc_image:
  assumes
    D: "wf_heap_dom_weak h" and
    I: "wf_heap_image h" and
    M: "unbounded_heap h" and
    A: "alloc h C = (h', \<iota>)"
  shows
    "wf_heap_image h'"
proof -
  {
    fix v
    assume
      "v \<in> \<chi>\<^sub>v h' ` dom (\<chi>\<^sub>v h')"
    then obtain \<iota>' f' where
      F: "(\<iota>', f') \<in> dom (\<chi>\<^sub>v h')" and
      V: "v = (\<chi>\<^sub>v h') (\<iota>', f')" by auto
    have "v = Some null \<or> (\<exists>\<iota>. v = Some (value.non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h'))"
    proof (cases "(\<iota>', f') \<in> dom (\<chi>\<^sub>v h)")
      case True
      moreover from A M have
        "\<forall> \<iota>''. (\<iota> = \<iota>'' \<or> \<iota>'' \<in> dom (\<chi>\<^sub>c h')) \<or> \<iota>'' \<notin> dom (\<chi>\<^sub>c h)" using alloc by auto
      ultimately show ?thesis using A M D F I V using alloc alloc_null alloc_value
        unfolding wf_heap_image_def by (metis image_eqI)
    next
      case False
      with A M D F have "(\<chi>\<^sub>v h') (\<iota>', f') = Some null" using alloc_null alloc_value by (metis domIff)
      with V show ?thesis by simp
    qed
  }
  then show ?thesis unfolding wf_heap_image_def by simp
qed

definition heap_lookup :: "('a, 'C) h \<Rightarrow> 'a \<iota> \<Rightarrow> f \<rightharpoonup> 'a v" ("\<chi> _ _ _")
  where
    "heap_lookup h \<iota> f = (\<chi>\<^sub>v h) (\<iota>, f)"
declare heap_lookup_def [simp]

subsubsection \<open>Class lookup\<close>

definition cls :: "('a, 'C) h \<Rightarrow> 'a \<iota> \<rightharpoonup> 'C"
  where
    "cls \<equiv> \<chi>\<^sub>c"
declare cls_def [iff]

lemma cls_conv [iff]: "cls (h\<^sub>v, h\<^sub>c) = h\<^sub>c"
  by simp

subsubsection \<open>Successor\<close>

definition predecessor :: "('a, 'C) h \<Rightarrow> ('a, 'C) h \<Rightarrow> bool" (infixl "\<le>" 50)
  where
    "predecessor h\<^sub>1 h\<^sub>2 \<equiv> dom (\<chi>\<^sub>c h\<^sub>1) \<subseteq> dom (\<chi>\<^sub>c h\<^sub>2) \<and> (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). (cls h\<^sub>2 \<iota>) = cls h\<^sub>1 \<iota>)"

lemma predecessor_transitive [simp]: "h\<^sub>1 \<le> h\<^sub>2 \<Longrightarrow> h\<^sub>2 \<le> h\<^sub>3 \<Longrightarrow> h\<^sub>1 \<le> (h\<^sub>3 :: ('a, 'C) h)"
  unfolding predecessor_def by (simp add: subset_iff)

lemma predecessor_reflexive [simp]: "h \<le> (h :: ('a, 'C) h)"
  unfolding predecessor_def by simp

lemma predecessor_dom [simp]: "h\<^sub>1 \<le> h\<^sub>2 \<Longrightarrow> dom (\<chi>\<^sub>c h\<^sub>1) \<subseteq> dom (\<chi>\<^sub>c h\<^sub>2)"
  unfolding predecessor_def by simp

lemma predecessor_cls [simp]: "h\<^sub>1 \<le> h\<^sub>2 \<Longrightarrow> \<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). (cls h\<^sub>2 \<iota>) = cls h\<^sub>1 \<iota>"
  unfolding predecessor_def by simp

lemma (in heap) alloc_predecessor: 
  fixes
    h h' :: "('a, 'C) h"
  assumes
    M: "unbounded_heap h" and
    A: "alloc h C = (h', \<iota>)"
  shows
    "h \<le> h'"
  unfolding predecessor_def using A M alloc by auto

end