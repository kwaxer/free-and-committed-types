(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 11 (Initialisation)\<close>

theory Initialization imports Semantics Reachability Update begin

definition (in lookup) init :: "('a, 'C) h \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
    [iff]: "init h \<iota> \<equiv> (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      \<not> nullable (the (fType (the (cls h \<iota>), f))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null)"

lemma (in lookup) init_update_non_null:
  assumes
    "init h \<iota>"
  shows
    "init ((\<chi>\<^sub>v h)((\<iota>'', f) := Some (non_null \<iota>')), \<chi>\<^sub>c h) \<iota>"
  using assms by simp

lemma (in lookup) init_update_null:
  assumes
    "init h \<iota>"
    "nullable (the (fType (the (cls h \<iota>), f)))"
  shows
    "init ((\<chi>\<^sub>v h)((\<iota>', f) := Some null), \<chi>\<^sub>c h) \<iota>"
  using assms by auto

lemma (in runtime) init_alloc:
  assumes
    "init h \<iota>"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>')"
    "\<iota> \<noteq> \<iota>'"
  shows
    "init h' \<iota>"
  using assms
  using alloc_type alloc_value by auto

definition (in runtime) deep_init :: "('a, 'C) h \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
    [iff]: "deep_init h \<iota> \<equiv> \<forall> \<iota>'. reaches (\<chi>\<^sub>v h) \<iota> \<iota>' \<longrightarrow> init h \<iota>'"

(*
lemma (in runtime) deep_init [simp]: "deep_init h \<iota> \<Longrightarrow> reaches (\<chi>\<^sub>v h) \<iota> \<iota>' \<Longrightarrow> init h \<iota>'"
  by simp
*)

definition (in runtime) deep_init\<^sub>v :: "('a, 'C) h \<Rightarrow> 'a v \<Rightarrow> bool"
  where
    [iff]: "deep_init\<^sub>v h v \<equiv> \<forall> \<iota>'. reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>') \<longrightarrow> init h \<iota>'"

(*
lemma (in runtime) deep_init\<^sub>v [simp]: "deep_init\<^sub>v h v \<Longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>) \<Longrightarrow> init h \<iota>"
  by simp
*)

lemma (in runtime) deep_init\<^sub>v_non_null [iff]: "deep_init\<^sub>v h (non_null \<iota>) \<longleftrightarrow> deep_init h \<iota>"
  by (simp add: reaches\<^sub>v_reaches)

lemma (in runtime) deep_init\<^sub>v_null [simp]: "deep_init\<^sub>v h null"
  using reaches\<^sub>v_left_null by simp

lemma (in runtime) deep_init\<^sub>v_deep_init: "deep_init h \<iota> \<longleftrightarrow> deep_init\<^sub>v h (non_null \<iota>)"
  using reaches\<^sub>v_reaches by simp

lemma (in runtime) reaches_deep_init:
  assumes
    "reaches (\<chi>\<^sub>v h) \<iota> \<iota>'"
    "deep_init h \<iota>"
  shows
    "deep_init h \<iota>'"
  using assms unfolding deep_init_def reaches_def by (meson rtranclp_trans)

lemma (in runtime) reaches_deep_init\<^sub>v:
  assumes
    "reaches\<^sub>v (\<chi>\<^sub>v h) v v'"
    "deep_init\<^sub>v h v"
  shows
    "deep_init\<^sub>v h v'"
  using assms using deep_init\<^sub>v_def reaches\<^sub>v_trans by blast

lemma (in runtime) deep_init_upd:
  assumes
    "deep_init h \<iota>"
    "deep_init h \<iota>'"
  shows
    "deep_init ((\<chi>\<^sub>v h) ((\<iota>'', f) := Some (non_null \<iota>')), (\<chi>\<^sub>c h)) \<iota>"
  using assms unfolding deep_init_def
  using \<chi>\<^sub>v_conv deep_init\<^sub>v_def deep_init\<^sub>v_non_null deep_init_def init_update_non_null
  using lemma_5\<^sub>2 reaches_upd_other
  by metis

lemma (in runtime) deep_init\<^sub>v_upd_non_null:
  assumes
    "deep_init\<^sub>v h v"
    "deep_init\<^sub>v h v'"
    "v' \<noteq> null"
  shows
    "deep_init\<^sub>v ((\<chi>\<^sub>v h) ((\<iota>, f) := Some v'), (\<chi>\<^sub>c h)) v"
proof (cases v)
  case (non_null)
  moreover from assms(3) obtain \<iota>' where
    "v' = non_null \<iota>'" using value.exhaust by auto
  moreover with assms(2) have
    "deep_init h \<iota>'" using deep_init\<^sub>v_non_null by blast
  moreover note assms(1)
  ultimately show ?thesis using deep_init\<^sub>v_non_null deep_init_upd by blast
next
  case null
  then show ?thesis using deep_init\<^sub>v_null by blast
qed

lemma (in runtime) deep_init_upd_null:
  assumes
    "deep_init h \<iota>"
    "nullable (the (fType (the (cls h \<iota>'), f)))"
  shows
    "deep_init ((\<chi>\<^sub>v h) ((\<iota>', f) := Some null), (\<chi>\<^sub>c h)) \<iota>"
  using assms unfolding deep_init_def init_def
  by (metis \<chi>\<^sub>v_conv cls_conv cls_def fun_upd_apply lemma_5\<^sub>2 prod.inject reaches\<^sub>v_left_null reaches_upd_other)

lemma (in runtime) deep_init\<^sub>v_upd_null:
  assumes
    "deep_init\<^sub>v h v"
    "nullable (the (fType (the (cls h \<iota>), f)))"
  shows
    "deep_init\<^sub>v ((\<chi>\<^sub>v h) ((\<iota>, f) := Some null), (\<chi>\<^sub>c h)) v"
  using assms deep_init_upd_null deep_init\<^sub>v_null by (cases v) blast+

lemma (in runtime) deep_init_upd_other:
  assumes
    "deep_init h \<iota>"
    "\<not> reaches (\<chi>\<^sub>v h) \<iota> \<iota>'"
  shows
    "deep_init ((\<chi>\<^sub>v h) ((\<iota>', f) := v), (\<chi>\<^sub>c h)) \<iota>"
  using assms reaches_upd_other by simp

lemma (in runtime) deep_init\<^sub>v_upd_other:
  assumes
    "deep_init\<^sub>v h v"
    "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>')"
  shows
    "deep_init\<^sub>v ((\<chi>\<^sub>v h) ((\<iota>', f) := v'), (\<chi>\<^sub>c h)) v"
  using assms reaches\<^sub>v_upd_other by simp

lemma (in runtime) deep_init\<^sub>v_alloc:
  assumes
    "deep_init\<^sub>v h v"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>')"
    "wf_heap_dom h"
    "wf_heap_image h"
    "v \<noteq> non_null \<iota>'"
  shows
    "deep_init\<^sub>v h' v"
  using assms deep_init\<^sub>v_def init_alloc reaches\<^sub>v_alloc reaches\<^sub>v_alloc_to_new wf_heap_dom_weak
  by metis

lemma (in runtime) not_init_deep_init\<^sub>v: "reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>) \<Longrightarrow> \<not> init h \<iota> \<Longrightarrow>
  \<not> deep_init\<^sub>v h v"
  by auto

end