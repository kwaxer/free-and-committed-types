(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 6 (Static Type Assignment)\<close>

theory Typing imports Type Expression Lookup Statement Expectation begin

type_synonym 'C type_environment = "x \<rightharpoonup> 'C T"
type_synonym 'C \<Gamma> = "'C type_environment"

type_synonym assigned_variables = "x set"
type_synonym \<Delta> = assigned_variables

type_synonym assigned_fields = "f set"
type_synonym \<Sigma> = assigned_fields

subsubsection "Expression typing"

inductive (in lookup) expression_typing :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> e \<Rightarrow> 'C T \<Rightarrow> bool" ("_, _ \<turnstile> _ : _")
  where
    Tvar: "\<lbrakk>x \<in> dom \<Gamma>; the (\<Gamma> x) = T; nullable T \<or> x \<in> \<Delta>\<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile> (\<V> x): T" |
    Tnull: "nullity T = ? \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile> Null: T" |
    Tfld: "\<lbrakk>
      \<Gamma>, \<Delta> \<turnstile> (\<V> x): (C, k\<^sub>1, !);
      fType (C, f) = Some (D, n\<^sub>1);
      k\<^sub>2 = (if k\<^sub>1 = \<one> then \<one> else \<diamondop>);
      n\<^sub>2 = (if n\<^sub>1 = ! \<and> k\<^sub>1 = \<one> then ! else ?)
    \<rbrakk> \<Longrightarrow>
    \<Gamma>, \<Delta> \<turnstile> x \<^bsub>\<bullet>\<^esub> f: (D, k\<^sub>2, n\<^sub>2)"

declare (in lookup) expression_typing.intros[intro!]

lemmas (in lookup) expression_typing_induct = expression_typing.induct[split_format(complete)]
inductive_cases (in lookup) TvarE[elim]: "\<Gamma>, \<Delta> \<turnstile> (\<V> x): T"
inductive_cases (in lookup) TnullE[elim]: "\<Gamma>, \<Delta> \<turnstile> Null: T"
inductive_cases (in lookup) TfldE[elim]: "\<Gamma>, \<Delta> \<turnstile> x \<^bsub>\<bullet>\<^esub> f: T"

lemma (in lookup) Tvar1:
  fixes
    \<Gamma> :: "'C \<Gamma>"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> (\<V> x): the (\<Gamma> x)"
  shows
    "x \<in> dom \<Gamma>" and
    "nullable (the (\<Gamma> x)) \<or> x \<in> \<Delta>"
  using assms by auto

lemma (in lookup) unique_nullity:
    "\<Gamma>, \<Delta> \<turnstile> e: T\<^sub>1 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile> e: T\<^sub>2 \<Longrightarrow> nullity T\<^sub>1 = nullity T\<^sub>2"
  by (induction arbitrary: T\<^sub>2 rule: expression_typing.induct) fastforce+

lemma (in lookup) variable_typing: "\<Gamma>, \<Delta> \<turnstile> (\<V> x): T \<Longrightarrow> \<Gamma> x = Some T"
  by auto

lemma (in lookup) target_typing:
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x \<^bsub>\<bullet>\<^esub> f: T"
  obtains
    C k
  where
    "\<Gamma>, \<Delta> \<turnstile> (\<V> x): (C, k, !)"
  using assms by auto

definition (in lookup) variables_typing :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> x list \<Rightarrow> 'C T\<^sub>i \<Rightarrow> bool" ("_, _ \<turnstile> _ : _")
  where
    "variables_typing \<Gamma> \<Delta> xs T\<^sub>i \<equiv>
      length xs = length T\<^sub>i \<and>
      (\<forall> x. x \<in> set (zip xs T\<^sub>i) \<longrightarrow> \<Gamma>, \<Delta> \<turnstile> \<V> (fst x): snd x)"

lemma (in lookup) variables_typing_zip: "\<Gamma>, \<Delta> \<turnstile> map fst z\<^sub>i : map snd z\<^sub>i \<longleftrightarrow>
  (\<forall> x. x \<in> set z\<^sub>i \<longrightarrow> \<Gamma>, \<Delta> \<turnstile> \<V> (fst x): snd x)"
  by (simp add: variables_typing_def zip_map_fst_snd)

lemma (in lookup) variables_typing_length: "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i \<Longrightarrow> length x\<^sub>i = length T\<^sub>i"
  unfolding variables_typing_def by simp

lemma (in lookup) variables_typing_elem_type:
  fixes
    x\<^sub>i :: "x list" and T\<^sub>i :: "'C T\<^sub>i"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i" and
    "i < length x\<^sub>i"
  shows
    "\<Gamma>, \<Delta> \<turnstile> \<V> (x\<^sub>i ! i): (T\<^sub>i ! i)"
  using assms unfolding variables_typing_def
proof (induction i arbitrary: x\<^sub>i T\<^sub>i)
  case 0
  then show ?case by (metis fst_conv in_set_zip snd_conv)
next
  case (Suc i)
  then obtain x xs T Ts where
    x\<^sub>i: "x\<^sub>i = x # xs" and
    T\<^sub>i: "T\<^sub>i = T # Ts" by (metis Suc_length_conv Suc_lessE) 
  moreover with Suc have
    "\<Gamma>, \<Delta> \<turnstile> \<V> xs ! i : Ts ! i" by simp
  moreover from x\<^sub>i T\<^sub>i Suc.prems(1) have
    "\<Gamma>, \<Delta> \<turnstile> \<V> x : T" by (fastforce simp add: Suc.prems(1))
  ultimately show ?case by simp
qed

lemma (in lookup) variables_typing:
  fixes
    x\<^sub>i :: "x list" and T\<^sub>i :: "'C T\<^sub>i"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i"
  shows
    "map \<Gamma> x\<^sub>i = map Some T\<^sub>i"
proof -
  obtain z\<^sub>i where
    z: "z\<^sub>i = zip x\<^sub>i T\<^sub>i" by simp
  with assms have
    "\<Gamma>, \<Delta> \<turnstile> map fst z\<^sub>i: map snd z\<^sub>i" using variables_typing_length by simp
  then have
    "map \<Gamma> (map fst z\<^sub>i) = map Some (map snd z\<^sub>i)"
    by (induction "z\<^sub>i")
       (simp, metis (no_types) list.set_intros list.simps(9) variable_typing variables_typing_zip)
  with z assms show ?thesis using variables_typing_length by simp
qed

lemma (in lookup) variables_typing_type:
  fixes
    x\<^sub>i :: "x list" and T\<^sub>i :: "'C T\<^sub>i"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i"
  shows
    "map (the o \<Gamma>) x\<^sub>i  = T\<^sub>i"
  using assms variables_typing
  by (metis (mono_tags) comp_def length_map nth_equalityI nth_map option.sel)

lemma (in lookup) variables_typing_dom:
  fixes
    x\<^sub>i :: "x list" and T\<^sub>i :: "'C T\<^sub>i"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i"
  shows
    "set x\<^sub>i \<subseteq> dom \<Gamma>"
proof (rule subsetI)
  fix x
  assume
    "x \<in> set x\<^sub>i"
  with assms obtain T where
    "\<Gamma>, \<Delta> \<turnstile> \<V> x: T" unfolding variables_typing_def
    by (metis fst_conv in_set_impl_in_set_zip1)
  then show
    "x \<in> dom \<Gamma>" by blast
qed  

lemma (in lookup) committed_variables:
  fixes
    x\<^sub>i :: "x list" and T\<^sub>i :: "'C T\<^sub>i"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i"
    "committed_all T\<^sub>i"
  shows
    "\<forall> x \<in> set x\<^sub>i. committed (the (\<Gamma> x))"
proof (rule ballI)
  fix x
  assume
    "x \<in> set x\<^sub>i"
  then obtain i where
    "i < length x\<^sub>i" and
    "x\<^sub>i ! i = x" by (meson in_set_conv_nth) 
  moreover from assms(1) have
    "map (the o \<Gamma>) x\<^sub>i  = T\<^sub>i" by (rule variables_typing_type)
  moreover note assms(2)
  ultimately show "committed (the (\<Gamma> x))" by auto
qed

subsubsection "Statement typing"

declare initialization_leq []
declare initialization_extension_def []

inductive (in lookup) statement_typing :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> 'C s \<Rightarrow> \<Delta> \<Rightarrow> \<Sigma> \<Rightarrow> bool" ("_, _ \<turnstile> _ | _, _")
  where
    TvarAss: "\<lbrakk>
        \<Gamma>, \<Delta> \<turnstile> e: T;
        Some T \<sqsubseteq> \<Gamma> x
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> x \<Colon>= e | \<Delta> \<union> {x}, {}" |
    TfldAss: "\<lbrakk>
        \<Gamma>, \<Delta> \<turnstile> \<V> x: (C, k\<^sub>1, !);
        fType (C, f) = Some (D, n);
        \<Gamma>, \<Delta> \<turnstile> \<V> y: T;
        T \<sqsubseteq> (D, k\<^sub>2, n);
        k\<^sub>1 = \<zero> \<or> k\<^sub>2 = \<one>;
        \<Sigma> = (if x = this then {f} else {})
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> x \<^bsub>\<bullet>\<^esub> f \<Colon>= y | \<Delta>, \<Sigma>" |
    Tcall: "\<lbrakk>
        \<Gamma>, \<Delta> \<turnstile> \<V> y: (C, k, !);
        mSig (C, m) = Some (\<gamma>, X\<^sub>i, S, Y\<^sub>j);
        \<theta> \<in> instances S ((C, \<gamma>, !) # (\<tau> X\<^sub>i));
          \<comment> \<open>The report uses @{term \<open>instance \<theta> S ((C, \<gamma>, !) # (variable_types X\<^sub>i))\<close>},
              but well-formedness relies on @{term \<open>instances S ((C, \<gamma>, !) # (variable_types X\<^sub>i))\<close>}
              that requires minimal domain on which the instance is defined.\<close>
        \<Gamma>, \<Delta> \<turnstile> z\<^sub>i: T\<^sub>i;
        map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (\<tau> X\<^sub>i);
        \<ss> \<theta> S \<sqsubseteq> \<Gamma> x;
        Some k \<sqsubseteq> \<ee> \<theta> \<gamma>
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> x := y \<^bsub>\<bullet>\<^esub> m (z\<^sub>i) | \<Delta> \<union> {x}, {}" |
    Tcreate: "\<lbrakk>
        cSig C = Some (X\<^sub>i, Y\<^sub>j);
        \<theta> \<in> instances (C, Inl \<zero>, !) (\<tau> X\<^sub>i);
          \<comment> \<open>The report uses @{term \<open>instance \<theta> (C, Inl \<zero>, !) (variable_types X\<^sub>i)\<close>},
              but well-formedness relies on @{term \<open>instances (C, Inl \<zero>, !) (variable_types X\<^sub>i)\<close>}
              that requires minimal domain on which the instance is defined.\<close>
        \<Gamma>, \<Delta> \<turnstile> z\<^sub>i: T\<^sub>i;
        map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (\<tau> X\<^sub>i);
        k = (if committed_all T\<^sub>i then \<one> else \<zero>);
        Some (C, k, !) \<sqsubseteq> \<Gamma> x
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> x \<Colon>= new C (z\<^sub>i) | \<Delta> \<union> {x}, {}" |
    Tcast: "\<lbrakk>
        \<Gamma>, \<Delta> \<turnstile> \<V> y: (C, k, n\<^sub>1);
        t = (D, n\<^sub>2);
        (D, k, n\<^sub>2) \<sqsubseteq> T\<^sub>x;
        \<Gamma> x = Some T\<^sub>x
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> x \<Colon>= \<lparr>t\<rparr> y | \<Delta> \<union> {x}, {}" |
    Tseq: "\<lbrakk>
        \<Gamma>, \<Delta> \<turnstile> s\<^sub>1 | \<Delta>\<^sub>1, \<Sigma>\<^sub>1;
        \<Gamma>, \<Delta>\<^sub>1 \<turnstile> s\<^sub>2 | \<Delta>\<^sub>2, \<Sigma>\<^sub>2
      \<rbrakk> \<Longrightarrow>
      \<Gamma>, \<Delta> \<turnstile> s\<^sub>1 ;; s\<^sub>2 | \<Delta>\<^sub>2, \<Sigma>\<^sub>1 \<union> \<Sigma>\<^sub>2"

inductive_cases (in lookup) TvarAssE [elim!]: "\<Gamma>, \<Delta> \<turnstile> x \<Colon>= e | \<Delta>', \<Sigma>"
inductive_cases (in lookup) TfldAssE [elim!]: "\<Gamma>, \<Delta> \<turnstile> x \<^bsub>\<bullet>\<^esub> f \<Colon>= y | \<Delta>', \<Sigma>"
inductive_cases (in lookup) TcallE [elim!]: "\<Gamma>, \<Delta> \<turnstile> x := y \<^bsub>\<bullet>\<^esub> m (z\<^sub>i) | \<Delta>', \<Sigma>"
inductive_cases (in lookup) TcreateE [elim!]: "\<Gamma>, \<Delta> \<turnstile> x \<Colon>= new C (z\<^sub>i) | \<Delta>', \<Sigma>"
inductive_cases (in lookup) TcastE [elim!]: "\<Gamma>, \<Delta> \<turnstile> x \<Colon>= \<lparr>t\<rparr> y | \<Delta>', \<Sigma>"
inductive_cases (in lookup) TseqE [elim!]: "\<Gamma>, \<Delta> \<turnstile> s\<^sub>1 ;; s\<^sub>2 | \<Delta>', \<Sigma>"

lemma (in lookup) call_target_typing:
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x := y \<^bsub>\<bullet>\<^esub> m (z\<^sub>i) | \<Delta>', \<Sigma>"
  obtains
    C k
  where
    "\<Gamma>, \<Delta> \<turnstile> \<V> y: (C, k, !)"
  using assms TcallE by auto

end