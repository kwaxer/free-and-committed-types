(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 12 (Good Configurations)\<close>

theory Configuration imports Evaluation Initialization Semantics Typing begin

definition c\<^sub>1 :: "'C \<Gamma> \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>1 \<Gamma> \<sigma> \<equiv> dom \<sigma> = dom \<Gamma> \<and> this \<in> dom \<sigma>"

notation c\<^sub>1 ("_ \<turnstile>\<^sub>1 _")

definition (in lookup) c\<^sub>2 :: "('a, 'C) h \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>2 h \<equiv> \<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      (the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType ((the (cls h \<iota>)), f)))))"

notation (in lookup) c\<^sub>2 ("\<turnstile>\<^sub>2 _")

lemma (in lookup) c\<^sub>2I: "(\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      (the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType ((the (cls h \<iota>)), f)))))) \<Longrightarrow>
  \<turnstile>\<^sub>2 h"
  by simp

definition (in lookup) c\<^sub>2_restriction :: "'a \<iota> \<Rightarrow> ('a, 'C) h \<Rightarrow> bool"
  where
    "c\<^sub>2_restriction \<iota> h \<equiv> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow> (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      (the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType ((the (cls h \<iota>)), f)))))"

notation (in lookup) c\<^sub>2_restriction ("_ \<turnstile>\<^sub>2 _")

lemma (in lookup) c\<^sub>2_restrictionI: "\<iota> \<in> dom (\<chi>\<^sub>c h) \<Longrightarrow> (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      (the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType ((the (cls h \<iota>)), f))))) \<Longrightarrow>
   \<iota> \<turnstile>\<^sub>2 h"
  by (simp add: c\<^sub>2_restriction_def)

definition (in lookup) c\<^sub>2_restrictions :: "'a \<iota> set \<Rightarrow> ('a, 'C) h \<Rightarrow> bool"
  where
    "c\<^sub>2_restrictions I h \<equiv> \<forall> \<iota> \<in> I. \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow> (\<forall> f \<in> set (fields (the (cls h \<iota>))).
      (the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType ((the (cls h \<iota>)), f)))))"

notation (in lookup) c\<^sub>2_restrictions ("_ \<turnstile>\<^sub>2 _")

lemma (in lookup) c\<^sub>2_restrictions_allI:
  fixes \<iota> :: "'a \<iota>" and I :: "'a \<iota> set" and h :: "('a, 'C) h"
  assumes
    "\<forall> \<iota> \<in> I. \<iota> \<turnstile>\<^sub>2 h"
  shows
    "I \<turnstile>\<^sub>2 h"
  using assms unfolding c\<^sub>2_restriction_def c\<^sub>2_restrictions_def by blast

lemma (in lookup) c\<^sub>2_union:
  fixes I\<^sub>1 I\<^sub>2 :: "'a \<iota> set" and h :: "('a, 'C) h"
  shows
    "I\<^sub>1 \<turnstile>\<^sub>2 h \<Longrightarrow> I\<^sub>2 \<turnstile>\<^sub>2 h \<Longrightarrow> (I\<^sub>1 \<union> I\<^sub>2) \<turnstile>\<^sub>2 h"
  unfolding c\<^sub>2_restrictions_def by blast

lemma (in lookup) c\<^sub>2I_union: "dom (\<chi>\<^sub>c h) \<subseteq> I\<^sub>1 \<union> I\<^sub>2 \<Longrightarrow> I\<^sub>1 \<turnstile>\<^sub>2 h \<Longrightarrow> I\<^sub>2 \<turnstile>\<^sub>2 h \<Longrightarrow> \<turnstile>\<^sub>2 h"
  unfolding c\<^sub>2_def c\<^sub>2_restrictions_def by blast

lemma (in lookup) c\<^sub>2_insert: "dom (\<chi>\<^sub>c h) \<subseteq> I \<union> {\<iota>} \<Longrightarrow> I \<turnstile>\<^sub>2 h \<Longrightarrow> \<iota> \<turnstile>\<^sub>2 h \<Longrightarrow> \<turnstile>\<^sub>2 h"
  unfolding c\<^sub>2_def c\<^sub>2_restriction_def c\<^sub>2_restrictions_def by blast 

lemma (in lookup) c\<^sub>2_subset:
  fixes A B :: "'a \<iota> set" and h :: "('a, 'C) h"
  assumes
    "A \<subseteq> B" and
    "B \<turnstile>\<^sub>2 h"
  shows
    "A \<turnstile>\<^sub>2 h"
  using assms unfolding c\<^sub>2_def c\<^sub>2_restrictions_def by blast

lemma (in lookup) c\<^sub>2_null: " \<forall> f \<in> set (fields (the (cls h \<iota>))). the (\<chi>\<^sub>v h (\<iota>, f)) = null \<Longrightarrow> \<iota> \<turnstile>\<^sub>2 h"
  using c\<^sub>2_restriction_def by blast

lemma (in lookup) c\<^sub>2_nulls: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
  \<forall> f \<in> set (fields (the (cls h \<iota>))). the (\<chi>\<^sub>v h (\<iota>, f)) = null \<Longrightarrow> \<turnstile>\<^sub>2 h"
  by simp

lemma (in lookup) c\<^sub>2_copy:
  fixes I :: "'a \<iota> set" and h h' :: "('a, 'C) h"
  assumes
    "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<chi>\<^sub>c h \<iota> = \<chi>\<^sub>c h' \<iota> \<and> (\<forall> f \<in> set (fields (the (cls h \<iota>))). \<chi>\<^sub>v h (\<iota>, f) = \<chi>\<^sub>v h' (\<iota>, f))" and
    "\<turnstile>\<^sub>2 h"
  shows
    "dom (\<chi>\<^sub>c h) \<turnstile>\<^sub>2 h'"
  using assms unfolding c\<^sub>2_restrictions_def using c\<^sub>2_def cls_def
  by (metis (no_types) domD insertI1 insert_dom)

definition c\<^sub>3 :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>3 \<Gamma> \<Delta> \<sigma> \<equiv> \<forall> x \<in> dom \<sigma>. \<not> nullable (the (\<Gamma> x)) \<and> x \<in> \<Delta> \<longrightarrow> the (\<sigma> x) \<noteq> null"

notation c\<^sub>3 ("_, _ \<turnstile>\<^sub>3 _")

definition c\<^sub>3_restriction :: "x \<Rightarrow>'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    "c\<^sub>3_restriction x \<Gamma> \<Delta> \<sigma> \<equiv> x \<in> dom \<sigma> \<longrightarrow> \<not> nullable (the (\<Gamma> x)) \<and> x \<in> \<Delta> \<longrightarrow> the (\<sigma> x) \<noteq> null"

notation c\<^sub>3_restriction ("_, _, _ \<turnstile>\<^sub>3 _")

lemma c\<^sub>3_restrictionI: "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> x \<in> dom \<sigma> \<Longrightarrow> x, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  by (simp add: c\<^sub>3_restriction_def)

definition c\<^sub>3_restrictions :: "x list \<Rightarrow>'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    "c\<^sub>3_restrictions X \<Gamma> \<Delta> \<sigma> \<equiv> \<forall> x \<in> set X.
      x \<in> dom \<sigma> \<longrightarrow> \<not> nullable (the (\<Gamma> x)) \<and> x \<in> \<Delta> \<longrightarrow> the (\<sigma> x) \<noteq> null"

notation c\<^sub>3_restrictions ("_, _, _ \<turnstile>\<^sub>3 _")

lemma c\<^sub>3_restrictionsI: "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> set X \<subseteq> dom \<sigma> \<Longrightarrow> X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  by (simp add: c\<^sub>3_restrictions_def)

lemma c\<^sub>3_restrictions_cons: "x, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> x # X , \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restriction_def c\<^sub>3_restrictions_def by simp

lemma c\<^sub>3_restrictions_append:
  fixes X Y :: "x list"
  shows
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> Y, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> (X @ Y) , \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3_restrictions_elem:
  fixes X Y :: "x list"
  shows
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> x \<in> set X \<Longrightarrow> x, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restriction_def c\<^sub>3_restrictions_def by simp

lemma c\<^sub>3_restrictions_subset:
  fixes X Y :: "x list"
  shows
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> set Y \<subseteq> set X \<Longrightarrow> Y, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3I_restrictions:
  fixes x :: x and X :: "x list"
  assumes
    "dom \<sigma> \<subseteq> set X" and
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  shows
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  using assms unfolding c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3I_cons:
  fixes x :: x and X :: "x list"
  assumes
    "dom \<sigma> \<subseteq> {x} \<union> set X" and
    "x, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" and
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  shows
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  using assms unfolding c\<^sub>3_restriction_def c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3I_append:
  fixes X Y :: "x list"
  assumes
    "dom \<sigma> \<subseteq> set X \<union> set Y" and
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" and
    "Y, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  shows
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  using assms unfolding c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3_restrictions_zip_transfer:
  fixes Z :: "(x \<times> x) list"
  assumes
    "map fst Z, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
    "\<forall> x \<in> set Z. \<sigma> (fst x) = \<sigma>' (snd x)"
    "\<forall> x \<in> set Z. nullity (the (\<Gamma> (fst x))) \<sqsubseteq> nullity (the (\<Gamma>' (snd x)))"
    "\<forall> x \<in> set Z. snd x \<in> \<Delta>' \<longrightarrow> (nullity (the (\<Gamma>' (snd x))) = ?) \<or> fst x \<in> \<Delta>"
  shows
    "map snd Z, \<Gamma>', \<Delta>' \<turnstile>\<^sub>3 \<sigma>'"
using assms
proof (induction Z)
  case Nil
  then show ?case unfolding c\<^sub>3_restrictions_def by simp
next
  case (Cons z Z)
  moreover
  then have
    F: "fst z # map fst Z, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" by simp
  then have
    "map fst Z, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" using c\<^sub>3_restrictions_subset by (metis set_subset_Cons)
  moreover
  from Cons have
    "\<forall>x\<in>set Z. \<sigma> (fst x) = \<sigma>' (snd x)"
    "\<forall>x\<in>set Z. nullity (the (\<Gamma> (fst x))) \<sqsubseteq> nullity (the (\<Gamma>' (snd x)))"
    "\<forall>x\<in>set Z. snd x \<in> \<Delta>' \<longrightarrow> (nullity (the (\<Gamma>' (snd x))) = ?) \<or> fst x \<in> \<Delta>" by simp_all
  ultimately have
    "map snd Z, \<Gamma>', \<Delta>' \<turnstile>\<^sub>3 \<sigma>'" by blast
  moreover
  from F have
    C: "fst z, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" using c\<^sub>3_restrictions_elem by fastforce
  from Cons have
    S: "\<sigma> (fst z) = \<sigma>' (snd z)" and
    N: "nullity (the (\<Gamma> (fst z))) \<sqsubseteq> nullity (the (\<Gamma>' (snd z)))" and
    D: "snd z \<in> \<Delta>' \<longrightarrow> (nullity (the (\<Gamma>' (snd z))) = ?) \<or> fst z \<in> \<Delta>" by simp_all
  have
    "snd z, \<Gamma>', \<Delta>' \<turnstile>\<^sub>3 \<sigma>'" unfolding c\<^sub>3_restriction_def 
  proof (rule impI, rule impI)
    assume
      "snd z \<in> dom \<sigma>'"
    with S have
      "fst z \<in> dom \<sigma>" by (cases "\<sigma> (fst z)") (simp_all add: domIff)
    moreover
    assume
      "\<not> nullable (the (\<Gamma>' (snd z))) \<and> snd z \<in> \<Delta>'"
    with D N have
      "\<not> nullable (the (\<Gamma> (fst z))) \<and> fst z \<in> \<Delta>"
      unfolding nullity_def by simp_all
    moreover note C S
    ultimately show "the (\<sigma>' (snd z)) \<noteq> null" by (simp add: c\<^sub>3_restriction_def)
  qed
  ultimately show ?case by (simp add: c\<^sub>3_restrictions_cons)
qed

lemma c\<^sub>3_restrictions_transfer:
  fixes X X':: "x list"
  assumes
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>"
    "map \<sigma> X = map \<sigma>' X'"
    "map (nullity \<circ> the \<circ> \<Gamma>) X \<sqsubseteq> map (nullity \<circ> the \<circ> \<Gamma>') X'"
    "\<forall> i < length X'. X' ! i \<in> \<Delta>' \<longrightarrow> (nullity (the (\<Gamma>' (X' ! i))) = ?) \<or> (X ! i) \<in> \<Delta>"
  shows
    "X', \<Gamma>', \<Delta>' \<turnstile>\<^sub>3 \<sigma>'"
proof -
  from assms(2) have
    "length X = length X'" using nth_map_eq by auto
  then obtain Z where
    Z: "Z = zip X X'" and
    X: "X = map fst Z" and
    X': "X' = map snd Z" by fastforce
  from X assms(1) have
    "map fst Z, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" by simp
  moreover from Z assms(2) have
    "\<forall> x \<in> set Z. \<sigma> (fst x) = \<sigma>' (snd x)" by (metis in_set_zip nth_map)
  moreover from X X' assms(3) have
    "\<forall> x \<in> set Z. nullity (the (\<Gamma> (fst x))) \<sqsubseteq> nullity (the (\<Gamma>' (snd x)))" by simp
  moreover from Z assms(4) have
    "\<forall> x \<in> set Z. snd x \<in> \<Delta>' \<longrightarrow> (nullity (the (\<Gamma>' (snd x))) = ?) \<or> fst x \<in> \<Delta>"
    by (metis in_set_zip)
  ultimately have
    "map snd Z, \<Gamma>', \<Delta>' \<turnstile>\<^sub>3 \<sigma>'" using c\<^sub>3_restrictions_zip_transfer by fastforce
  with X' show ?thesis by simp
qed

lemma c\<^sub>3_restrictions_context:
  fixes X :: "x list"
  shows
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> \<forall> x \<in> set X. nullable (the (\<Gamma> x)) = nullable (the (\<Gamma>' x)) \<Longrightarrow> X, \<Gamma>', \<Delta> \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restrictions_def by auto

lemma c\<^sub>3_restrictions_set:
  fixes X :: "x list"
  shows
    "X, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma> \<Longrightarrow> set X \<subseteq> \<Delta> \<Longrightarrow> set X \<subseteq> \<Delta>' \<Longrightarrow> X, \<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>"
  unfolding c\<^sub>3_restrictions_def by auto

definition (in classes) c\<^sub>4 :: "'C \<Gamma> \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>4 \<Gamma> h \<sigma> \<equiv> \<forall> C k n. \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<and> the (\<Gamma> x) = (C, k, n) \<longrightarrow>
      h \<turnstile> the (\<sigma> x) : (C, n)"

notation (in classes) c\<^sub>4 ("_ \<turnstile>\<^sub>4 _, _")

lemma (in classes) c\<^sub>4I:
  "\<forall> C k n. \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<and> the (\<Gamma> x) = (C, k, n) \<longrightarrow> h \<turnstile> the (\<sigma> x) : (C, n) \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>4 h, \<sigma>"
  by simp

lemma (in classes) c\<^sub>4I':
  "\<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<longrightarrow> h \<turnstile> the (\<sigma> x) : type_as_t (the (\<Gamma> x)) \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>4 h, \<sigma>"
  by auto

lemma (in classes) c\<^sub>4D: "\<Gamma> \<turnstile>\<^sub>4 h, \<sigma> \<Longrightarrow>
  \<forall> C k n. \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<and> the (\<Gamma> x) = (C, k, n) \<longrightarrow> h \<turnstile> the (\<sigma> x) : (C, n)"
  by simp

lemma (in classes) c\<^sub>4D': "\<Gamma> \<turnstile>\<^sub>4 h, \<sigma> \<Longrightarrow>
  \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<longrightarrow> h \<turnstile> the (\<sigma> x) : type_as_t (the (\<Gamma> x))"
  using type_as_t c\<^sub>4D by (metis old.prod.exhaust)

lemma (in classes) c\<^sub>4_iff [iff]: "\<Gamma> \<turnstile>\<^sub>4 h, \<sigma> =
  (\<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<longrightarrow> h \<turnstile> the (\<sigma> x) : type_as_t (the (\<Gamma> x)))"
  using c\<^sub>4D' c\<^sub>4I' by blast

definition (in runtime) c\<^sub>5 :: "'C \<Gamma> \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>5 \<Gamma> h \<sigma> \<equiv> \<forall> x \<in> dom \<sigma>. \<forall> y \<in> dom \<sigma>.
       committed (the (\<Gamma> x)) \<longrightarrow> deep_init\<^sub>v h (the (\<sigma> x)) \<and>
         (free (the (\<Gamma> y)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (the (\<sigma> y)))"

notation (in runtime) c\<^sub>5 ("_ \<turnstile>\<^sub>5 _, _")

definition c\<^sub>6 :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> bool"
  where
    [iff]: "c\<^sub>6 \<Gamma> \<Delta> \<equiv> \<not> nullable (the (\<Gamma> this)) \<and> this \<in> \<Delta>"

notation c\<^sub>6 ("_, _ \<turnstile>\<^sub>6")

definition (in runtime) good_configuration :: "'C \<Gamma> \<Rightarrow> \<Delta> \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> bool" ("_, _ \<turnstile> _, _")
  where
    [iff]: "good_configuration \<Gamma> \<Delta> h \<sigma> \<equiv>
      (\<Gamma> \<turnstile>\<^sub>1 \<sigma>) \<and>
      (\<turnstile>\<^sub>2 h) \<and>
      (\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>) \<and>
      (\<Gamma> \<turnstile>\<^sub>4 h, \<sigma>) \<and>
      (\<Gamma> \<turnstile>\<^sub>5 h, \<sigma>) \<and>
      (\<Gamma>, \<Delta> \<turnstile>\<^sub>6)"

end