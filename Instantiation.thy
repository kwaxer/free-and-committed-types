(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 3 (Signature Type Instantiations)\<close>

theory Instantiation imports Expectation Lookup begin

lemma (in classes) vars_in_expectations:
  fixes
    \<theta> :: \<theta> and S:: "'C S"
  assumes
    "vars [S] \<subseteq> dom \<theta>"
  obtains
    k
  where
    "\<ee> \<theta> S = Some k"
  using assms initialization_extension_value signature_expectation_extension_def vars_value'
  by metis

lemma (in classes) lemma_3\<^sub>1:
  fixes
    \<theta> :: \<theta> and S S'::"'C S"
  assumes
    A: "S \<sqsubseteq> S'" and
    D: "dom \<theta> \<supseteq> vars [S, S']"
  obtains k k'
  where
    "\<ee> \<theta> S = Some k" and
    "\<ee> \<theta> S' = Some k'" and
    "k \<sqsubseteq> k'"
proof -
  have "vars [S, S'] = vars [S] \<union> vars [S']" using vars_cons by blast
  with D have
    V: "vars [S] \<subseteq> dom \<theta>" and
    V': "vars [S'] \<subseteq> dom \<theta>" by simp_all
  then obtain k k' where
    K: "\<ee> \<theta> S = Some k" and
    K': "\<ee> \<theta> S' = Some k'"
    using vars_in_expectations by meson
  obtain \<gamma> and \<gamma>' where
    G: "\<gamma> = initialization S" and
    G': "\<gamma>' = initialization S'" by simp
  with A have "\<gamma> \<sqsubseteq> \<gamma>'" using initialization_subtyping by blast
  with G G' K K' have "k \<sqsubseteq> k'" using initialization_extension_def by (cases \<gamma>, cases \<gamma>') auto
  with K K' show ?thesis using that by simp
qed

lemma (in classes) lemma_3\<^sub>1':
  fixes
    \<theta> :: \<theta> and S S':: "'C S"
  assumes
    "S \<sqsubseteq> S'" and
    "dom \<theta> \<supseteq> vars [S, S']"
  shows
    "\<ee> \<theta> S \<sqsubseteq> \<ee> \<theta> S'"
  using assms lemma_3\<^sub>1
  apply simp
  by (metis initialization_def initialization_extension_def lemma_3\<^sub>1 option.simps(5) signature_expectation_extension_def)

lemma (in classes) lemma_3\<^sub>1'':
  fixes
    \<theta> :: \<theta> and S S':: "'C S"
  assumes
    "S \<sqsubseteq> S'" and
    "dom \<theta> \<supseteq> vars [S, S']"
  shows
    "\<ss> \<theta> S \<sqsubseteq> \<ss> \<theta> S'"
proof -
  from assms obtain k k' where
    "\<ee> \<theta> S = Some k" and
    "\<ee> \<theta> S' = Some k'" and
    "k \<sqsubseteq> k'" using lemma_3\<^sub>1 by blast
  moreover
  then have
    "initialization (the (\<ss> \<theta> S)) = the (\<ee> \<theta> S)" and
    "initialization (the (\<ss> \<theta> S')) = the (\<ee> \<theta> S')" and
    n: "\<ss> \<theta> S \<noteq> None" and
    n': "\<ss> \<theta> S' \<noteq> None"
    using substitution_initialization some_expectation_substitution by blast+
  ultimately have
    "initialization (the (\<ss> \<theta> S)) \<sqsubseteq> initialization (the (\<ss> \<theta> S'))" by simp
  moreover
  from assms(1) have
    "base S \<sqsubseteq> base S'" using base_subtyping by blast
  with n n' have
    "base (the (\<ss> \<theta> S)) \<sqsubseteq> base (the (\<ss> \<theta> S'))" using substitution_base by metis
  moreover from assms(1) have
    "nullity S \<sqsubseteq> nullity S'" using nullity_subtyping by blast
  with n n' have
    "nullity (the (\<ss> \<theta> S)) \<sqsubseteq> nullity (the (\<ss> \<theta> S'))" using substitution_nullity by metis
  ultimately have
    "the (\<ss> \<theta> S) \<sqsubseteq> the (\<ss> \<theta> S')" using subtyping by blast
  with n n' show ?thesis by auto
qed

lemma (in lookup) lemma_3\<^sub>2':
  fixes
    \<gamma>::\<gamma> and \<theta>::\<theta> and xS\<^sub>i:: "'C xS\<^sub>i"
  assumes
    "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))"
  obtains k::k
  where
    "\<ee> \<theta> \<gamma> = Some k" and
    "is_type (D, Inl k, !)"
proof -
  from assms have
    D: "vars [(D, \<gamma>, !)] \<subseteq> dom \<theta>" using vars_cons instance_def by blast
  then have "var \<gamma> \<subseteq> dom \<theta>" using vars_value' initialization_def by (simp add: vars_def var1_def)
  then obtain k where "\<ee> \<theta> \<gamma> = Some k" using D vars_variable using initialization_extension_def
    by (cases \<gamma>) (auto simp add: vars_def var1_def)
  moreover have "is_type (D, Inl k, !)" by (simp add: is_type_def)
  ultimately show ?thesis by (simp add: that) 
qed

lemma (in lookup) lemma_3\<^sub>2'':
  fixes
    \<gamma>::\<gamma> and \<theta>::\<theta> and xS\<^sub>i yS\<^sub>j :: "'C xS\<^sub>i"
  assumes
    "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))"
  obtains T\<^sub>i :: "'C T\<^sub>i"
  where
    "map (\<ss> \<theta>) (\<tau> xS\<^sub>i) = map Some T\<^sub>i"
  using assms instance_arg_types instance_cons by metis

lemma (in lookup) lemma_3\<^sub>2'''':
  fixes
    \<gamma>::\<gamma> and \<theta>::\<theta> and xS\<^sub>i yS\<^sub>j:: "'C xS\<^sub>i"
  assumes
    S: "mSig (D, m) = Some (\<gamma>, xS\<^sub>i, S, yS\<^sub>j)" and
    I: "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))"
  obtains T\<^sub>j :: "'C T\<^sub>i"
  where
    "map (\<ss> \<theta>) (\<tau> yS\<^sub>j) = map Some T\<^sub>j"
proof -
  have "yS\<^sub>j = method_locals (\<gamma>, xS\<^sub>i, S, yS\<^sub>j)" by simp
  moreover have "xS\<^sub>i = method_arguments (\<gamma>, xS\<^sub>i, S, yS\<^sub>j)" by simp
  moreover note S method_local_expectation
  ultimately have "vars (\<tau> yS\<^sub>j) \<subseteq> vars [(D, \<gamma>, !)] \<union> vars (\<tau> xS\<^sub>i)" by fastforce
  with I have "vars (\<tau> yS\<^sub>j) \<subseteq> dom \<theta>"
    by (simp add: instance_def vars_def subset_eq)
  then show ?thesis using signature_extension_map that by blast
qed

context lookup begin

  definition lemma_3\<^sub>3_P1:
    "lemma_3\<^sub>3_P1 \<theta> R S\<^sub>i \<theta>' \<longleftrightarrow> (\<forall> \<gamma> :: \<gamma>. var \<gamma> \<subseteq> vars S\<^sub>i \<longrightarrow> \<ee> \<theta>' \<gamma> = \<ee> \<theta> \<gamma>)"

  definition lemma_3\<^sub>3_P2:
    "lemma_3\<^sub>3_P2 \<theta> R S\<^sub>i \<theta>' \<longleftrightarrow> (\<forall> S :: 'C S. var1 S \<subseteq> vars S\<^sub>i \<longrightarrow> \<ss> \<theta>' S = \<ss> \<theta> S)"

  definition lemma_3\<^sub>3_P3:
    "lemma_3\<^sub>3_P3 \<theta> R S\<^sub>i \<theta>' \<longleftrightarrow> (\<forall> S\<^sub>j :: 'C S\<^sub>i. vars S\<^sub>j \<subseteq> vars S\<^sub>i \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j = map (\<ss> \<theta>) S\<^sub>j)"

  definition lemma_3\<^sub>3_P4:
    "lemma_3\<^sub>3_P4 \<theta> R S\<^sub>i \<theta>' \<longleftrightarrow> \<ss> \<theta>' R = \<ss> \<theta> R"

end

lemma (in lookup) lemma_3\<^sub>3:
  fixes
    \<theta>::\<theta> and S\<^sub>i S\<^sub>j:: "'C S\<^sub>i" and R S :: "'C S"
  assumes
    "instance \<theta> R S\<^sub>i"
  obtains
    \<theta>'::\<theta>
  where
    "\<theta>' \<in> instances R S\<^sub>i \<and>
    lemma_3\<^sub>3_P1 \<theta> R S\<^sub>i \<theta>' \<and>
    lemma_3\<^sub>3_P2 \<theta> R S\<^sub>i \<theta>' \<and>
    lemma_3\<^sub>3_P3 \<theta> R S\<^sub>i \<theta>' \<and>
    lemma_3\<^sub>3_P4 \<theta> R S\<^sub>i \<theta>'"
proof -
  from assms have
    RD: "var1 R \<subseteq> dom \<theta>" and
    SD: "vars S\<^sub>i \<subseteq> dom \<theta>" using instanceD(1,2) by blast+
  moreover obtain \<theta>' where
    T': "\<theta>' = \<theta> |` (var1 R \<union> vars S\<^sub>i)" by simp
  {
    fix S T
    assume
      A: "\<ss> \<theta>' S = Some T"
    then have "\<ss> \<theta> S = Some T"
    proof (cases "initialization S")
      case Inl
      with A T' show ?thesis by (simp add: substitution_restriction var0 var1_def)
    next
      case (Inr \<alpha>)
      then obtain C n where
        S: "S = (C, Inr \<alpha>, n)" using initialization_def by (metis prod.collapse)
      then have
        "\<ss> \<theta>' (C, Inr \<alpha>, n) = (case \<theta> \<alpha> of
        Some k \<Rightarrow> Some (C, k, n) |
        _ \<Rightarrow> None)"
        apply (cases "\<theta>' \<alpha>")
        apply (metis A Inr initialization_extension_def old.sum.simps(6) option.distinct(1) signature_expectation_extension_def some_expectation_substitution)
        by (metis (no_types, lifting) Inr T' base initialization_extension_def initialization_extension_var_iff nullity old.sum.simps(6) option.case_eq_if option.distinct(1) restrict_map_def signature_extension var1_def)
      from A S obtain k where
        "\<theta>' \<alpha> = Some k"
        by (metis initialization initialization_extension_def old.sum.simps(6) option.collapse option.discI signature_expectation_extension_def some_expectation_substitution)
      moreover with T' have
        "\<theta> \<alpha> = Some k" by (metis option.discI restrict_in restrict_out)
      ultimately show ?thesis using A S
        by (metis Inr initialization_extension_def initialization_extension_var_iff old.sum.simps(6) option.distinct(1) signature_extension var1_def)
    qed
  }
  moreover note that
  then have
    "\<And>T S. \<ss> \<theta>' S = Some T \<Longrightarrow> \<ss> \<theta> S = Some T"
      using calculation(3) by blast
  ultimately have
    V: "dom \<theta>' = var1 R \<union> vars S\<^sub>i"
    by (simp add: T' domIff subset_antisym subset_iff)
  moreover have "instance \<theta>' R S\<^sub>i" using T' assms unfolding instance_def
    by (metis Un_upper1 Un_upper2 calculation domI restrict_in)
  ultimately have
    "\<theta>' \<in> instances R S\<^sub>i" by (simp add: instances_def)
  moreover from T' have
    "\<forall> \<gamma>. var \<gamma> \<subseteq> vars S\<^sub>i \<longrightarrow> \<ee> \<theta>' \<gamma> = \<ee> \<theta> \<gamma>"
    by (metis Un_commute initialization_extension_restriction le_supI1)
  moreover from T' have
    "\<forall> S. var1 S \<subseteq> vars S\<^sub>i \<longrightarrow> \<ss> \<theta>' S = \<ss> \<theta> S"
    by (simp add: subset_trans substitution_restriction)
  moreover from T' have
    "\<forall> S\<^sub>j. vars S\<^sub>j \<subseteq> vars S\<^sub>i \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j = map (\<ss> \<theta>) S\<^sub>j"
    by (metis Un_commute expectation_restriction le_supI1)
  moreover from T' have
    "\<ss> \<theta>' R = \<ss> \<theta> R" by (simp add: substitution_restriction)
  ultimately show ?thesis using that
    unfolding lemma_3\<^sub>3_P1
    unfolding lemma_3\<^sub>3_P2
    unfolding lemma_3\<^sub>3_P3
    unfolding lemma_3\<^sub>3_P4
    by blast
qed

lemma (in lookup) lemma_3\<^sub>4:
  fixes
    \<gamma> \<gamma>'::\<gamma> and \<theta>::\<theta> and C D::'C and S S':: "'C S" and xS\<^sub>i xS\<^sub>i' yS\<^sub>l yS\<^sub>l':: "'C xS\<^sub>i"
  assumes
    C: "D \<sqsubseteq> C" and
    M\<^sub>D: "mSig (D, m) = Some (\<gamma>, xS\<^sub>i, S, yS\<^sub>l)" and
    M\<^sub>C: "mSig (C, m) = Some (\<gamma>', xS\<^sub>i', S', yS\<^sub>l')" and
    I: "instance \<theta> S' ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))"
  obtains
    \<theta>'::\<theta>
  where
    "\<theta>' \<in> instances S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" and
    "\<ee> \<theta>' \<gamma> = \<ee> \<theta> \<gamma>" and
    "map (\<ss> \<theta>') (\<tau> xS\<^sub>i) = map (\<ss> \<theta>) (\<tau> xS\<^sub>i)" and
    "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" and
    "map (\<ss> \<theta>') (\<tau> yS\<^sub>l) = map (\<ss> \<theta>) (\<tau> yS\<^sub>l)"
proof -
  from M\<^sub>C M\<^sub>D have
    M\<^sub>C': "m \<in> methods C" and
    M\<^sub>D': "m \<in> methods D" by (simp_all add: domI methods_dom)
  moreover from M\<^sub>D M\<^sub>C have
    T\<^sub>D: "method_target (the (mSig (D, m))) = \<gamma>" and
    T\<^sub>C: "method_target (the (mSig (C, m))) = \<gamma>'" and
    A\<^sub>D: "method_arguments (the (mSig (D, m))) = xS\<^sub>i" and
    A\<^sub>C: "method_arguments (the (mSig (C, m))) = xS\<^sub>i'" and
    R\<^sub>D: "method_result (the (mSig (D, m))) = S" and
    R\<^sub>C: "method_result (the (mSig (C, m))) = S'" and
    L\<^sub>D: "method_locals (the (mSig (D, m))) = yS\<^sub>l" and
    L\<^sub>C: "method_locals (the (mSig (C, m))) = yS\<^sub>l'"
    by simp_all
  moreover note C
  ultimately have
    G: "\<gamma>' \<sqsubseteq> \<gamma>" and
    T: "\<tau> xS\<^sub>i' \<sqsubseteq> \<tau> xS\<^sub>i" and
    R: "S \<sqsubseteq> S'"
    using methods_contravariant_target methods_contravariant_arguments methods_covariant_result by blast+
  from G T have
    M: "map initialization ((C, \<gamma>', !) # (\<tau> xS\<^sub>i')) \<sqsubseteq> map initialization ((D, \<gamma>, !) # \<tau> xS\<^sub>i)"
    using initializations_subtyping list_leq_cons by fastforce
  have
    v\<gamma>: "var \<gamma> \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using vars_cons vars_value' by fastforce
  have
    vx: "vars (\<tau> xS\<^sub>i) \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" by (simp add: sup.coboundedI2 vars_cons1)
  from A\<^sub>D L\<^sub>D M\<^sub>D T\<^sub>D have
    vy: "vars (\<tau> yS\<^sub>l) \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using method_local_expectation vars_cons
    by (metis (no_types) Un_left_commute option.sel sup.absorb_iff2)
  from M\<^sub>C' C T\<^sub>C T\<^sub>D have "vars [(D, \<gamma>, !)] \<subseteq> vars [(C, \<gamma>', !)]" using target_vars by auto
  moreover from M\<^sub>C' C A\<^sub>C A\<^sub>D have "vars (\<tau> xS\<^sub>i) \<subseteq> vars (\<tau> xS\<^sub>i')" using arguments_vars by auto
  ultimately have
    V: "vars ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<subseteq> vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" using vars_cons
    by (metis (no_types) inf_sup_aci(6) sup_mono)
  show ?thesis
  proof (cases "var1 S = {}"; cases "var1 S' = {}")
    assume
      vS: "var1 S = {}" and
      vS': "var1 S' = {}"
    then have
      "var1 S \<subseteq> dom \<theta>" by simp
    with M I have
       "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using instance_restriction by blast
    then obtain \<theta>' where
      \<theta>': "\<theta>' \<in> instances S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" and
      "lemma_3\<^sub>3_P1 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P2 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P3 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P4 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'"
      using lemma_3\<^sub>3 by metis
    then have
      "\<forall> \<gamma>'' :: \<gamma> . var \<gamma>'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ee> \<theta>' \<gamma>'' = \<ee> \<theta> \<gamma>''" and
      "\<forall> S'' ::'C S. var1 S'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ss> \<theta>' S'' = \<ss> \<theta> S''" and
      "\<forall> S\<^sub>j'' ::'C S\<^sub>i. vars S\<^sub>j'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j'' = map (\<ss> \<theta>) S\<^sub>j''" and
      "\<ss> \<theta>' S = \<ss> \<theta> S"
      by (simp_all add: lemma_3\<^sub>3_P1 lemma_3\<^sub>3_P2 lemma_3\<^sub>3_P3 lemma_3\<^sub>3_P4)
    moreover from R vS vS' calculation(4) have
      "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" using substitution_subtyping by (metis empty_subsetI)
    moreover note v\<gamma> vx vy \<theta>'
    ultimately show ?thesis using that by simp
  next
    assume
      "var1 S = {}"
      "var1 S' \<noteq> {}"
    with R show ?thesis using var1_le by blast
  next
    assume
      vS: "var1 S \<noteq> {}" and
      vS': "var1 S' = {}"
    then obtain \<alpha> where
      vS: "var1 S = {\<alpha>}" unfolding var1_def using vars_value' vars_any by metis
    from R have
      SS': "initialization S \<sqsubseteq> initialization S'" using initialization_subtyping by blast
    then have
      "initialization S' = Inl \<diamondop>"
    proof (cases "initialization S'")
      case (Inl k)
      with SS' show ?thesis
        apply (cases k)
        apply (metis generic_initialization_specialization insert_not_empty top_k_def vS vS' var1_def)
        apply (metis generic_initialization_specialization insert_not_empty top_k_def vS vS' var1_def)
        by simp
    next
      case (Inr \<beta>)
      then show ?thesis
        by (metis insert_not_empty vS' var1 var1_def)
    qed
    show ?thesis
    proof (cases "\<alpha> \<in> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))")
      case True
      then have
        "\<alpha> \<in> dom \<theta>" using I V instanceD(2) by blast
      with vS have
        vD: "var1 S \<subseteq> dom \<theta>" by simp
      with M I have
         "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using instance_restriction by blast
      then obtain \<theta>' where
        \<theta>': "\<theta>' \<in> instances S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" and
        "lemma_3\<^sub>3_P1 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P2 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P3 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P4 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'"
        using lemma_3\<^sub>3 by metis
      then have
        "\<forall> \<gamma>'' :: \<gamma> . var \<gamma>'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ee> \<theta>' \<gamma>'' = \<ee> \<theta> \<gamma>''" and
        "\<forall> S'' ::'C S. var1 S'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ss> \<theta>' S'' = \<ss> \<theta> S''" and
        "\<forall> S\<^sub>j'' ::'C S\<^sub>i. vars S\<^sub>j'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j'' = map (\<ss> \<theta>) S\<^sub>j''" and
        "\<ss> \<theta>' S = \<ss> \<theta> S"
        by (simp_all add: lemma_3\<^sub>3_P1 lemma_3\<^sub>3_P2 lemma_3\<^sub>3_P3 lemma_3\<^sub>3_P4)
      moreover from R vD vS' calculation(4) have
        "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" using substitution_subtyping by (metis empty_subsetI)
      moreover note v\<gamma> vx vy \<theta>'
      ultimately show ?thesis using that by auto
    next
      case False
      then obtain \<theta>'' where
        \<theta>'': "\<theta>'' = \<theta> (\<alpha> := Some \<diamondop>)" by simp
      with vS have
        SD: "var1 S \<subseteq> dom \<theta>''" by simp
      moreover from I have
        vCD: "vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i')) \<subseteq> dom \<theta>" using instanceD(2) by blast
      with V \<theta>'' have
        "vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<subseteq> dom \<theta>''" by auto
      moreover have
        *: "\<forall> i j \<alpha>.
          i < length ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<and>
          j < length ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<and>
          i \<noteq> j \<and>
          initialization (nth ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) i) = Inr \<alpha> \<and>
          initialization (nth ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) j) = Inr \<alpha> \<longrightarrow>
          \<theta> \<alpha> \<noteq> Some \<diamondop>"
        by (meson I M instanceD(1) instanceD(3) instance_restriction)
      have
        "\<forall> i j \<alpha>.
          i < length ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<and>
          j < length ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<and>
          i \<noteq> j \<and>
          initialization (nth ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) i) = Inr \<alpha> \<and>
          initialization (nth ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) j) = Inr \<alpha> \<longrightarrow>
          \<theta>'' \<alpha> \<noteq> Some \<diamondop>"
      proof (rule allI, rule allI, rule allI, rule impI)
        fix i j \<beta>
        assume
          "i < length ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<and>
           j < length ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<and>
           i \<noteq> j \<and>
           initialization (((D, \<gamma>, !) # \<tau> xS\<^sub>i) ! i) = Inr \<beta> \<and>
           initialization (((D, \<gamma>, !) # \<tau> xS\<^sub>i) ! j) = Inr \<beta>"
        moreover have
          "initialization (((D, \<gamma>, !) # \<tau> xS\<^sub>i) ! i) \<noteq> Inr \<alpha>"
        proof (rule ccontr)
          assume
            "\<not> initialization (((D, \<gamma>, !) # \<tau> xS\<^sub>i) ! i) \<noteq> Inr \<alpha>"
          then have
            "var1 (((D, \<gamma>, !) # \<tau> xS\<^sub>i) ! i) = {\<alpha>}" using initialization_var by blast
          then have
            "\<alpha> \<in> vars ((D, \<gamma>, !) # \<tau> xS\<^sub>i)" using vars_item
            by (metis calculation insert_subset nth_mem)
          with False show False by simp
        qed
        moreover note * \<theta>''
        ultimately show "\<theta>'' \<beta> \<noteq> Some \<diamondop>" by simp
      qed
      ultimately have
        "instance \<theta>'' S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using instanceI by blast
      then obtain \<theta>' where
        \<theta>': "\<theta>' \<in> instances S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" and
        "lemma_3\<^sub>3_P1 \<theta>'' S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P2 \<theta>'' S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P3 \<theta>'' S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
        "lemma_3\<^sub>3_P4 \<theta>'' S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'"
        using lemma_3\<^sub>3 by metis
      then have
        "\<forall> \<gamma>'' :: \<gamma> . var \<gamma>'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ee> \<theta>' \<gamma>'' = \<ee> \<theta>'' \<gamma>''" and
        "\<forall> S'' ::'C S. var1 S'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ss> \<theta>' S'' = \<ss> \<theta>'' S''" and
        "\<forall> S\<^sub>j'' ::'C S\<^sub>i. vars S\<^sub>j'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j'' = map (\<ss> \<theta>'') S\<^sub>j''" and
        "\<ss> \<theta>' S = \<ss> \<theta>'' S"
        by (simp_all add: lemma_3\<^sub>3_P1 lemma_3\<^sub>3_P2 lemma_3\<^sub>3_P3 lemma_3\<^sub>3_P4)
      moreover
      from R vS vS' SD \<theta>'' calculation(4) have
        "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" using signature_expectation_other substitution_subtyping
        by (metis empty_iff empty_subsetI)
      moreover from v\<gamma> False \<theta>'' calculation(1) have
        "\<ee> \<theta>' \<gamma> = \<ee> \<theta> \<gamma>" using initialization_expectation_other by fastforce
      moreover from vx vy False \<theta>'' calculation(3) vCD V have
        "map (\<ss> \<theta>') (\<tau> xS\<^sub>i) = map (\<ss> \<theta>) (\<tau> xS\<^sub>i)"
        "map (\<ss> \<theta>') (\<tau> yS\<^sub>l) = map (\<ss> \<theta>) (\<tau> yS\<^sub>l)"
        using signature_expectation_map_other
        by (metis (no_types) subset_iff)+
      moreover note \<theta>'
      ultimately show ?thesis using that by simp
    qed
  next
    assume
      vN: "var1 S \<noteq> {}"
      "var1 S' \<noteq> {}"
    then obtain \<alpha> where
      vS': "var1 S' = {\<alpha>}" unfolding var1_def using vars_value' vars_any by metis
    moreover with vN R have
      vS: "var1 S = {\<alpha>}"
      unfolding var1_def by (cases "initialization S")
        (simp add: var0, metis subset_singletonD var1 var1_def var1_le)
    moreover note R
    ultimately have
      "initialization S' \<sqsubseteq> initialization S" unfolding var1_def using var0
      by (cases "initialization S") (simp, metis initialization_subtyping varI1)
    moreover from vS I vS' that have
      SD: "var1 S \<subseteq> dom \<theta>" using instanceD(1) by metis
    moreover note I M that
    ultimately have
      "instance \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" using instance_restriction by blast
    then obtain \<theta>' where
      \<theta>': "\<theta>' \<in> instances S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))" and
      "lemma_3\<^sub>3_P1 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P2 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P3 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'" and
      "lemma_3\<^sub>3_P4 \<theta> S ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<theta>'"
      using lemma_3\<^sub>3 by metis
    then have
      "\<forall> \<gamma>'' :: \<gamma> . var \<gamma>'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ee> \<theta>' \<gamma>'' = \<ee> \<theta> \<gamma>''" and
      "\<forall> S'' ::'C S. var1 S'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> \<ss> \<theta>' S'' = \<ss> \<theta> S''" and
      "\<forall> S\<^sub>j'' ::'C S\<^sub>i. vars S\<^sub>j'' \<subseteq> vars ((D, \<gamma>, !) # (\<tau> xS\<^sub>i)) \<longrightarrow> map (\<ss> \<theta>') S\<^sub>j'' = map (\<ss> \<theta>) S\<^sub>j''" and
      "\<ss> \<theta>' S = \<ss> \<theta> S"
        by (simp_all add: lemma_3\<^sub>3_P1 lemma_3\<^sub>3_P2 lemma_3\<^sub>3_P3 lemma_3\<^sub>3_P4)
    moreover from R vS vS' SD have
      "\<ss> \<theta> S \<sqsubseteq> \<ss> \<theta> S'" using lemma_3\<^sub>1' using substitution_subtyping by metis
    with calculation(4) have
      "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" by simp
    moreover note v\<gamma> vx vy that \<theta>'
    ultimately show ?thesis by blast
  qed
qed

(*
lemma (in lookup) lemma_3\<^sub>4:
  fixes
    \<gamma> \<gamma>'::\<gamma> and \<theta>::\<theta> and C D::'C and S S':: "'C S" and xS\<^sub>i xS\<^sub>i' yS\<^sub>l yS\<^sub>l':: "'C xS\<^sub>i"
  assumes
    C: "D \<sqsubseteq> C" and
    M\<^sub>D: "mSig (D, m) = Some (\<gamma>, xS\<^sub>i, S, yS\<^sub>l)" and
    M\<^sub>C: "mSig (C, m) = Some (\<gamma>', xS\<^sub>i', S', yS\<^sub>l')" and
    I: "instance \<theta> ((D, \<gamma>, !) # (\<tau> xS\<^sub>i))"
  obtains
    \<theta>'::\<theta>
  where
    "\<theta>' \<in> instances ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" and
    "\<ee> \<theta>' \<gamma>' = \<ee> \<theta> \<gamma>'" and
    "map (\<ss> \<theta>') (\<tau> xS\<^sub>i') = map (\<ss> \<theta>) (\<tau> xS\<^sub>i')" and
    "\<ee> \<theta>' S' = \<ee> \<theta> S'" and
    "map (\<ss> \<theta>') (\<tau> yS\<^sub>l') = map (\<ss> \<theta>) (\<tau> yS\<^sub>l')"
proof -
  from M\<^sub>C M\<^sub>D have
    M\<^sub>C': "m \<in> methods C" and
    M\<^sub>D': "m \<in> methods D" by (simp_all add: domI methods_dom)
  moreover from M\<^sub>D M\<^sub>C have
    T\<^sub>D: "method_target (the (mSig (D, m))) = \<gamma>" and
    T\<^sub>C: "method_target (the (mSig (C, m))) = \<gamma>'" and
    A\<^sub>D: "method_arguments (the (mSig (D, m))) = xS\<^sub>i" and
    A\<^sub>C: "method_arguments (the (mSig (C, m))) = xS\<^sub>i'" and
    R\<^sub>D: "method_result (the (mSig (D, m))) = S" and
    R\<^sub>C: "method_result (the (mSig (C, m))) = S'" and
    L\<^sub>D: "method_locals (the (mSig (D, m))) = yS\<^sub>l" and
    L\<^sub>C: "method_locals (the (mSig (C, m))) = yS\<^sub>l'"
    by simp_all
  moreover note C
  ultimately have
    G: "\<gamma> \<sqsubseteq> \<gamma>'" and
    T: "\<tau> xS\<^sub>i \<sqsubseteq> \<tau> xS\<^sub>i'" and
    R: "S \<sqsubseteq> S'"
    using methods_covariant_target methods_covariant_arguments methods_covariant_result by blast+
  from M\<^sub>C' C T\<^sub>C T\<^sub>D have "vars [(C, \<gamma>', !)] \<subseteq> vars [(D, \<gamma>, !)]" using target_vars by auto
  moreover from M\<^sub>C' C A\<^sub>C A\<^sub>D have "vars (\<tau> xS\<^sub>i') \<subseteq> vars (\<tau> xS\<^sub>i)" using arguments_vars by auto
  moreover from I have "vars ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<subseteq> dom \<theta>" by (simp add: instance_def)
  ultimately have
    V: "vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i')) \<subseteq> dom \<theta>" using vars_cons by blast
  from I have
    "(\<forall> i j \<alpha>.
      i < length ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<and>
      j < length ((D, \<gamma>, !) # \<tau> xS\<^sub>i) \<and>
       i \<noteq> j \<and>
      initialization (nth ((D, \<gamma>, !) # \<tau> xS\<^sub>i) i) = Inr \<alpha> \<and>
      initialization (nth ((D, \<gamma>, !) # \<tau> xS\<^sub>i) j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>)" using instance_def by blast
  moreover obtain \<gamma>\<^sub>i \<gamma>\<^sub>i' :: "\<gamma> list" where
    G1: "\<gamma>\<^sub>i = \<gamma> # map initialization (\<tau> xS\<^sub>i)" and
    G2: "\<gamma>\<^sub>i' = \<gamma>' # map initialization (\<tau> xS\<^sub>i')" by simp
  moreover with G T have
    L: "\<gamma>\<^sub>i \<sqsubseteq> \<gamma>\<^sub>i'"
    using initializations_subtyping list_leq_cons by blast
  then have
    I: "\<forall> i. i < length \<gamma>\<^sub>i \<longrightarrow> \<gamma>\<^sub>i ! i \<sqsubseteq> \<gamma>\<^sub>i' ! i" using list_leq_iff by blast
  {
    fix i
    assume
      "i < length \<gamma>\<^sub>i \<longrightarrow> \<gamma>\<^sub>i ! i \<sqsubseteq> \<gamma>\<^sub>i' ! i"
    then have "i < length \<gamma>\<^sub>i \<longrightarrow> \<gamma>\<^sub>i ! i = \<gamma>\<^sub>i' ! i \<or> \<gamma>\<^sub>i' ! i = Inl \<diamondop>"
      using generic_initialization_specialization by simp
  }
  with I have
    E: "\<forall> i. i < length \<gamma>\<^sub>i \<longrightarrow> \<gamma>\<^sub>i ! i = \<gamma>\<^sub>i' ! i \<or> \<gamma>\<^sub>i' ! i = Inl \<diamondop>" by blast
  from G1 G2 have
    G1': "\<gamma>\<^sub>i = map initialization ((D, \<gamma>, !) # \<tau> xS\<^sub>i)" and
    G2': "\<gamma>\<^sub>i' = map initialization ((C, \<gamma>', !) # \<tau> xS\<^sub>i')"
    by simp_all
  ultimately have
    A: "(\<forall> i j \<alpha>. i < length \<gamma>\<^sub>i \<and> j < length \<gamma>\<^sub>i \<and> i \<noteq> j \<and> \<gamma>\<^sub>i ! i = Inr \<alpha> \<and> \<gamma>\<^sub>i ! j = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>)" using nth_map length_map
    by (metis (no_types))
  with G T L have
    "\<forall> i j \<alpha>. i < length \<gamma>\<^sub>i' \<and> j < length \<gamma>\<^sub>i' \<and> i \<noteq> j \<and> \<gamma>\<^sub>i' ! i = Inr \<alpha> \<and> \<gamma>\<^sub>i' ! j = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>"
    by (metis (no_types) E Inr_not_Inl list_leq_iff)
  with T G G2' have
    "(\<forall> i j \<alpha>.
      i < length ((C, \<gamma>', !) # \<tau> xS\<^sub>i') \<and>
      j < length ((C, \<gamma>', !) # \<tau> xS\<^sub>i') \<and>
      i \<noteq> j \<and>
      initialization (nth ((C, \<gamma>', !) # \<tau> xS\<^sub>i') i) = Inr \<alpha> \<and>
      initialization (nth ((C, \<gamma>', !) # \<tau> xS\<^sub>i') j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>)" by (metis length_map nth_map)
  with V have
    \<Theta>\<^sub>I: "instance \<theta> ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" using instance_def by blast
  then obtain \<theta>' where
    \<Theta>': "\<theta>' = \<theta> |` vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" by simp
  then have
    "dom \<theta>' = dom \<theta> \<inter> vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" by simp
  moreover from \<Theta>\<^sub>I V have
    "vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i')) \<subseteq> dom \<theta>" by simp
  ultimately have
    \<Theta>\<^sub>D: "dom \<theta>' = vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" by (simp add: Int_absorb1)
  moreover from \<Theta>\<^sub>I \<Theta>' \<Theta>\<^sub>D have
    i\<theta>': "instance \<theta>' ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" unfolding instance_def
    by (metis domI equalityE restrict_in)
  ultimately have
    "\<theta>' \<in> instances ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" using instancesI by simp
  moreover from G2 G2' \<Theta>' have
    "\<ee> \<theta>' \<gamma>' = \<ee> \<theta> \<gamma>'" using initialization_extension_restriction vars_set vars_value'
    by (metis list.inject list.set_intros(1) list.simps(9))
  moreover from V have
    V': "vars (\<tau> xS\<^sub>i') \<subseteq> dom \<theta>" using vars_cons by fastforce
  with \<Theta>' i\<theta>' \<Theta>\<^sub>D have
    "map (\<ss> \<theta>') (\<tau> xS\<^sub>i') = map (\<ss> \<theta>) (\<tau> xS\<^sub>i')"
    using expectation_restriction instance_cons instance_dom by metis
  moreover from A\<^sub>C M\<^sub>C R\<^sub>C T\<^sub>C have
    V\<^sub>R: "vars [S'] \<subseteq> vars ((C, \<gamma>', !) # (\<tau> xS\<^sub>i'))" using method_result_expectation vars_cons
      by (metis option.sel)
  with \<Theta>' have
    "\<ee> \<theta>' S' = \<ee> \<theta> S'"
      using initialization_extension_restriction signature_expectation_extension_def vars_value'
      by metis
  moreover from M\<^sub>C obtain M where
    "M = (\<gamma>', xS\<^sub>i', S', yS\<^sub>l')" and
    "the (mSig (C, m)) = M"
    by simp
  with A\<^sub>C M\<^sub>C L\<^sub>C T\<^sub>C have
    V\<^sub>L: "vars (\<tau> yS\<^sub>l') \<subseteq> vars [(C, \<gamma>', !)] \<union> vars (\<tau> xS\<^sub>i')"
    using method_local_expectation by fastforce
  with \<Theta>' M\<^sub>C \<Theta>\<^sub>I have
    "map (\<ss> \<theta>') (\<tau> yS\<^sub>l') = map (\<ss> \<theta>) (\<tau> yS\<^sub>l')" using expectation_restriction vars_cons
    by metis
  ultimately show ?thesis using that by blast
qed
*)

end