@echo off

if not defined ISABELLE_DIR (
	echo Environment variable ISABELLE_DIR should be set to a valid Isabelle installation directory.
	echo Build failed!
	exit /b -1
)
if not exist "%ISABELLE_DIR%" (
	echo Environment variable ISABELLE_DIR should be set to a valid Isabelle installation directory.
	echo Build failed!
	exit /b -1
)

set TEMP_WINDOWS=%TEMP%
set HOME=%HOMEDRIVE%%HOMEPATH%
set LANG=en_US.UTF-8
set CHERE_INVOKING=true

rem start /b /w "Isabelle" 
"%ISABELLE_DIR%\contrib\cygwin\bin\bash" --login -c "isabelle build -d . -v Free_and_Committed_Types"
