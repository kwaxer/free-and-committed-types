(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 5 (Statements)\<close>

theory Statement imports Expression Type begin

datatype 'C statement =
  Variable_assignment x e ("_ \<Colon>= _") |
  Field_assignment x f x ("_ \<^bsub>\<bullet>\<^esub> _ \<Colon>= _") |
  Method_call x x m "x list" ("_ := _ \<^bsub>\<bullet>\<^esub> _ ( _ )") |
  Object_creation x 'C "x list" ("_ \<Colon>= new _ ( _ )") |
  Cast x "'C t" x ("_ \<Colon>= \<lparr> _ \<rparr> _") |
  Sequential_composition "'C statement" "'C statement" ("_ ;; _")

type_synonym 'C s = "'C statement"

fun wf_statement :: "'C s \<Rightarrow> bool"
  where
    wfAss: "wf_statement (x \<Colon>= _) \<longleftrightarrow> x \<noteq> this " |
    wfCall: "wf_statement (x := _ \<^bsub>\<bullet>\<^esub> _ ( _ )) \<longleftrightarrow> x \<noteq> this" |
    wfCreation: "wf_statement (x \<Colon>= new _ ( _ )) \<longleftrightarrow> x \<noteq> this" |
    wfCast: "wf_statement (x \<Colon>= \<lparr> _ \<rparr> _) \<longleftrightarrow> x \<noteq> this " |
    wfSeq: "wf_statement (s\<^sub>1 ;; s\<^sub>2) \<longleftrightarrow> wf_statement s\<^sub>1 \<and> wf_statement s\<^sub>2" |
    wfOther: "wf_statement _ \<longleftrightarrow> True"

end