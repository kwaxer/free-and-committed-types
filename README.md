# Null safety with free and committed types #

This is a machine-checkable proof of soundness of null safety based on free and committed types formalized in Isabelle/HOL.

Free and committed types is a technique that guarantees absence of null dereferencing in Java/C#-like object-oriented languages (the theory is provided only for a significantly reduced subset, involving key constructs). Main publication [Freedom before commitment: a lightweight type system for object initialisation](https://dl.acm.org/doi/10.1145/2076021.2048142) refers to an extended version with a formal proof on paper in report [Freedom Before Commitment:
Simple Flexible Initialisation for Non-Null Types](https://doi.org/10.3929/ethz-a-006904372). However, the theories in the publications differ: the report adds a notion of generic expectation variables. The main publication does not provide a formal proof for the simpler version of the type system.

## Installation ##

1. The theories are checked against [Isabelle 2020](https://isabelle.in.tum.de/installation.html).
2. Source code can be downloaded or checked out depending on your needs
	* Latest stable version: branch `stable`
	* Specific stable version: specific tag, such as `1.0.0`
3. All theories can be built with _Isabelle_ using session `Free_and_Committed_Types` specified in a file `ROOT` in the root directory of the repository.

## For developers ##

Source tree follows [Anti-gitflow](http://endoflineblog.com/gitflow-considered-harmful) pattern when all changes go to a branch `master`. If you need a stable version, checkout or download a specific tag, not `HEAD` from `master`. Alternatively you can checkout or download the latest stable version using a branch `stable`. As a consequence of this policy you should not checkout `master` for evaluation or any other use, it is meant only for development and may contain broken code.

## License ##

[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Related resources ##

* [Void Safety](https://bitbucket.org/kwaxer/void_safety/): Machine-checkable proofs of void safety formalized in Isabelle/HOL.
* [Null Safety Benchmark](https://bitbucket.org/kwaxer/null-safety-benchmark/): Examples to test and compare different null safety solutions for their soundness and expressiveness.

## Revision history ##

[1.0.22](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.22)

* Proved goal 13 in Lemma 1 for call (all cases and goals have been proved).
* Provided proofs for set-based versions of Lemma 5 using relation-based versions.

[1.0.21](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.21)

* Proved goal 20 in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 13.

[1.0.20](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.20)

* Proved goal 21 in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 13, 20.

[1.0.19](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.19)

* Proved goal 19 in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 13, 20–21.

[1.0.18](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.18)

* Proved goal 22 in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 13, 19–21.

[1.0.17](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.17)

* Proved goal 12 in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 13, 19–22.

[1.0.16](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.16)

* Updated rules for method signatures:
	* changed variance of target and arguments to be contravariant as in the original paper and report;
	* added a restriction that result does not appear in argument and local list.
* Changed definition of `instance` and `instances` to treat result type outside of the type list to allow for mapping result generic initialization variable (if any) to "unclassified" bypassing a restriction imposed on argument generic initialization variables (and to allow for defining an identity function).
* Reviewed all conclusions of lemma 3(4) to follow the intention of the report and to make them correct and usable.
* Proved some goals in Lemma 1 for call.
	* Remaining cases and goals of Lemma 1:
		* Remaining goals for call: 12, 13, 19–22.

[1.0.15](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.15)

* Proved goal 13 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.

[1.0.14](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.14)

* Proved goal 20 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 13.

[1.0.13](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.13)

* Proved goal 21 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 13, 20.

[1.0.12](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.12)

* Proved goal 19 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 13, 20–21.

[1.0.11](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.11)

* Proved goal 22 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 13, 19–21.

[1.0.10](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.10)

* Proved goal 12 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 13, 19–22.

[1.0.9](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.9)

* Fixed typos in comments that caused errors when building a theory document.
* Replaced proofs using `smt` with proofs without it to ensure stability of proof checks and to (re)enable theory document generation.
* Proved goal 22 in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 12, 13, 19–22.

[1.0.8](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.8)

* Added additional lemmas for:
	* function update lists (they are similar to a function update, but deal with multiple updates at the same time);
	* monotonicity and subtyping;
	* compile-time and runtime typing of variable lists and updates based on them;
	* operations involving initialization instances;
	* properties of a heap after allocating a new object;
	* a transitive closure and a trace with a reachable element in it.
* Replaced `foldr_conj_true` with a stronger lemma `foldr_conj_map`.
* Restricted existing rules:
	* `this` is not present in locals and arguments;
	* all locals and arguments are distinct;
	* statement typing uses `instances` instead of `instace` to rely on a minimal instance as required by the updated well-formedness rules (it might be possible to relax the requirement later by computing a minimal instance from a given one and proving that they are identical on all suitable signatures);
	* the statement comprising a method or constructor body is well-formed.
* Fixed typos in the rules:
	* corrected typing for call and create instructions;
	* taken into account that well-formedness concerns all possible substitutions, not just a single one.
* Changed notation:
	* supported typing notation for lists;
	* avoided conflicts by using different symbols;
	* introduced terser notation for good configuration clauses.
* Introduced locales with well-formed methods and constructors.
* Introduced notions of unbounded memory and heap that are required to ensure that the heap locale does not lead to a contradiction (the heap parameter should satisfy the condition of unboundedness, otherwise the allocation assumption can be used to derive False).
* Extended prerequisites of lemma 1 to support object allocation by requiring that
	* the heap is unbounded;
	* the heap is well-formed with respect to its image.
* Made lemmas to be closer to the original by using selectors.
* Made theorem 1 a corollary of lemma 1.
* Proved some goals in Lemma 1 for create.
	* Remaining cases and goals of Lemma 1:
		* Remaining case: call.
		* Remaining goals for create: 12, 13, 17, 19–22.

[1.0.7](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.7)

* Proved goal 22 in Lemma 1 for all current cases.
	* Remaining cases of Lemma 1: call, create.

[1.0.6](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.6)

* Proved goal 21 in Lemma 1 for all current cases by adding a more general goal 21'.
	* Remaining cases and goals of Lemma 1:
		* Remaining cases: call, create.
		* Remaining goals: 22.

[1.0.5](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.5)

* Proved goal 20 in Lemma 1 for all current cases by adding a more general goal 20'.
	* Remaining cases and goals of Lemma 1:
		* Remaining cases: call, create.
		* Remaining goals: 21–22.

[1.0.4](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.4)

* Proved goal 19 in Lemma 1 for all current cases.
	* Remaining cases and goals of Lemma 1:
		* Remaining cases: call, create.
		* Remaining goals: 20–22.
* Added copyright notes.

[1.0.3](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.3)

* Added resources and a script to generate PDF from the theories.

[1.0.2](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.2)

* Proved all current goals in Lemma 1 for cast instruction.
	* Remaining cases and goals of Lemma 1:
		* Remaining cases: call, create.
		* Remaining goals: 19–22.

[1.0.1](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.1)

* Proved goal 13 in Lemma 1 for fldAss.
	* Remaining cases and goals of Lemma 1:
		* Remaining cases: call, create, cast.
		* Remaining goals: 19–22.

[1.0.0](https://bitbucket.org/kwaxer/free-and-committed-types/commits/tag/1.0.0)

* Initial import:
	* All notions are rules are formalized.
	* All lemmas are proved except for *Lemma 1 (Preservation and Safety (stregthened))* and *Theorem 1 (Preservation and Safety)*.
	* Lemma 1 is extended with the following assumptions (on which the proof on paper relies imlicitly):
		* *wf_heap_dom h*: mentioned in the paper in a weaker form without any proofs;
		* *wf_statement s*: mentioned in the paper in the form of syntax restrictions.
	* The following cases and goals are proved in Lemma 1:
		* Cases: varAss, varAssBad, fldAss, fldAssBad, seq, seqBad.
		* Goals: 9–12, 14–18, additional *¬ nullable (Γ this)*, *this ∈ Δ'*, *∀ f ∈ Σ. f ∈ fields (Γ this)*, *wf_heap_dom h'*.
