(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 5 (Heap Update Properties)\<close>

theory Update_Set imports Update Reachable_Set begin

lemma (in heap) lemma_5\<^sub>1:
  assumes
    E: "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" and
    W: "wf_heap_dom h"
  shows
    "reachable\<^sub>v (\<chi>\<^sub>v h) v \<subseteq> reachable\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x))"
proof (cases v)
  case (non_null \<iota>\<^sub>v)
  from E have
    C: "(case the (\<sigma> x) of
         non_null \<iota> \<Rightarrow>
            if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))
            then Inl (the (\<chi>\<^sub>v h (\<iota>, f)))
            else Inr derefExc |
         _ \<Rightarrow> Inr derefExc) = Inl v" by simp
  then obtain \<iota> where
    N: "the (\<sigma> x) = non_null \<iota>" by (cases "the (\<sigma> x)") simp_all
  with C have
    I: "(if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))
        then Inl (the (\<chi>\<^sub>v h (\<iota>, f)))
        else Inr derefExc) = Inl v" by simp
  then have
    T: "\<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))" by (meson Inr_Inl_False)
  with W have
    "(\<iota>, f) \<in> dom (\<chi>\<^sub>v h)" using access_dom by fastforce
  with I T have
    "\<chi>\<^sub>v h (\<iota>, f) = Some v" by auto
  then have
    "\<exists>f. \<chi>\<^sub>v h (\<iota>, f) = Some v" by blast
  with non_null have
    "the (\<sigma> x) = value.non_null \<iota> \<Longrightarrow>
        case the (\<sigma> x) of
          non_null \<iota>\<^sub>1 \<Rightarrow> case v of
            non_null \<iota>\<^sub>2 \<Rightarrow> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 |
            null \<Rightarrow> False |
          null \<Rightarrow> False" unfolding reaches\<^sub>1_def reaches_def reaches_def
      by (simp add: r_into_rtranclp)
    with N have "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) v " unfolding reaches\<^sub>v_def by simp
  then show ?thesis using reachable\<^sub>v_reaches\<^sub>v reachable\<^sub>v_path by auto
next
  case null
  then show ?thesis by simp
qed

lemma (in heap) lemma_5\<^sub>2:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> \<iota>':: "'a \<iota>"
  assumes
    "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)"
    "\<iota> \<in> reachable h\<^sub>v \<iota>'"
  shows
    "reachable h\<^sub>v' \<iota>' \<subseteq> reachable\<^sub>v\<^sub>\<iota> h\<^sub>v v \<union> reachable h\<^sub>v \<iota>'"
proof -
  from assms(2) have
    "reaches h\<^sub>v \<iota>' \<iota>" by simp
  with assms(1) have
    "\<forall> \<iota>''. reaches h\<^sub>v' \<iota>' \<iota>'' \<longrightarrow> reaches\<^sub>v h\<^sub>v v (non_null \<iota>'') \<or> reaches h\<^sub>v \<iota>' \<iota>''"
    using lemma_5\<^sub>2 by blast
  then have
    "\<forall> \<iota>''. reaches h\<^sub>v' \<iota>' \<iota>'' \<longrightarrow> reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<iota>'' \<or> reaches h\<^sub>v \<iota>' \<iota>''"
    using reaches\<^sub>v\<^sub>\<iota>_left_non_null reaches\<^sub>v_reaches by (metis value.collapse value.sel)
  then show ?thesis using reachable\<^sub>v\<^sub>\<iota>_reaches\<^sub>v\<^sub>\<iota> reachable\<^sub>v_reaches\<^sub>v
    by (simp add: Collect_mono_iff Un_def)
qed

lemma (in heap) lemma_5\<^sub>2\<^sub>v:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> \<iota>':: "'a \<iota>"
  assumes
    "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)"
    "reaches\<^sub>v h\<^sub>v v' (non_null \<iota>)"
  shows
    "reachable\<^sub>v h\<^sub>v' v' \<subseteq> reachable\<^sub>v h\<^sub>v v \<union> reachable\<^sub>v h\<^sub>v v'"
  using assms using reaches\<^sub>v_reachable\<^sub>v_iff lemma_5\<^sub>2\<^sub>v by fastforce

lemma (in heap) lemma_5\<^sub>3:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> \<iota>':: "'a \<iota>"
  assumes
    "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)"
    "\<iota> \<notin> reachable h\<^sub>v \<iota>'"
  shows
    "reachable h\<^sub>v' \<iota>' = reachable h\<^sub>v \<iota>'"
  using assms reachable_upd_other by auto

lemma (in heap) lemma_5\<^sub>4:
  fixes
    h h':: "('a, 'C) h" and \<iota>':: "'a \<iota>"
  assumes
    "unbounded_heap h" and
    "(h', \<iota>') = alloc h C" and
    "wf_heap_dom_weak h" and
    "wf_heap_image h"
  shows
    "\<forall> \<iota>. \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow> \<iota>' \<notin> reachable (\<chi>\<^sub>v h') \<iota> \<and> \<iota> \<notin> reachable (\<chi>\<^sub>v h') \<iota>'"
  using assms lemma_5\<^sub>4 by auto

end