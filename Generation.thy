(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 2 (Generation Lemmas)\<close>

theory Generation imports Heap Typing begin

lemma (in lookup) lemma_2\<^sub>1:
  assumes
    E: "\<Gamma>, \<Delta> \<turnstile> (e::e) : T" and
    S: "T \<sqsubseteq> (C, \<zero>, n)"
  shows
    "free T" and
    "e = Null \<or> (\<exists> x. e = \<V> x \<and> (free (the (\<Gamma> x))))"
proof -
  obtain D k m where
    T: "T = (D, k, m)" using prod_cases3 by blast
  with S show "free T" by (cases k, simp_all add: leq_prod_def)
  moreover with E S T show "e = Null \<or> (\<exists> x. e = \<V> x \<and> (free (the (\<Gamma> x))))"
    by (induction rule:expression_typing.induct)
    (simp, simp, metis initialization k.distinct(1) k.distinct(3))
qed

lemma (in lookup) lemma_2\<^sub>1_vars:
  assumes
    "\<Gamma>, \<Delta> \<turnstile> (xs::x list) : Ts" and
    "Ts \<sqsubseteq> Ts'" and
    "\<forall> T \<in> set Ts'. initialization T \<noteq> \<diamondop>"
  shows
    "map (initialization o the o \<Gamma>) xs = map initialization Ts'"
  using assms initializations_classified_subtyping variables_typing_type
  by (metis (no_types) comp_assoc map_map)

lemma (in lookup) lemma_2\<^sub>2':
  assumes
    "\<Gamma>, \<Delta> \<turnstile> (e::e) : T" and
    "T \<sqsubseteq> (C, \<one>, n)"
  shows
    "committed T"
  using assms
  by (induction rule:expression_typing.induct, auto simp add: leq_prod_def)

lemma (in lookup) lemma_2\<^sub>2'':
  assumes
    E: "\<Gamma>, \<Delta> \<turnstile> (e::e) : T" and
    S: "T \<sqsubseteq> (C, \<one>, n)"
  shows
    "e = Null \<or> (\<exists> x. (e = \<V> x \<or> (\<exists> f. e = x\<^bsub>\<bullet>\<^esub>f)) \<and> committed (the (\<Gamma> x)))"
  using assms lemma_2\<^sub>2'
  by (induction arbitrary: C n rule:expression_typing.inducts) (fastforce simp add: leq_prod_def)+

lemma (in lookup) lemma_2\<^sub>3':
  fixes
    T :: "'C T"
  assumes
    "T \<sqsubseteq> (C, k, !)"
  shows
    "\<not> nullable T"
  using assms by (simp add: leq_prod_def)

lemma (in lookup) lemma_2\<^sub>3'':
  fixes
    e :: "e" and T :: "'C T" and \<Gamma> :: "'C \<Gamma>"
  assumes
    E: "\<Gamma>, \<Delta> \<turnstile> e : T" and
    S: "T \<sqsubseteq> (C, k, !)"
  shows
    "\<exists> x. (e = \<V> x \<or> (\<exists> f. e = x\<^bsub>\<bullet>\<^esub>f)) \<and> \<not> nullable (the (\<Gamma> x))"
  using assms lemma_2\<^sub>3'
  by (induction arbitrary: C k rule:expression_typing.inducts) fastforce+

lemma (in lookup) lemma_2\<^sub>4':
  fixes
    T :: "'C T" and \<Gamma> :: "'C \<Gamma>"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> \<V> x : T"
  shows
    "T = the (\<Gamma> x)"
proof -
  from assms obtain e where
    "\<Gamma>, \<Delta> \<turnstile> e : T" and
    "e = \<V> x" by simp
  then have
    "Some T = \<Gamma> x" by (induction arbitrary: x rule:expression_typing.inducts) auto
  then show ?thesis by (cases "\<Gamma> x") simp_all
qed

lemma (in lookup) lemma_2\<^sub>4'':
  fixes
    T :: "'C T" and \<Gamma> :: "'C \<Gamma>"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> \<V> x : T"
  shows
    "nullable T \<or> x \<in> \<Delta>"
proof -
  obtain e where
    E: "e = \<V> x" by simp
  with assms show ?thesis
    by (induction rule:expression_typing.induct, simp_all add: E)
qed

lemma (in lookup) lemma_2\<^sub>4:
  fixes
    T :: "'C T" and \<Gamma> :: "'C \<Gamma>"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> \<V> x : T"
  shows
    "T = the (\<Gamma> x)" and
    "nullable T \<or> x \<in> \<Delta>"
  using assms lemma_2\<^sub>4' lemma_2\<^sub>4'' by auto

lemma (in lookup) lemma_2\<^sub>5:
  fixes
    T :: "'C T" and \<Gamma> :: "'C \<Gamma>"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> x\<^bsub>\<bullet>\<^esub>f : T"
  shows
    "x \<in> dom \<Gamma>"
proof -
  obtain e where
    E: "e = x\<^bsub>\<bullet>\<^esub>f" by simp
  with assms show ?thesis
    by (induction rule:expression_typing.induct) (auto simp add: E)
qed

end