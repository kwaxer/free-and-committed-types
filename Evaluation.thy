(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 9 (Expression evaluation)\<close>

theory Evaluation imports Expression Heap begin

type_synonym 'a lookup = "x \<rightharpoonup> 'a v"
type_synonym 'a \<sigma> = "'a lookup"

datatype derefExc = derefExc

type_synonym 'a extended_value = "'a v + derefExc"
type_synonym 'a V = "'a extended_value"

no_syntax "_record_update" :: "'a => field_updates => 'b"("_/(3\<lparr>_\<rparr>)" [900, 0] 900)

fun (in heap) expression_evaluation :: "e \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> 'a extended_value" ("\<lfloor>_\<rfloor>\<^bsub>_,_\<^esub>")
  where
    "\<lfloor>\<V> x\<rfloor>\<^bsub>h, \<sigma>\<^esub> = Inl (the (\<sigma> x))" |
    "\<lfloor>Null\<rfloor>\<^bsub>h, \<sigma>\<^esub> = Inl value.null" |
    "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h, \<sigma>\<^esub> = (case the (\<sigma> x) of
       non_null \<iota> \<Rightarrow>
          if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))
          then Inl (the (\<chi>\<^sub>v h (\<iota>, f)))
          else Inr derefExc |
       _ \<Rightarrow> Inr derefExc)"
(*
    "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h, \<sigma>\<^esub> = (case \<sigma> x of
      Some (non_null \<iota>) \<Rightarrow> (case cls h \<iota> of
        Some C \<Rightarrow> (if (f \<in> set (fields C))
          then (case \<chi> h \<iota> f of
            Some v \<Rightarrow> Inl v |
            _ \<Rightarrow> Inr derefExc)
          else Inr derefExc) |
        _ \<Rightarrow> Inr derefExc) |
      _ \<Rightarrow> Inr derefExc)"
*)

definition (in heap) expressions_evaluation :: "e list \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> 'a extended_value list" ("\<lfloor>_\<rfloor>\<^bsub>_,_\<^esub>")
  where
    [simp]: "\<lfloor>e\<^sub>i\<rfloor>\<^bsub>h, \<sigma>\<^esub> = map (\<lambda> e. \<lfloor>e\<rfloor>\<^bsub>h, \<sigma>\<^esub>) e\<^sub>i"

lemma (in heap) evaluation_dereference:
  assumes
    "\<lfloor>e\<rfloor>\<^bsub>h, \<sigma>\<^esub> = Inr derefExc"
  shows
    "\<exists> x f. (e = x \<^bsub>\<bullet>\<^esub> f) \<and>
    (the (\<sigma> x) = null \<or>
     ref (the (\<sigma> x)) \<notin> dom (\<chi>\<^sub>c h) \<or>
     f \<notin> set (fields (the (cls h (ref (the (\<sigma> x)))))))"
  using assms
proof (cases e)
  case (Field_access x f)
  have
    "the (\<sigma> x) = null \<or>
    ref (the (\<sigma> x)) \<notin> dom (\<chi>\<^sub>c h) \<or>
    f \<notin> set (fields (the (cls h (ref (the (\<sigma> x))))))"
  proof (cases "the (\<sigma> x)")
    case (non_null \<iota>)
    then show ?thesis
    proof (cases "ref (the (\<sigma> x)) \<notin> dom (\<chi>\<^sub>c h)")
      case False
      with assms show ?thesis using Field_access non_null by auto
    qed simp
  qed simp
  then show ?thesis using Field_access by simp
qed simp_all

subsubsection \<open>Empty lookup\<close>

definition "empty_lookup" :: "'a \<sigma>"
  where
    "empty_lookup \<equiv> \<lambda> _. None"
declare "empty_lookup_def" [simp]
notation empty_lookup ("\<sigma>\<^sub>\<emptyset>")

lemma empty_lookup_dom: "dom \<sigma>\<^sub>\<emptyset> = {}"
  by simp

end