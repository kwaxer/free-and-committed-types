(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 11 (Reachability)\<close>

text \<open>This section defines sets of reachable elements as specified in the original technical report.
But these definitions are used only to demonstrate their equivalence to the corresponding predicates
that are used in theories instead.\<close>

theory Reachable_Set imports Reachability begin

definition (in heap) reachable :: "'a h\<^sub>v \<Rightarrow> 'a \<iota> \<Rightarrow> 'a \<iota> set"
  where
    [iff]: "reachable h\<^sub>v \<iota> \<equiv> {\<iota>'. reaches h\<^sub>v \<iota> \<iota>'}"

lemma (in heap) reaches_reachable_iff: "reaches h\<^sub>v \<iota> \<iota>' \<longleftrightarrow> \<iota>' \<in> reachable h\<^sub>v \<iota>"
  by simp

lemma (in heap) self_reachable: "\<iota> \<in> reachable h \<iota>"
  by simp

lemma (in heap) reachable_path: "reaches h \<iota> \<iota>' \<Longrightarrow> reachable h \<iota>' \<subseteq> reachable h \<iota>"
  by auto

lemma (in heap) reachable_element: "\<iota>' \<in> reachable h \<iota> \<Longrightarrow> reachable h \<iota>' \<subseteq> reachable h \<iota>"
  by auto

lemma (in heap) reachable_upd_other:
  assumes
    "\<not> reaches h \<iota> \<iota>'"
  shows
    "reachable (h ((\<iota>', f) := v)) \<iota> = reachable h \<iota>"
  using assms reaches_upd_other by auto

definition (in heap) reachable\<^sub>v :: "'a h\<^sub>v \<Rightarrow> 'a v \<Rightarrow> 'a v set"
  where
    [iff]: "reachable\<^sub>v h v \<equiv> (case v of
      null \<Rightarrow> {} |
      non_null \<iota> \<Rightarrow> {non_null \<iota>' | \<iota>'. reaches h \<iota> \<iota>'})"

lemma (in heap) reachable\<^sub>v_reaches\<^sub>v: "reachable\<^sub>v h v = {v'. reaches\<^sub>v h v v'}"
proof (cases v)
  case (non_null \<iota>)
  moreover have "{non_null \<iota>' | \<iota>'. reaches h \<iota> \<iota>'} = {v'. (case v' of
        null \<Rightarrow> False |
        non_null \<iota>\<^sub>2 \<Rightarrow> reaches h \<iota> \<iota>\<^sub>2)}"
  proof rule
    show "{value.non_null \<iota>' |\<iota>'. reaches h \<iota> \<iota>'} \<subseteq>
      {v'. case v' of value.non_null \<iota>\<^sub>2 \<Rightarrow> reaches h \<iota> \<iota>\<^sub>2 | null \<Rightarrow> False}" by auto
  next
    show "{v'. case v' of value.non_null \<iota>\<^sub>2 \<Rightarrow> reaches h \<iota> \<iota>\<^sub>2 | null \<Rightarrow> False} \<subseteq>
      {value.non_null \<iota>' |\<iota>'. reaches h \<iota> \<iota>'}"
    proof
      fix x
      assume
        "x \<in> {v'. case v' of value.non_null \<iota>\<^sub>2 \<Rightarrow> reaches h \<iota> \<iota>\<^sub>2 | null \<Rightarrow> False}"
      then show "x \<in> {value.non_null \<iota>' |\<iota>'. reaches h \<iota> \<iota>'}"
        by (cases x) simp_all
    qed
  qed
  ultimately show ?thesis using reaches\<^sub>v_def by (simp add: non_null)
next
  case null
  then show ?thesis using reaches\<^sub>v_def by simp
qed

lemma (in heap) reaches\<^sub>v_reachable\<^sub>v_iff: "reaches\<^sub>v h v v' \<longleftrightarrow> v' \<in> reachable\<^sub>v h v"
  using reachable\<^sub>v_reaches\<^sub>v by simp

lemma (in heap) reachable\<^sub>v_null: "reachable\<^sub>v h null = {}"
  by simp

lemma (in heap) reachable\<^sub>v_non_null: "reachable\<^sub>v h (non_null \<iota>) = {non_null \<iota>' | \<iota>'. \<iota>' \<in> reachable h \<iota>}"
  by simp

lemma (in heap) reachable\<^sub>v_path: "v' \<in> reachable\<^sub>v h v \<Longrightarrow> reachable\<^sub>v h v' \<subseteq> reachable\<^sub>v h v"
  by (cases v) auto

definition (in heap) reaches\<^sub>v\<^sub>\<iota> :: "'a h\<^sub>v \<Rightarrow> 'a v \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
    [iff]: "reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<equiv> (case v of
            non_null \<iota> \<Rightarrow> reaches h\<^sub>v \<iota> |
            null \<Rightarrow> \<lambda> \<iota>. False)"

lemma (in heap) "reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<iota> = reaches\<^sub>v h\<^sub>v v (non_null \<iota>)"
  using reaches\<^sub>v_def by (simp add: value.case_eq_if)

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_left_null: "\<not> reaches\<^sub>v\<^sub>\<iota> h null v"
  by simp

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_left_non_null: "reaches\<^sub>v\<^sub>\<iota> h\<^sub>v (non_null \<iota>) = reaches h\<^sub>v \<iota>"
  by simp

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_upd_other:
  assumes
    "\<not> reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<iota>"
  shows
    "reaches\<^sub>v\<^sub>\<iota> (h\<^sub>v ((\<iota>, f) := v')) v \<iota>' = reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<iota>'"
  using assms reaches_upd_other by (cases v) simp_all

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_source_dom\<^sub>v:
  assumes
    "reaches\<^sub>v\<^sub>\<iota> h\<^sub>v v \<iota>"
    "v \<noteq> non_null \<iota>"
  obtains f
  where
    "(ref v, f) \<in> dom h\<^sub>v"
  using assms unfolding reaches\<^sub>v\<^sub>\<iota>_def using reaches_source_dom\<^sub>v
  by (metis value.case_eq_if value.discI value.expand value.sel)

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_source_dom:
  assumes
    "reaches\<^sub>v\<^sub>\<iota> (\<chi>\<^sub>v h) v \<iota>"
    "v \<noteq> non_null \<iota>"
    "wf_heap_dom h"
  shows
    "ref v \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>v_def using reaches_source_dom by (cases v) simp_all

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_target_dom:
  assumes
    "reaches\<^sub>v\<^sub>\<iota> (\<chi>\<^sub>v h) v \<iota>"
    "ref v \<in> dom (\<chi>\<^sub>c h)"
    "wf_heap_image h"
  shows
    "\<iota> \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>v_def using reaches_target_dom by (cases v) simp_all

definition (in heap) reachable\<^sub>v\<^sub>\<iota> :: "'a h\<^sub>v \<Rightarrow> 'a v \<Rightarrow> 'a \<iota> set"
  where
    [iff]: "reachable\<^sub>v\<^sub>\<iota> h\<^sub>v v \<equiv> (case v of
      null \<Rightarrow> {} |
      non_null \<iota> \<Rightarrow> reachable h\<^sub>v \<iota>)"

lemma (in heap) reachable\<^sub>v\<^sub>\<iota>_null: "reachable\<^sub>v\<^sub>\<iota> h null = {}"
  by simp

lemma (in heap) reachable\<^sub>v\<^sub>\<iota>_non_null: "reachable\<^sub>v\<^sub>\<iota> h (non_null \<iota>) = reachable h \<iota>"
  by simp

lemma (in heap) reachable\<^sub>v\<^sub>\<iota>_reachable\<^sub>v: "reachable\<^sub>v\<^sub>\<iota> h v = {\<iota>. non_null \<iota> \<in> reachable\<^sub>v h v}"
  by (cases v) simp_all

lemma (in heap) reachable\<^sub>v\<^sub>\<iota>_reaches\<^sub>v\<^sub>\<iota>: "reachable\<^sub>v\<^sub>\<iota> h v = {\<iota>. reaches\<^sub>v\<^sub>\<iota> h v \<iota>}"
  unfolding reachable\<^sub>v\<^sub>\<iota>_def reachable_def reaches\<^sub>v_def by (cases v) auto

lemma (in heap) reaches\<^sub>v\<^sub>\<iota>_reachable\<^sub>v\<^sub>\<iota>_iff: "reaches\<^sub>v\<^sub>\<iota> h v \<iota> \<longleftrightarrow> \<iota> \<in> reachable\<^sub>v\<^sub>\<iota> h v"
  using reachable\<^sub>v\<^sub>\<iota>_reaches\<^sub>v\<^sub>\<iota> by simp

lemma (in heap) reachable\<^sub>v\<^sub>\<iota>_upd_other:
  assumes
    "\<iota>' \<notin> reachable\<^sub>v\<^sub>\<iota> h v"
  shows
    "reachable\<^sub>v\<^sub>\<iota> (h ((\<iota>', f) := v')) v = reachable\<^sub>v\<^sub>\<iota> h v"
  using assms unfolding reachable\<^sub>v\<^sub>\<iota>_def using reachable_upd_other by (cases v) simp_all

end