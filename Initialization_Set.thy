(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 11 (Initialisation)\<close>

theory Initialization_Set imports Initialization Reachable_Set begin

definition (in runtime) deep_init' :: "('a, 'C) h \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
    [iff]: "deep_init' h \<iota> \<equiv> \<forall> \<iota>' \<in> (reachable (\<chi>\<^sub>v h) \<iota>). init h \<iota>'"

lemma (in runtime) deep_init_eq: "deep_init = deep_init'"
  unfolding deep_init_def deep_init'_def by simp

definition (in runtime) deep_init\<^sub>v' :: "('a, 'C) h \<Rightarrow> 'a v \<Rightarrow> bool"
  where
    [iff]: "deep_init\<^sub>v' h v \<equiv> \<forall> \<iota>' \<in> reachable\<^sub>v\<^sub>\<iota> (\<chi>\<^sub>v h) v. init h \<iota>'"

lemma (in runtime) deep_init\<^sub>v_eq: "deep_init\<^sub>v = deep_init\<^sub>v'"
  unfolding deep_init\<^sub>v_def deep_init\<^sub>v'_def
  by (rule, rule) (metis deep_init\<^sub>v_def deep_init\<^sub>v_non_null deep_init_def empty_iff reachable\<^sub>v\<^sub>\<iota>_non_null reachable\<^sub>v\<^sub>\<iota>_null reaches\<^sub>v_reaches reaches_reachable_iff value.collapse)

lemma (in runtime) deep_init\<^sub>v: "deep_init\<^sub>v h v = (\<forall> \<iota>' \<in> reachable\<^sub>v\<^sub>\<iota> (\<chi>\<^sub>v h) v. init h \<iota>')"
  by (simp add: deep_init\<^sub>v_eq)

lemma (in runtime) reachable_deep_init:
  assumes
    "\<iota>' \<in> reachable (\<chi>\<^sub>v h) \<iota>"
    "deep_init h \<iota>"
  shows
    "deep_init h \<iota>'"
  using assms reaches_deep_init reaches_reachable_iff by blast

lemma (in runtime) reachable_deep_init\<^sub>v:
  assumes
    "v' \<in> reachable\<^sub>v (\<chi>\<^sub>v h) v"
    "deep_init\<^sub>v h v"
  shows
    "deep_init\<^sub>v h v'"
  using assms reaches\<^sub>v_reachable\<^sub>v_iff reaches_deep_init\<^sub>v by blast

lemma (in runtime) deep_init\<^sub>v_upd_unreachable:
  assumes
    "deep_init\<^sub>v h v"
    "\<iota>' \<notin> reachable\<^sub>v\<^sub>\<iota> (\<chi>\<^sub>v h) v"
  shows
    "deep_init\<^sub>v ((\<chi>\<^sub>v h) ((\<iota>', f) := v'), (\<chi>\<^sub>c h)) v"
  by (metis assms deep_init\<^sub>v_upd_other reachable\<^sub>v\<^sub>\<iota>_non_null reaches\<^sub>v_reaches reaches_reachable_iff value.collapse value.sel)

end