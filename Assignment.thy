(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 4 (Runtime Type Assignment Properties)\<close>

theory Assignment imports Configuration Semantics begin

lemma (in heap) lemma_4\<^sub>1:
  fixes
    t\<^sub>1 t\<^sub>2:: "'C t"
  assumes
    "h \<turnstile> v : t\<^sub>1" and
    "t\<^sub>1 \<sqsubseteq> t\<^sub>2"
  shows
    "h \<turnstile> v : t\<^sub>2"
  using assms runtime_type_assignment_leq by blast

lemma (in heap) lemma_4\<^sub>1_list:
  fixes
    t\<^sub>1 t\<^sub>2:: "'C t list"
  assumes
    "h \<turnstile> v : t\<^sub>1" and
    "t\<^sub>1 \<sqsubseteq> t\<^sub>2"
  shows
    "h \<turnstile> v : t\<^sub>2"
proof -
  obtain X Y where
    X: "X = zip v t\<^sub>1" and
    Y: "Y = zip v t\<^sub>2" by simp
  from assms(1) have
    Lv: "length v = length t\<^sub>1" using runtime_type_assignments_length by blast
  with X assms(1) have
    H: "h \<turnstile> map fst X : map snd X" by simp
  from assms(2) have
    L: "length t\<^sub>1 = length t\<^sub>2" by (simp add: list_leq_length)
  with X Y Lv assms have
    "map fst X = map fst Y" and
    "map snd X \<sqsubseteq> map snd Y" by simp_all
  with H have
    "h \<turnstile> map fst Y : map snd Y"
  proof (induction X arbitrary: Y)
    case Nil
    then show ?case by simp
  next
    case (Cons x X)
    then obtain y Y' where
      Y: "Y = y # Y'" by auto
    with Cons.prems(2) have
      f: "fst x = fst y" by simp
    from Cons.prems(3) Y have
      M: "map snd X \<sqsubseteq> map snd Y'" unfolding list_leq_def by simp
    from Cons.prems(1) have
      "h \<turnstile> map fst X : map snd X" by (simp add: runtime_type_assignments_tail)
    with Cons.IH Y Cons.prems(2) M have
      hY: "h \<turnstile> map fst Y' : map snd Y'" by simp
    from Cons.prems(1) have
      "h \<turnstile> fst x : snd x" by (simp add: runtime_type_assignments_head)
    with f have
      "h \<turnstile> fst y : snd x" by simp
    moreover from Cons.prems(3) Y have
      "snd x \<sqsubseteq> snd y" unfolding list_leq_def by simp
    ultimately have
      "h \<turnstile> fst y : snd y" using lemma_4\<^sub>1 by blast 
    with hY Y show ?case using runtime_type_assignments_cons by simp 
  qed
  with L Lv Y show ?thesis by simp
qed


(*
proof (cases rule: runtime_type_assignment.cases)
  case (RNull D)
  then show ?thesis
    using assms(2) runtime_type_assignment_leq by blast
  then have
    D: "D = simple_base t\<^sub>1" by (simp add: simple_base_def) 
  from RNull have "simple_nullity t\<^sub>2 = ?" using assms(2) simple_nullity_subtyping_null by blast
  moreover obtain C where
    C: "C = simple_base t\<^sub>2" by simp
  ultimately have "t\<^sub>2 = (C, ?)" using simple_type by metis
  moreover from C D have "D \<sqsubseteq> C" using simple_base_subtyping assms(2) by simp
  ultimately show ?thesis
    by (simp add: RNull(1) runtime_type_assignment.RNull)
next
  case (RAddr \<iota> C n)
  then show ?thesis
    using assms(2) runtime_type_assignment_leq by blast
(*
  case (RAddr \<iota> A C\<^sub>1 n\<^sub>1)
  then have
    C\<^sub>1: "C\<^sub>1 = simple_base t\<^sub>1" and 
    N\<^sub>1: "n\<^sub>1 = simple_nullity t\<^sub>1" by (simp_all add: simple_base_def simple_nullity_def)
  moreover obtain C\<^sub>2 n\<^sub>2 where
    C\<^sub>2: "C\<^sub>2 = simple_base t\<^sub>2" and 
    N\<^sub>2: "n\<^sub>2 = simple_nullity t\<^sub>2" by (simp_all add: simple_base_def simple_nullity_def)
  ultimately have
    "C\<^sub>1 \<sqsubseteq> C\<^sub>2" and
    "n\<^sub>1 \<le> n\<^sub>2" using assms(2) by (simp_all only: simple_base_subtyping simple_nullity_subtyping)
  then have "A \<sqsubseteq> C\<^sub>2" using leq_trans local.RAddr(4) by blast
  with RAddr C\<^sub>2 show ?thesis using simple_type runtime_type_assignment.RAddr by metis
*)
qed
*)

lemma (in heap) lemma_4\<^sub>2:
  assumes
    "h \<turnstile> v : (C, n)"
  shows
    "v = null \<or> the (cls h (ref v))  \<sqsubseteq> C"
  using assms by (cases v) auto

(*
lemma (in heap) lemma_4\<^sub>2:
  assumes
    "h \<turnstile> v : (C, n)"
  shows
    "v = null \<or> (\<exists> \<iota> D.(v = non_null \<iota> \<and> cls h \<iota> = Some D \<and> D \<sqsubseteq> C))"
  using assms
  by (cases rule: runtime_type_assignment.cases) simp_all
*)

lemma (in runtime) lemma_4\<^sub>3:
  assumes
    "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" and
    "\<Gamma>, \<Delta> \<turnstile> e: (C, k, n)" and
    "wf_heap_dom h"
  obtains
    v
  where
    "\<lfloor>e\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" and
    "h \<turnstile> v : (C, n)"
proof -
  obtain T where
    T: "T = (C, k, n)" by simp
  with assms(2) have "\<Gamma>, \<Delta> \<turnstile> e: T" by simp
  then obtain
    v
  where
    "\<lfloor>e\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" and
    "h \<turnstile> v : (C, n)"
  using assms(1) T
  proof (induction arbitrary: C k n rule: expression_typing.induct)
    case (Tvar x \<Gamma> T \<Delta>)
    have "dom \<sigma> = dom \<Gamma>"
      using Tvar.prems(2) by auto
    with Tvar.hyps(1) obtain v where
      V: "\<sigma> x = Some v" by auto
    then have
      "\<lfloor>\<V> x\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" by simp
    moreover from "Tvar.prems"(2) have
      "\<Gamma> \<turnstile>\<^sub>4 h, \<sigma>" by simp
    have
      "h \<turnstile> v : (C, n)" using Tvar.prems(3) V c\<^sub>4_def Tvar.hyps Tvar.prems(2) c\<^sub>3_def good_configuration_def
        nullity_def RNull
        by (metis (mono_tags) domI option.sel snd_conv)
    ultimately show ?case using Tvar.prems(1) by auto
  next
    case (Tnull T \<Gamma> \<Delta>)
    then show ?case by (auto simp add: RNull)
  next
    case (Tfld \<Gamma> \<Delta> x C k\<^sub>1 f D n\<^sub>1 k\<^sub>2 n\<^sub>2 E)
    then obtain w where
      V: "\<lfloor>\<V> x\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl w" using expression_evaluation.simps(1) by blast
    from Tfld.hyps have
      X: "\<Gamma> x = Some (C, k\<^sub>1, !)" and
      D: "x \<in> \<Delta>" by auto
    from X have
      N: "\<not> nullable (the (\<Gamma> x))" by simp
    from Tfld.prems(2) have
      S: "dom \<sigma> = dom \<Gamma>" by simp
    from Tfld.prems(2) X N D have
      Q: "the (\<sigma> x) \<noteq> null" by (simp add: domI)
    with X D Tfld.prems(2) obtain \<iota> where
      I: "\<sigma> x = Some (non_null \<iota>)" using value.exhaust by (cases "\<sigma> x") auto
    with V have
      N: "w = non_null \<iota>" by auto
    with I S X Tfld.prems(2) Q have
      H: "h \<turnstile> w : (C, !)" using good_configuration_def c\<^sub>4_def by (metis domI option.sel)
    with N have
      C: "the (cls h \<iota>) \<sqsubseteq> C" by auto
    then obtain A where
      A: "A = the (cls h \<iota>)" and
      AC: "A \<sqsubseteq> C" by simp
(*
    with N obtain A where
      C: "cls h \<iota> = Some A" and
      A: "A \<sqsubseteq> C" by auto
*)
    with C Tfld.hyps(2) have
      F\<^sub>A: "f \<in> set (fields (the (cls h \<iota>)))" using fields_dom monotonic_fields by (meson domI subsetD)
    then have
      F\<^sub>C: "f \<in> set (fields C)" by (simp add: Tfld.hyps(2) domI fields_dom)
    from H N have
      \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" by auto
    from Tfld.prems(2) have
      "\<turnstile>\<^sub>2 h" using good_configuration_def by blast
    with \<iota>D F\<^sub>A have
      Q: "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
        ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h) \<and>
        the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType (the (cls h \<iota>), f)))"
      by auto
    then obtain v where
      "the (\<chi>\<^sub>v h (\<iota>, f)) = non_null v" and
      "ref (the (\<chi>\<^sub>v h (\<iota>, f))) \<in> dom (\<chi>\<^sub>c h)" and
      "the (cls h (ref (the (\<chi>\<^sub>v h (\<iota>, f))))) \<sqsubseteq> simple_base (the (fType (the (cls h \<iota>), f)))"
      if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" using value.exhaust_sel by auto
    then have
      "the (\<chi>\<^sub>v h (\<iota>, f)) = non_null v" if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" using that by blast
    moreover from F\<^sub>A C assms(3) \<iota>D have
      Dh: "\<chi>\<^sub>v h (\<iota>, f) \<noteq> None" unfolding wf_heap_dom_def by auto
    ultimately have
      V: "\<chi>\<^sub>v h (\<iota>, f) = Some (non_null v)" if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" using that by fastforce
    then have
      "\<chi> h \<iota> f = Some (non_null v)" if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" using that unfolding heap_lookup_def by simp
    with I C F\<^sub>A \<iota>D have
      "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl (non_null v)" if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" using that heap_lookup_def
      by (simp add: heap_lookup_def)
    moreover from A Q C F\<^sub>C V Tfld.hyps(2) have
      "h \<turnstile> non_null v : (D, n\<^sub>2)" if "the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null"
      using RAddr fields_subclass simple_base that value.sel by (metis option.sel)
    ultimately have
      ?thesis if "\<chi>\<^sub>v h (\<iota>, f) \<noteq> Some null" using that
      using Tfld.prems(1) Tfld.prems(3) Dh by auto
    moreover then have
      ?thesis if "\<chi>\<^sub>v h (\<iota>, f) = None" using that by auto 
    moreover
    have
      ?thesis if "\<chi>\<^sub>v h (\<iota>, f) = Some null" using that
    proof -
      from C F\<^sub>A I \<iota>D have
        "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl null" if "\<chi>\<^sub>v h (\<iota>, f) = Some null"
        using that by (simp add: heap_lookup_def)
      moreover
        have "n\<^sub>2 = ?" if "\<chi>\<^sub>v h (\<iota>, f) = Some null" using that
      proof (cases n\<^sub>1)
        case unknown
        then show ?thesis by (simp add: Tfld.hyps(4))
      next
        case non_null
        then show ?thesis
        proof (cases k\<^sub>1)
          case free
          then show ?thesis by (simp add: Tfld.hyps(4))
        next
          case committed
          then have "committed (C, k\<^sub>1, !)" by simp
          with X I Tfld.prems(2) have "deep_init\<^sub>v h (non_null \<iota>)"
            using c\<^sub>5_def good_configuration_def by (metis domI option.sel)
          then have "init h \<iota>" using reaches\<^sub>v_reaches by simp
          moreover from Tfld.hyps(2) AC F\<^sub>C have
            "fType (A, f) = Some (D, n\<^sub>1)" using fields_subclass by simp
          moreover from Tfld.hyps(2) non_null have
            "\<not> nullable (the (fType (C, f)))" by (simp add: simple_nullity)
          moreover note C F\<^sub>A Tfld.hyps(2) A
          ultimately have
            "\<chi>\<^sub>v h (\<iota>, f) \<noteq> Some null" by auto
          then show ?thesis using that by simp
        next
          case unclassified
          then show ?thesis by (simp add: Tfld.hyps(4))
        qed
      qed
      with Tfld.hyps(4) have "h \<turnstile> null : (D, n\<^sub>2)" if "\<chi>\<^sub>v h (\<iota>, f) = Some null" using that RNull by simp
      ultimately show ?thesis if "\<chi>\<^sub>v h (\<iota>, f) = Some null"
        using Tfld.prems(1) Tfld.prems(3) that by blast
    qed
    ultimately show ?case by auto
  qed
  then show ?thesis by (simp add: that)
qed

lemma (in runtime) lemma_4\<^sub>3':
  fixes
    T :: "'C T"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" and
    "\<Gamma>, \<Delta> \<turnstile> e: T" and
    "wf_heap_dom h"
  obtains
    v
  where
    "\<lfloor>e\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" and
    "h \<turnstile> v : type_as_t T"
  using assms lemma_4\<^sub>3 type_as_t by (metis prod_cases3)

lemma (in runtime) lemma_4\<^sub>3_vars:
  fixes
    x\<^sub>i :: "x list"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" and
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i" and
    "wf_heap_dom h"
  obtains
    v\<^sub>i :: "'a value list"
  where
    "\<lfloor>map Variable x\<^sub>i\<rfloor>\<^bsub>h,\<sigma>\<^esub> = map Inl v\<^sub>i" and
    "h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i"
  using assms
proof -
  have
    "\<exists> v\<^sub>i :: 'a value list. \<lfloor>map Variable x\<^sub>i\<rfloor>\<^bsub>h,\<sigma>\<^esub> = map Inl v\<^sub>i \<and> h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i"
  using assms
  proof (induction "x\<^sub>i" arbitrary: T\<^sub>i)
    case Nil
    then show ?case  unfolding variables_typing_def runtime_type_assignments_def by simp
  next
    case (Cons x x\<^sub>i)
    then have
      "length (x # x\<^sub>i) = length T\<^sub>i" unfolding variables_typing_def by simp
    then obtain T T\<^sub>i' where
      T: "T\<^sub>i = T # T\<^sub>i'" by (metis Suc_length_conv length_Cons)
    moreover
    with Cons.prems(2) have
      "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i : T\<^sub>i'" unfolding variables_typing_def by simp
    with Cons have
      "\<exists>v\<^sub>i. \<lfloor>map Variable x\<^sub>i\<rfloor>\<^bsub>h,\<sigma>\<^esub> = map Inl v\<^sub>i \<and> h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i'" by blast
    moreover
    from Cons.prems(2) T have
      "\<Gamma>, \<Delta> \<turnstile> \<V> x : T" unfolding variables_typing_def
      by (metis fst_conv list.set_intros(1) snd_conv zip_Cons_Cons)
    with Cons.prems(1) Cons.prems(3) obtain v where
      "\<lfloor>\<V> x\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v \<and> h \<turnstile> v : type_as_t T" using lemma_4\<^sub>3 type_as_t
      by (metis prod_cases3)
    ultimately show ?case using runtime_type_assignments_cons expressions_evaluation_def
      by (metis (no_types) list.simps(9))
  qed
  then show ?thesis using that by blast
qed

lemma (in runtime) lemma_4\<^sub>3_vars':
  fixes
    x\<^sub>i :: "x list"
  assumes
    "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" and
    "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i: T\<^sub>i" and
    "wf_heap_dom h"
  obtains
    v\<^sub>i :: "'a value list"
  where
    "map \<sigma> x\<^sub>i = map Some v\<^sub>i" and
    "h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i"
  using assms
proof -
  from assms(2) have
    "\<forall> x \<in> set x\<^sub>i. x \<in> dom \<Gamma>" unfolding variables_typing_def using TvarE
    by (metis fst_conv in_set_impl_in_set_zip1)
  with assms(1) have
    Dx: "\<forall> x \<in> set x\<^sub>i. x \<in> dom \<sigma>" by simp
  have
    "\<exists> v\<^sub>i :: 'a value list. map \<sigma> x\<^sub>i = map Some v\<^sub>i \<and> h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i"
  using assms Dx
  proof (induction "x\<^sub>i" arbitrary: T\<^sub>i)
    case Nil
    then show ?case  unfolding variables_typing_def runtime_type_assignments_def by simp
  next
    case (Cons x x\<^sub>i)
    then have
      "length (x # x\<^sub>i) = length T\<^sub>i" unfolding variables_typing_def by simp
    then obtain T T\<^sub>i' where
      T: "T\<^sub>i = T # T\<^sub>i'" by (metis Suc_length_conv length_Cons)
    moreover
    with Cons.prems(2) have
      "\<Gamma>, \<Delta> \<turnstile> x\<^sub>i : T\<^sub>i'" unfolding variables_typing_def by simp
    with Cons.IH Cons.prems(4) assms(1,3) have
      "\<exists>v\<^sub>i. map \<sigma> x\<^sub>i = map Some v\<^sub>i \<and> h \<turnstile> v\<^sub>i : map type_as_t T\<^sub>i'" by (meson list.set_intros(2))
    moreover
    from Cons.prems(2) T have
      "\<Gamma>, \<Delta> \<turnstile> \<V> x : T" unfolding variables_typing_def
      by (metis fst_conv list.set_intros(1) snd_conv zip_Cons_Cons)
    with Cons.prems(1) Cons.prems(3) obtain v where
      "\<lfloor>\<V> x\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v \<and> h \<turnstile> v : type_as_t T" using lemma_4\<^sub>3 type_as_t
      by (metis prod_cases3)
    with Cons.prems(4) have
      "\<sigma> x = Some v \<and> h \<turnstile> v : type_as_t T" by auto
    ultimately show ?case using runtime_type_assignments_cons expressions_evaluation_def
      by (metis (no_types) list.simps(9))
  qed
  then show ?thesis using that by blast
qed

end