(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 1 (Preservation and Safety (strengthened))\<close>

theory Safety imports
  Assignment
  Configuration
  Generation
  Heap
  Instantiation
  Semantics
  Typing
  Update
  WellFormedness
begin

no_notation initialization_extension ("\<epsilon>")
no_notation signature_expectation_extension ("\<epsilon>")

definition "values" :: "'a \<sigma> \<Rightarrow> (x \<Rightarrow> bool) \<Rightarrow> 'a v set"
  where
    [iff]: "values \<sigma> p \<equiv> {v. \<exists> x \<in> dom \<sigma>. v = the (\<sigma> x) \<and> p x}"

lemma (in heap) reaches\<^sub>V\<^sub>v_values:
  assumes
    "reaches\<^sub>V\<^sub>v h\<^sub>v (values \<sigma> p) v"
  obtains
    x
  where
    "x \<in> dom \<sigma>" and
    "p x" and
    "reaches\<^sub>v h\<^sub>v (the (\<sigma> x)) v"
  using assms by auto

lemma (in heap) reaches\<^sub>v\<^sub>V_values:
  assumes
    "reaches\<^sub>v\<^sub>V h\<^sub>v v (values \<sigma> p)"
  obtains
    x
  where
    "x \<in> dom \<sigma>" and
    "p x" and
    "reaches\<^sub>v h\<^sub>v v (the (\<sigma> x))"
  using assms by auto

lemma (in heap) reaches\<^sub>V\<^sub>vI:
  assumes
    "x \<in> dom \<sigma>" and
    "p x" and
    "reaches\<^sub>v h\<^sub>v (the (\<sigma> x)) v"
  shows
    "reaches\<^sub>V\<^sub>v h\<^sub>v (values \<sigma> p) v"
  using assms by auto

lemma (in heap) reaches\<^sub>v\<^sub>VI:
  assumes
    "x \<in> dom \<sigma>" and
    "p x" and
    "reaches\<^sub>v h\<^sub>v v (the (\<sigma> x))"
  shows
    "reaches\<^sub>v\<^sub>V h\<^sub>v v (values \<sigma> p)"
  using assms by auto

locale wf_runtime = runtime + wf_lookup
  
no_notation disj (infixr "|" 30)

lemma (in wf_runtime) lemma_1:
  fixes
    h h' :: "('b, 'a) h" and
    \<sigma> \<sigma>' ::"'b \<sigma>" and
    s :: "'a s"
  assumes
    D: "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" and
    8: "\<Gamma>, \<Delta> \<turnstile> s | \<Delta>', \<Sigma>" and
    1: "ok, h, \<sigma>, s \<leadsto> h', \<sigma>', \<epsilon>" and
    2: "\<epsilon> \<noteq> castExc" and
    Wh: "wf_heap_dom h" and
    wi: "wf_heap_image h" and
    Ws: "wf_statement s" and
    uh: "unbounded_heap h"
  shows
    0: "(\<Gamma>, \<Delta>' \<turnstile> h', \<sigma>') \<and> \<epsilon> = ok" and
    1: "\<sigma>' this = \<sigma> this \<and> dom \<sigma>' = dom \<sigma> \<and> h \<le> h'" and
    2: "\<forall> f \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
        the (\<chi>\<^sub>v h' (ref (the (\<sigma> this)), f)) \<noteq> null" and
    3: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))).
        \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
          the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null" and
    4: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). init h \<iota> \<longrightarrow> init h' \<iota>" and
    5: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h' \<iota>" and
    6: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'. reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>' x)) (non_null \<iota>) \<and>
        \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
          (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" and
    7: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'. reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>' x)) (non_null \<iota>) \<and>
        committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> (ref (the (\<sigma>' x))) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
        (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" and
    8: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'. reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the (\<sigma>' x)) \<and>
        free (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> (ref (the (\<sigma>' x))) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
        (\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y)))" and
    9: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
        reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
        (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
        (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
          free (the (\<Gamma> y)) \<and>
          \<comment> \<open>The conclusion in the report also mentions @{term \"free (the (\<Gamma> z))\"},
              but both the description of the properties preserved by theorem 1
              and goal 22 in the proof of lemma 1 do not have it.\<close>
          reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
          reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
proof -
  have
    c\<^sub>4I: "\<And> \<sigma> h \<Gamma>. dom \<sigma> = dom \<Gamma> \<Longrightarrow>
     \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<longrightarrow> h \<turnstile> the (\<sigma> x) : type_as_t (the (\<Gamma> x)) \<Longrightarrow>
    \<forall> C k n. \<forall> x \<in> dom \<sigma>. the (\<sigma> x) \<noteq> null \<and> the (\<Gamma> x) = (C, k, n) \<longrightarrow> h \<turnstile> the (\<sigma> x) : (C, n)"
    unfolding type_as_t_def by auto
  from D have
    3: "\<Gamma> \<turnstile>\<^sub>1 \<sigma>" and
    4: "\<turnstile>\<^sub>2 h" and
    5: "\<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" and
    6: "\<Gamma> \<turnstile>\<^sub>4 h, \<sigma>" and
    7: "\<Gamma> \<turnstile>\<^sub>5 h, \<sigma>" and
    TN: "\<Gamma>, \<Delta> \<turnstile>\<^sub>6" by auto
  let ?g9 = "\<Gamma> \<turnstile>\<^sub>1 \<sigma>'"
  let ?g10 = "\<turnstile>\<^sub>2 h'"
  let ?g11 = "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>'"
  let ?g12 = "\<forall> x \<in> dom \<sigma>'. the (\<sigma>' x) \<noteq> null \<longrightarrow> h' \<turnstile> the (\<sigma>' x) : type_as_t (the (\<Gamma> x))"
  let ?g13 = "\<Gamma> \<turnstile>\<^sub>5 h', \<sigma>'"
  let ?gTN = "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6"
  let ?g14 = "\<epsilon> = ok"
  let ?g15 = "\<sigma>' this = \<sigma> this \<and> dom \<sigma>' = dom \<sigma> \<and> h \<le> h'"
  let ?g16 = "\<forall> f \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
              the (\<chi>\<^sub>v h' (ref (the (\<sigma> this)), f)) \<noteq> null"
  let ?g17 = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))).
              \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
              the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null"
  let ?g18 = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h' \<iota>"
  let ?g19 = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'.
              reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>' x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
                (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
  let ?g20' = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
                reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>' (committed o (the o \<Gamma>))) (non_null \<iota>) \<longrightarrow>
                  reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed o (the o \<Gamma>))) (non_null \<iota>)"
  let ?g20 = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'. reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>' x)) (non_null \<iota>) \<and>
              committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> (ref (the (\<sigma>' x))) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
                (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
  let ?g21' = "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>' (free o (the o \<Gamma>))) \<longrightarrow>
                  reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free o (the o \<Gamma>)))"
  let ?g22 = "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
              reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
              (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
              (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
                free (the (\<Gamma> y)) \<and>
                reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
                reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
  let ?gS = "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))"
  let ?gWh' = "wf_heap_dom h'"
  let ?gwi = "wf_heap_image h'"
  let ?guh = "unbounded_heap h'"
  (* The default unification bound does not allow the induction method to proceed.
     The current value is set by trial-and-error approach. *)
  from 1 2 3 4 5 6 7 TN 8 Wh wi Ws uh [unify_search_bound = 73] have
    "?g9 \<and> ?g10 \<and> ?g11 \<and> ?g12 \<and> ?g13 \<and> ?gTN \<and> ?g14 \<and> ?g15 \<and> ?g16 \<and> ?g17 \<and> ?g18 \<and> ?g19 \<and>
    ?g20' \<and> ?g20 \<and> ?g21' \<and> ?g22 \<and> ?gS \<and> ?gWh' \<and> ?gwi \<and> ?guh"
  proof (induction arbitrary: \<Gamma> \<Delta> \<Delta>' \<Sigma> rule:big_step_report.induct)
    case (varAss e h \<sigma> v x)
    from varAss.prems(2) varAss.prems(3) varAss.prems(4) varAss.prems(5) varAss.prems(6) varAss.prems(7) varAss.prems(8) have
      C: "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" by auto
    from varAss have
      28: "\<Delta>' = \<Delta> \<union> {x}" using TvarAssE by blast
    from varAss have
      29: "\<Sigma> = {}" by blast
    from varAss obtain T where
      31: "\<Gamma>, \<Delta> \<turnstile> e: T" and
      32: "Some T \<sqsubseteq> \<Gamma> x" by auto
    then have
      30: "x \<in> dom \<Gamma>" by (simp add: option_leq_dom)
    then obtain T\<^sub>x where
      \<Gamma>x: "\<Gamma> x = Some T\<^sub>x" by blast
    with 32 have
      TTx: "T \<sqsubseteq> T\<^sub>x" by simp
    obtain C k n C\<^sub>x k\<^sub>x n\<^sub>x where
      T: "T = (C, k, n)" and
      Tx: "T\<^sub>x = (C\<^sub>x, k\<^sub>x, n\<^sub>x)" using prod_cases3 by metis
    with 31 have
      "\<Gamma>, \<Delta> \<turnstile> e: (C, k, n)" by simp
    with C varAss.hyps varAss.prems(9) have
      V: "h \<turnstile> v : (C, n)" using lemma_4\<^sub>3 by (metis Inl_inject)
    from 30 varAss.prems(2) have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma> (x \<mapsto> v)" by auto
    from varAss.prems(3) have
      10: "\<turnstile>\<^sub>2 h" .
    have
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>(x \<mapsto> v)" unfolding c\<^sub>3_def
      proof (rule ballI, rule impI)
        fix y
        assume
          yD: "y \<in> dom (\<sigma>(x \<mapsto> v))" and
          yA: "\<not> nullable (the (\<Gamma> y)) \<and> y \<in> \<Delta>'"
        then have
          yN: "\<not> nullable (the (\<Gamma> y))" and
          yS: "y \<in> \<Delta>'" by simp_all
        show "the ((\<sigma>(x \<mapsto> v)) y) \<noteq> null"
        proof (cases "x = y")
          case True
          then show ?thesis
          proof (cases "nullable (the (\<Gamma> x))")
            case True
            with "28" varAss.prems(4) yA yD show ?thesis by auto
          next
            case False
            with TTx \<Gamma>x have
              "\<not> nullable T" using nullity_nullable_subtyping by fastforce
            with T V True show ?thesis by auto
          qed
        next
          case False
          with varAss.prems(4,8) yA yD show ?thesis by auto
        qed
      qed
    have
      12: "\<forall> y \<in> dom (\<sigma>(x \<mapsto> v)). the ((\<sigma>(x \<mapsto> v)) y) \<noteq> null \<longrightarrow>
           h \<turnstile> the ((\<sigma>(x \<mapsto> v)) y) : type_as_t (the (\<Gamma> y))"
      proof (rule, rule impI)
        fix \<iota> y
        assume
          AD: "y \<in> dom (\<sigma>(x \<mapsto> v))" and
          AV: "the ((\<sigma>(x \<mapsto> v)) y) \<noteq> null"
        show "h \<turnstile> the ((\<sigma>(x \<mapsto> v)) y) : type_as_t (the (\<Gamma> y))"
        proof (cases "x = y")
          case True
          with AV V have
            "h \<turnstile> the ((\<sigma>(x \<mapsto> v)) y) : (C, !)" by (simp add: runtime_type_assignment.simps)
          moreover from TTx T Tx have
            "(C, !) \<sqsubseteq> (C\<^sub>x, !)" by (simp add: leq_prod_def)
          ultimately have "h \<turnstile> the ((\<sigma>(x \<mapsto> v)) y) : (C\<^sub>x, !)"using lemma_4\<^sub>1 by blast
          with True Tx \<Gamma>x show ?thesis unfolding type_as_t_def
            by (simp add: runtime_type_assignment.simps)
        next
          case False
          with 9 AD AV varAss.prems(2) varAss.prems(5) show ?thesis
            unfolding type_as_t_def base_def nullity_def c\<^sub>1_def c\<^sub>4_def
            by (metis fun_upd_def prod.collapse)
        qed
      qed
    from varAss.prems(6) have
      cd: "\<forall> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<longrightarrow> deep_init\<^sub>v h (the (\<sigma> x))" by simp
    from varAss.prems(6) have
      fn: "\<forall> x \<in> dom \<sigma>. \<forall> y \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<longrightarrow>
        (free (the (\<Gamma> y)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (the (\<sigma> y)))" by simp
    have
      13: "\<Gamma> \<turnstile>\<^sub>5 h, \<sigma> (x \<mapsto> v)" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI)
      fix z y
      assume
        zD: "z \<in> dom (\<sigma>(x \<mapsto> v))" and
        yD: "y \<in> dom (\<sigma>(x \<mapsto> v))" and
        cz: "committed (the (\<Gamma> z))"
      have
        "deep_init\<^sub>v h (the ((\<sigma>(x \<mapsto> v)) z))"
      proof (cases "x = z")
        case True
        with cz have
          cx: "committed (the (\<Gamma> x))" by simp
        with 31 TTx Tx \<Gamma>x have
          E: "e = Null \<or> (\<exists> x. (e = \<V> x \<or> (\<exists> f. e = x\<^bsub>\<bullet>\<^esub>f)) \<and> committed (the (\<Gamma> x)))"
          using lemma_2\<^sub>2'' by auto
        then show ?thesis
        proof (cases e)
          case (Variable a)
          with True 31 cd cx varAss.hyps varAss.prems(2) TTx True \<Gamma>x E show ?thesis by auto
        next
          case (Field_access a g)
          with varAss.hyps have
            ev: "\<lfloor>a \<^bsub>\<bullet>\<^esub> g\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" by blast
          then have
            "(case the (\<sigma> a) of
              non_null \<iota> \<Rightarrow>
                if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))
                then Inl (the (\<chi>\<^sub>v h (\<iota>, g)))
                else Inr derefExc |
              _ \<Rightarrow> Inr derefExc) = Inl v"
          proof (cases "a \<^bsub>\<bullet>\<^esub> g")
            case (Field_access x f)
            show ?thesis
            proof (cases "the (\<sigma> a)")
              case (non_null \<iota>)
              then show ?thesis
              proof (cases "\<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))")
                case True
                with ev non_null show ?thesis by simp
              next
                case False
                from non_null ev have "(if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))
                    then Inl (the (\<chi>\<^sub>v h (\<iota>, g)))
                    else Inr derefExc.derefExc) =
                  Inl v" by simp
                with False show ?thesis by (metis Inl_Inr_False)
              qed
            next
              case null
              with ev show ?thesis by simp
            qed
          qed simp_all
          moreover then obtain \<iota> where
            "the (\<sigma> a) = non_null \<iota>" and
            "\<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))" and
            "the (\<chi>\<^sub>v h (\<iota>, g)) = v"
          proof (cases "the (\<sigma> a)")
            case (non_null \<iota>)
            assume
              A: "\<And>\<iota>. the (\<sigma> a) = value.non_null \<iota> \<Longrightarrow>
                   \<iota> \<in> dom (\<chi>\<^sub>c h) \<and>
                   g \<in> set (fields (the (cls h \<iota>))) \<Longrightarrow>
                   the (\<chi>\<^sub>v h (\<iota>, g)) = v \<Longrightarrow> thesis" and
              "(case the (\<sigma> a) of
                   value.non_null \<iota> \<Rightarrow>
                     if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))
                     then Inl (the (\<chi>\<^sub>v h (\<iota>, g))) else Inr \<epsilon>.derefExc
                   | null \<Rightarrow> Inr \<epsilon>.derefExc) =
                  Inl v"
            with non_null have
              "(if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))
                   then Inl (the (\<chi>\<^sub>v h (\<iota>, g))) else Inr \<epsilon>.derefExc) = Inl v" by simp
            with A non_null show ?thesis by (meson Inl_inject Inr_Inl_False)
          next
            case null
            assume
              "(case the (\<sigma> a) of
                 value.non_null \<iota> \<Rightarrow>
                   if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> g \<in> set (fields (the (cls h \<iota>)))
                   then Inl (the (\<chi>\<^sub>v h (\<iota>, g))) else Inr \<epsilon>.derefExc
                 | null \<Rightarrow> Inr \<epsilon>.derefExc) =
                Inl v"
            with null show ?thesis by simp
          qed
          then have
            "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
            "g \<in> set (fields (the (cls h \<iota>)))" by blast+
          from ev varAss.prems(9) have
            "\<forall> v'. reaches\<^sub>v (\<chi>\<^sub>v h) v v' \<longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> a)) v'" using lemma_5\<^sub>1 by blast
          with 31 E Field_access cd varAss.prems(2) have
            "deep_init\<^sub>v h v" by (auto elim!: TvarE)
          with True show ?thesis by simp
        next
          case Null
          with True varAss.hyps show ?thesis using deep_init\<^sub>v_null by simp
        qed
      next
        case False
        with cz varAss.prems(6) zD show ?thesis by simp_all
      qed
      moreover have
        "free (the (\<Gamma> y)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (the ((\<sigma>(x \<mapsto> v)) y))"
      proof (rule impI)
        assume
          fy: "free (the (\<Gamma> y))"
        with cz have
          yz: "y \<noteq> z" by auto
        show "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (the ((\<sigma>(x \<mapsto> v)) y))"
        proof (cases "x = z")
          case True
          have
            "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) v (the (\<sigma> y))"
          proof (cases "e")
            case (Variable a)
            with 9 31 TTx True Tx \<Gamma>x cz fn fy yD varAss.hyps varAss.prems(2) show
              ?thesis using lemma_2\<^sub>2' lemma_2\<^sub>4(1) Tvar1(1) c\<^sub>1_def
              using initialization expression_evaluation.simps(1)
              by (metis (no_types) Inl_inject option.sel)
          next
            case (Field_access a g)
            with 9 31 TTx True Tx \<Gamma>x cz fn fy yD varAss.hyps varAss.prems(2) varAss.prems(9) show
              ?thesis using lemma_2\<^sub>2'' lemma_2\<^sub>5 lemma_5\<^sub>1 unfolding c\<^sub>1_def using initialization
              by (metis (mono_tags) expression.distinct(1) expression.distinct(5) option.sel)
          next
            case Null
            with varAss.hyps show ?thesis using reaches\<^sub>v_left_null by simp
          qed
          with True yz show ?thesis by simp
        next
          case False
          moreover have
            "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the ((\<sigma>(x \<mapsto> v)) y))"
          proof (cases "y = x")
            case True
            have
              "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) v"
            proof (cases e)
              case (Variable a)
              with 31 9 TTx True TvarE Tx \<Gamma>x cz varAss.hyps varAss.prems(2) zD fn fy show ?thesis
                using lemma_2\<^sub>1(1) c\<^sub>1_def
                using expression_evaluation.simps(1) initialization_classified_subtyping
                using k.distinct(3) by (metis (mono_tags) Inl_inject option.sel)
            next
              case (Field_access a g)
              with 31 TTx True \<Gamma>x fy Tx show ?thesis using lemma_2\<^sub>1(2)
                using expression.distinct(1,5) initialization by (metis option.sel)
            next
              case Null
              with varAss.hyps show ?thesis using reaches\<^sub>v_right_null by simp
            qed
            with True show ?thesis by simp
          next
            case False
            moreover from 9 cz fn fy varAss.prems(2) yD zD have
              "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> y))" unfolding c\<^sub>1_def by simp
            ultimately show ?thesis by simp
          qed
          ultimately show ?thesis by simp
        qed
      qed
      ultimately show
          "deep_init\<^sub>v h (the ((\<sigma>(x \<mapsto> v)) z)) \<and>
          (free (the (\<Gamma> y)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (the ((\<sigma>(x \<mapsto> v)) y)))" by simp
    qed
    from 28 varAss.prems(7) have
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" by simp
    have
      14: "ok = ok" by simp
    from 30 varAss.prems(2) varAss.prems(11) have
      15: "(\<sigma> (x \<mapsto> v)) this = \<sigma> this \<and> dom (\<sigma> (x \<mapsto> v)) = dom \<sigma> \<and> h \<le> h" by auto
    from 29 have
      16: "\<forall>f\<in>\<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
           the (\<chi>\<^sub>v h (ref (the (\<sigma> this)), f)) \<noteq> null" by simp
    have
      17: "\<forall>f. \<forall>\<iota>\<in>dom (\<chi>\<^sub>c h). \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null
          \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" by simp
    have
      18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h \<iota>" by simp
    have
      19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> z \<in> dom (\<sigma>(x \<mapsto> v)).
              reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      let ?thesis = "\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (value.non_null \<iota>)"
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)"
        "z \<in> dom (\<sigma>(x \<mapsto> v))"
        "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma> (x \<mapsto> v)) z)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h)"
      with 15 have
        "z \<in> dom \<sigma>" and
        r: "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma> (x \<mapsto> v)) z)) (non_null \<iota>)" by blast+
      then have ?thesis if "x \<noteq> z" using that by auto
      moreover
      from r have "reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>)" if "x = z" using that by auto
      then have ?thesis if "x = z" using that
      proof (cases e)
        case (Variable w)
        moreover
        assume
          "x = z \<Longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>)"
          "x = z"
        moreover note "15" "31" "9" varAss.hyps
        ultimately show ?thesis by auto
      next
        case (Field_access w g)
        moreover
        assume
          "x = z \<Longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>)"
          "x = z"
        moreover note "15" "31" "9" varAss.hyps varAss.prems(9)
        ultimately show ?thesis using lemma_2\<^sub>5 lemma_5\<^sub>1 c\<^sub>1_def by metis
      next
        case Null
        moreover
        assume
          "x = z \<Longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>)"
          "x = z"
        moreover note varAss.hyps
        ultimately show ?thesis using reaches\<^sub>v_left_null by simp
      qed
      ultimately show ?thesis by blast
    qed
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> z \<in> dom (\<sigma>(x \<mapsto> v)).
          reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (non_null \<iota>) \<and>
          committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x \<mapsto> v)) z)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
            (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
      proof (rule ballI, rule ballI, rule impI)
        fix \<iota> z
        assume
          \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
          zD: "z \<in> dom (\<sigma>(x \<mapsto> v))" and
          "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (non_null \<iota>) \<and> committed (the (\<Gamma> z)) \<and>
           \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x \<mapsto> v)) z)) \<in> dom (\<chi>\<^sub>c h)"
        then have
          r: "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (non_null \<iota>)" and
          cz: "committed (the (\<Gamma> z))" and
          "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
          zD\<^sub>h: "ref (the ((\<sigma>(x \<mapsto> v)) z)) \<in> dom (\<chi>\<^sub>c h)" by simp_all
        show "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
        proof (cases "x = z")
          case True
          with r cz zD\<^sub>h have
            rv: "reaches\<^sub>v (\<chi>\<^sub>v h) v (non_null \<iota>)" and
            cz: "committed (the (\<Gamma> x))" and
            "ref v \<in> dom (\<chi>\<^sub>c h)" by auto
          show ?thesis
          proof (cases "e")
            case (Variable y)
            with 31 TTx \<Gamma>x cz have
              "committed (the (\<Gamma> y))" using committed_subtyping lemma_2\<^sub>4' by (metis option.sel)
            with 9 15 31 Tvar1(1) rv Variable varAss.hyps show ?thesis using lemma_2\<^sub>4(1)
              using c\<^sub>1_def expression_evaluation.simps(1)
              by (metis old.sum.inject(1))
          next
            case (Field_access y f)
            with 9 15 31 TTx Tx \<Gamma>x cz rv varAss.hyps varAss.prems(9) show ?thesis
              using lemma_2\<^sub>2'' lemma_2\<^sub>5 lemma_5\<^sub>1 unfolding c\<^sub>1_def
              using expression.distinct(1) expression.distinct(5) initialization
              by (metis option.sel)
          next
            case Null
            with rv varAss.hyps show ?thesis using reaches\<^sub>v_left_null by simp
          qed
        next
          case False
          with cz r zD show ?thesis by auto
        qed
      qed
    have
      g20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
              reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values (\<sigma>(x \<mapsto> v)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
              reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
        r': "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values (\<sigma>(x \<mapsto> v)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
      moreover
      then obtain z where
        zD: "z \<in> dom (\<sigma> (x \<mapsto> v))" and
        r: "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x \<mapsto> v)) z)) (non_null \<iota>)" and
        cz: "committed (the (\<Gamma> z))" using reaches\<^sub>V\<^sub>v_values by (metis (mono_tags) comp_apply)
      moreover
      from r \<iota>D varAss.prems(9) have
        "ref (the ((\<sigma>(x \<mapsto> v)) z)) \<in> dom (\<chi>\<^sub>c h)" using reaches\<^sub>v_source_dom by fastforce
      moreover note 20
      ultimately have
        "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
        by blast
      then show
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" by auto
    qed
    have
      g21': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values (\<sigma>(x \<mapsto> v)) (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)"
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values (\<sigma>(x \<mapsto> v)) (free \<circ> (the \<circ> \<Gamma>)))"
      then obtain z where
        "z \<in> dom (\<sigma>(x \<mapsto> v))" and
        fz: "free (the (\<Gamma> z))" and
        r': "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the ((\<sigma>(x \<mapsto> v)) z))" using reaches\<^sub>v\<^sub>V_values
        by (metis (mono_tags) comp_apply)
      with 15 have
        zD: "z \<in> dom \<sigma>" by simp
      have "\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y))"
      proof (cases "x = z")
        case True
        with r' have
          r: "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) v" by simp
        show ?thesis
        proof (cases e)
          case (Variable y)
          with 9 15 31 T r varAss.hyps TTx True Tx \<Gamma>x fz show ?thesis using lemma_2\<^sub>1(1)
            using expression_evaluation.simps(1) TvarE initialization unfolding c\<^sub>1_def
            by (metis Inl_inject option.sel)
        next
          case (Field_access y f)
          with 31 TTx True \<Gamma>x fz Tx show ?thesis using lemma_2\<^sub>1
            using expression.distinct(1) expression.distinct(5) initialization
            by (metis option.sel)
        next
          case Null
          with varAss.hyps r show ?thesis using reaches\<^sub>v_reaches by simp
        qed
      next
        case False
        with r' zD fz show ?thesis by auto
      qed
      then show "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by auto
    qed
    have
      22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
            reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
            (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
            (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
               free (the (\<Gamma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))" by simp
    from varAss.prems(8) have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by auto
    from varAss.prems(9) have
      Wh': "wf_heap_dom h" .
    from varAss.prems(10) have
      wi: "wf_heap_image h" .
    from varAss.prems(12) have
      uh: "unbounded_heap h" .
    from 9 10 11 12 13 TN 14 15 16 17 18 19 g20' 20 g21' 22 gS Wh' wi uh show ?case by blast
  next
    case (varAssBad e h \<sigma> x)
    then obtain y f where
      E: "e = y \<^bsub>\<bullet>\<^esub> f" using evaluation_dereference by meson
    with varAssBad.prems(8) have
      "y \<in> dom \<Gamma>" by blast
    with varAssBad.prems(2) have
      yD: "y \<in> dom \<sigma>" by simp
    with varAssBad.hyps E have
      S: "the (\<sigma> y) = null \<or>
      ref (the (\<sigma> y)) \<notin> dom (\<chi>\<^sub>c h) \<or>
      f \<notin> set (fields (the (cls h (ref (the (\<sigma> y))))))" using evaluation_dereference by blast
    with E varAssBad.prems(8) obtain T where
      T: "\<Gamma>, \<Delta> \<turnstile> (y \<^bsub>\<bullet>\<^esub> f) : T" using TvarAssE by meson
    then obtain Y k where
      "\<Gamma>, \<Delta> \<turnstile> \<V> y : (Y, k, !)" and
      F: "f \<in> set (fields Y)"
      using fields_dom by (meson TfldE domI)
    then have
      \<Gamma>y: "\<Gamma> y = Some (Y, k, !)" and
      "y \<in> \<Delta>" using lemma_2\<^sub>4 by auto
    moreover then have
      "\<not> nullable (the (\<Gamma> y))" by simp
    moreover note varAssBad.prems(4) yD
    ultimately have
      A: "\<sigma> y \<noteq> Some null" using lemma_2\<^sub>5 c\<^sub>3_def by (metis option.sel)
    with yD obtain \<iota> where
      \<sigma>: "\<sigma> y = Some (non_null \<iota>)" by (metis domD value.collapse)
    with T varAssBad.prems(2) have
      \<sigma>y: "\<sigma> y = Some (non_null \<iota>)" by blast
    with A T \<sigma> F varAssBad.prems(2) varAssBad.prems(5) \<sigma>y \<Gamma>y have
      H: "h \<turnstile> (non_null \<iota>) : (Y, !)" using lemma_2\<^sub>5 c\<^sub>1_def c\<^sub>4_def by (metis option.sel)
    with T F \<sigma> have
      L: "cls h \<iota> \<sqsubseteq> Some Y" by auto
    with H have
      L: "the (cls h \<iota>) \<sqsubseteq> Y" by auto
    from H have
      I: "\<iota> \<in> dom (\<chi>\<^sub>c h)" by auto
    also from varAssBad.prems(9) have
      "\<dots> = {\<iota>. \<exists>f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h)} \<union> {\<iota>. \<exists>C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}"
      using wf_heap_dom_weak wf_heap_dom_weak_def by blast
    finally have
      "\<iota> \<in> {\<iota>. \<exists>f. (\<iota>, f) \<in> dom (\<chi>\<^sub>v h)} \<union> {\<iota>. \<exists>C. \<chi>\<^sub>c h \<iota> = Some C \<and> fields C = []}" by simp
    with F H S \<sigma>y have
      "fields (the (cls h \<iota>)) = []" if "(\<iota>, f) \<notin> dom (\<chi>\<^sub>v h)" using monotonic_fields by fastforce
    with F L have "\<chi> h \<iota> f \<noteq> None" unfolding heap_lookup_def
      using monotonic_fields by fastforce
    with F L I S \<sigma>y have
      "False" using monotonic_fields by auto
    then show ?case by simp
  next
    case (fldAss \<sigma> x \<iota> h f y)
    from fldAss.prems(8) obtain C k\<^sub>1 D n T k\<^sub>2 where
      38: "\<Gamma>, \<Delta> \<turnstile> \<V> x: (C, k\<^sub>1, !)" and
      39: "fType (C, f) = Some (D, n)" and
      40: "\<Gamma>, \<Delta> \<turnstile> \<V> y: T" and
      41: "T \<sqsubseteq> (D, k\<^sub>2, n)" and
      42: "k\<^sub>1 = \<zero> \<or> k\<^sub>2 = \<one>" and
      43: "\<Sigma> = (if x = this then {f} else {})" using TfldAssE by metis
    from fldAss.hyps have
      xD: "x \<in> dom \<sigma>" by blast
    from 40 have
      \<Gamma>y: "the (\<Gamma> y) = T" by auto
    moreover obtain C\<^sub>y k\<^sub>y n\<^sub>y where
      T: "T = (C\<^sub>y, k\<^sub>y, n\<^sub>y)" using prod_cases3 by blast 
    ultimately have
      \<Gamma>y': "the (\<Gamma> y) = (C\<^sub>y, k\<^sub>y, n\<^sub>y)" by simp
    from 40 fldAss.prems(2) have
      y\<sigma>: "y \<in> dom \<sigma>" by auto
    from 40 41 T \<Gamma>y' fldAss.prems(4) fldAss.prems(5) y\<sigma> have
      44: "h \<turnstile> the (\<sigma> y): (D, n)" using RNull lemma_2\<^sub>4'' lemma_4\<^sub>1 nullity subtyping_as_simple_p
      unfolding c\<^sub>4_def c\<^sub>3_def by metis
    from 41 T have
      "n\<^sub>y = !" if "n = !" using that lemma_2\<^sub>3' nullity_notnullable_subtyping by fastforce  
    with 44 have
      yN: "the (\<sigma> y) \<noteq> null" if "n = !" using that by auto
    from 38 have
      45: "\<Gamma> x = Some (C, k\<^sub>1, !)" by auto
    with 6 fldAss.hyps fldAss.prems(5) have
      hx: "h \<turnstile> the (\<sigma> x) : (C, !)" using value.discI c\<^sub>4_def by (metis domI option.sel)
    with fldAss.hyps have
      hxC: "the (cls h (ref (the (\<sigma> x)))) \<sqsubseteq> C" by auto
    with 39 fldAss.hyps have
      SN: "simple_nullity (the (fType (the (cls h \<iota>), f))) = n"
      using fields_dom fields_subclass simple_nullity by fastforce
    from fldAss.hyps hxC have
      "the (\<chi>\<^sub>c h \<iota>) \<sqsubseteq> C" by simp
    with 39 have
      fT: "fType (the (\<chi>\<^sub>c h \<iota>), f) = Some (D, n)" using fields_dom fields_subclass by fastforce
    from fldAss.prems(2) have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma>" .
    have
      10: "\<turnstile>\<^sub>2 ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)" unfolding c\<^sub>2_def
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' g
      assume
        "\<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h))" and
        gI: "g \<in> set (fields (the (cls ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<iota>')))" and
        "the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (\<iota>', g)) \<noteq> null"
      then have
        iD: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
        iN: "the (((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (\<iota>', g)) \<noteq> null" by simp_all
      with gI 44 fldAss.prems(3) have 
        "ref (the (((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (\<iota>', g))) \<in> dom (\<chi>\<^sub>c h)"
          unfolding \<chi>\<^sub>c_def cls_def c\<^sub>2_def using RAddrE
        by (metis fun_upd_apply snd_conv value.collapse)
      moreover
      have
       "the (\<chi>\<^sub>c h (ref (the (((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (\<iota>', g))))) \<sqsubseteq>
       simple_base (the (fType (the (\<chi>\<^sub>c h \<iota>'), g)))"
      proof (cases "(\<iota>', g) = (\<iota>, f)")
        case True
        with iN have
          "the (\<sigma> y) \<noteq> null" by simp
        with 44 fT have
          "the (\<chi>\<^sub>c h (ref (the (\<sigma> y)))) \<sqsubseteq> simple_base (the (fType (the (\<chi>\<^sub>c h \<iota>), f)))"
          using lemma_4\<^sub>2 simple_base cls_def by (metis option.sel)
        with True show ?thesis by simp
      next
        case False
        with fldAss.prems(3) gI iD iN show ?thesis using \<chi>\<^sub>c_conv cls_def c\<^sub>2_def
          by (metis fun_upd_apply)
      qed
      ultimately show
        "ref (the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (\<iota>', g))) \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) \<and>
         the (cls ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (ref (the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (\<iota>', g))))) \<sqsubseteq>
         simple_base (the (fType (the (cls ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<iota>'), g)))" by simp
    qed
    from fldAss.prems(4,8)have
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>" by auto
    have
      12: "\<forall> z \<in> dom \<sigma>.
           the (\<sigma> z) \<noteq> null \<longrightarrow>
           ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<turnstile> the (\<sigma> z) : type_as_t (the (\<Gamma> z))"
      proof (rule, rule)
        fix z
        assume
          AD: "z \<in> dom \<sigma>" and
          AV: "the (\<sigma> z) \<noteq> null"
        then show "((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<turnstile> the (\<sigma> z) : type_as_t (the (\<Gamma> z))"
        proof (cases "z = x")
          case True
          with 45 have
            "\<Gamma> z = Some (C, k\<^sub>1, !)" by blast
          moreover with AD AV fldAss.prems(5) have
            "((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<turnstile> the (\<sigma> z) : (C, !)"
            by (simp add: runtime_type_assignment.simps)
          ultimately show ?thesis by simp
        next
          case False
          from AD 9 obtain C\<^sub>z k\<^sub>z n\<^sub>z where
            "\<Gamma> z = Some (C\<^sub>z, k\<^sub>z, n\<^sub>z)" by auto
          with AD AV fldAss.prems(5) show ?thesis by (simp add: runtime_type_assignment.simps)
        qed
      qed
    have
      17: "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c h). \<forall> g \<in> set (fields (the (cls h \<iota>'))). \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<and>
          the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null \<longrightarrow> the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (\<iota>', g)) \<noteq> null"
      proof (rule ballI, rule ballI, rule impI)
        fix \<iota>' g
        assume
          "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
          G: "g \<in> set (fields (the (cls h \<iota>')))" and
          "\<not> nullable (the (fType (the (cls h \<iota>'), g))) \<and> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null"
        then have
          N: "\<not> nullable (the (fType (the (cls h \<iota>'), g)))" and
          "the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null" by auto
        moreover from G obtain Z n' where
          "fType (the (cls h \<iota>'), g) = Some (Z, n')" using fields_dom by auto
        moreover with N have
          "n' = !" using simple_nullity by (cases "n'") fastforce+
        moreover note fT yN
        ultimately show "the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (\<iota>', g)) \<noteq> null" by auto
      qed
    have
      18: "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)). \<iota>' \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow>
        init ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) \<iota>'" by simp
    have
      13: "\<Gamma> \<turnstile>\<^sub>5 ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h), \<sigma>" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI, rule conjI)
      fix w z
      assume
        wD: "w \<in> dom \<sigma>" and
        zD: "z \<in> dom \<sigma>" and
        cw: "committed (the (\<Gamma> w))"
      let ?thesis = "deep_init\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (the (\<sigma> w))"
      note 42
      moreover have
        ?thesis if "k\<^sub>1 = \<zero>" using that
      proof (cases "the (\<sigma> w)")
        case (non_null \<iota>\<^sub>w)
        moreover
        assume
          "k\<^sub>1 = \<zero>"
        with 45 have
          "free (the (\<Gamma> x))" by simp
        with fldAss.prems(6) wD cw xD have
          "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> x))" by simp
        moreover
        with non_null fldAss.hyps have
          nh: "\<not> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>w \<iota>" by (simp add: reaches\<^sub>v_reaches)
        with y\<sigma> have
          r: "reaches ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) \<iota>\<^sub>w = reaches (\<chi>\<^sub>v h) \<iota>\<^sub>w" using lemma_5\<^sub>3 by fastforce
        moreover from cw fldAss.prems(6) wD have
          wI: "deep_init\<^sub>v h (the (\<sigma> w))" by simp
        moreover from nh r have
          "\<not> reaches ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) \<iota>\<^sub>w \<iota>" by simp
        moreover note fldAss.hyps
        ultimately show ?thesis using deep_init\<^sub>v_upd_other by simp
      next
        case null
        then show ?thesis using deep_init\<^sub>v_null by presburger
      qed
      moreover
      {
        assume
          k\<^sub>2: "k\<^sub>2 = \<one>"
        then have
          cy: "committed (the (\<Gamma> y))" using "40" "41" lemma_2\<^sub>2' by blast
        have ?thesis
        proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> x))")
          case True
          then obtain \<iota>\<^sub>w where
            \<iota>\<^sub>w: "the (\<sigma> w) = non_null \<iota>\<^sub>w" by (metis reaches\<^sub>v_left_null value.collapse)
          from True fldAss.hyps have
            "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (non_null \<iota>)" by simp
          with \<iota>\<^sub>w have
            "reaches (\<chi>\<^sub>v h) \<iota>\<^sub>w \<iota>" by (simp add: reaches\<^sub>v_reaches)
          with y\<sigma> have
            "\<forall> q. reaches ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) \<iota>\<^sub>w q \<longrightarrow> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null q) \<or> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>w q"
            using lemma_5\<^sub>2 by (metis domD option.sel)
          moreover from fldAss.prems(6) have
            X: "\<forall>x\<in>dom \<sigma>. committed (the (\<Gamma> x)) \<longrightarrow> deep_init\<^sub>v h (the (\<sigma> x))" by simp
          with cy y\<sigma> have
            "\<forall> \<iota>'. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>') \<longrightarrow> (\<forall> g \<in> set (fields (the (cls h \<iota>'))).
              \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null)"
            unfolding deep_init\<^sub>v_def init_def by blast
          moreover from X cw wD have
            "\<forall> \<iota>'. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (non_null \<iota>') \<longrightarrow> (\<forall> g \<in> set (fields (the (cls h \<iota>'))).
              \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null)"
            unfolding deep_init\<^sub>v_def init_def by blast
          with \<iota>\<^sub>w have
            "\<forall> \<iota>'. reaches (\<chi>\<^sub>v h) \<iota>\<^sub>w \<iota>' \<longrightarrow> (\<forall> g \<in> set (fields (the (cls h \<iota>'))).
              \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null)"
            using deep_init\<^sub>v_non_null by simp
          ultimately have
            "\<forall> \<iota>'. reaches ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) \<iota>\<^sub>w \<iota>' \<longrightarrow> (\<forall> g \<in> set (fields (the (cls h \<iota>'))).
              \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null)"
            by auto
          with \<iota>\<^sub>w have
            "\<forall> \<iota>'. reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> w)) (non_null \<iota>') \<longrightarrow> (\<forall> g \<in> set (fields (the (cls h \<iota>'))).
              \<not> nullable (the (fType (the (cls h \<iota>'), g))) \<longrightarrow> the (\<chi>\<^sub>v h (\<iota>', g)) \<noteq> null)"
            by (simp add: reaches\<^sub>v_reaches)
          from X cw wD have
            dw: "deep_init\<^sub>v h (the (\<sigma> w))" using cw wD by blast
          from X cy y\<sigma> have
            dy: "deep_init\<^sub>v h (the (\<sigma> y))" by blast
          show ?thesis
          proof (cases "\<sigma> y = Some null")
            case True
            with dw yN fT SN show ?thesis using deep_init\<^sub>v_upd_null by (cases n) auto
          next
            case False
            with dw dy y\<sigma> show ?thesis using deep_init\<^sub>v_upd_non_null by force
          qed
        next
          case False
          with fldAss.hyps have
            ni: "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (non_null \<iota>)" by auto
          with cw wD fldAss.prems(6) show ?thesis using deep_init\<^sub>v_upd_other c\<^sub>5_def by blast
        qed
      }
      ultimately show ?thesis by blast
      let ?thesis = "free (the (\<Gamma> z)) \<longrightarrow> \<not> reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> w)) (the (\<sigma> z))"
      note 42
      moreover have
        ?thesis if "k\<^sub>1 = \<zero>"
      proof
        from 45 fldAss.prems(6) wD cw xD that have
          "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> x))" by simp
        moreover assume
          "free (the (\<Gamma> z))"
        with fldAss.prems(6) cw wD zD have
          "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> z))" by simp
        moreover note fldAss.hyps
        ultimately show "\<not> reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> w)) (the (\<sigma> z))"
          using reaches\<^sub>v_upd_other by simp
      qed
      moreover have
        ?thesis if "k\<^sub>2 = \<one>"
      proof
        have
          k\<^sub>2: "k\<^sub>2 = \<one>" using that by simp
        then have
          cy: "committed (the (\<Gamma> y))" using "40" "41" lemma_2\<^sub>2' by blast
        assume
          fz: "free (the (\<Gamma> z))"
        with cw fldAss.prems(6) wD zD have
          zw: "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> z))" by simp
        from fldAss.prems(6) y\<sigma> zD fz cy have
          zy: "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (the (\<sigma> z))" by simp
        show "\<not> reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> w)) (the (\<sigma> z))"
        proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> x))")
          case True
          with fldAss.hyps y\<sigma> zw zy show ?thesis using lemma_5\<^sub>2\<^sub>v by fastforce
        next
          case False
          with fldAss.hyps zw show ?thesis using reaches\<^sub>v_upd_other by simp
        qed
      qed
      ultimately show "free (the (\<Gamma> z)) \<longrightarrow>
        \<not> reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> w)) (the (\<sigma> z))" by auto
    qed
    from fldAss.prems(7,8) have
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" by auto
    have
      14: "ok = ok" by simp
    have
      15: "\<sigma> this = \<sigma> this \<and> dom \<sigma> = dom \<sigma> \<and> h \<le> ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)"
      unfolding predecessor_def by simp
    have
      16: "\<forall> g \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), g))) \<longrightarrow>
           the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (ref (the (\<sigma> this)), g)) \<noteq> null"
    proof (rule ballI, rule impI)
      fix g
      assume
        gI: "g \<in> \<Sigma>" and
        N: "\<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), g)))"
      with 43 hx have
        "h \<turnstile> the (\<sigma> this): (C, !)" by (metis empty_iff)
      from 43 gI hxC have
        "the (cls h (ref (the (\<sigma> this)))) \<sqsubseteq> C" by (metis empty_iff)
      with 43 N SN fldAss.hyps gI have
        "n = !" using n.exhaust value.sel by (metis empty_iff option.sel singletonD)
      with yN have
        "the (\<sigma> y) \<noteq> null" by simp
      with 43 fldAss.hyps gI have
        "the (((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (ref (the (\<sigma> this)), g)) \<noteq> null" using value.sel
        by (metis empty_iff fun_upd_same option.sel singletonD)
      then show "the (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) (ref (the (\<sigma> this)), g)) \<noteq> null" by simp
    qed
    have
      19: "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)). \<forall> x \<in> dom \<sigma>.
            reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> x)) (non_null \<iota>') \<and>
            \<iota>' \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists>y\<in>dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>'))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' z
      assume
        "\<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h))" and
        zD: "z \<in> dom \<sigma>" and
        "reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> z)) (non_null \<iota>') \<and>
         \<iota>' \<in> dom (\<chi>\<^sub>c h)"
      then have
        r: "reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> z)) (non_null \<iota>')" and
        "\<iota>' \<in> dom (\<chi>\<^sub>c h)" by simp_all
      show
        "\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>')"
      proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>)")
        case True
        with r y\<sigma> zD show ?thesis using lemma_5\<^sub>2\<^sub>v by (metis domD option.sel)
      next
        case False
        with r zD show ?thesis using reaches\<^sub>v_upd_other by auto
      qed
    qed
    have
      20: "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)). \<forall> x \<in> dom \<sigma>.
          reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> x)) (non_null \<iota>') \<and>
          committed (the (\<Gamma> x)) \<and> \<iota>' \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma> x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
            (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>'))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' z
      let ?thesis = "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>')"
      assume
        "\<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h))" and
        zD\<^sub>\<sigma>: "z \<in> dom \<sigma>" and
        a: "reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> z)) (non_null \<iota>') \<and>
        committed (the (\<Gamma> z)) \<and> \<iota>' \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)"
      then have
        rz: "reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (the (\<sigma> z)) (non_null \<iota>')" and
        cz: "committed (the (\<Gamma> z))" and
        \<iota>D\<^sub>h: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
        xD\<^sub>h: "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" by simp_all
      from 45 rz cz zD\<^sub>\<sigma> xD fldAss.hyps fldAss.prems(6) have
        ?thesis if "k\<^sub>1 = \<zero>" using that reaches\<^sub>v_upd_other \<chi>\<^sub>v_conv initialization using c\<^sub>5_def
        by (metis option.sel)
      moreover have
        ?thesis if "k\<^sub>2 = \<one>"
      proof -
        have
          k\<^sub>2: "k\<^sub>2 = \<one>" using that by simp
        with 40 41 have
          cy: "committed (the (\<Gamma> y))" using lemma_2\<^sub>2' by blast
        show ?thesis
        proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>)")
          case True
          with cy cz rz y\<sigma> zD\<^sub>\<sigma> show ?thesis using lemma_5\<^sub>2\<^sub>v by fastforce
        next
          case False
          with rz have
            "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>')" using reaches\<^sub>v_upd_other by blast
          with cz zD\<^sub>\<sigma> show ?thesis by blast
        qed
      qed
      moreover note 42
      ultimately show ?thesis by blast
    qed
    have
      g20': "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c h).
        reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>') \<longrightarrow>
        reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>')"
    proof (rule ballI, rule impI)
      fix \<iota>'
      assume
        \<iota>D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)"
      moreover
      assume
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>')"
      with \<iota>D obtain x where
        "x \<in> dom \<sigma>" and
        "reaches\<^sub>v (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (the (\<sigma> x)) (non_null \<iota>')"
        "committed (the (\<Gamma> x))" by auto
      moreover
      with 12 have
        "ref (the (\<sigma> x)) \<in> dom (\<chi>\<^sub>c h)" using \<chi>\<^sub>c_def cls_conv cls_def RAddrE
            reaches\<^sub>v_reaches type_as_t value.exhaust_sel by (metis prod_cases3)
      moreover note 20
      ultimately have
        "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>')" by auto
      then show "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>')" by auto
    qed
    have
      g21': "\<forall> \<iota>' \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (non_null \<iota>') (values \<sigma> (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>') (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>'
      let ?e = "\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> y))"
      assume
        "\<iota>' \<in> dom (\<chi>\<^sub>c h)"
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) (non_null \<iota>') (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
      then obtain z where
        zD: "z \<in> dom \<sigma>" and
        fz: "free (the (\<Gamma> z))" and
        r': "reaches\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (non_null \<iota>') (the (\<sigma> z))" by auto
      with 12 have
        "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using RAddrE \<chi>\<^sub>c_conv \<chi>\<^sub>c_def reaches\<^sub>v_reaches
        using runtime_type_assignment.cases value.collapse by metis
      have ?e if "k\<^sub>1 = \<zero>"
      proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> x))")
        case True
        with 45 xD that show ?thesis by force
      next
        case False
        with fldAss.hyps fz r' zD show ?thesis using reaches\<^sub>v_upd_other by auto
      qed
      moreover from 40 41 \<Gamma>y fz r' y\<sigma> zD fldAss.prems(6) have ?e if "k\<^sub>2 = \<one>" using that
        using lemma_2\<^sub>2' lemma_5\<^sub>2\<^sub>v
        using reaches\<^sub>v_upd_other initialization_substitution_value unfolding c\<^sub>5_def
        by (metis option.sel)
      moreover note 42
      ultimately show "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>') (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by auto
    qed
    have
      22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
          reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
          (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
          (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
            free (the (\<Gamma> y)) \<and>
            reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
            reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>\<^sub>1 \<iota>\<^sub>2
      let ?thesis =
        "reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
         (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
         (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
            free (the (\<Gamma> y)) \<and>
            reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
            reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
      assume
        "\<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h)"
        "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h)"
        "reaches (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)) \<iota>\<^sub>1 \<iota>\<^sub>2"
      then have
        r: "reaches ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) \<iota>\<^sub>1 \<iota>\<^sub>2" by simp
      have ?thesis if "k\<^sub>1 = \<zero>" and "\<not> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2"
      proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)")
        case True
        with r fldAss.hyps show ?thesis using reaches\<^sub>v_reaches reaches\<^sub>v_trans reaches_upd_other
          using value.discI value.sel by (metis option.sel)
      next
        case False
        from that 45 have
          "free (the (\<Gamma> x))" by simp
        with r that(2) xD fldAss.hyps have
          "\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y))"
          using reaches\<^sub>v_def reaches_upd_other
          using value.simps(4) by (metis option.sel)
        moreover
        from r y\<sigma> that(2) have
          "\<exists> z \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2)" using lemma_5\<^sub>2
          using initialization_substitution_value reaches_upd_other by (metis option.sel)
        ultimately show ?thesis by blast
      qed
      moreover
      have ?thesis if "k\<^sub>2 = \<one>" and "\<not> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2"
      proof (cases "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)")
        case True
        with r fldAss.hyps show ?thesis using reaches\<^sub>v_reaches reaches\<^sub>v_trans reaches_upd_other
          using value.discI value.sel by (metis option.sel)
      next
        case False
        with 40 41 \<Gamma>y r y\<sigma> show ?thesis using lemma_2\<^sub>2' lemma_5\<^sub>2 using reaches_upd_other that(1)
          by (metis domD option.sel)
      qed
      moreover note 42
      ultimately show ?thesis by blast
    qed
    from 39 43 45 have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" using fields_dom by auto
    from fldAss.hyps have
      "x \<in> dom \<sigma>" by blast
    moreover note fldAss.hyps
    moreover from 38 have
      "\<Gamma> x = Some (C, k\<^sub>1, !)" by auto
    moreover note fldAss.prems(5)
    ultimately have
      V: "h \<turnstile> value.non_null \<iota> : (C, !)" using c\<^sub>4_def by (metis option.sel value.distinct(1))
    then have
      "\<iota> \<in> dom (\<chi>\<^sub>c h)" by auto
    moreover from 39 have
      "(C, f) \<in> dom fType" by blast
    then have
      "f \<in> set (fields C)" using fields_dom by simp
    with V have
      "f \<in> set (fields (the (\<chi>\<^sub>c h \<iota>)))" using monotonic_fields by fastforce
    moreover note fldAss.prems(9)
    ultimately have
      "(\<iota>, f) \<in> dom (\<chi>\<^sub>v h)" using access_dom by blast
    moreover from 40 9 have
      \<sigma>yE: "\<sigma> y \<noteq> None" by auto
    ultimately have
      d: "dom ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) = dom (\<chi>\<^sub>v h)" by auto
    with fldAss.prems(9) have
      Wh': "wf_heap_dom ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)"
      unfolding wf_heap_dom_def by simp
    have
      wi: "wf_heap_image ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)" unfolding wf_heap_image_def
    proof (rule, rule)
      from 44 have
        \<sigma>y: "ref (the (\<sigma> y)) \<in> dom (\<chi>\<^sub>c h)" if "the (\<sigma> y) \<noteq> null" using that
        using runtime_type_assignment.cases value.sel by metis
      fix v
      let ?thesis = "v = Some null \<or>
         (\<exists>\<iota>'. v = Some (value.non_null \<iota>') \<and> \<iota>' \<in> dom (\<chi>\<^sub>c ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)))"
      assume
        A: "v \<in> \<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h) ` dom (\<chi>\<^sub>v ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h))"
      then have
        "v \<in> (\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y) ` dom ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y))" by simp
      with d have
        "v \<in> (\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y) ` dom (\<chi>\<^sub>v h)" by simp
      then have
        "\<exists> \<iota>' f'. (\<iota>', f') \<in> dom (\<chi>\<^sub>v h) \<and> ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (\<iota>', f') = v"
        by (metis (no_types) image_iff old.prod.exhaust)
      then obtain \<iota>' f' where
        d': "(\<iota>', f') \<in> dom (\<chi>\<^sub>v h)" and
        v: "((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y)) (\<iota>', f') = v" by blast
      show ?thesis
      proof (cases "(\<iota>', f') = (\<iota>, f)")
        case True
        with \<sigma>yE \<sigma>y v show ?thesis by force
      next
        case False
        then show ?thesis
        proof (cases "v")
          case None
          with d d' v show ?thesis by blast
        next
          case (Some a)
          with fldAss.prems(10) False d d' v show ?thesis using wf_heap_image_def \<chi>\<^sub>c_conv
            using fun_upd_def image_iff by metis
        qed
      qed
    qed
    from fldAss.prems(12) have
      uh: "unbounded_heap ((\<chi>\<^sub>v h)((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h)" unfolding unbounded_heap_def by simp
    from 9 10 11 12 13 TN 14 15 16 17 18 19 g20' 20 g21' 22 gS Wh' wi uh show ?case by blast
  next
    case (fldAssBad \<sigma> x h f y)
    then obtain C k\<^sub>1 where
      "\<Gamma>, \<Delta> \<turnstile> \<V> x : (C, k\<^sub>1, !)" by (meson TfldAssE)
    then have
      "x \<in> \<Delta>" and
      "\<Gamma> x = Some (C, k\<^sub>1, !)" using lemma_2\<^sub>4 by auto
    moreover then have
      "\<not> nullable (the (\<Gamma> x))" by simp
    moreover note fldAssBad.prems(4)
    ultimately have
      "\<sigma> x \<noteq> Some null" by force
    with fldAssBad.hyps have
      "False" by simp
    then show ?case by simp
  next
    case (call \<sigma> y \<iota> h D m \<gamma> X\<^sub>i S Y\<^sub>j x\<^sub>i y\<^sub>j \<sigma>\<^sub>1 z\<^sub>i s h' \<sigma>\<^sub>2 \<epsilon> x \<Gamma> \<Delta> \<Delta>' \<Sigma>)
    from call.prems(2,3,4,5,6,7,8) have
      D: "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" unfolding good_configuration_def by blast
    from call.hyps(3,4) have
      dxt: "this \<notin> set x\<^sub>i" using method_no_this_in_arguments by fastforce
    from call.hyps(3,5) have
      dyt: "this \<notin> set y\<^sub>j" using method_no_this_in_locals by fastforce
    from call.hyps(3,4,5) have
      dxy: "set x\<^sub>i \<inter> set y\<^sub>j = {}" using method_distinct_variables
      unfolding method_arguments_def method_locals_def
      by (metis distinct_append fst_conv snd_conv)
    from call.hyps(3,4) have
      dxr: "res \<notin> set x\<^sub>i" using method_no_result_in_arguments by fastforce
    from call.hyps(3,5) have
      dyr: "res \<notin> set y\<^sub>j" using method_no_result_in_locals by fastforce
    from call.hyps(3,4) have
      dx: "distinct x\<^sub>i" using method_distinct_variables unfolding method_arguments_def
      by (metis distinct_append fst_conv snd_conv)
    from call.hyps(3,5) have
      dy: "distinct y\<^sub>j" using method_distinct_variables unfolding method_locals_def
      by (metis distinct_append snd_conv)
    with call.hyps(6) have
      \<sigma>\<^sub>1y\<^sub>j: "map \<sigma>\<^sub>1 y\<^sub>j =  map (\<lambda>x. Some null) y\<^sub>j" using fun_updr_same
      by (metis length_map order_refl take_all)
    from call.hyps(1) have
      yd\<sigma>: "y \<in> dom \<sigma>" by (simp add: domI)
    from call.prems(8) obtain C k X\<^sub>i' Y\<^sub>j' S' \<theta> T\<^sub>i  and \<gamma>' :: \<gamma> where
      \<Delta>': "\<Delta>' = insert x \<Delta>" and 
      59: "\<Sigma> = {}" and
      60: "\<Gamma>, \<Delta> \<turnstile> (\<V> y) : (C, k, !)" and
      62: "mSig (C, m) = Some (\<gamma>', X\<^sub>i', S', Y\<^sub>j')" and
      63: "\<theta> \<in> instances S' ((C, \<gamma>', !) # variable_types X\<^sub>i')" and
      61: "\<Gamma>, \<Delta> \<turnstile> z\<^sub>i : T\<^sub>i" and
      64: "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta> \<circ> \<tau>) X\<^sub>i'" and
      65: "\<ss> \<theta> S' \<sqsubseteq> \<Gamma> x" and
      66: "Some k \<sqsubseteq> \<ee> \<theta> \<gamma>'"
      using TcallE unfolding variable_type_def by auto
    from call.prems(8) have
      58: "\<Delta>' = \<Delta> \<union> {x}" using TcallE by blast
    from call.hyps(1) call.prems(5) yd\<sigma> have
      h\<iota>t: "h \<turnstile> non_null \<iota> : type_as_t (the (\<Gamma> y))" using c\<^sub>4_iff value.distinct(1) by (metis option.sel)
    then have
      "the (cls h \<iota>) \<sqsubseteq> base (the (\<Gamma> y))" using lemma_4\<^sub>2
      using type_as_t_def value.discI value.sel by metis
    with 60 call.hyps(2) have
      DC: "D \<sqsubseteq> C" by auto
    from 62 call.hyps(3) have
      "method_arguments (the (mSig (C, m))) = X\<^sub>i'"
      "method_arguments (the (mSig (D, m))) = X\<^sub>i" by simp_all
    with 62 DC have
      X\<^sub>i'X\<^sub>i: "\<tau> X\<^sub>i' \<sqsubseteq> \<tau> X\<^sub>i" using methods_contravariant_arguments methods_dom by blast
    from 62 call.hyps(3) have
      "method_target (the (mSig (C, m))) = \<gamma>'"
      "method_target (the (mSig (D, m))) = \<gamma>" by simp_all
    with DC 62 have
      \<gamma>\<gamma>: "\<gamma>' \<sqsubseteq> \<gamma>" using methods_contravariant_target methods_dom by blast
    from 63 have
      "vars [(C, \<gamma>', !)] \<subseteq> dom \<theta>" using vars_cons instanceD(2) instances_instance by blast
    then have
      "var \<gamma>' \<subseteq> dom \<theta>" by (simp add: vars_value')
    from 62 call.hyps(3) have
      "method_result (the (mSig (C, m))) = S'"
      "method_result (the (mSig (D, m))) = S" by simp_all
    with DC 62 have
      SS': "S \<sqsubseteq> S'" using methods_covariant_result methods_dom by blast
    from 63 have
      "var1 S' \<subseteq> dom \<theta>" using instanceD(1) instances_instance by blast
    with 65 have
      x\<Gamma>: "x \<in> dom \<Gamma>" using option_leq_dom by (fastforce simp add: signature_extension)
    with call.prems(2) have
      x\<sigma>: "x \<in> dom \<sigma>" using c\<^sub>1_def by blast
    from SS' have
      "[S] \<sqsubseteq> [S']" using leq_refl list_leq_cons by blast
    then have
      "vars [S'] \<subseteq> vars [S]" using vars_le by simp
    from 64 have
      lTX': "length T\<^sub>i = length X\<^sub>i'" using list_leq_length by fastforce
    moreover from 61 have
      lzT: "length z\<^sub>i = length T\<^sub>i" using variables_typing_length by simp
    ultimately have
      lzX': "length z\<^sub>i = length X\<^sub>i'" by simp
    from 63 have
      "vars (\<tau> X\<^sub>i') \<subseteq> dom \<theta>" using instance_cons instanceD(2) instances_instance by blast
    moreover with X\<^sub>i'X\<^sub>i have
      "vars (\<tau> X\<^sub>i) \<subseteq> dom \<theta>" using vars_le by blast
    moreover from 64 have
      "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (map \<tau> X\<^sub>i')" using map_map by simp
    moreover note X\<^sub>i'X\<^sub>i
    ultimately have
      "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (map \<tau> X\<^sub>i)" unfolding variable_types_def variable_type_def
      using substitutions_subtyping leq_trans by blast
    then have
      67: "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (\<tau> X\<^sub>i)" unfolding variable_types_def variable_type_def by simp
    with 61 have
      \<Gamma>z\<^sub>iX\<^sub>i: "map \<Gamma> z\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (\<tau> X\<^sub>i)" using variables_typing by simp
    from 67 have
      lTX: "length T\<^sub>i = length X\<^sub>i" using list_leq_length length_map by fastforce
    with lzT call.hyps(4) have
      lzx: "length z\<^sub>i = length x\<^sub>i" unfolding variable_names_def by simp
    moreover from dxy call.hyps(6) dxr have
      "map \<sigma>\<^sub>1 x\<^sub>i = map (\<sigma>\<^sub>\<emptyset> (this \<mapsto> non_null \<iota>) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr>) x\<^sub>i"
      using fun_updr_others using map_fun_upd by metis
    moreover note dx
    ultimately have
      \<sigma>\<^sub>1x\<^sub>i: "map \<sigma>\<^sub>1 x\<^sub>i = map \<sigma> z\<^sub>i" using fun_updr_same by (metis length_map order_refl take_all)
    then have
      \<sigma>\<^sub>1x\<^sub>in:"\<forall> i. i < length x\<^sub>i \<longrightarrow> \<sigma>\<^sub>1 (x\<^sub>i ! i) = \<sigma> (z\<^sub>i ! i)" using nth_map_eq by blast
    from call.hyps(6) dxt dyt this_res have
      \<sigma>\<^sub>1t: "\<sigma>\<^sub>1 this = Some (non_null \<iota>)" using fun_updr_other using fun_upd_apply by metis
    from call.hyps(6) dyr have
      \<sigma>\<^sub>1r: "\<sigma>\<^sub>1 res = Some null" using fun_updr_other by (metis fun_upd_same)
    from call.hyps(5) have
      lyY: "length y\<^sub>j = length Y\<^sub>j" unfolding variable_names_def by simp
    then have
      lyT: "length y\<^sub>j = length (\<tau> Y\<^sub>j)" unfolding variable_types_def by simp
    from call.hyps(4) have
      lxX: "length x\<^sub>i = length X\<^sub>i" by simp
    then have
      lxT: "length x\<^sub>i = length (\<tau> X\<^sub>i)" unfolding variable_types_def by simp
    from call.hyps(3) have
      "\<turnstile>\<^sub>m D, m" using wf_method methods_dom by (simp add: domI)
    with call.hyps(3,7) obtain \<Delta>'' \<Sigma>' \<theta>\<^sub>l where
      "wf_statement s" and
         \<comment> \<open>The report uses @{term \<open>committed (the (\<ss> \<theta> S)) \<Longrightarrow> res \<in> \<Delta>''\<close>} in (73),
            but @{thm wfMeth} talks about nullity, not initialization status.\<close>
      73: "\<not> (nullable S) \<Longrightarrow> res \<in> \<Delta>''" and
      "\<theta>\<^sub>l = instances S ((D, \<gamma>, !) # variable_types X\<^sub>i)" and
      "\<forall> \<theta> \<in> \<theta>\<^sub>l. (\<upsilon> (\<upsilon> Map.empty \<theta> X\<^sub>i (this \<mapsto> (D, the (\<ee> \<theta> \<gamma>), !))) \<theta> Y\<^sub>j)
       (res := \<ss> \<theta> S), set (\<eta> X\<^sub>i) \<union> {this} \<turnstile> s | \<Delta>'', \<Sigma>'"
      by fastforce
    moreover from 63 have
      "instance \<theta> S' ((C, \<gamma>', !) # \<tau> X\<^sub>i')" using instances_instance by simp
    with DC call.hyps(3) 62 obtain \<theta>' \<Gamma>' \<Delta>''' where
      \<theta>': "\<theta>' \<in> instances S ((D, \<gamma>, !) # variable_types X\<^sub>i)" and
      \<theta>'\<theta>\<gamma>: "\<ee> \<theta>' \<gamma> = \<ee> \<theta> \<gamma>" and
      \<ss>SS': "\<ss> \<theta>' S \<sqsubseteq> \<ss> \<theta> S'" and
      \<ss>X\<^sub>i: "map (\<ss> \<theta>') (\<tau> X\<^sub>i) = map (\<ss> \<theta>) (\<tau> X\<^sub>i)" and
      70: "\<Gamma>' = (\<upsilon> (\<upsilon> Map.empty \<theta>' X\<^sub>i (this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !))) \<theta>' Y\<^sub>j) (res := \<ss> \<theta>' S)" and
         \<comment> \<open>The report uses @{term \<Delta>'} in (58) and (70) — a clear abuse of notation.
            To avoid any confusion, a new variable is introduced in (70).\<close>
      71: "\<Delta>''' = set (\<eta> X\<^sub>i) \<union> {this}"
      using lemma_3\<^sub>4 by metis
    ultimately have
      72: "\<Gamma>', \<Delta>''' \<turnstile> s | \<Delta>'', \<Sigma>'" by auto
    from \<theta>' have
      \<theta>'SN: "\<ss> \<theta>' S \<noteq> None" using  expectation_substitutions_signature_extension_none
        instances_instance instanceD(1) var1_def vars_value' by blast
    from 70 have
      \<Gamma>'res: "\<Gamma>' res = \<ss> \<theta>' S" by simp
    from 70 dyt call.hyps(5) have
      \<Gamma>'t: "\<Gamma>' this = Some (D, the (\<ee> \<theta>' \<gamma>), !)" unfolding xS_upds_def
      using this_res fun_updr_other variable_names_def by (metis (no_types) fun_upd_apply)
    have
      \<Gamma>'x\<^sub>i': "map \<Gamma>' x\<^sub>i = map (\<ss> \<theta>') (\<tau> X\<^sub>i)"
    proof -
      from 70 dxr have
        "map \<Gamma>' x\<^sub>i = map ((\<upsilon> (\<upsilon> Map.empty \<theta>' X\<^sub>i(this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !))) \<theta>' Y\<^sub>j)) x\<^sub>i" by simp
      with call.hyps(5) dxy have
        "map \<Gamma>' x\<^sub>i = map ((\<upsilon> Map.empty \<theta>' X\<^sub>i(this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !)))) x\<^sub>i"
        unfolding xS_upds_def variable_names_def using fun_updr_others by metis
      with dxt have
        "map \<Gamma>' x\<^sub>i = map (\<upsilon> Map.empty \<theta>' X\<^sub>i) x\<^sub>i" by simp
      moreover from lxX have
        "length x\<^sub>i \<le> length X\<^sub>i" by simp
      moreover note call.hyps(4) dx
      ultimately have
        "map \<Gamma>' x\<^sub>i = take (length x\<^sub>i) (map (\<ss> \<theta>' \<circ> snd) X\<^sub>i)" unfolding xS_upds_def variable_names_def
        using fun_updr_same using length_map by metis
      with lxX show ?thesis by simp
    qed
    with \<ss>X\<^sub>i have
      \<Gamma>'x\<^sub>i: "map \<Gamma>' x\<^sub>i = map (\<ss> \<theta>) (\<tau> X\<^sub>i)" by simp
    with \<Gamma>z\<^sub>iX\<^sub>i have
      \<Gamma>z\<^sub>ix\<^sub>i: "map \<Gamma> z\<^sub>i \<sqsubseteq> map \<Gamma>' x\<^sub>i" unfolding variable_types_def variable_type_def by simp
    from \<theta>' call.hyps(3) obtain T\<^sub>j where
      mY: "map (\<ss> \<theta>') (\<tau> Y\<^sub>j) = map Some T\<^sub>j" using instances_instance lemma_3\<^sub>2'''' by blast
    with lyT have
      "length y\<^sub>j = length (map Some T\<^sub>j)" using length_map by metis
    then have
      leyS: "length y\<^sub>j \<le> length T\<^sub>j" by simp
    from \<theta>' obtain Q\<^sub>i where
      mX: "map (\<ss> \<theta>') (\<tau> X\<^sub>i) = map Some Q\<^sub>i" using instances_instance lemma_3\<^sub>2'' by blast
    with lxT have
      "length x\<^sub>i = length (map Some Q\<^sub>i)" using length_map by metis
    then have
      lexS: "length x\<^sub>i \<le> length Q\<^sub>i" by simp
    from 70 \<theta>'SN have
      "dom \<Gamma>' = dom (\<upsilon> (\<upsilon> Map.empty \<theta>' X\<^sub>i (this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !))) \<theta>' Y\<^sub>j) \<union> {res}" by auto
    with mY call.hyps(5) have
      "dom \<Gamma>' = dom ((\<upsilon> Map.empty \<theta>' X\<^sub>i (this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !))) \<lparr>y\<^sub>j := map Some T\<^sub>j\<rparr>) \<union> {res}"
      unfolding xS_upds_def variable_names_def variable_types_def by simp
    with leyS have
      "dom \<Gamma>' = dom (\<upsilon> Map.empty \<theta>' X\<^sub>i (this \<mapsto> (D, the (\<ee> \<theta>' \<gamma>), !))) \<union> set y\<^sub>j \<union> {res}"
      using fun_updr_some_le_dom by metis
    with mX call.hyps(4) have
      "dom \<Gamma>' = dom (Map.empty \<lparr>x\<^sub>i := map Some Q\<^sub>i\<rparr>) \<union> {this} \<union> set y\<^sub>j \<union> {res}" by simp
    with lexS have
      d\<Gamma>'D: "dom \<Gamma>' = set x\<^sub>i \<union> {this} \<union> set y\<^sub>j \<union> {res}" using fun_updr_some_le_dom dom_empty
      by (metis sup_bot.left_neutral)
    from 61 have
      "set z\<^sub>i \<subseteq> dom \<Gamma>" using variables_typing_dom by simp
    with call.prems(2) have
      z\<^sub>i\<sigma>: "set z\<^sub>i \<subseteq> dom \<sigma>" by simp
    obtain ny\<^sub>j :: "'b value list" where
      "map Some ny\<^sub>j = map (\<lambda>x. Some null) y\<^sub>j" by (induction y\<^sub>j) (simp, metis list.simps(9))
    with call.hyps(6) have
      "dom \<sigma>\<^sub>1 = dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> value.non_null \<iota>) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr> (res \<mapsto> null) \<lparr>y\<^sub>j := map Some ny\<^sub>j\<rparr>)" and
      "length y\<^sub>j \<le> length ny\<^sub>j" using map_eq_imp_length_eq by fastforce+
    then have
      "dom \<sigma>\<^sub>1 = dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> value.non_null \<iota>) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr> (res \<mapsto> null)) \<union> set y\<^sub>j"
      using fun_updr_some_le_dom by metis
    then have
      "dom \<sigma>\<^sub>1 = dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> value.non_null \<iota>) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr>) \<union> {res} \<union> set y\<^sub>j" by simp
    with z\<^sub>i\<sigma> lexS lzx have
      "dom \<sigma>\<^sub>1 = dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> value.non_null \<iota>)) \<union> set x\<^sub>i \<union> {res} \<union> set y\<^sub>j"
      using fun_updr_some_le_dom substitutions_value
      by (metis (no_types) inf.absorb_iff2 inf.cobounded2 map_eq_imp_length_eq)
    then have
      d\<sigma>\<^sub>1D: "dom \<sigma>\<^sub>1 = {this} \<union> set x\<^sub>i \<union> {res} \<union> set y\<^sub>j" using dom_empty by simp
    from \<sigma>\<^sub>1t call.hyps(1) have
      \<sigma>yt: "\<sigma> y = \<sigma>\<^sub>1 this" by simp
    have
      iit: "initialization (the (\<Gamma> y)) = initialization (the (\<Gamma>' this))"
      if "initialization (the (\<Gamma>' this)) \<noteq> \<diamondop>"
    proof -
      from that \<Gamma>'t \<gamma>\<gamma> \<theta>'\<theta>\<gamma> \<open>var \<gamma>' \<subseteq> dom \<theta>\<close> have
        "\<ee> \<theta> \<gamma> = Some (initialization (the (\<Gamma>' this)))"
        using initialization initialization_extension_value var_le
        by (metis option.sel subset_trans)
      with \<gamma>\<gamma> 66 60 show
        ?thesis unfolding initialization_extension_def using that
        by (cases "initialization (the (\<Gamma>' this))") auto
    qed
    have
      ixz: "\<forall> i < length x\<^sub>i. \<sigma> (z\<^sub>i ! i) = \<sigma>\<^sub>1 (x\<^sub>i ! i) \<and>
        (initialization (the (\<Gamma>' (x\<^sub>i ! i))) \<noteq> \<diamondop> \<longrightarrow>
          (initialization (the (\<Gamma> (z\<^sub>i ! i))) = initialization (the (\<Gamma>' (x\<^sub>i ! i)))))"
    proof (rule allI, rule impI)
      fix i
      assume
        i: "i < length x\<^sub>i"
      with \<Gamma>z\<^sub>ix\<^sub>i lzx have
        "\<Gamma> (z\<^sub>i ! i) \<sqsubseteq> \<Gamma>' (x\<^sub>i ! i)" unfolding list_leq_def using specialization_list_item
        by (metis length_map nth_map)
      moreover from \<open>set z\<^sub>i \<subseteq> dom \<Gamma>\<close> i lzx have
        "z\<^sub>i ! i \<in> dom \<Gamma>" by (simp add: subset_iff)
      moreover note d\<Gamma>'D
      ultimately have
        "the (\<Gamma> (z\<^sub>i ! i)) \<sqsubseteq> the (\<Gamma>' (x\<^sub>i ! i))" using option_leq
        by (cases "\<Gamma> (z\<^sub>i ! i)"; cases "\<Gamma>' (x\<^sub>i ! i)") (simp_all add: domIff)
      then have
        "initialization (the (\<Gamma> (z\<^sub>i ! i))) = initialization (the (\<Gamma>' (x\<^sub>i ! i)))"
        if "initialization (the (\<Gamma>' (x\<^sub>i ! i))) \<noteq> \<diamondop>"
        using that by (auto dest: initialization_subtyping)
      with \<sigma>\<^sub>1x\<^sub>in i lzx show
        "\<sigma> (z\<^sub>i ! i) = \<sigma>\<^sub>1 (x\<^sub>i ! i) \<and>
        (initialization (the (\<Gamma>' (x\<^sub>i ! i))) \<noteq> \<diamondop> \<longrightarrow>
          initialization (the (\<Gamma> (z\<^sub>i ! i))) = initialization (the (\<Gamma>' (x\<^sub>i ! i))))" by simp
    qed
    with lzx have
      iix: "\<forall> x \<in> set x\<^sub>i. \<exists> z \<in> set z\<^sub>i. \<sigma> z = \<sigma>\<^sub>1 x \<and>
        (initialization (the (\<Gamma>' x)) \<noteq> \<diamondop> \<longrightarrow>
          (initialization (the (\<Gamma> z)) = initialization (the (\<Gamma>' x))))" by (metis in_set_conv_nth)
    from ixz lzx have
      uzx: "\<forall> i < length z\<^sub>i. \<sigma>\<^sub>1 (x\<^sub>i ! i) = \<sigma> (z\<^sub>i ! i) \<and>
        (unclassified (the (\<Gamma> (z\<^sub>i ! i))) \<longrightarrow> unclassified (the (\<Gamma>' (x\<^sub>i ! i))))" by auto
    note call.prems(1)
    moreover from d\<Gamma>'D d\<sigma>\<^sub>1D have
      "\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>1" unfolding c\<^sub>1_def by simp
    moreover note call.prems(3)
    moreover have
      "\<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1" unfolding c\<^sub>3_def
    proof (rule ballI, rule impI)
      fix x
      let ?thesis = "the (\<sigma>\<^sub>1 x) \<noteq> null"
      assume
        xD: "x \<in> dom \<sigma>\<^sub>1" and
        xN: "nullity (the (\<Gamma>' x)) \<noteq> ? \<and> x \<in> \<Delta>'''"
      then have
        nx: "nullity (the (\<Gamma>' x)) = !" and
        x\<Delta>: "x \<in> \<Delta>'''" using n.exhaust by blast+
      from \<sigma>\<^sub>1t have
        ?thesis if "x = this" using that by simp
      moreover have
        ?thesis if "x \<in> set x\<^sub>i"
      proof -
        from that d\<Gamma>'D have
          xd\<Gamma>': "x \<in> dom \<Gamma>'" by simp
        from that obtain i where
          i: "i < length x\<^sub>i" and
          xi: "x\<^sub>i ! i = x" by (meson in_set_conv_nth)
        with \<sigma>\<^sub>1x\<^sub>in have
          \<sigma>\<^sub>1x: "\<sigma>\<^sub>1 x = \<sigma> (z\<^sub>i ! i)" by blast
        with \<Gamma>z\<^sub>ix\<^sub>i i xi have
          "\<Gamma> (z\<^sub>i ! i) \<sqsubseteq> \<Gamma>' x" using list_leq_iff by (metis length_map nth_map)
        with xd\<Gamma>' have
          "the (\<Gamma> (z\<^sub>i ! i)) \<sqsubseteq> the (\<Gamma>' x)" using option_leq by (cases "\<Gamma> (z\<^sub>i ! i)") auto
        with nx have
          zN: "nullity (the (\<Gamma> (z\<^sub>i ! i))) = !" using nullity_notnullable_subtyping by blast
        moreover from 61 i lzx have
          "\<Gamma>, \<Delta> \<turnstile> \<V> (z\<^sub>i ! i) : T\<^sub>i ! i" using variables_typing_elem_type by simp
        ultimately have
          "z\<^sub>i ! i \<in> \<Delta>" using lemma_2\<^sub>4' by auto
        with call.prems(4) zN xN i lzx z\<^sub>i\<sigma> \<sigma>\<^sub>1x show
          ?thesis using subsetD by fastforce
      qed
      moreover note 71 d\<sigma>\<^sub>1D call.hyps(4) x\<Delta>
      ultimately show ?thesis by blast
    qed
    moreover have
      "\<Gamma>' \<turnstile>\<^sub>4 h, \<sigma>\<^sub>1" unfolding c\<^sub>4_def
    proof (rule allI, rule allI, rule allI, rule ballI, rule impI)
      fix C' k' n' x
      let ?thesis = "h \<turnstile> the (\<sigma>\<^sub>1 x) : (C', n')"
      assume
        xD: "x \<in> dom \<sigma>\<^sub>1"
      moreover assume
        "the (\<sigma>\<^sub>1 x) \<noteq> null \<and> the (\<Gamma>' x) = (C', k', n')"
      then have
        xN: "the (\<sigma>\<^sub>1 x) \<noteq> null" and
        \<Gamma>'x: "the (\<Gamma>' x) = (C', k', n')" by simp_all
      from call.hyps(6) dyr have
        "\<sigma>\<^sub>1 res = (\<sigma>\<^sub>\<emptyset>(this \<mapsto> value.non_null \<iota>) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr> (res \<mapsto> null)) res" 
        using fun_updr_other by metis
      then have
        "\<sigma>\<^sub>1 res = Some null" by simp
      with xN have ?thesis if "x = res" using that by simp
      moreover from xN \<sigma>\<^sub>1y\<^sub>j have ?thesis if "x \<in> set y\<^sub>j" using that by simp
      moreover have ?thesis if "x = this" using that RAddr
      proof -
        from \<sigma>\<^sub>1t have
          "the (\<sigma>\<^sub>1 this) = non_null \<iota>" by simp
        moreover from \<Gamma>'t \<Gamma>'x have
          "C' = D"
          "n' = !" using that by simp_all
        with call.hyps(2) have
          "the (cls h \<iota>) \<sqsubseteq> C'" using leq_refl by simp
        moreover note h\<iota>t
        ultimately show ?thesis unfolding type_as_t_def using that by auto
      qed
      moreover have ?thesis if "x \<in> set x\<^sub>i" using that
      proof -
        from that obtain i where
          i: "i < length x\<^sub>i" and
          x: "x\<^sub>i ! i = x" by (meson in_set_conv_nth)
        from \<sigma>\<^sub>1x\<^sub>in i have
          xz: "the (\<sigma>\<^sub>1 (x\<^sub>i ! i)) = the (\<sigma> (z\<^sub>i ! i))" by simp
        show ?thesis
        proof (cases "the (\<sigma> (z\<^sub>i ! i))")
          case (non_null \<iota>)
          obtain C'' k'' n'' where
            \<Gamma>z: "the (\<Gamma> (z\<^sub>i ! i)) = (C'', k'', n'')"
            using prod_cases3 by blast
          with call.prems(5) lzx i z\<^sub>i\<sigma> have
            c: "the (cls h \<iota>) \<sqsubseteq> C''" unfolding c\<^sub>4_def using lemma_4\<^sub>2
            using non_null x xN xz value.sel by (metis nth_mem subset_code(1))
          from \<open>set z\<^sub>i \<subseteq> dom \<Gamma>\<close> i lzx have
            zid\<Gamma>: "z\<^sub>i ! i \<in> dom \<Gamma>" by (metis nth_mem subset_iff)
          from \<open>\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>1\<close> x xD have
            xid\<Gamma>: "x\<^sub>i ! i \<in> dom \<Gamma>'" by simp
          from \<Gamma>z\<^sub>ix\<^sub>i i lzx have
            "\<Gamma> (z\<^sub>i ! i) \<sqsubseteq> \<Gamma>' (x\<^sub>i ! i)" by (simp add: list_leq_iff)
          with zid\<Gamma> xid\<Gamma> have
            "the (\<Gamma> (z\<^sub>i ! i)) \<sqsubseteq> the (\<Gamma>' (x\<^sub>i ! i))" using option_leq by auto
          with \<Gamma>z \<Gamma>'x c x have
            "the (cls h \<iota>) \<sqsubseteq> C'" using base_subtyping using leq_trans by force
          with i \<sigma>\<^sub>1x\<^sub>in call.prems(5) x xD xN show ?thesis using c\<^sub>4_iff
            using initialization_substitution_value non_null runtime_type_assignment.simps
            by (metis (mono_tags) domI value.sel)
        next
          case null
          with xz x xN show ?thesis  by simp
        qed
      qed
      moreover note d\<sigma>\<^sub>1D
      ultimately show ?thesis by blast
    qed
    moreover have
      "\<Gamma>' \<turnstile>\<^sub>5 h, \<sigma>\<^sub>1" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI)
      fix x z
      let ?thesis = "deep_init\<^sub>v h (the (\<sigma>\<^sub>1 x)) \<and>
           (free (the (\<Gamma>' z)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 z)))"
      assume
        xD: "x \<in> dom \<sigma>\<^sub>1" and
        zD: "z \<in> dom \<sigma>\<^sub>1" and
        cx: "committed (the (\<Gamma>' x))"
      from \<sigma>\<^sub>1r have ?thesis if "x = res" using that reaches\<^sub>v_left_null by simp
      moreover have ?thesis if "x = this" using that
      proof -
        have xt: "x = this" using that .
        with iit cx have
          cy: "committed (the (\<Gamma> y))" by simp
        moreover note \<sigma>\<^sub>1t call.prems(6) that \<sigma>yt
        ultimately have
          "deep_init\<^sub>v h (the (\<sigma>\<^sub>1 x))" unfolding c\<^sub>5_def by fastforce
        moreover
          let ?r = "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 z))"
          have "free (the (\<Gamma>' z)) \<longrightarrow> ?r"
          proof (rule impI)
            assume
              fz: "free (the (\<Gamma>' z))"
            note d\<sigma>\<^sub>1D zD
            moreover from \<sigma>\<^sub>1r have ?r if "z = res" using reaches\<^sub>v_right_null that by simp
            moreover from \<sigma>\<^sub>1y\<^sub>j have ?r if "z \<in> set y\<^sub>j" using reaches\<^sub>v_right_null that by simp
            moreover from cx fz xt have ?r if "z = this" using that by simp
            moreover have ?r if "z \<in> set x\<^sub>i" using that
            proof -
              from fz iix have
                "\<exists> x \<in> set z\<^sub>i. initialization (the (\<Gamma> x)) = initialization (the (\<Gamma>' z)) \<and> \<sigma> x = \<sigma>\<^sub>1 z"
                using that by fastforce
              with call.prems(2) fz z\<^sub>i\<sigma> obtain x where
                "x \<in> dom \<sigma>" and
                "free (the (\<Gamma> x))" and
                "\<sigma> x = \<sigma>\<^sub>1 z" using that by auto
              with call.prems(6) xt cy \<sigma>yt yd\<sigma> show ?thesis unfolding c\<^sub>1_def c\<^sub>5_def by metis
            qed
            ultimately show ?r by blast
          qed
        ultimately show ?thesis by blast
      qed
      moreover from \<sigma>\<^sub>1y\<^sub>j have ?thesis if "x \<in> set y\<^sub>j" using that reaches\<^sub>v_left_null by simp
      moreover have ?thesis if "x \<in> set x\<^sub>i" using that
      proof -
        have xt: "x \<in> set x\<^sub>i" using that .
        from that cx iix obtain y where
          "y \<in> set z\<^sub>i" and
          cy: "committed (the (\<Gamma> y))" and
          \<sigma>yx: "\<sigma> y = \<sigma>\<^sub>1 x" by auto
        moreover note call.hyps(1) call.prems(6) that z\<^sub>i\<sigma>
        ultimately have
          "deep_init\<^sub>v h (the (\<sigma>\<^sub>1 x))" unfolding c\<^sub>5_def by (metis in_mono)
        moreover
          let ?r = "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 z))"
          have "free (the (\<Gamma>' z)) \<longrightarrow> ?r"
          proof (rule impI)
            assume
              fz: "free (the (\<Gamma>' z))"
            note d\<sigma>\<^sub>1D zD
            moreover from \<sigma>\<^sub>1r have ?r if "z = res" using reaches\<^sub>v_right_null that by simp
            moreover from \<sigma>\<^sub>1y\<^sub>j have ?r if "z \<in> set y\<^sub>j" using reaches\<^sub>v_right_null that by simp
            moreover from call.prems(6) iit \<sigma>yt fz xD yd\<sigma> cy \<sigma>yx have ?r if "z = this" using that
              unfolding c\<^sub>5_def by (metis domD domI k.distinct(3))
            moreover have ?r if "z \<in> set x\<^sub>i" using that
            proof -
              from fz iix zD call.prems(2) have
                "\<exists> x \<in> dom \<Gamma>. initialization (the (\<Gamma> x)) = initialization (the (\<Gamma>' z)) \<and> \<sigma> x = \<sigma>\<^sub>1 z"
                using that by fastforce
              with call.prems(2) fz obtain x where
                "x \<in> dom \<sigma>" and
                "free (the (\<Gamma> x))" and
                "\<sigma> x = \<sigma>\<^sub>1 z" using that by auto
              with call.prems(6) \<sigma>yx cy xD show ?thesis unfolding c\<^sub>5_def by (metis domD domI)
            qed
            ultimately show ?r by blast
          qed
        ultimately show ?thesis by blast
      qed
      moreover note d\<sigma>\<^sub>1D xD
      ultimately show ?thesis by blast
    qed
    moreover from 71 \<Gamma>'t have
      "\<Gamma>', \<Delta>''' \<turnstile>\<^sub>6" unfolding c\<^sub>6_def by simp
    moreover note 72
    moreover note call.prems(9)
    moreover note call.prems(10)
    moreover note \<open>wf_statement s\<close>
    moreover note call.prems(12)
    ultimately have
      "(\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>2) \<and>
      (\<turnstile>\<^sub>2 h') \<and>
      (\<Gamma>', \<Delta>'' \<turnstile>\<^sub>3 \<sigma>\<^sub>2) \<and>
      (\<forall>x\<in>dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h' \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma>' x))) \<and>
      (\<Gamma>' \<turnstile>\<^sub>5 h', \<sigma>\<^sub>2) \<and>
      (\<Gamma>', \<Delta>'' \<turnstile>\<^sub>6) \<and>
      \<epsilon> = ok \<and>
      (\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this \<and> dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1 \<and> h \<le> h') \<and>
      (\<forall>f\<in>\<Sigma>'.
          simple_nullity (the (fType (the (cls h (ref (the (\<sigma>\<^sub>1 this)))), f))) \<noteq> ? \<longrightarrow>
          the (\<chi>\<^sub>v h' (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h).
          \<forall>f\<in>set (fields (the (cls h \<iota>))).
             simple_nullity (the (fType (the (cls h \<iota>), f))) \<noteq> ? \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
             the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h' \<iota>) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h').
          \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h).
          reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>) \<longrightarrow>
          reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>)) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h').
          \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
             committed (the (\<Gamma>' x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1.
                 committed (the (\<Gamma>' y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h).
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>'))) \<longrightarrow>
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))) \<and>
      (\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h).
          \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h).
             reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
             reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
             (\<exists>x\<in>dom \<sigma>\<^sub>1.
                 Type.committed (the (\<Gamma>' x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
             (\<exists>y\<in>dom \<sigma>\<^sub>1.
                 \<exists>z\<in>dom \<sigma>\<^sub>1.
                    Type.free (the (\<Gamma>' y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))) \<and>
      (\<forall>f\<in>\<Sigma>'. f \<in> set (fields (base (the (\<Gamma>' this))))) \<and>
      wf_heap_dom h' \<and>
      wf_heap_image h' \<and>
      unbounded_heap h'" by (rule call.IH)
    then have
      IH_9: "\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>2" and
      10: "\<turnstile>\<^sub>2 h'" and
      IH_11: "\<Gamma>', \<Delta>'' \<turnstile>\<^sub>3 \<sigma>\<^sub>2" and
      IH_12: "\<forall>x\<in>dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h' \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma>' x))" and
      IH_13: "\<Gamma>' \<turnstile>\<^sub>5 h', \<sigma>\<^sub>2" and
      "\<Gamma>', \<Delta>'' \<turnstile>\<^sub>6" and
      14: "\<epsilon> = ok" and
      IH_15: "\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this \<and> dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1 \<and> h \<le> h'" and
      "\<forall>f\<in>\<Sigma>'.
          simple_nullity (the (fType (the (cls h (ref (the (\<sigma>\<^sub>1 this)))), f))) \<noteq> ? \<longrightarrow>
          the (\<chi>\<^sub>v h' (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null" and
      17: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))).
             simple_nullity (the (fType (the (cls h \<iota>), f))) \<noteq> ? \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
             the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null" and
      18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h' \<iota>" and
      IH_19: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<forall>x\<in>dom \<sigma>\<^sub>2.
                reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
                  (\<exists>y\<in>dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH_20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
                reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>) \<longrightarrow>
                reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>)" and
      "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h').
          \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
             committed (the (\<Gamma>' x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1.
                 committed (the (\<Gamma>' y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH_21': "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h).
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>'))) \<longrightarrow>
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))" and
      IH_22: "\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h).
          \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h).
             reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
             reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
             (\<exists>x\<in>dom \<sigma>\<^sub>1.
                 Type.committed (the (\<Gamma>' x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
             (\<exists>y\<in>dom \<sigma>\<^sub>1.
                 \<exists>z\<in>dom \<sigma>\<^sub>1.
                    Type.free (the (\<Gamma>' y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))" and
      "\<forall>f\<in>\<Sigma>'. f \<in> set (fields (base (the (\<Gamma>' this))))" and
      Wh': "wf_heap_dom h'" and
      wi: "wf_heap_image h'" and
      uh: "unbounded_heap h'" by linarith+
    from \<ss>SS' 65 have
      68: "\<ss> \<theta>' S \<sqsubseteq> \<Gamma> x" using leq_trans by blast
    from \<Gamma>'res "68" \<theta>'SN x\<Gamma> have
      \<Gamma>rx: "the (\<Gamma>' res) \<sqsubseteq> the (\<Gamma> x)" by fastforce
    then have
      t\<Gamma>rx: "type_as_t (the (\<Gamma>' res)) \<sqsubseteq> type_as_t (the (\<Gamma> x))"
        using subtyping_as_simple_t by fastforce
    from \<theta>' have
      "var1 S \<subseteq> dom \<theta>'" using instanceD(1) instances_instance by blast
    from 70 \<theta>'SN have
      "res \<in> dom \<Gamma>'" by simp
    with IH_9 have
      r\<sigma>\<^sub>2:"res \<in> dom \<sigma>\<^sub>2" by auto
    with x\<sigma> call.prems(2) have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma> (x := \<sigma>\<^sub>2 res)" unfolding c\<^sub>1_def by auto
    have
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma> (x := \<sigma>\<^sub>2 res)" unfolding c\<^sub>3_def
    proof (rule ballI, rule impI)
      fix z
      assume
        z\<sigma>: "z \<in> dom (\<sigma>(x := \<sigma>\<^sub>2 res))" and
        "nullity (the (\<Gamma> z)) \<noteq> ? \<and> z \<in> \<Delta>'"
      then have
        zN: "nullity (the (\<Gamma> z)) = !" and
        z\<Delta>: "z \<in> \<Delta>'" using n.exhaust by blast+
      show
        "the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z) \<noteq> null"
      proof (cases "z = x")
        case True
        moreover from 65 x\<Gamma> have
          "the (\<ss> \<theta> S') \<sqsubseteq> the (\<Gamma> x)" using option_leq by (cases "\<ss> \<theta> S'") auto
        moreover note SS' \<open>var1 S' \<subseteq> dom \<theta>\<close> zN
        ultimately have
          "nullity S = !" using nullity_notnullable_subtyping signature_extension_value
            substitution_nullity by (metis option.discI)
        moreover with \<Gamma>'res \<theta>'SN have
          "nullity (the (\<Gamma>' res)) \<noteq> ?" using substitution_nullity by fastforce
        moreover note IH_11 r\<sigma>\<^sub>2 73 True
        ultimately show ?thesis unfolding c\<^sub>3_def by simp
      next
        case False
        with 9 zN z\<Delta> z\<sigma> \<Delta>' call.prems(2,4) show ?thesis by simp
      qed
    qed
    from IH_15 have
      hh': \<open>h \<le> h'\<close> by blast
    have
      12: "\<forall> z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res)).
           the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z) \<noteq> null \<longrightarrow>
           h' \<turnstile> the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z) : type_as_t (the (\<Gamma> z))"
    proof (rule ballI, rule impI)
      fix z
      let ?thesis = "h' \<turnstile> the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z) : type_as_t (the (\<Gamma> z))"
      from IH_12 73 IH_11 \<Gamma>'res \<theta>'SN r\<sigma>\<^sub>2 have
        "h' \<turnstile> the (\<sigma>\<^sub>2 res) : type_as_t (the (\<Gamma>' res))" unfolding c\<^sub>3_def
        using RNull substitution_nullity type_as_t_def by metis
      moreover note t\<Gamma>rx
      ultimately have
        ?thesis if "z = x" using runtime_type_assignment_leq that by fastforce
      moreover assume
        z\<sigma>U: "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        \<sigma>Uz: "the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z) \<noteq> null"
      with 9 call.prems(2) have
        "z \<in> dom \<sigma>" and
        \<sigma>zN: "the (\<sigma> z) \<noteq> null" if "z \<noteq> x" using that by simp_all
      with call.prems(5) have
        "h \<turnstile> the (\<sigma> z) : type_as_t (the (\<Gamma> z))" if "z \<noteq> x" using that c\<^sub>4D' by blast
      with hh' have
        ?thesis if "z \<noteq> x" using that runtime_type_assignment_predecessor by (metis fun_upd_other)
      ultimately show ?thesis by simp
    qed
    from call.prems(7) \<Delta>' have
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" by simp
    from call.prems(11) have
      "x \<noteq> this" by simp
    with 9 IH_15 r\<sigma>\<^sub>2 x\<sigma> call.prems(2) have
      15: "(\<sigma>(x := \<sigma>\<^sub>2 res)) this = \<sigma> this \<and> dom (\<sigma>(x := \<sigma>\<^sub>2 res)) = dom \<sigma> \<and> h \<le> h'" by simp
    from 59 have
      16: "\<forall> f \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
               the (\<chi>\<^sub>v h' (ref (the (\<sigma> this)), f)) \<noteq> null" by simp
    have
      22: "\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h). \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h).
              reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
              reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
              (\<exists>x\<in>dom \<sigma>.
                  committed (the (\<Gamma> x)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
              (\<exists>y\<in>dom \<sigma>.
                  \<exists>z\<in>dom \<sigma>.
                     free (the (\<Gamma> y)) \<and>
                     reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
                     reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' \<iota>''
      let ?H1 = "reaches (\<chi>\<^sub>v h) \<iota>' \<iota>''"
      let ?H2 = "\<exists> x \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>'')"
      let ?H3 = "\<exists> y \<in> dom \<sigma>\<^sub>1. \<exists> z \<in> dom \<sigma>\<^sub>1.
                  free (the (\<Gamma>' y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma>\<^sub>1 y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>'')"
      let ?G1 = "reaches (\<chi>\<^sub>v h) \<iota>' \<iota>''"
      let ?G2 = "\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>'')"
      let ?G3 = "\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
                  free (the (\<Gamma> y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>'')"
      assume
        "\<iota>'D": "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
        "\<iota>''D": "\<iota>'' \<in> dom (\<chi>\<^sub>c h)"
      moreover assume
        "reaches (\<chi>\<^sub>v h') \<iota>' \<iota>''"
      moreover note IH_22
      ultimately have
        "?H1 \<or> ?H2 \<or> ?H3" by blast
      moreover have
        ?G1 if ?H1 using that by simp
      moreover have
        ?G2 if ?H2 using that
      proof (rule bexE)
        fix z
        assume
          zD: "z \<in> dom \<sigma>\<^sub>1"
        moreover assume
          A: "committed (the (\<Gamma>' z)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>'')"
        with iit \<sigma>yt yd\<sigma> have
          ?G2 if "z = this" using that by auto
        moreover from iix A z\<^sub>i\<sigma> have
          ?G2 if "z \<in> set x\<^sub>i" using that by fastforce
        moreover from A \<sigma>\<^sub>1y\<^sub>j have
          ?G2 if "z \<in> set y\<^sub>j" using that reaches\<^sub>v_reaches by simp
        moreover from \<sigma>\<^sub>1r A have
          ?G2 if "z = res" using that reaches\<^sub>v_left_null by simp
        moreover note d\<sigma>\<^sub>1D
        ultimately show ?G2 by blast
      qed
      moreover have
        ?G3 if ?H3
      proof -
        from that obtain u v where
          uD: "u \<in> dom \<sigma>\<^sub>1" and
          vD: "v \<in> dom \<sigma>\<^sub>1" and
          fv: "free (the (\<Gamma>' v))" and
          ru: "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>'')" and
          rv: "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma>\<^sub>1 v))" by blast
        from ru rv fv \<sigma>yt yd\<sigma> iit have
          ?G3 if "u = this" and "v = this" using that by (metis k.distinct(3))
        moreover from ru rv fv \<sigma>yt yd\<sigma> iix z\<^sub>i\<sigma> have
          ?G3 if "u = this" and "v \<in> set x\<^sub>i" using that by (metis (no_types) in_mono k.distinct(3))
        moreover from rv \<sigma>\<^sub>1y\<^sub>j have
          ?G3 if "v \<in> set y\<^sub>j" using reaches\<^sub>v_reaches that by simp
        moreover from rv \<sigma>\<^sub>1r have
          ?G3 if "v = res" using that reaches\<^sub>v_right_null by simp
        moreover from ru rv fv \<sigma>yt yd\<sigma> iit iix z\<^sub>i\<sigma> have
          ?G3 if "u \<in> set x\<^sub>i" and "v = this" using that by (metis k.distinct(3) subset_code(1))
        moreover from ru rv fv iix z\<^sub>i\<sigma> have
          ?G3 if "u \<in> set x\<^sub>i" and "v \<in> set x\<^sub>i" using that by (metis k.distinct(3) subset_code(1))
        moreover from ru \<sigma>\<^sub>1y\<^sub>j have
          ?G3 if "u \<in> set y\<^sub>j" using reaches\<^sub>v_reaches that by simp
        moreover from ru \<sigma>\<^sub>1r have
          ?G3 if "u = res" using reaches\<^sub>v_left_null that by simp
        moreover note d\<sigma>\<^sub>1D uD vD
        ultimately show
          ?G3 by blast
      qed
      ultimately show "?G1 \<or> ?G2 \<or> ?G3" by blast
    qed
    have
      19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> z \<in> dom (\<sigma>(x := \<sigma>\<^sub>2 res)).
            reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' z
      assume
        \<iota>D': "\<iota>' \<in> dom (\<chi>\<^sub>c h')" and
        zD: "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>') \<and> \<iota>' \<in> dom (\<chi>\<^sub>c h)"
      then have
        r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>')" and
        \<iota>D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" by simp_all
      show "\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>')"
      proof (cases "z = x")
        case True
        with r have
          "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 res)) (non_null \<iota>')" by simp
        with IH_19 \<iota>D' \<sigma>\<^sub>1r IH_15 \<iota>D have
          "\<exists> t \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 t)) (value.non_null \<iota>')" by blast
        then obtain t where
          t: "t \<in> dom \<sigma>\<^sub>1" and
          rt: "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 t)) (value.non_null \<iota>')" by blast
        moreover from t rt \<sigma>yt have
          ?thesis if "t = this" using that by force
        moreover from t rt iix z\<^sub>i\<sigma> have
          ?thesis if "t \<in> set x\<^sub>i" using that by (metis subsetD)
        moreover from rt \<sigma>\<^sub>1r have
          ?thesis if "t = res" using reaches\<^sub>v_left_null that by simp
        moreover from rt \<sigma>\<^sub>1y\<^sub>j have
          ?thesis if "t \<in> set y\<^sub>j" using reaches\<^sub>v_left_null that by simp
        moreover note d\<sigma>\<^sub>1D
        ultimately show ?thesis by blast
      next
        case False
        with r have
          "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> z)) (non_null \<iota>')" by simp
        moreover then obtain \<iota>'' where
           "the (\<sigma> z) = non_null \<iota>''" using reaches\<^sub>v_left_null by (metis value.exhaust)
        moreover with False call.prems(5) 15 zD have
          "\<iota>'' \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4_iff runtime_type_assignment.cases
          by (metis value.discI value.sel)
        moreover note 22 \<iota>D 15 zD
        ultimately show ?thesis using reaches\<^sub>v_reaches by (metis value.sel)
      qed
    qed
    from iix have
      cx\<^sub>i: "\<forall> y \<in> set x\<^sub>i. committed (the (\<Gamma>' y)) \<longrightarrow> (\<exists> u \<in> set z\<^sub>i. committed (the (\<Gamma> u)) \<and> \<sigma> u = \<sigma>\<^sub>1 y)"
      using committed_subtyping by fastforce
    from iix have
      fx\<^sub>i: "\<forall> y \<in> set x\<^sub>i. free (the (\<Gamma>' y)) \<longrightarrow> (\<exists> u \<in> set z\<^sub>i. free (the (\<Gamma> u)) \<and> \<sigma> u = \<sigma>\<^sub>1 y)"
      using free_subtyping by fastforce
    have
      g21': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values (\<sigma>(x := \<sigma>\<^sub>2 res)) (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>'
      assume
        \<iota>'D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
        r: "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>') (values (\<sigma>(x := \<sigma>\<^sub>2 res)) (free \<circ> (the \<circ> \<Gamma>)))"
      from \<iota>'D hh' have
        \<iota>'D': "\<iota>' \<in> dom (\<chi>\<^sub>c h')" using predecessor_dom by blast
      from r obtain z where
        zD': "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        r': "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z))" and
        fz: "free (the (\<Gamma> z))" by auto
      have "\<exists> z' \<in> dom \<sigma>. free (the (\<Gamma> z')) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> z'))"
      proof (cases "z \<noteq> x")
        case True
        with zD' 9 call.prems(2) r' have
          zD: "z \<in> dom \<sigma>" and
          r: "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>') (the (\<sigma> z))" and
          *: "(\<sigma> (x := \<sigma>\<^sub>2 res)) z = \<sigma> z" by simp_all
        from \<iota>'D' r wi have
          "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h')" using reaches\<^sub>v_target_dom by auto
        from r have
          zN: "the (\<sigma> z) \<noteq> null" using reaches\<^sub>v_reaches by blast
        moreover note zD call.prems(5)
        ultimately have
          "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4D' using runtime_type_assignment.simps value.sel
          by metis
        with 22 \<iota>'D r have
          "reaches (\<chi>\<^sub>v h) \<iota>' (ref (the (\<sigma> z))) \<or>
          (\<exists> w \<in> dom \<sigma>. committed (the (\<Gamma> w)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> z))) \<or>
          (\<exists> w \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> z)))" using reaches\<^sub>v_reaches by simp
        moreover from zD fz zN have
          ?thesis if "reaches (\<chi>\<^sub>v h) \<iota>' (ref (the (\<sigma> z)))" using that reaches\<^sub>v_reaches by auto
        moreover from zD fz call.prems(6) have
          ?thesis if "\<exists> w \<in> dom \<sigma>. committed (the (\<Gamma> w)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (the (\<sigma> z))"
          using that unfolding c\<^sub>5_def by blast
        moreover have
          ?thesis if "\<exists> w \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> z))"
          using that by blast
        ultimately show ?thesis by blast
      next
        case False
        with x\<sigma> r' fz have
          zD: "z \<in> dom \<sigma>" and
          r'': "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>') (the (\<sigma>\<^sub>2 res))" and
          fx: "free (the (\<Gamma> x))" by simp_all
        from fx \<Gamma>rx have
          fr: "free (the (\<Gamma>' res))" using free_subtyping by blast
        with r'' r\<sigma>\<^sub>2 have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>') (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>')))" by auto
        with \<iota>'D IH_21' have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>') (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))" by blast
        then obtain w where
          wD: "w \<in> dom \<sigma>\<^sub>1" and
          rw: "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma>\<^sub>1 w))" and
          fw: "free (the (\<Gamma>' w))" by auto
        from rw fw \<sigma>yt iit yd\<sigma> have
          ?thesis if "w = this" using that by auto
        moreover from rw fw fx\<^sub>i z\<^sub>i\<sigma> have
          ?thesis if "w \<in> set x\<^sub>i" using that by fastforce
        moreover from rw fw \<sigma>\<^sub>1r have
          ?thesis if "w = res" using that reaches\<^sub>v_right_null by simp 
        moreover from rw fw \<sigma>\<^sub>1y\<^sub>j have
          ?thesis if "w \<in> set y\<^sub>j" using that reaches\<^sub>v_reaches by simp
        moreover note d\<sigma>\<^sub>1D wD
        ultimately show ?thesis by blast
      qed
      then show "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>') (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by auto
    qed
    have
      g20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma> (x := \<sigma>\<^sub>2 res)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
    proof (rule ballI, rule impI)
      fix \<iota>'
      assume
        \<iota>'D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)"
      with hh' have
        \<iota>'D': "\<iota>' \<in> dom (\<chi>\<^sub>c h')" using predecessor_dom by blast
      assume
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma> (x := \<sigma>\<^sub>2 res)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>')"
      then obtain z where
        zD': "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        cz: "committed (the (\<Gamma>  z))" and
        r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>')" using reaches\<^sub>V\<^sub>v_values
        by (metis (mono_tags, lifting) comp_apply)
      with 15 have
        zD: "z \<in> dom \<sigma>" by blast
      from \<sigma>\<^sub>1y\<^sub>j have
        ry\<^sub>jl: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. x \<in> set y\<^sub>j \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) v"
        using reaches\<^sub>v_left_null by simp
      from \<sigma>\<^sub>1y\<^sub>j have
        ry\<^sub>jr: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. x \<in> set y\<^sub>j \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) v (the (\<sigma>\<^sub>1 x))"
        using reaches\<^sub>v_right_null by simp
      have "\<exists> z' \<in> dom \<sigma>. committed (the (\<Gamma> z')) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z')) (non_null \<iota>') "
      proof (cases "x = z")
        case True
        with x\<sigma> r' cz have
          zD: "z \<in> dom \<sigma>" and
          r'': "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 res)) (non_null \<iota>')" and
          cx: "committed (the (\<Gamma> x))" by simp_all
        from cx \<Gamma>rx have
          fr: "committed (the (\<Gamma>' res))" using committed_subtyping by blast
        with r'' r\<sigma>\<^sub>2 have
          "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>')" by auto
        with \<iota>'D IH_20' have
          "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>')" by blast
        then obtain w where
          wD: "w \<in> dom \<sigma>\<^sub>1" and
          rw: "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 w)) (non_null \<iota>')" and
          cw: "committed (the (\<Gamma>' w))" by auto
        from rw cw \<sigma>yt iit yd\<sigma> have
          ?thesis if "w = this" using that by auto
        moreover from rw cw cx\<^sub>i z\<^sub>i\<sigma> have
          ?thesis if "w \<in> set x\<^sub>i" using that by fastforce
        moreover from rw cw \<sigma>\<^sub>1r have
          ?thesis if "w = res" using that reaches\<^sub>v_left_null by simp 
        moreover from rw cw \<sigma>\<^sub>1y\<^sub>j have
          ?thesis if "w \<in> set y\<^sub>j" using that reaches\<^sub>v_reaches by simp
        moreover note d\<sigma>\<^sub>1D wD
        ultimately show ?thesis by blast
      next
        case False
        with zD' 9 call.prems(2) r' have
          zD: "z \<in> dom \<sigma>" and
          r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> z)) (non_null \<iota>')" and
          *: "(\<sigma> (x := \<sigma>\<^sub>2 res)) z = \<sigma> z" by simp_all
        from \<iota>'D' r Wh' have
          "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h')" using reaches\<^sub>v_source_dom by fastforce
        from r have
          zN: "the (\<sigma> z) \<noteq> null" using reaches\<^sub>v_reaches by blast
        moreover note zD call.prems(5)
        ultimately have
          "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4D' using runtime_type_assignment.simps value.sel
          by metis
        with 22 \<iota>'D r have
          "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> z))) \<iota>' \<or>
          (\<exists> w \<in> dom \<sigma>. committed (the (\<Gamma> w)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (non_null \<iota>')) \<or>
          (\<exists> w \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>'))" using reaches\<^sub>v_reaches by simp
        moreover from zD cz zN have
          ?thesis if "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> z))) \<iota>'" using that reaches\<^sub>v_reaches by auto
        moreover from zD cz call.prems(6) have
          ?thesis if "\<exists> w \<in> dom \<sigma>. committed (the (\<Gamma> w)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> w)) (non_null \<iota>')"
          using that unfolding c\<^sub>5_def by blast
        moreover from zD cz call.prems(6) have
          ?thesis if "\<exists> w \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> w)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>')"
          using that using c\<^sub>5_def by blast
        ultimately show ?thesis by blast
      qed
      then show
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>')" by auto
    qed
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res)).
              reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>) \<and>
              committed (the (\<Gamma> z)) \<and>
              \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h')"
        "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))"
        "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>) \<and>
              committed (the (\<Gamma> z)) \<and>
              \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) \<in> dom (\<chi>\<^sub>c h)"
      moreover then have
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma>(x := \<sigma>\<^sub>2 res)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
        using reaches\<^sub>V\<^sub>vI by (metis (mono_tags) comp_def)
      moreover note "g20'"
      ultimately show "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
        using reaches\<^sub>V\<^sub>v_values by (metis (mono_tags) comp_def)
    qed
    from 59 have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by simp
    have
      13: "\<Gamma> \<turnstile>\<^sub>5 h', \<sigma> (x := \<sigma>\<^sub>2 res)" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI)
      fix z y
      let ?thesis1 = "deep_init\<^sub>v h' (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z))"
      let ?thesis2 = "free (the (\<Gamma> y)) \<longrightarrow>
        \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) z)) (the ((\<sigma>(x := \<sigma>\<^sub>2 res)) y))"
      assume
        zD': "z \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        yD': "y \<in> dom (\<sigma> (x := \<sigma>\<^sub>2 res))" and
        cz: "committed (the (\<Gamma> z))"
      from 9 call.prems(2) zD' have
        zD: "z \<in> dom \<sigma>" by simp
      from 15 yD' have
        yD: "y \<in> dom \<sigma>" by blast
      show "?thesis1 \<and> ?thesis2"
      proof (cases "z = x")
        case True
        with cz have
          cx: "committed (the (\<Gamma> x))" by simp
        with \<Gamma>rx have
          cr: "committed (the (\<Gamma>' res))" using committed_subtyping by blast
        with True IH_13 r\<sigma>\<^sub>2 have
          d: ?thesis1 by simp
        moreover have
          "free (the (\<Gamma> y)) \<longrightarrow>
          \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 res)) (the ((\<sigma> (x \<mapsto> the (\<sigma>\<^sub>2 res))) y))"
        proof (rule impI, rule ccontr)
          assume
            fy: "free (the (\<Gamma> y))"
          with cx have
            yx: "y \<noteq> x" by auto
          moreover assume
            r'': "\<not> \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 res)) (the ((\<sigma> (x \<mapsto> the (\<sigma>\<^sub>2 res))) y))"
          ultimately have
            yN: "the (\<sigma> y) \<noteq> null" using reaches\<^sub>v_right_null by auto
          moreover note call.prems(5)
          ultimately have
            yDh: "ref (the (\<sigma> y)) \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4D' RAddrE yD type_as_t_def \<chi>\<^sub>c_def
            using value.exhaust_sel by metis
          from r'' yx cx x\<sigma> 15 have
            "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma>(x := \<sigma>\<^sub>2 res)) (committed \<circ> (the \<circ> \<Gamma>))) (the (\<sigma> y))" by auto
          moreover note g20' yDh yN
          ultimately have
            "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (the (\<sigma> y))"
            by (metis value.collapse) 
          with call.prems(6) fy yD True show False by auto
        qed
        with True have
          ?thesis2 by auto
        ultimately show ?thesis by simp
      next
        case False
        have
          ?thesis1 unfolding deep_init\<^sub>v_def
        proof (rule allI, rule impI, rule ccontr)
          fix \<iota>'
          assume
            r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z)) (non_null \<iota>')"
          with 12 wi zD' have
            \<iota>'D': "\<iota>' \<in> dom (\<chi>\<^sub>c h')" using reaches\<^sub>v_reaches reaches\<^sub>v_target_dom
            using runtime_type_assignment.cases value.sel by (metis (mono_tags))
          moreover assume
            ih': "\<not> init h' \<iota>'"
          moreover note 18
          ultimately have
            \<iota>'D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" by blast
          from r' False have
            zN: "the (\<sigma> z) \<noteq> null" using reaches\<^sub>v_left_null by auto
          from r' False zD zN call.prems(5) have
            vD\<sigma>: "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4D' \<chi>\<^sub>c_def RAddrE
            using runtime_type_assignment.cases value.exhaust_sel by metis
          from r' False have
            r: "reaches (\<chi>\<^sub>v h') (ref (the (\<sigma> z))) \<iota>'" using reaches\<^sub>v_reaches by simp
          from 17 \<iota>'D hh' ih' have
            ih: "\<not> init h \<iota>'" using init_def predecessor_cls by metis
          with call.prems(6) cz zD have False if "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>')"
            using that c\<^sub>5_def deep_init\<^sub>v_def by blast
          with r' False have False if "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> z))) \<iota>'"
            using reaches\<^sub>v_reaches that by simp
          moreover from call.prems(6) ih have False if
            "\<exists> u \<in> dom \<sigma>. committed (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> u)) (non_null \<iota>')"
            using that c\<^sub>5_def deep_init\<^sub>v_def by blast
          moreover from call.prems(6) cz zD have False if
            "\<exists> u \<in> dom \<sigma>. free (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> u))"
            using that c\<^sub>5_def by blast
          then have False if "\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>')" using that by blast
          moreover note 22 \<iota>'D vD\<sigma> zN r
          ultimately show False using value.collapse by metis
        qed
        moreover have
          ?thesis2
        proof (rule impI, rule ccontr)
          assume
            fy: "free (the (\<Gamma> y))" and
            "\<not> \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z)) (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) y))"
          then have
            r'': "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) z)) (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) y))" by simp
          with False have
            r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> z)) (the ((\<sigma> (x := \<sigma>\<^sub>2 res)) y))" by simp
          show False
          proof (cases "y = x")
            case True
            with r' have
              "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> z)) (the (\<sigma>\<^sub>2 res))" by simp
            with zD call.prems(5) have
              "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using reaches\<^sub>v_reaches c\<^sub>4D' RAddrE \<chi>\<^sub>c_def
              using runtime_type_assignment.cases value.exhaust_sel by metis
            with g21' cz fy yD' zD r'' call.prems(6) False show ?thesis
              unfolding c\<^sub>5_def using reaches\<^sub>v_reaches reaches\<^sub>v\<^sub>VI reaches\<^sub>v\<^sub>V_values
              using value.collapse
              by (metis (mono_tags) comp_def fun_upd_other)
          next
            case False
            with r' have
              "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> z)) (the (\<sigma> y))" by simp
            moreover from call.prems(6) fy cz zD yD have False if "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> y))"
              using that c\<^sub>5_def by blast
            moreover from call.prems(6) fy yD have False
              if "\<exists> u \<in> dom \<sigma>. committed (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> u)) (the (\<sigma> y))"
              using that c\<^sub>5_def by blast
            moreover from call.prems(6) cz zD have False
              if "\<exists> u \<in> dom \<sigma>. free (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> u))"
              using that c\<^sub>5_def by blast
            moreover note 22 zD yD call.prems(5)
            ultimately show ?thesis using RAddrE \<chi>\<^sub>c_def c\<^sub>4_iff reaches\<^sub>v_reaches type_as_t_def
              using value.collapse by metis
          qed
        qed
        ultimately show ?thesis by simp
      qed 
    qed
    from 9 10 11 12 13 TN 14 15 16 17 18 19 g20' 20 g21' 22 gS Wh' wi uh show ?case by blast
  next
    case (callBad \<sigma> y h x m z\<^sub>i)
    from callBad.prems(8) obtain C k\<^sub>1 where
      "\<Gamma>, \<Delta> \<turnstile> \<V> y : (C, k\<^sub>1, !)" using call_target_typing by auto
    then have
      "y \<in> \<Delta>" and
      "\<Gamma> y = Some (C, k\<^sub>1, !)" using lemma_2\<^sub>4 by auto
    moreover then have
      "\<not> nullable (the (\<Gamma> y))" by simp
    moreover note callBad.prems(4)
    ultimately have
      "\<sigma> y \<noteq> Some null" by force
    with callBad.hyps have
      "False" by simp
    then show ?case by simp
  next
    case (create C X\<^sub>i Y\<^sub>j x\<^sub>i y\<^sub>j h h\<^sub>1 \<iota>\<^sub>1 \<sigma>\<^sub>1 \<sigma> z\<^sub>i s h' \<sigma>\<^sub>2 \<epsilon> x \<Gamma> \<Delta> \<Delta>' \<Sigma>)
    from create.prems(2,3,4,5,6,7) have
      D: "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" unfolding c\<^sub>1_def c\<^sub>2_def c\<^sub>3_def c\<^sub>4_def c\<^sub>5_def c\<^sub>6_def good_configuration_def by blast
    from create.hyps(1,2) have
      dxt: "this \<notin> set x\<^sub>i" using constructor_no_this_in_arguments by fastforce
    from create.hyps(1,3) have
      dyt: "this \<notin> set y\<^sub>j" using constructor_no_this_in_locals by fastforce
    from create.hyps(1,2,3) have
      dxy: "set x\<^sub>i \<inter> set y\<^sub>j = {}" using constructor_distinct_variables by force
    from create.hyps(5) dxt dyt have
      \<sigma>\<^sub>1t: "the (\<sigma>\<^sub>1 this) = non_null \<iota>\<^sub>1" using fun_updr_other
      by (metis fun_upd_same option.sel)
    from create.hyps(1,3) have
      "distinct y\<^sub>j" using constructor_distinct_variables by auto
    with create.hyps(5) have
      \<sigma>\<^sub>1y\<^sub>j: "map \<sigma>\<^sub>1 y\<^sub>j =  map (\<lambda>x. Some null) y\<^sub>j" using fun_updr_same
      by (metis length_map order_refl take_all)
    from create.hyps(1) create.prems(8) obtain \<theta> T\<^sub>i k where
      81: "cSig C = Some (X\<^sub>i, Y\<^sub>j)" and
      82: "\<theta> \<in> instances (C, Inl \<zero>, !) (\<tau> X\<^sub>i)" and
      83: "\<Gamma>, \<Delta> \<turnstile> z\<^sub>i : T\<^sub>i" and
      84: "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) (\<tau> X\<^sub>i)" and
      85: "k = (if committed_all T\<^sub>i then \<one> else \<zero>)" and
      86: "Some (C, k, !) \<sqsubseteq> \<Gamma> x" and
      87: "\<Sigma> = {}" and
      88: "\<Delta>' = insert x \<Delta>" by auto
    from 83 have
      \<Gamma>z\<^sub>i: "map (the \<circ> \<Gamma>) z\<^sub>i = T\<^sub>i" using variables_typing_type by blast
    from 83 have
      LT\<^sub>iz\<^sub>i:"length T\<^sub>i = length z\<^sub>i" using variables_typing_length by simp
    moreover from 84 have
      LT\<^sub>iX\<^sub>i: "length T\<^sub>i = length X\<^sub>i" using list_leq_length by fastforce
    moreover note create.hyps(2)
    ultimately have
      Lxz: "length x\<^sub>i = length z\<^sub>i" unfolding variable_names_def by simp
    moreover from create.hyps(1,2) have
      dx\<^sub>i: "distinct x\<^sub>i" using constructor_distinct_variables by auto
    moreover from dxy create.hyps(5) have
      "map \<sigma>\<^sub>1 x\<^sub>i = map (\<sigma>\<^sub>\<emptyset> (this \<mapsto> non_null \<iota>\<^sub>1) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr>) x\<^sub>i" using fun_updr_others by blast
    ultimately have
      \<sigma>\<^sub>1x\<^sub>i: "map \<sigma>\<^sub>1 x\<^sub>i = map \<sigma> z\<^sub>i" using fun_updr_same by (metis length_map order_refl take_all)
    with Lxz have
      \<sigma>\<^sub>1x\<^sub>in:"\<forall> i. i < length x\<^sub>i \<longrightarrow> \<sigma>\<^sub>1 (x\<^sub>i ! i) = \<sigma> (z\<^sub>i ! i)" using nth_map_eq by blast
    from 84 have
      "map the (map Some T\<^sub>i) \<sqsubseteq> map the (map (\<ss> \<theta>) (\<tau> X\<^sub>i))"
      by (metis (no_types) comp_the_Some list.map_id map_map map_some_partial_order map_some_structure)
    then have
      "T\<^sub>i \<sqsubseteq> map the (map (\<ss> \<theta>) (\<tau> X\<^sub>i))" by simp
    also have
      "\<dots> = map (the \<circ> (\<ss> \<theta>)) (\<tau> X\<^sub>i)" by simp
    finally have
      T\<^sub>iX\<^sub>i: "T\<^sub>i \<sqsubseteq> map (the \<circ> (\<ss> \<theta>)) (\<tau> X\<^sub>i)" by simp
    moreover
    from 84 obtain Tx' where
      Tx': "map (\<ss> \<theta>) (\<tau> X\<^sub>i) = map Some Tx'" using map_some_structure by blast
    then have
      "map (type_as_t \<circ> the \<circ> (\<ss> \<theta>)) (\<tau> X\<^sub>i) = map type_as_t (\<tau> X\<^sub>i)"
      using substitution_t by (induction X\<^sub>i arbitrary: Tx') fastforce+
    ultimately have
      tT\<^sub>iX\<^sub>i: "map type_as_t T\<^sub>i \<sqsubseteq> map type_as_t (\<tau> X\<^sub>i)" using partial_mono_type_as_t
      by (metis (no_types) map_map partial_mono_map)
    then have
      baseT\<^sub>iX\<^sub>i: "map base T\<^sub>i \<sqsubseteq> map base (\<tau> X\<^sub>i)"
    proof -
      have
        "map base T\<^sub>i = map simple_base (map type_as_t T\<^sub>i)" using type_as_t_class by auto
      also from tT\<^sub>iX\<^sub>i have
        "\<dots> \<sqsubseteq> map simple_base (map type_as_t (\<tau> X\<^sub>i))"
        using partial_mono_def partial_mono_map simple_base_subtyping by blast
      also have
        "\<dots> = map base (\<tau> X\<^sub>i)" using type_as_t_class[symmetric] by auto
      finally show ?thesis by blast
    qed
    have
      "\<turnstile>\<^sub>C C" using wf_constructor by simp
    with create.hyps(6) 81 82 obtain \<Gamma>' \<Delta>''' \<Sigma>' and \<theta>\<^sub>l and \<Delta>'' where
      89: "\<Gamma>' = (\<upsilon> ((\<upsilon> Map.empty \<theta> X\<^sub>i) (this := Some (C, \<zero>, !))) \<theta> Y\<^sub>j)" and
         \<comment> \<open>The report uses @{term \<Delta>'} in (89) and (90) — a clear abuse of notation.
            To avoid any confusion, a new variable is introduced in (90).\<close>
      90: "\<Delta>''' = set (\<eta> X\<^sub>i) \<union> {this}" and
      91: "\<Gamma>', \<Delta>''' \<turnstile> s | \<Delta>'', \<Sigma>'" and
      92: "{f. f \<in> set (fields C) \<and> \<not> (nullable (the (fType (C, f))))} \<subseteq> \<Sigma>'" and
      "wf_statement s" and
      "\<theta>\<^sub>l = instances (C, Inl \<zero>, !) (\<tau> X\<^sub>i)" and
      "\<forall> \<theta> \<in> \<theta>\<^sub>l. (\<upsilon> ((\<upsilon> Map.empty \<theta> X\<^sub>i) (this := Some (C, \<zero>, !))) \<theta> Y\<^sub>j), set (\<eta> X\<^sub>i) \<union> {this} \<turnstile> s | \<Delta>'', \<Sigma>'"
      using wfConsE by auto
    from 81 have
      vY: "vars (\<tau> Y\<^sub>j) \<subseteq> vars (\<tau> X\<^sub>i)" using constructor_local_expectation
      unfolding constructor_arguments_def constructor_locals_def by fastforce
    have
      d1: "dom (\<sigma>\<^sub>\<emptyset> (this \<mapsto> non_null \<iota>\<^sub>1)) = {this}" by simp
    from 83 have
      zD\<Gamma>: "set z\<^sub>i \<subseteq> dom \<Gamma>" unfolding variables_typing_def
      using map_of_SomeD map_of_zip_is_Some by fastforce
    with create.prems(2) have
      zD\<sigma>: "set z\<^sub>i \<subseteq> dom \<sigma>" by simp
    then obtain zs where
      zs: "map \<sigma> z\<^sub>i = map Some zs"
      by (induction z\<^sub>i)
        (simp, metis domD subset_eq list.simps(9) list.set_intros(1) list.set_intros(2))
    have
      \<sigma>z\<^sub>iD: "\<And> z. z \<in> set z\<^sub>i \<Longrightarrow> the (\<sigma> z) \<noteq> null \<Longrightarrow> ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)"
    proof -
      fix z
      assume
        "z \<in> set z\<^sub>i"
      with zD\<sigma> have
        "z \<in> dom \<sigma>" by auto
      moreover with create.prems(2) have
        "z \<in> dom \<Gamma>" by simp
      then obtain C\<^sub>z k\<^sub>z n\<^sub>z where
        "the (\<Gamma> z) = (C\<^sub>z, k\<^sub>z, n\<^sub>z)" by auto
      moreover assume
        zN: "the (\<sigma> z) \<noteq> null"
      moreover note D create.prems(5)
      ultimately have
        "h \<turnstile> the (\<sigma> z) : (C\<^sub>z, n\<^sub>z)" by auto
      with zN show "ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" by (cases "the (\<sigma> z)") auto
    qed
    with \<sigma>\<^sub>1x\<^sub>in Lxz have
      \<sigma>\<^sub>1x\<^sub>iD: "\<And> x. x \<in> set x\<^sub>i \<Longrightarrow> the (\<sigma>\<^sub>1 x) \<noteq> null \<Longrightarrow> ref (the (\<sigma>\<^sub>1 x)) \<in> dom (\<chi>\<^sub>c h)"
      by (metis in_set_conv_nth)
    from create.hyps(2) have
      LxX: "length x\<^sub>i = length X\<^sub>i" unfolding variable_names_def by simp
    then have
      LxX': "length x\<^sub>i \<le> length X\<^sub>i" by simp
    from 89 create.hyps(2,3) have
      \<Gamma>': "\<Gamma>' = (Map.empty \<lparr>x\<^sub>i := map (\<ss> \<theta> \<circ> \<tau>) X\<^sub>i \<rparr>(this \<mapsto> (C, \<zero>, !))) \<lparr>y\<^sub>j := map (\<ss> \<theta> \<circ> \<tau>) Y\<^sub>j \<rparr>"
      unfolding xS_upds_def variable_names_def variable_type_def by simp
    moreover from create.hyps(1,3) have
      dy\<^sub>j: "distinct y\<^sub>j" using constructor_distinct_variables by auto
    moreover
    from create.hyps(3) have
      LyY: "length y\<^sub>j = length Y\<^sub>j" unfolding variable_names_def by simp
    then have
      LyY': "length y\<^sub>j \<le> length Y\<^sub>j" by simp
    then have
      "length y\<^sub>j \<le> length (map (\<ss> \<theta> \<circ> \<tau>) Y\<^sub>j)" by simp
    with \<Gamma>' dy\<^sub>j have
      "map \<Gamma>' y\<^sub>j = take (length y\<^sub>j) (map (\<ss> \<theta> \<circ> \<tau>) Y\<^sub>j)" using fun_updr_same by blast
    with LyY have
      \<Gamma>'y\<^sub>j: "map \<Gamma>' y\<^sub>j = map (\<ss> \<theta> \<circ> \<tau>) Y\<^sub>j" by simp
    from \<Gamma>' dxy dxt have
      "map \<Gamma>' x\<^sub>i = map (Map.empty \<lparr>x\<^sub>i := map (\<ss> \<theta> \<circ> \<tau>) X\<^sub>i \<rparr>) x\<^sub>i"
      using fun_updr_others by (metis map_fun_upd) 
    moreover from LxX have
      "length x\<^sub>i \<le> length (map (\<ss> \<theta> \<circ> \<tau>) X\<^sub>i)" by simp
    moreover note dx\<^sub>i
    ultimately have
      "map \<Gamma>' x\<^sub>i = take (length x\<^sub>i) (map (\<ss> \<theta> \<circ> \<tau>) X\<^sub>i)" using fun_updr_same by metis
    with LxX have
      \<Gamma>'x\<^sub>i: "map \<Gamma>' x\<^sub>i = map (\<ss> \<theta>) (\<tau> X\<^sub>i)" by simp
    then have
      "map the (map (\<Gamma>') x\<^sub>i) = map the (map (\<ss> \<theta>) (\<tau> X\<^sub>i))" by simp
    then have
      "map type_as_t (map (the \<circ> \<Gamma>') x\<^sub>i) = map type_as_t (map (the \<circ> (\<ss> \<theta>)) (\<tau> X\<^sub>i))" by simp
    with 82 have
      "map (type_as_t \<circ> (the \<circ> \<Gamma>')) x\<^sub>i = map type_as_t (\<tau> X\<^sub>i)" using instance_types_t
      using \<open>map (type_as_t \<circ> the \<circ> \<ss> \<theta>) (\<tau> X\<^sub>i) = map type_as_t (\<tau> X\<^sub>i)\<close> by auto
    with tT\<^sub>iX\<^sub>i have
      "map type_as_t T\<^sub>i \<sqsubseteq> map (type_as_t \<circ> (the \<circ> \<Gamma>')) x\<^sub>i" by simp
    from \<Gamma>z\<^sub>i \<Gamma>'x\<^sub>i T\<^sub>iX\<^sub>i have
      t\<Gamma>z\<^sub>ix\<^sub>i: "map (the \<circ> \<Gamma>) z\<^sub>i \<sqsubseteq> map (the \<circ> \<Gamma>') x\<^sub>i" using List.map.compositionality by metis
    from 83 have
      "map \<Gamma> z\<^sub>i = map Some T\<^sub>i" using variables_typing by simp
    with 84 \<Gamma>'x\<^sub>i have
      \<Gamma>z\<^sub>ix\<^sub>i: "map \<Gamma> z\<^sub>i \<sqsubseteq> map \<Gamma>' x\<^sub>i" by simp
    from Lxz have
      "length x\<^sub>i \<le> length (map \<sigma> z\<^sub>i)" by simp
    with d1 zs have
      "dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> non_null \<iota>\<^sub>1) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr>) = {this} \<union> set x\<^sub>i"
      using fun_updr_some_le_dom by (metis length_map)
    moreover
    obtain ys :: "'b value list" where
      ys: "map (\<lambda>x. Some null) y\<^sub>j = map Some ys" by (induction y\<^sub>j) (simp, metis list.simps(9))
    moreover have
      "length y\<^sub>j \<le> length (map (\<lambda>x. Some null) y\<^sub>j)" by simp
    ultimately have
      "dom (\<sigma>\<^sub>\<emptyset>(this \<mapsto> non_null \<iota>\<^sub>1) \<lparr> x\<^sub>i := map \<sigma> z\<^sub>i \<rparr> \<lparr> y\<^sub>j := map Some ys \<rparr>) =
        {this} \<union> set x\<^sub>i \<union> set y\<^sub>j"
      using fun_updr_some_le_dom by (metis length_map)
    with create.hyps(5) ys have
      \<sigma>u: "dom \<sigma>\<^sub>1 = {this} \<union> set x\<^sub>i \<union> set y\<^sub>j" by simp
    moreover
    from create.hyps(2) 82 have
      "dom (\<upsilon> Map.empty \<theta> X\<^sub>i) = set x\<^sub>i" using instance_env_upd_dom instances_instance
      by (metis dom_empty sup_bot.left_neutral)
    then have
      "dom ((\<upsilon> Map.empty \<theta> X\<^sub>i) (this := Some (C, \<zero>, !))) = set x\<^sub>i \<union> {this}" by simp
    with create.hyps(3) 82 vY have
      "dom (\<upsilon> ((\<upsilon> Map.empty \<theta> X\<^sub>i) (this := Some (C, \<zero>, !))) \<theta> Y\<^sub>j) = set x\<^sub>i \<union> {this} \<union> set y\<^sub>j"
      using instance_sub_env_upd_dom instances_instance by metis
    with 89 have
      "dom \<Gamma>' = {this} \<union> set x\<^sub>i \<union> set y\<^sub>j" by simp
    ultimately have
      \<sigma>\<^sub>1\<Gamma>': "dom \<sigma>\<^sub>1 = dom \<Gamma>'" by simp
    with \<sigma>u have
      "\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>1" unfolding c\<^sub>1_def by simp
    moreover
    have
      "\<lfloor>map Variable z\<^sub>i\<rfloor>\<^bsub>h,\<sigma>\<^esub> =  map Inl (map the (map \<sigma> z\<^sub>i))" unfolding expressions_evaluation_def
      using expression_evaluation.simps(1) by simp
    with D 83 create.prems(9) have
      vt: "h \<turnstile> map the (map \<sigma> z\<^sub>i) : map type_as_t T\<^sub>i" using lemma_4\<^sub>3_vars
      by (metis (mono_tags) Inl_inject list.inj_map_strong)
    then have
      vt': "h \<turnstile> map (the \<circ> \<sigma>) z\<^sub>i : map type_as_t T\<^sub>i" by simp
    from \<sigma>u \<sigma>\<^sub>1\<Gamma>' have
      x\<^sub>iD: "set x\<^sub>i \<subseteq> dom \<Gamma>'" and
      y\<^sub>jD: "set y\<^sub>j \<subseteq> dom \<Gamma>'" and
      tD\<Gamma>': "this \<in> dom \<Gamma>'" using calculation by auto
    from Tx' 84 have
      "map Some T\<^sub>i \<sqsubseteq> map Some Tx'" by simp
    then have
      "T\<^sub>i \<sqsubseteq> Tx'" by simp
    then have
      "map type_as_t T\<^sub>i \<sqsubseteq> map type_as_t Tx'" using partial_mono_type_as_t partial_mono_map
      by (simp add: partial_mono_map partial_mono_type_as_t)
    with Tx' have
      "map type_as_t T\<^sub>i \<sqsubseteq> map type_as_t (map the (map (\<ss> \<theta> \<circ> snd) X\<^sub>i))" by simp
    with vt have
      "h \<turnstile> map the (map \<sigma> z\<^sub>i) : map type_as_t (map the (map (\<ss> \<theta> \<circ> snd) X\<^sub>i))"
      using lemma_4\<^sub>1_list by blast
    with \<sigma>\<^sub>1x\<^sub>i have
      "h \<turnstile> map the (map \<sigma>\<^sub>1 x\<^sub>i) : map type_as_t (map the (map (\<ss> \<theta> \<circ> snd) X\<^sub>i))" by simp
    from create.hyps(4) create.prems(12) have
      \<iota>\<^sub>1D: "\<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using heap.alloc heap_axioms by (metis domI fun_upd_same)
    then have
      "the (cls h\<^sub>1 \<iota>\<^sub>1) \<sqsubseteq> the (cls h\<^sub>1 \<iota>\<^sub>1)" using leq_refl by simp
    with \<iota>\<^sub>1D have
      h\<^sub>1\<iota>\<^sub>1: "h\<^sub>1 \<turnstile> (non_null \<iota>\<^sub>1) : (the (cls h\<^sub>1 \<iota>\<^sub>1), !)" unfolding cls_def using RAddr by simp
    from create.hyps(4) create.prems(12) have
      \<iota>\<^sub>1N: "\<iota>\<^sub>1 \<notin> dom (\<chi>\<^sub>c h)" using alloc by blast
    with create.hyps(4) create.prems(12) have
      \<chi>\<^sub>ch\<^sub>1: "\<chi>\<^sub>c h\<^sub>1 = \<chi>\<^sub>c h (\<iota>\<^sub>1 \<mapsto> C)" using alloc by blast
    with \<iota>\<^sub>1N have
      dh\<^sub>1: "dom (\<chi>\<^sub>c h\<^sub>1) = dom (\<chi>\<^sub>c h) \<union> {\<iota>\<^sub>1}" by simp
    from create.prems(3) have
      "\<turnstile>\<^sub>2 h" unfolding c\<^sub>2_def by blast
    with \<iota>\<^sub>1N create.hyps(4) create.prems(12) have
      "dom (\<chi>\<^sub>c h) \<turnstile>\<^sub>2 h\<^sub>1" using alloc_type alloc_value c\<^sub>2_copy by metis
    from create.hyps(4) create.prems(12) have
      "\<iota>\<^sub>1 \<turnstile>\<^sub>2 h\<^sub>1" using c\<^sub>2_null alloc_null_fields using alloc cls_def
      by (metis (mono_tags) map_upd_Some_unfold option.sel)
    with dh\<^sub>1 \<open>dom (\<chi>\<^sub>c h) \<turnstile>\<^sub>2 h\<^sub>1\<close> have
      "\<turnstile>\<^sub>2 h\<^sub>1" using c\<^sub>2_insert by (metis subset_refl)
    moreover
    from D have
      "\<Gamma>, \<Delta>  \<turnstile>\<^sub>3 \<sigma>" using good_configuration_def by blast
    from create.hyps(2) create.hyps(3) have
      x\<^sub>i: "\<eta> (constructor_arguments (X\<^sub>i, Y\<^sub>j)) = x\<^sub>i" and
      y\<^sub>j: "\<eta> (constructor_locals (X\<^sub>i, Y\<^sub>j)) = y\<^sub>j"
      unfolding constructor_arguments_def variable_names_def by simp_all
    from \<sigma>\<^sub>1t have
      c\<^sub>3t: "this, \<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1" unfolding c\<^sub>3_restriction_def by simp
    from 84 have
      mT: "map nullity T\<^sub>i \<sqsubseteq> map nullity (\<tau> X\<^sub>i)" using substitutions_nullity_subtyping by metis
    from 83 have
      "map (the \<circ> \<Gamma>) z\<^sub>i = T\<^sub>i" using variables_typing_type by blast
    then have
      mz: "map (nullity \<circ> the \<circ> \<Gamma>) z\<^sub>i = map nullity T\<^sub>i" by auto
    from create.hyps(5) create.prems(4) zD\<sigma> have
      c\<^sub>3a: "z\<^sub>i, \<Gamma>, \<Delta> \<turnstile>\<^sub>3 \<sigma>" by (simp add: c\<^sub>3_restrictions_def)
    from 82 89 have
      m1: "map nullity (\<tau> X\<^sub>i) = map (nullity \<circ> the \<circ> (\<ss> \<theta>)) (\<tau> X\<^sub>i)" unfolding xS_upds_def
      using instance_types_nullity[symmetric] instances_instance by blast
    with \<Gamma>'x\<^sub>i have
      m2: "\<dots> = map (nullity \<circ> the) (map \<Gamma>' x\<^sub>i)" by simp
    then have
      "\<dots> = map (nullity \<circ> the \<circ> \<Gamma>') x\<^sub>i" by simp
    with m1 m2 have
      "map nullity (\<tau> X\<^sub>i) = map (nullity \<circ> the \<circ> \<Gamma>') x\<^sub>i" by presburger
    with mz mT have
      MN: "map (nullity \<circ> the \<circ> \<Gamma>) z\<^sub>i \<sqsubseteq> map (nullity \<circ> the \<circ> \<Gamma>') x\<^sub>i" by simp
    from 89 dyt create.hyps(3) have
      \<Gamma>'t: "\<Gamma>' this = Some (C, \<zero>, !)"
      using fun_updr_other unfolding variable_names_def xS_upds_def
      by (metis (mono_tags) map_upd_Some_unfold)
    from \<chi>\<^sub>ch\<^sub>1 have
      \<chi>\<^sub>ch\<^sub>1\<iota>\<^sub>1: "\<chi>\<^sub>c h\<^sub>1 \<iota>\<^sub>1 = Some C" by simp
    have
      "\<forall>i<length x\<^sub>i. x\<^sub>i ! i \<in> \<Delta>''' \<longrightarrow> (nullity (the (\<Gamma>' (x\<^sub>i ! i))) = ?) \<or> z\<^sub>i ! i \<in> \<Delta>"
    proof (rule allI, rule impI, rule impI)
      fix i
      let ?thesis = "nullity (the (\<Gamma>' (x\<^sub>i ! i))) = ? \<or> z\<^sub>i ! i \<in> \<Delta>"
      assume
        i: "i < length x\<^sub>i" and
        "x\<^sub>i ! i \<in> \<Delta>'''"
      then have
        "x\<^sub>i ! i \<in> \<Delta>'''" by simp_all
      show ?thesis
      proof (cases "nullity (the (\<Gamma>' (x\<^sub>i ! i))) = ?")
        case False
        then have
          "nullity (the (\<Gamma>' (x\<^sub>i ! i))) = !" using n.exhaust by blast
        moreover from Lxz i have
          "i < length (map (nullity \<circ> the \<circ> \<Gamma>) z\<^sub>i)" by fastforce
        with MN have
          "map (nullity \<circ> the \<circ> \<Gamma>) z\<^sub>i ! i \<sqsubseteq> map (nullity \<circ> the \<circ> \<Gamma>') x\<^sub>i ! i"
          using list_leq_iff by blast
        moreover note Lxz i
        ultimately have "nullity (the (\<Gamma> (z\<^sub>i ! i))) = !" by simp
        moreover
        from 83 Lxz i have
          "\<Gamma>, \<Delta> \<turnstile> \<V> (z\<^sub>i ! i) : the (\<Gamma> (z\<^sub>i ! i))"
          using variables_typing_def lemma_2\<^sub>4' variables_typing_elem_type by metis
        ultimately show ?thesis by auto
      qed simp
    qed
    with MN \<sigma>\<^sub>1x\<^sub>i[symmetric] c\<^sub>3a have
      c\<^sub>3x: "x\<^sub>i, \<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1" using c\<^sub>3_restrictions_transfer by blast
    from create.hyps x\<^sub>i y\<^sub>j dyt 90 have
      "\<forall> x \<in> set y\<^sub>j. x \<notin> \<Delta>'''" using constructor_disjoint_variables
      by (metis Un_iff disjoint_insert(1) mk_disjoint_insert singleton_iff)
    then have
      "y\<^sub>j, \<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1" unfolding c\<^sub>3_restrictions_def by simp
    with c\<^sub>3t c\<^sub>3x \<sigma>u have
      "\<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1" using c\<^sub>3I_cons c\<^sub>3_restrictions_append
      by (metis (no_types) set_append sup.cobounded2 sup_assoc sup_left_idem)
    moreover from \<sigma>\<^sub>1x\<^sub>i have
      "map \<sigma>\<^sub>1 x\<^sub>i = map \<sigma> z\<^sub>i" using fun_updr_same by simp
    have
      "\<Gamma>' \<turnstile>\<^sub>4 h\<^sub>1, \<sigma>\<^sub>1" unfolding c\<^sub>4_def
    proof (rule allI, rule allI, rule allI, rule ballI, rule impI)
      fix C k n x
      assume
        xD\<sigma>\<^sub>1: "x \<in> dom \<sigma>\<^sub>1"
      then have
        "x = this \<or> x \<in> set x\<^sub>i \<or> x \<in> set y\<^sub>j" using \<sigma>u by simp
      moreover assume
        "the (\<sigma>\<^sub>1 x) \<noteq> null \<and> the (\<Gamma>' x) = (C, k, n)"
      then have
        \<sigma>\<^sub>1xN: "the (\<sigma>\<^sub>1 x) \<noteq> null" and
        \<Gamma>'x: "the (\<Gamma>' x) = (C, k, n)" and
        base\<Gamma>'x: "base (the (\<Gamma>' x)) = C" by simp_all
      then obtain \<iota>\<^sub>x where
        \<sigma>\<^sub>1x: "the (\<sigma>\<^sub>1 x) = non_null \<iota>\<^sub>x" using value.exhaust by blast
      with create.hyps(5) \<sigma>\<^sub>1t have
        \<iota>\<^sub>xt: "\<iota>\<^sub>x = \<iota>\<^sub>1" if "x = this" using that by simp
      with \<iota>\<^sub>1D have
        "\<iota>\<^sub>x \<in> dom (\<chi>\<^sub>c h\<^sub>1)" if "x = this" using that by simp
      with \<Gamma>'x \<Gamma>'t have
        "h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : (C, n)" if "x = this" using \<chi>\<^sub>ch\<^sub>1\<iota>\<^sub>1 \<sigma>\<^sub>1t h\<^sub>1\<iota>\<^sub>1 that by simp
      moreover from T\<^sub>iX\<^sub>i have
        "h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : (C, n)" if "x \<in> set x\<^sub>i"
      proof (cases x\<^sub>i)
        case Nil
        then show ?thesis using that by simp
      next
        case (Cons a list)
        then have
          Lx\<^sub>i0: "length x\<^sub>i > 0" by simp
        have
          "x \<in> set x\<^sub>i" using that by simp
        with Cons obtain i where
          x: "x = x\<^sub>i ! i" and
          iLx\<^sub>i: "i < length x\<^sub>i" by (induction x\<^sub>i) (simp, metis in_set_conv_nth)
        from LxX iLx\<^sub>i have
          iLX\<^sub>i: "i < length X\<^sub>i" and
          iL\<tau>X\<^sub>i: "i < length (\<tau> X\<^sub>i)" by simp_all
        from base\<Gamma>'x x have
          "C = base (the (\<Gamma>' (x\<^sub>i ! i)))" by simp
        also from \<Gamma>'x\<^sub>i iLx\<^sub>i have
          "\<dots> = base (the (map (\<ss> \<theta>) (\<tau> X\<^sub>i) ! i))" using nth_map by metis
        also from iL\<tau>X\<^sub>i have
          "\<dots> = map (base o the o \<ss> \<theta>) (\<tau> X\<^sub>i) ! i" using nth_map by simp
        also from 82 have
          "\<dots> = map base (\<tau> X\<^sub>i) ! i" using instance_types_base instances_instance by metis
        also from iL\<tau>X\<^sub>i have
          "\<dots> = base (\<tau> X\<^sub>i ! i)" using nth_map by simp
        finally have
          C: "C = base (\<tau> X\<^sub>i ! i)" .
        from x iLx\<^sub>i \<sigma>\<^sub>1x\<^sub>i \<sigma>\<^sub>1x have
          \<sigma>z\<^sub>i: "the (\<sigma> (z\<^sub>i ! i)) = non_null \<iota>\<^sub>x" by (metis nth_map_eq)
        with xD\<sigma>\<^sub>1 \<sigma>\<^sub>1x\<^sub>in iLx\<^sub>i x have
          "\<sigma> (z\<^sub>i ! i) \<noteq> None" by (simp add: domIff)
        moreover from iLx\<^sub>i Lxz have
          "i < length (map (the \<circ> \<sigma>) z\<^sub>i)" by simp
        moreover note vt'
        ultimately have
          "h \<turnstile> (map (the \<circ> \<sigma>) z\<^sub>i) ! i : (map type_as_t T\<^sub>i) ! i"
          using runtime_type_assignments_nth by blast
        moreover from Lxz iLx\<^sub>i LT\<^sub>iz\<^sub>i have
          iLT\<^sub>i: "i < length T\<^sub>i" and
          iLbaseT\<^sub>i: "i < length (map base T\<^sub>i)" by simp_all
        moreover note Lxz iLx\<^sub>i
        ultimately have
          hz\<^sub>i: "h \<turnstile> the (\<sigma> (z\<^sub>i ! i)): type_as_t (T\<^sub>i ! i)" by simp
        with \<sigma>z\<^sub>i have
          \<iota>\<^sub>xD: "\<iota>\<^sub>x \<in> dom (\<chi>\<^sub>c h)" using RAddrE \<chi>\<^sub>c_def runtime_type_assignment.cases by metis
        with dh\<^sub>1 have
          \<iota>\<^sub>xDh\<^sub>1: "\<iota>\<^sub>x \<in> dom (\<chi>\<^sub>c h\<^sub>1)" by simp
        from hz\<^sub>i \<sigma>z\<^sub>i have
          *: "the (cls h \<iota>\<^sub>x) \<sqsubseteq> base (T\<^sub>i ! i)"
          using RAddrE unfolding \<chi>\<^sub>c_def type_as_t_def by simp
        from * create.hyps(4) \<chi>\<^sub>ch\<^sub>1 \<iota>\<^sub>1N \<iota>\<^sub>xD have
          "the (cls h\<^sub>1 \<iota>\<^sub>x) \<sqsubseteq> base (T\<^sub>i ! i)" by auto
        also from iLT\<^sub>i have
          "\<dots> = nth (map base T\<^sub>i) i" by simp
        also from baseT\<^sub>iX\<^sub>i iLbaseT\<^sub>i have
          "\<dots> \<sqsubseteq> (map base (\<tau> X\<^sub>i)) ! i" using list_leq_iff by blast
        also from iL\<tau>X\<^sub>i have
          "\<dots> = base ((\<tau> X\<^sub>i) ! i)" using nth_map by simp
        moreover note \<sigma>\<^sub>1x \<iota>\<^sub>xDh\<^sub>1 C
        ultimately show ?thesis by auto
      qed
      moreover from \<sigma>\<^sub>1y\<^sub>j \<sigma>\<^sub>1x have
        "h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : (C, n)" if "x \<in> set y\<^sub>j" using that by auto
      ultimately show "h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : (C, n)" by blast
    qed
    moreover have
      "\<Gamma>' \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI, rule conjI; (rule impI)?)
      fix x y
      assume
        xD: "x \<in> dom \<sigma>\<^sub>1" and
        cx: "committed (the (\<Gamma>' x))"
      with \<Gamma>'t have
        "deep_init\<^sub>v h\<^sub>1 (the (\<sigma>\<^sub>1 x))" if "x = this" using that by simp
      moreover from \<sigma>\<^sub>1y\<^sub>j have
        "deep_init\<^sub>v h\<^sub>1 (the (\<sigma>\<^sub>1 x))" if "x \<in> set y\<^sub>j" using that deep_init\<^sub>v_null by simp
      moreover have
        "deep_init\<^sub>v h\<^sub>1 (the (\<sigma>\<^sub>1 x))" if "x \<in> set x\<^sub>i" using that
      proof -
        assume
          "x \<in> set x\<^sub>i"
        with Lxz obtain i where
          ix\<^sub>i: "i < length x\<^sub>i" and
          iz\<^sub>i: "i < length z\<^sub>i" and
          x: "x = x\<^sub>i ! i" by (metis in_set_conv_nth) 
        with \<Gamma>z\<^sub>ix\<^sub>i have
          "\<Gamma> (z\<^sub>i ! i) \<sqsubseteq> \<Gamma>' (x\<^sub>i ! i)" unfolding list_leq_def using specialization_list_item
          by (metis nth_map length_map)
        with iz\<^sub>i \<sigma>\<^sub>1\<Gamma>' x xD have
          "the (\<Gamma> (z\<^sub>i ! i)) \<sqsubseteq> the (\<Gamma>' (x\<^sub>i ! i))" by (cases "\<Gamma>' (x\<^sub>i ! i)"; cases "\<Gamma> (z\<^sub>i ! i)") auto
        moreover note x cx
        ultimately have
          "committed (the (\<Gamma> (z\<^sub>i ! i)))"
          using initialization_subtyping by fastforce
        with create.prems(6) zD\<sigma> iz\<^sub>i have
          "deep_init\<^sub>v h (the (\<sigma> (z\<^sub>i ! i)))" using nth_mem c\<^sub>5_def by blast
        with \<sigma>\<^sub>1x\<^sub>in ix\<^sub>i x have
          "deep_init\<^sub>v h (the (\<sigma>\<^sub>1 x))" by simp
        moreover have
          "\<sigma> (z\<^sub>i ! i) \<noteq> Some (non_null \<iota>\<^sub>1)"
        proof (cases "\<sigma> (z\<^sub>i ! i)")
          case (Some v)
          then show ?thesis
          proof (cases "the (\<sigma> (z\<^sub>i ! i))")
            case (non_null \<iota>')
            with create.hyps(4) create.prems(5) \<iota>\<^sub>1N \<sigma>z\<^sub>iD iz\<^sub>i show ?thesis
              using value.discI value.sel by (metis nth_mem option.sel)
          qed simp
        qed simp
        with \<sigma>\<^sub>1x\<^sub>in ix\<^sub>i x xD have
          "the (\<sigma>\<^sub>1 x) \<noteq> non_null \<iota>\<^sub>1" by auto
        moreover note create.hyps(4) create.prems(9,10,12)
        ultimately show ?thesis using deep_init\<^sub>v_alloc by blast
      qed
      moreover note \<sigma>u xD 
      ultimately show "deep_init\<^sub>v h\<^sub>1 (the (\<sigma>\<^sub>1 x))" by blast
      assume
        yD: "y \<in> dom \<sigma>\<^sub>1" and
        fy: "free (the (\<Gamma>' y))"
      from \<Gamma>'t cx have
        "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))" if "x = this" using that by simp
      moreover from \<sigma>\<^sub>1y\<^sub>j have
        "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))" if "x \<in> set y\<^sub>j"
        using that reaches\<^sub>v_reaches by simp
      moreover have
        "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))" if "x \<in> set x\<^sub>i" using that
      proof (cases "the (\<sigma>\<^sub>1 x)")
        from that Lxz obtain i where
          ix\<^sub>i: "i < length x\<^sub>i" and
          iz\<^sub>i: "i < length z\<^sub>i" and
          x: "x = x\<^sub>i ! i" by (metis in_set_conv_nth) 
        with \<Gamma>z\<^sub>ix\<^sub>i have
          "\<Gamma> (z\<^sub>i ! i) \<sqsubseteq> \<Gamma>' (x\<^sub>i ! i)" unfolding list_leq_def using specialization_list_item
          by (metis nth_map length_map)
        with iz\<^sub>i \<sigma>\<^sub>1\<Gamma>' x xD have
          "the (\<Gamma> (z\<^sub>i ! i)) \<sqsubseteq> the (\<Gamma>' (x\<^sub>i ! i))" by (cases "\<Gamma>' (x\<^sub>i ! i)"; cases "\<Gamma> (z\<^sub>i ! i)") auto
        moreover note x cx
        ultimately have
          cz: "committed (the (\<Gamma> (z\<^sub>i ! i)))"
          using initialization_subtyping by fastforce
        with create.prems(6) zD\<sigma> iz\<^sub>i have
          "deep_init\<^sub>v h (the (\<sigma> (z\<^sub>i ! i)))" using nth_mem c\<^sub>5_def by blast
        case (non_null \<iota>\<^sub>x)
        moreover from that non_null \<sigma>\<^sub>1x\<^sub>iD have
          "ref (the (\<sigma>\<^sub>1 x)) \<in> dom (\<chi>\<^sub>c h)" by fastforce
        moreover from create.hyps(4) create.prems(9,10,12) have
          "\<forall> v. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) v (non_null \<iota>\<^sub>1) \<longrightarrow> v = non_null \<iota>\<^sub>1"
          using reaches\<^sub>v_alloc_to_new wf_heap_dom_weak by blast
        moreover note \<sigma>\<^sub>1t yD \<iota>\<^sub>1N
        ultimately have
          ?thesis if "y = this" using that by auto
        moreover from \<sigma>\<^sub>1y\<^sub>j have
          ?thesis if "y \<in> set y\<^sub>j" using reaches\<^sub>v_reaches that by simp
        moreover have
          ?thesis if "y \<in> set x\<^sub>i"
        proof (cases "the (\<sigma>\<^sub>1 y)")
          case (non_null \<iota>\<^sub>y)
          from that obtain j where
            jl: "j < length x\<^sub>i" and
            x\<^sub>ij: "x\<^sub>i ! j = y" by (meson in_set_conv_nth)
          from LT\<^sub>iz\<^sub>i Lxz jl have
            jLT\<^sub>i: "j < length T\<^sub>i" by linarith
          with \<Gamma>z\<^sub>i \<open>map (the \<circ> \<Gamma>) z\<^sub>i \<sqsubseteq> map (the \<circ> \<Gamma>') x\<^sub>i\<close> have
            "T\<^sub>i ! j \<sqsubseteq> map (the \<circ> \<Gamma>') x\<^sub>i ! j" using list_leq_iff by blast
          with jl have
            "T\<^sub>i ! j \<sqsubseteq> the (\<Gamma>' (x\<^sub>i ! j))" by simp
          with x\<^sub>ij have
            "initialization (T\<^sub>i ! j) \<sqsubseteq> initialization (the (\<Gamma>' y))"
            using initialization_subtyping by blast
          with fy \<Gamma>z\<^sub>i jLT\<^sub>i have
            "free (the (\<Gamma> (z\<^sub>i ! j)))" by auto
          with Lxz cz create.prems(6) iz\<^sub>i jl zD\<sigma> have
            "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> (z\<^sub>i ! i))) (the (\<sigma> (z\<^sub>i ! j)))" using c\<^sub>5_def
            by (metis nth_mem subset_code(1))
          with \<sigma>\<^sub>1x\<^sub>in ix\<^sub>i jl x x\<^sub>ij have
            "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))" by auto
          moreover note create.hyps(4) create.prems(9,10,12)
          ultimately show "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))"
            using reaches\<^sub>v_alloc by blast
        next
          case null
          then show ?thesis using reaches\<^sub>v_right_null by simp
        qed
        moreover note \<sigma>u yD
        ultimately show ?thesis by blast
      next
        case null
        then show ?thesis using reaches\<^sub>v_left_null by simp
      qed
      moreover note \<sigma>u xD
      ultimately show "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (the (\<sigma>\<^sub>1 y))" by blast
    qed
    moreover
    from 81 have
      "this \<notin> set (\<eta> Y\<^sub>j)" using constructor_no_this_in_locals
      unfolding constructor_locals_def variable_names_def by fastforce
    with 89 have
      "\<Gamma>' this = Some (C, \<zero>, !)" using fun_updr_other
      unfolding xS_upds_def variable_names_def by (metis (no_types) fun_upd_same)
    then have
      "\<not> nullable (the (\<Gamma>' this))" by simp
    with 90 have
      "\<Gamma>', \<Delta>''' \<turnstile>\<^sub>6" unfolding c\<^sub>6_def by blast
    ultimately have
      "\<Gamma>', \<Delta>''' \<turnstile> h\<^sub>1, \<sigma>\<^sub>1" unfolding good_configuration_def by blast
    note create.prems(1)
    moreover from \<sigma>\<^sub>1\<Gamma>' tD\<Gamma>' have
      "\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>1" by simp
    moreover note \<open>\<turnstile>\<^sub>2 h\<^sub>1\<close>
    moreover note \<open>\<Gamma>', \<Delta>''' \<turnstile>\<^sub>3 \<sigma>\<^sub>1\<close>
    moreover note \<open>\<Gamma>' \<turnstile>\<^sub>4 h\<^sub>1, \<sigma>\<^sub>1\<close>
    moreover note \<open>\<Gamma>' \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1\<close>
    moreover note \<open>\<Gamma>', \<Delta>''' \<turnstile>\<^sub>6\<close>
    moreover note 91
    moreover from create.hyps(4) create.prems(9,12) have
      "wf_heap_dom h\<^sub>1" using alloc_dom by blast
    moreover from create.prems(9,10,12) create.hyps(4) have
      "wf_heap_image h\<^sub>1" using alloc_image wf_heap_dom_weak by blast
    moreover note \<open>wf_statement s\<close>
    moreover from create.hyps(4) create.prems(12) have
      "unbounded_heap h\<^sub>1" using alloc_unbounded_heap by blast
    ultimately have
      "(\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>2) \<and>
      (\<turnstile>\<^sub>2 h') \<and>
      (\<Gamma>', \<Delta>'' \<turnstile>\<^sub>3 \<sigma>\<^sub>2) \<and>
      (\<forall>x\<in>dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h' \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma>' x))) \<and>
      (\<Gamma>' \<turnstile>\<^sub>5 h', \<sigma>\<^sub>2) \<and>
      (\<Gamma>', \<Delta>'' \<turnstile>\<^sub>6) \<and>
      \<epsilon> = ok \<and>
      (\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this \<and> dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1 \<and> h\<^sub>1 \<le> h') \<and>
      (\<forall>f\<in>\<Sigma>'.
          \<not> nullable (the (fType (the (cls h\<^sub>1 (ref (the (\<sigma>\<^sub>1 this)))), f))) \<longrightarrow>
          the (\<chi>\<^sub>v h' (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1).
          \<forall>f\<in>set (fields (the (cls h\<^sub>1 \<iota>))).
             \<not> nullable (the (fType (the (cls h\<^sub>1 \<iota>), f))) \<and> the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null \<longrightarrow>
             the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow> init h' \<iota>) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1).
          reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>) \<longrightarrow>
          reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>)) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
             committed (the (\<Gamma>' x)) \<and>
             \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<and>
             ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. committed (the (\<Gamma>' y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1).
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>'))) \<longrightarrow>
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))) \<and>
      (\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h\<^sub>1). \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h\<^sub>1). reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
             reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
             (\<exists>x\<in>dom \<sigma>\<^sub>1.
                 committed (the (\<Gamma>' x)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. \<exists>z\<in>dom \<sigma>\<^sub>1.
                    Type.free (the (\<Gamma>' y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))) \<and>
      (\<forall>f\<in>\<Sigma>'. f \<in> set (fields (base (the (\<Gamma>' this))))) \<and>
      wf_heap_dom h' \<and>
      wf_heap_image h' \<and>
      unbounded_heap h'" by (rule create.IH)
    then have
      "\<Gamma>' \<turnstile>\<^sub>1 \<sigma>\<^sub>2" and
      10: "\<turnstile>\<^sub>2 h'" and
      "\<Gamma>', \<Delta>'' \<turnstile>\<^sub>3 \<sigma>\<^sub>2" and
      IH_12: "\<forall>x\<in>dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h' \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma>' x))" and
      "\<Gamma>' \<turnstile>\<^sub>5 h', \<sigma>\<^sub>2" and
      "\<Gamma>', \<Delta>'' \<turnstile>\<^sub>6" and
      14: "\<epsilon> = ok" and
      IH_15: "\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this \<and> dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1 \<and> h\<^sub>1 \<le> h'" and
      IH_16: "\<forall>f\<in>\<Sigma>'.
          \<not> nullable (the (fType (the (cls h\<^sub>1 (ref (the (\<sigma>\<^sub>1 this)))), f))) \<longrightarrow>
          the (\<chi>\<^sub>v h' (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null" and
      IH_17: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> f \<in> set (fields (the (cls h\<^sub>1 \<iota>))).
             \<not> nullable (the (fType (the (cls h\<^sub>1 \<iota>), f))) \<and> the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null \<longrightarrow>
             the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null" and
      IH_18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow> init h' \<iota>" and
      IH_19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
             (\<exists> y \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH_20': "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1).
              reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>) \<longrightarrow>
              reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>'))) (non_null \<iota>)" and
      IH_20: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<forall>x\<in>dom \<sigma>\<^sub>2.
             reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
             committed (the (\<Gamma>' x)) \<and>
             \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<and>
             ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. committed (the (\<Gamma>' y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH_21': "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1).
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>'))) \<longrightarrow>
          reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))" and
      IH_22: "\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h\<^sub>1). \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h\<^sub>1). reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
             reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
             (\<exists>x\<in>dom \<sigma>\<^sub>1.
                 committed (the (\<Gamma>' x)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
             (\<exists>y\<in>dom \<sigma>\<^sub>1. \<exists>z\<in>dom \<sigma>\<^sub>1.
                    Type.free (the (\<Gamma>' y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
                    reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))" and
      "\<forall>f\<in>\<Sigma>'. f \<in> set (fields (base (the (\<Gamma>' this))))" and
      gWh': "wf_heap_dom h'" and
      wi: "wf_heap_image h'" and
      uh: "unbounded_heap h'" by metis+
    from 86 have
      "x \<in> dom \<Gamma>" using option_leq_dom by simp
    with create.prems(2) have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)" by auto
    from 88 create.prems(4) have
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)" by simp
    from create.hyps(4) create.prems(12) have
      hh\<^sub>1: "h \<le> h\<^sub>1" using alloc_predecessor by blast
    with IH_15 have
      hh': "h \<le> h'" using predecessor_transitive by blast
    have
      12: "\<forall> z \<in> dom (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)).
           the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z) \<noteq> null \<longrightarrow>
           h' \<turnstile> the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) z) : type_as_t (the (\<Gamma> z))"
    proof (rule ballI, rule impI)
      fix z
      let ?thesis = "h' \<turnstile> the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) z) : type_as_t (the (\<Gamma> z))"
      from IH_12 IH_15 \<Gamma>'t \<sigma>\<^sub>1\<Gamma>' \<sigma>\<^sub>1t tD\<Gamma>' have
        "h' \<turnstile> non_null \<iota>\<^sub>1 : (C, !)" using type_as_t value.distinct(1) by (metis option.sel)
      moreover from 86 have
        "(C, !) \<sqsubseteq> type_as_t (the (\<Gamma> x))" using subtyping_as_simple_t option_leq type_as_t
        by (metis (no_types, lifting) case_optionE option.discI option.sel)
      ultimately have
        ?thesis if "z = x" using runtime_type_assignment_leq that
        by (metis map_upd_Some_unfold option.sel)
      moreover assume
        z\<sigma>U: "z \<in> dom (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1))" and
        \<sigma>Uz: "the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) z) \<noteq> null"
      then have
        "z \<in> dom \<sigma>" and
        \<sigma>zN: "the (\<sigma> z) \<noteq> null" if "z \<noteq> x" using that by simp_all
      with create.prems(5) have
        "h \<turnstile> the (\<sigma> z) : type_as_t (the (\<Gamma> z))" if "z \<noteq> x" using that c\<^sub>4D' by blast
      with hh' have
        ?thesis if "z \<noteq> x" using that runtime_type_assignment_predecessor by (metis fun_upd_other)
      ultimately show ?thesis by simp
    qed
    from 88 create.prems(7) have
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" by simp
    from create.prems(2,11) 9 hh' have
      15: "(\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) this = \<sigma> this \<and> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) = dom \<sigma> \<and> h \<le> h'" by simp
    from 87 have
      16: "\<forall>f\<in>\<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
               the (\<chi>\<^sub>v h' (ref (the (\<sigma> this)), f)) \<noteq> null" by simp
    have
      17: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h). \<forall>f\<in>set (fields (the (cls h \<iota>))).
              \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
              the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> f
      assume
        \<iota>Dh: "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
        f: "f \<in> set (fields (the (cls h \<iota>)))"
      with dh\<^sub>1 \<iota>Dh hh\<^sub>1 have
        "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)"
        "f \<in> set (fields (the (cls h\<^sub>1 \<iota>)))" unfolding predecessor_def by simp_all
      moreover assume
        N: "\<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null"
      with \<iota>Dh hh\<^sub>1 have
        "\<not> nullable (the (fType (the (cls h\<^sub>1 \<iota>), f)))" unfolding predecessor_def by simp
      moreover from N \<iota>Dh \<iota>\<^sub>1N create.hyps(4) create.prems(12) have
        "the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null" using alloc_value by metis
      moreover note IH_17
      ultimately show "the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null" by blast
    qed
    have
      18: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h'). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h' \<iota>"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        \<iota>Dh': "\<iota> \<in> dom (\<chi>\<^sub>c h')" and
        \<iota>Dh: "\<iota> \<notin> dom (\<chi>\<^sub>c h)"
      show "init h' \<iota>"
      proof (cases "\<iota> = \<iota>\<^sub>1")
        case True
        show ?thesis unfolding init_def
        proof (rule ballI, rule impI)
          fix f
          assume
            "f \<in> set (fields (the (cls h' \<iota>)))"
            "\<not> nullable (the (fType (the (cls h' \<iota>), f)))"
          with True IH_15 \<chi>\<^sub>ch\<^sub>1\<iota>\<^sub>1 \<iota>\<^sub>1D have
            "f \<in> set (fields (C))"
            "\<not> nullable (the (fType (C, f)))" unfolding predecessor_def by simp_all
          moreover with 92 have
            "f \<in> \<Sigma>'" by (cases "fType (C, f)") auto
          moreover note \<chi>\<^sub>ch\<^sub>1\<iota>\<^sub>1 \<sigma>\<^sub>1t IH_16 True \<sigma>\<^sub>1t
          ultimately show
            "the (\<chi>\<^sub>v h' (\<iota>, f)) \<noteq> null" by simp
        qed
      next
        case False
        with \<iota>Dh' \<iota>Dh dh\<^sub>1 IH_18 show ?thesis by simp
      qed
    qed
    let ?F1 = "\<lambda> x z. \<sigma> z = \<sigma>\<^sub>1 x"
    let ?F2 = "\<lambda> x z. the (\<Gamma> z) \<sqsubseteq> the (\<Gamma>' x)"
    let ?F3 = "\<lambda> x z. \<forall> \<iota>. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>) \<longrightarrow>
        the (\<sigma> z) \<noteq> null \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>)"
    let ?F4 = "\<lambda> x z. \<forall> \<iota>. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (the (\<sigma>\<^sub>1 x)) \<longrightarrow>
        the (\<sigma> z) \<noteq> null \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> z))"
    have
      xz: "\<forall> x \<in> set x\<^sub>i. \<exists> z \<in> set z\<^sub>i. z \<in> dom \<sigma> \<and> ?F1 x z \<and> ?F2 x z  \<and> ?F3 x z \<and> ?F4 x z"
    proof (rule ballI)
      fix x
      assume
        "x \<in> set x\<^sub>i"
      then obtain i where
        iL: "i < length x\<^sub>i" and
        x: "x = x\<^sub>i ! i" by (metis in_set_conv_nth)
      moreover with Lxz zD\<sigma> obtain z where
        zD: "z \<in> set z\<^sub>i" and
        z: "z = z\<^sub>i ! i" by (metis nth_mem subsetD)
      moreover note \<sigma>\<^sub>1x\<^sub>in
      ultimately have
        F1: "?F1 x z" by simp
      moreover from Lxz iL t\<Gamma>z\<^sub>ix\<^sub>i LT\<^sub>iz\<^sub>i \<Gamma>z\<^sub>i x z have
        "?F2 x z" using list_leq_iff by (metis comp_apply nth_map)
      moreover from create.hyps(4) create.prems(9,10,12) F1 have
        "?F3 x z" using reaches\<^sub>v_alloc reaches\<^sub>v_left_null by auto
      moreover from create.hyps(4) create.prems(9,10,12) F1 have
        "?F4 x z" using reaches\<^sub>v_alloc reaches\<^sub>v_right_null by auto
      moreover note zD zD\<sigma>
      ultimately show "\<exists> z \<in> set z\<^sub>i. z \<in> dom \<sigma> \<and> ?F1 x z \<and> ?F2 x z  \<and> ?F3 x z \<and> ?F4 x z" by blast
    qed
    from 15 \<open>\<Gamma> \<turnstile>\<^sub>4 h, \<sigma>\<close> have
      \<sigma>nD: "\<forall> z \<in> dom \<sigma>. the (\<sigma> z) \<noteq> null \<longrightarrow> ref (the (\<sigma> z)) \<in> dom (\<chi>\<^sub>c h)" using c\<^sub>4_iff
      runtime_type_assignment.simps value.sel by metis
    from \<sigma>\<^sub>1t create.hyps(4) create.prems(9,12) have
      rtl: "\<forall> v. v \<noteq> non_null \<iota>\<^sub>1 \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 this)) v"
        using wf_heap_dom_weak reaches\<^sub>v_alloc_from_new by metis
    from \<sigma>\<^sub>1t create.hyps(4) create.prems(9,10,12) have
      rtr: "\<forall> v. v \<noteq> non_null \<iota>\<^sub>1 \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) v (the (\<sigma>\<^sub>1 this))"
        using wf_heap_dom_weak reaches\<^sub>v_alloc_to_new by metis
    from \<sigma>\<^sub>1y\<^sub>j have
      ry\<^sub>jl: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. x \<in> set y\<^sub>j \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) v" using reaches\<^sub>v_left_null by auto
    from \<sigma>\<^sub>1y\<^sub>j have
      ry\<^sub>jr: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. x \<in> set y\<^sub>j \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) v (the (\<sigma>\<^sub>1 x))" using reaches\<^sub>v_right_null by auto
    from \<sigma>u rtl ry\<^sub>jl have
      rx\<^sub>il: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. v \<noteq> non_null \<iota>\<^sub>1 \<longrightarrow> x \<notin> set x\<^sub>i \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) v"
      by auto
    from \<sigma>u rtr ry\<^sub>jr have
      rx\<^sub>ir: "\<forall> v. \<forall> x \<in> dom \<sigma>\<^sub>1. v \<noteq> non_null \<iota>\<^sub>1 \<longrightarrow> x \<notin> set x\<^sub>i \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) v (the (\<sigma>\<^sub>1 x))"
      by auto
    from xz have
      cx\<^sub>i: "\<forall> y \<in> set x\<^sub>i. committed (the (\<Gamma>' y)) \<longrightarrow> (\<exists> u \<in> dom \<sigma>. committed (the (\<Gamma> u)) \<and> \<sigma> u = \<sigma>\<^sub>1 y)"
      using committed_subtyping by blast
    from xz have
      fx\<^sub>i: "\<forall> y \<in> set x\<^sub>i. free (the (\<Gamma>' y)) \<longrightarrow> (\<exists> u \<in> dom \<sigma>. free (the (\<Gamma> u)) \<and> \<sigma> u = \<sigma>\<^sub>1 y)"
      using free_subtyping by blast
    have
      22: "\<forall>\<iota>\<^sub>1\<in>dom (\<chi>\<^sub>c h). \<forall>\<iota>\<^sub>2\<in>dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h') \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
              reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
              (\<exists>x\<in>dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
              (\<exists>y\<in>dom \<sigma>.
                  \<exists>z\<in>dom \<sigma>.
                     free (the (\<Gamma> y)) \<and>
                     reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
                     reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>' \<iota>''
      let ?H1 = "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>' \<iota>''"
      let ?H2 = "\<exists> x \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>'')"
      let ?H3 = "\<exists> y \<in> dom \<sigma>\<^sub>1. \<exists> z \<in> dom \<sigma>\<^sub>1.
                  free (the (\<Gamma>' y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>') (the (\<sigma>\<^sub>1 y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>'')"
      let ?G1 = "reaches (\<chi>\<^sub>v h) \<iota>' \<iota>''"
      let ?G2 = "\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>'')"
      let ?G3 = "\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
                  free (the (\<Gamma> y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>') (the (\<sigma> y)) \<and>
                  reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>'')"
      assume
        "\<iota>'D": "\<iota>' \<in> dom (\<chi>\<^sub>c h)" and
        "\<iota>''D": "\<iota>'' \<in> dom (\<chi>\<^sub>c h)"
      with dh\<^sub>1 have
        "\<iota>' \<in> dom (\<chi>\<^sub>c h\<^sub>1)"
        "\<iota>'' \<in> dom (\<chi>\<^sub>c h\<^sub>1)" by simp_all
      moreover assume
        "reaches (\<chi>\<^sub>v h') \<iota>' \<iota>''"
      moreover note IH_22
      ultimately have
        "?H1 \<or> ?H2 \<or> ?H3" by blast
      moreover from create.hyps(4) create.prems(9,10,12)have
        ?G1 if ?H1 using that reaches_alloc by auto
      moreover from \<iota>''D \<iota>\<^sub>1N xz rx\<^sub>il have
        ?G2 if ?H2 using that zD\<sigma>
        by (cases "x \<in> set x\<^sub>i") (metis in_mono committed_subtyping value.sel)+
      moreover obtain u v where
        "u \<in> dom \<sigma>\<^sub>1" and
        "v \<in> dom \<sigma>\<^sub>1" and
        fv: "free (the (\<Gamma>' v))" and
        ru: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>'')" and
        rv: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>') (the (\<sigma>\<^sub>1 v))" if ?H3 using that by blast
      with rx\<^sub>il rx\<^sub>ir \<iota>'D \<iota>''D \<iota>\<^sub>1N have
        "u \<in> set x\<^sub>i" and
        "v \<in> set x\<^sub>i" if ?H3 using that value.inject by metis+
      with fv ru rv xz have
        ?G3 if ?H3 using that free_subtyping by metis
      ultimately show "?G1 \<or> ?G2 \<or> ?G3" by blast
    qed
    have
      19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> z \<in> dom (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)).
              reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      assume
        \<iota>D': "\<iota> \<in> dom (\<chi>\<^sub>c h')" and
        zD: "z \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1))" and
        "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and>
           \<iota> \<in> dom (\<chi>\<^sub>c h)"
      then have
        r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>)" and
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" by simp_all
      with \<chi>\<^sub>ch\<^sub>1 have
        \<iota>D\<^sub>1: "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" by simp
      show "\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
      proof (cases "z = x")
        case True
        with r have
          "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (non_null \<iota>)" by simp
        with IH_15 IH_19 \<iota>D \<iota>D' \<iota>D\<^sub>1 \<iota>\<^sub>1N \<sigma>\<^sub>1\<Gamma>' \<sigma>\<^sub>1t rx\<^sub>il tD\<Gamma>' xz show ?thesis using value.sel by metis
      next
        case False
        with r \<iota>D 15 22 \<sigma>nD zD show ?thesis using reaches\<^sub>v_reaches value.sel by (metis fun_upd_apply)
      qed
    qed
(*
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> z \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)).
              reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and>
              committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h')" and
        zD: "z \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1))"
      assume
        "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and>
          committed (the (\<Gamma> z)) \<and>
          \<iota> \<in> dom (\<chi>\<^sub>c h) \<and>
          ref (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) \<in> dom (\<chi>\<^sub>c h)"
      then have
        r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>)" and
        cz: "committed (the (\<Gamma> z))" and
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
        rzD: "ref (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) \<in> dom (\<chi>\<^sub>c h)" by simp_all
      show "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
      proof (cases "z = x")
        case True
        with \<iota>\<^sub>1N rzD show ?thesis by simp
      next
        case False
        with r 22 zD \<iota>D \<sigma>nD have
          "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> z))) \<iota> \<or>
          (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>)) \<or>
          (\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>))"
          using reaches\<^sub>v_reaches by simp
        moreover from zD cz False r have ?thesis if "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> z))) \<iota>"
          using reaches\<^sub>v_reaches that by auto
        moreover have ?thesis
          if "\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>)"
          using that by blast
        moreover from 15 create.prems(6) cz zD have ?thesis
          if "\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>)" using c\<^sub>5_def that by blast
        ultimately show ?thesis by blast
      qed
    qed
*)
    have
      "20'": "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)"
      with hh' hh\<^sub>1 have
        \<iota>D': "\<iota> \<in> dom (\<chi>\<^sub>c h')" and
        \<iota>D\<^sub>1: "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom by blast+
      assume
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
      then obtain y where
        yD': "y \<in> dom (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1))" and
        cy: "committed (the (\<Gamma>  y))" and
        r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y)) (non_null \<iota>)" using reaches\<^sub>V\<^sub>v_values
        by (metis (mono_tags, lifting) comp_apply)
      with 15 have
        yD: "y \<in> dom \<sigma>" by blast
      show
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
      proof (cases "x = y")
        case True
        from True cy have
          "committed (the (\<Gamma> x))" by simp
        moreover from 86 \<open>x \<in> dom \<Gamma>\<close> have
          "(C, k, !) \<sqsubseteq> the (\<Gamma> x)" by auto
        ultimately have
          "k = \<one>" using committed_subtyping initialization by metis
        with 85 have
          cT\<^sub>i: "committed_all T\<^sub>i" using k.distinct(1) by presburger
        from True r have
          "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (non_null \<iota>)" by simp
        moreover from \<iota>D \<iota>\<^sub>1N \<sigma>\<^sub>1t rtl have
          "\<not> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (non_null \<iota>)" using value.sel by metis
        moreover note IH_15 IH_19 \<iota>D' \<iota>D\<^sub>1 \<sigma>\<^sub>1t \<sigma>u
        ultimately have
          "\<exists> v \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 v)) (non_null \<iota>)" by auto
        with \<iota>D \<iota>\<^sub>1N rx\<^sub>il have
          "\<exists> v \<in> set x\<^sub>i. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 v)) (non_null \<iota>)" using value.sel by metis
        with xz obtain z where
          zD: "z \<in> set z\<^sub>i" and
          rz: "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>)" by blast
        with \<Gamma>z\<^sub>i cT\<^sub>i LT\<^sub>iz\<^sub>i have
          "committed (the (\<Gamma> z))" using committed_all by auto 
        with rz zD zD\<sigma> show ?thesis using reaches\<^sub>V\<^sub>vI by (metis (mono_tags) comp_def subset_code(1))
      next
        case False
        with False r 22 \<iota>D \<sigma>nD yD have
          "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> y))) \<iota> \<or>
          (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>)) \<or>
          (\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>))"
          using reaches\<^sub>v_reaches by simp
        moreover from yD cy False r have ?thesis if "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> y))) \<iota>"
          using reaches\<^sub>v_reaches that by auto
        moreover have ?thesis
          if "\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>)"
          using that by auto
        moreover from create.prems(6) cy yD have ?thesis
          if "\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>)" using that c\<^sub>5_def by blast
        ultimately show ?thesis by blast
      qed
    qed
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> z \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)).
              reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and>
              committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h')"
        "z \<in> dom (\<sigma>(x \<mapsto> value.non_null \<iota>\<^sub>1))"
        "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) (non_null \<iota>) \<and>
           committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) z)) \<in> dom (\<chi>\<^sub>c h)"
      moreover then have
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h') (values (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
        using reaches\<^sub>V\<^sub>vI by (metis (mono_tags) comp_def)
      moreover note "20'"
      ultimately show "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
        using reaches\<^sub>V\<^sub>v_values by (metis (mono_tags) comp_def)
    qed
    have
      "21'": "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h).
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) (free \<circ> (the \<circ> \<Gamma>)))"
      then obtain y where
        yD: "y \<in> dom (\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1))" and
        fy: "free (the (\<Gamma> y))" and
        r: "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) y))" using reaches\<^sub>v\<^sub>V_values
        by (metis (mono_tags, lifting) comp_apply)
      have
        "\<exists> z \<in> dom \<sigma>. free (the (\<Gamma> z)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> z))"
      proof (cases "y = x")
        case True
        with r IH_15 \<sigma>\<^sub>1t have
          "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the (\<sigma>\<^sub>2 this))" by simp
        moreover from \<Gamma>'t have
          "free (the (\<Gamma>' this))" by simp
        moreover note IH_15 \<sigma>\<^sub>1\<Gamma>' tD\<Gamma>'
        ultimately have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>')))" using reaches\<^sub>v\<^sub>VI
          by (metis (mono_tags) comp_apply)
        moreover from \<iota>D \<chi>\<^sub>ch\<^sub>1 have
          "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" by simp
        moreover note IH_21'
        ultimately have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>')))" by blast
        then obtain z where
          "z \<in> dom \<sigma>\<^sub>1" and
          fz: "free (the (\<Gamma>' z))" and
          r\<^sub>1: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (the (\<sigma>\<^sub>1 z))" by auto
        with \<iota>D \<iota>\<^sub>1N rx\<^sub>ir xz show ?thesis using free_subtyping value.sel by metis
      next
        case False
        with r 22 yD \<iota>D \<sigma>nD have
          "reaches (\<chi>\<^sub>v h) \<iota> (ref (the (\<sigma> y))) \<or>
          (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (the (\<sigma> y))) \<or>
          (\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> y)))"
          using reaches\<^sub>v_reaches by simp
        moreover from yD fy False r have ?thesis if "reaches (\<chi>\<^sub>v h) \<iota> (ref (the (\<sigma> y)))"
          using reaches\<^sub>v_reaches that by auto
        moreover from 15 create.prems(6) fy yD have ?thesis
          if "\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (the (\<sigma> y))"
          using that c\<^sub>5_def by blast
        moreover have ?thesis
          if "\<exists> u \<in> dom \<sigma>. \<exists> v \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> y))"
          using that by blast
        ultimately show ?thesis by blast
      qed
      then show "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by auto
    qed
    from 87 have
      gS: "\<forall>f\<in>\<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by simp
    have
      13: "\<Gamma> \<turnstile>\<^sub>5 h', \<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI)
      fix y v
      assume
        vD': "v \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1))" and
        yD': "y \<in> dom (\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1))"
      with 9 create.prems(2) have
        vD: "v \<in> dom \<sigma>" and
        yD: "y \<in> dom \<sigma>" by auto
      assume
        cv: "committed (the (\<Gamma> v))"
      show
        "deep_init\<^sub>v h' (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) \<and>
       (free (the (\<Gamma> y)) \<longrightarrow>
          \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y)))"
      proof (cases "v = x")
        case True
        with cv have
          cx: "committed (the (\<Gamma> x))" by simp
        from 86 have
          "(C, k, !) \<sqsubseteq> the (\<Gamma> x)" by (cases "\<Gamma> x") simp_all
        with cx have
          "k = \<one>" using committed_subtyping initialization by metis
        with 85 have
          cT\<^sub>i: "committed_all T\<^sub>i" using k.distinct(1) by presburger
        have
          "deep_init\<^sub>v h' (non_null \<iota>\<^sub>1)" unfolding deep_init\<^sub>v_def
        proof (rule allI, rule impI, rule ccontr)
          fix \<iota>\<^sub>2
          assume
            r: "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (non_null \<iota>\<^sub>2)" and
            ih: "\<not> init h' \<iota>\<^sub>2"
          with IH_18 \<iota>\<^sub>1D gWh' wi have
            \<iota>\<^sub>2D\<^sub>1: "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using reaches\<^sub>v_source_dom reaches\<^sub>v_target_dom value.sel by metis
          with 18 IH_15 ih have
            \<iota>\<^sub>2D: "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h)" using predecessor_dom by blast
          with hh' have
            c\<iota>\<^sub>2: "cls h' \<iota>\<^sub>2 = cls h \<iota>\<^sub>2" using predecessor_cls by blast
          from create.hyps(4) create.prems(9,12) have
            "\<iota>\<^sub>2 = \<iota>\<^sub>1" if "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2"
            using reaches_alloc_from_new that wf_heap_dom_weak by blast
          with 18 \<iota>\<^sub>1N \<iota>\<^sub>1D IH_15 ih have
            rN: False if "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2" using that predecessor_dom by (metis subset_iff)
          moreover from \<open>\<Gamma>' \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1\<close> have
            "init h\<^sub>1 \<iota>\<^sub>2"
            if "\<exists> u \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>2)"
            using that unfolding c\<^sub>5_def deep_init\<^sub>v_def by blast
          with IH_15 IH_17 \<iota>\<^sub>2D\<^sub>1 ih have
            N2: False
            if "\<exists> u \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>2)"
            unfolding init_def using predecessor_cls that by metis
          moreover have
            False
            if "\<exists> u \<in> dom \<sigma>\<^sub>1. \<exists> u' \<in> dom \<sigma>\<^sub>1.
                 free (the (\<Gamma>' u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u')) (non_null \<iota>\<^sub>2)"
          proof -
            from that obtain u u' where
              "u \<in> dom \<sigma>\<^sub>1" and
              u'D: "u' \<in> dom \<sigma>\<^sub>1" and
              "free (the (\<Gamma>' u))" and
              r1: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 u))" and
              r2: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u')) (non_null \<iota>\<^sub>2)" by blast
            from r2 rtl have
              "\<iota>\<^sub>2 = \<iota>\<^sub>1" if "u' = this" using that by auto
            then have
              False if "u' = this" using that using calculation(1) by auto
            moreover from r2 u'D ry\<^sub>jl have
              False if "u' \<in> set y\<^sub>j" using that by blast
            moreover from xz obtain z where
              "z \<in> set z\<^sub>i" and
              zD: "z \<in> dom \<sigma>" and
              "\<sigma> z = \<sigma>\<^sub>1 u'" and
              \<Gamma>z: "the (\<Gamma> z) \<sqsubseteq> the (\<Gamma>' u')" and
              r\<^sub>1u'a: "\<forall>\<iota>. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u')) (non_null \<iota>) \<longrightarrow>
                the (\<sigma> z) \<noteq> null \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>)" and
              r\<^sub>1au': "\<forall>\<iota>. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (the (\<sigma>\<^sub>1 u')) \<longrightarrow>
                the (\<sigma> z) \<noteq> null \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> z))"
            if "u' \<in> set x\<^sub>i" using that by blast
            with 83 cT\<^sub>i have
              cz: "committed (the (\<Gamma> z))" if "u' \<in> set x\<^sub>i" using that committed_variables by blast
            with \<Gamma>z have
              "committed (the (\<Gamma>' u')) \<or> initialization (the (\<Gamma>' u')) = \<diamondop>" if "u' \<in> set x\<^sub>i"
              using that initialization_classified_subtyping by fastforce
            from N2 r2 u'D have
              False if "committed (the (\<Gamma>' u'))" using that by blast
            from \<sigma>\<^sub>1t r2 rN r\<^sub>1u'a rtr rx\<^sub>il u'D have
              "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2)" using reaches\<^sub>v_reaches value.sel by metis
            with 17 \<iota>\<^sub>2D create.prems(6) ih cz zD c\<iota>\<^sub>2 have
              False if "u' \<in> set x\<^sub>i" using not_init_deep_init\<^sub>v unfolding c\<^sub>5_def init_def
              using that by metis
            moreover note u'D \<sigma>u
            ultimately show False by blast
          qed
          moreover note IH_22 \<iota>\<^sub>1D \<iota>\<^sub>2D\<^sub>1 r
          ultimately show False using reaches\<^sub>v_reaches value.sel by metis
        qed
        with True have
          "deep_init\<^sub>v h' (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v))" by simp
        moreover have
          "free (the (\<Gamma> y)) \<longrightarrow>
          \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) y))"
        proof (rule impI, rule ccontr)
          assume
            fy: "free (the (\<Gamma> y))"
          with cx have
            yx: "y \<noteq> x" by auto
          moreover assume
            "\<not> \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (the ((\<sigma> (x \<mapsto> non_null \<iota>\<^sub>1)) y))"
          ultimately have
            r: "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>\<^sub>1) (the (\<sigma> y))" by simp
          then obtain \<iota>\<^sub>2 where
            \<iota>\<^sub>2: "\<iota>\<^sub>2 = ref (the (\<sigma> y))" by simp
          with \<sigma>nD hh\<^sub>1 r yD have
            \<iota>\<^sub>2D: "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom reaches\<^sub>v_reaches by blast 
          from create.hyps(4) create.prems(9,12) have
            "\<iota>\<^sub>2 = \<iota>\<^sub>1" if "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2"
            using reaches_alloc_from_new that wf_heap_dom_weak by blast
          with cx fy r \<iota>\<^sub>1N \<sigma>nD yD \<iota>\<^sub>2 have
            rN: False if "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2" using reaches\<^sub>v_reaches that by blast
          moreover have
            False
            if "\<exists> u \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>2)"
            using that
          proof (rule bexE)
            fix u
            assume
              uD: "u \<in> dom \<sigma>\<^sub>1"
            assume
              "committed (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>2)"
            then have
              cu: "committed (the (\<Gamma>' u))" and
              ru: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>2)" by simp_all
            from \<Gamma>'t cu have
              False if "u = this" using that by simp
            moreover from ry\<^sub>jl uD ru have
              False if "u \<in> set y\<^sub>j" using that by simp
            moreover from create.prems(6) ru xz fy yD 83 \<iota>\<^sub>2 cT\<^sub>i r have
              False if "u \<in> set x\<^sub>i" using c\<^sub>5_def committed_variables reaches\<^sub>v_right_null that
                value.collapse by metis
            moreover note \<sigma>u uD
            ultimately show False by auto
          qed
          moreover have
            False
            if "\<exists> u \<in> dom \<sigma>\<^sub>1. \<exists> u' \<in> dom \<sigma>\<^sub>1.
                 free (the (\<Gamma>' u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u')) (non_null \<iota>\<^sub>2)" using that
          proof -
            from that obtain u u' where
              "u \<in> dom \<sigma>\<^sub>1" and
              u'D: "u' \<in> dom \<sigma>\<^sub>1" and
              "free (the (\<Gamma>' u))" and
              ru: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 u))" and
              ru': "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u')) (non_null \<iota>\<^sub>2)" by blast
            with \<sigma>\<^sub>1t have
              False if "u' = this" using calculation(1) reaches\<^sub>v_reaches that by simp
            moreover from ry\<^sub>jl u'D ru' have
              False if "u' \<in> set y\<^sub>j" using that by blast
            moreover from 83 \<iota>\<^sub>2 cT\<^sub>i c\<^sub>5_def create.prems(6) r ru ru' xz yD fy have
              False if "u' \<in> set x\<^sub>i"
              using committed_variables reaches\<^sub>v_right_null that value.collapse by metis
            moreover note \<sigma>u u'D
            ultimately show False by blast
          qed
          moreover note IH_22 \<iota>\<^sub>1D \<iota>\<^sub>2 \<iota>\<^sub>2D r
          ultimately show False using reaches\<^sub>v_reaches value.sel by metis
        qed
        with True have
          "free (the (\<Gamma> y)) \<longrightarrow>
          \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y))" by simp
        ultimately show ?thesis by blast
      next
        case False
        have
          "deep_init\<^sub>v h' (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v))" unfolding deep_init\<^sub>v_def
        proof (rule allI, rule impI, rule ccontr)
          fix \<iota>'
          assume
            r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) (non_null \<iota>')"
          with 12 vD' wi have
            \<iota>'D': "\<iota>' \<in> dom (\<chi>\<^sub>c h')" using reaches\<^sub>v_reaches reaches\<^sub>v_target_dom
            using runtime_type_assignment.cases value.sel by (metis (mono_tags))
          moreover assume
            ih': "\<not> init h' \<iota>'"
          moreover note 18
          ultimately have
            \<iota>'D: "\<iota>' \<in> dom (\<chi>\<^sub>c h)" by blast
          from r' False have
            vN: "the (\<sigma> v) \<noteq> null" using reaches\<^sub>v_left_null by auto
          from r' False \<sigma>nD vD have
            vD\<sigma>: "ref (the (\<sigma> v)) \<in> dom (\<chi>\<^sub>c h)" using reaches\<^sub>v_reaches by auto
          from r' False have
            r: "reaches (\<chi>\<^sub>v h') (ref (the (\<sigma> v))) \<iota>'" using reaches\<^sub>v_reaches by simp
          from 17 \<iota>'D hh' ih' have
            ih: "\<not> init h \<iota>'" using init_def predecessor_cls by metis
          with create.prems(6) cv vD have False if "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (non_null \<iota>')"
            using that c\<^sub>5_def deep_init\<^sub>v_def by blast
          with r' False have False if "reaches (\<chi>\<^sub>v h) (ref (the (\<sigma> v))) \<iota>'"
            using reaches\<^sub>v_reaches that by simp
          moreover from create.prems(6) ih have False if
            "\<exists> u \<in> dom \<sigma>. committed (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> u)) (non_null \<iota>')"
            using that c\<^sub>5_def deep_init\<^sub>v_def by blast
          moreover from create.prems(6) cv vD have False if
            "\<exists> u \<in> dom \<sigma>. free (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> u))"
            using that c\<^sub>5_def by blast
          then have False if "\<exists> u \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
                 free (the (\<Gamma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> u)) \<and>
                 reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>')" using that by blast
          moreover note 22 \<iota>'D vD\<sigma> vN r
          ultimately show False using value.collapse by metis
        qed
        moreover have
          "free (the (\<Gamma> y)) \<longrightarrow>
          \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y))"
        proof (rule impI, rule ccontr)
          assume
            fy: "free (the (\<Gamma> y))" and
            "\<not> \<not> reaches\<^sub>v (\<chi>\<^sub>v h') (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) v)) (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y))"
          with False have
            r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> v)) (the ((\<sigma>(x \<mapsto> non_null \<iota>\<^sub>1)) y))" by simp
          show False
          proof (cases "y = x")
            case True
            with r' have
              r': "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> v)) (non_null \<iota>\<^sub>1)" by simp
            with \<sigma>nD vD have
              vD\<sigma>: "ref (the (\<sigma> v)) \<in> dom (\<chi>\<^sub>c h)" using reaches\<^sub>v_reaches by blast
            with hh\<^sub>1 have
              vD\<^sub>1\<sigma>: "ref (the (\<sigma> v)) \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom by blast
            from \<iota>\<^sub>1N \<sigma>\<^sub>1t \<sigma>nD rtr vD have False if "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma> v)) (non_null \<iota>\<^sub>1)"
              using that reaches\<^sub>v_reaches value.sel by metis
            moreover from \<open>\<Gamma>' \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1\<close> tD\<Gamma>' \<Gamma>'t \<sigma>\<^sub>1\<Gamma>' \<sigma>\<^sub>1t have False
              if "\<exists> u \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 u)) (non_null \<iota>\<^sub>1)"
              using that unfolding c\<^sub>5_def using initialization option.sel by metis
            moreover have
              False
              if "\<exists> u \<in> dom \<sigma>\<^sub>1. free (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma> v)) (the (\<sigma>\<^sub>1 u))"
              using that
            proof (rule bexE)
              fix u
              assume
                uD: "u \<in> dom \<sigma>\<^sub>1"
              assume
                "free (the (\<Gamma>' u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma> v)) (the (\<sigma>\<^sub>1 u))"
              then have
                "free (the (\<Gamma>' u))" and
                rvu: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma> v)) (the (\<sigma>\<^sub>1 u))" by simp_all
              with uD that \<sigma>\<^sub>1t \<sigma>u fx\<^sub>i ry\<^sub>jr have
                "\<exists> u' \<in> dom \<sigma>. free (the (\<Gamma> u')) \<and> \<sigma> u' = \<sigma>\<^sub>1 u"
                using calculation(1) by auto
              with create.prems(6) rvu cv uD vD \<iota>\<^sub>1N rx\<^sub>ir vD\<sigma> xz show False unfolding c\<^sub>5_def
                using reaches\<^sub>v_reaches using value.collapse value.sel by metis
            qed
            moreover note IH_22 \<iota>\<^sub>1D vD\<^sub>1\<sigma> create.prems(9,10,12) create.hyps(4) r'
            ultimately show ?thesis using reaches\<^sub>v_reaches value.sel by metis
          next
            case False
            with r' have
              r: "reaches\<^sub>v (\<chi>\<^sub>v h') (the (\<sigma> v)) (the (\<sigma> y))" by simp
            from create.prems(6) fy cv vD yD have False if "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> y))"
              using that c\<^sub>5_def by blast
            moreover from create.prems(6) fy yD have False
              if "\<exists> u \<in> dom \<sigma>. committed (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> u)) (the (\<sigma> y))"
              using that c\<^sub>5_def by blast
            moreover from create.prems(6) cv vD have False
              if "\<exists> u \<in> dom \<sigma>. free (the (\<Gamma> u)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> v)) (the (\<sigma> u))"
              using that c\<^sub>5_def by blast
            moreover note 22 r vD \<sigma>nD yD
            ultimately show ?thesis using reaches\<^sub>v_reaches value.collapse by metis
          qed
        qed
        ultimately show ?thesis by blast
      qed
    qed
    from 9 10 11 12 13 TN 14 15 16 17 18 19 "20'" 20 "21'" 22 gS gWh' wi uh show ?case by blast
  next
    case (cast \<sigma> y v h t x)
    from cast.prems(2,3,4,5,6,7,8) have
      C: "\<Gamma>, \<Delta> \<turnstile> h, \<sigma>" by auto
    from cast have
      28: "\<Delta>' = \<Delta> \<union> {x}" using TvarAssE  by blast
    from cast have
      29: "\<Sigma> = {}" by blast
    from cast.prems(8) obtain C k n\<^sub>1 D n\<^sub>2 T\<^sub>x where
      31: "\<Gamma>, \<Delta> \<turnstile> \<V> y: (C, k, n\<^sub>1)" and
      t: "t = (D, n\<^sub>2)" and
      32: "(D, k, n\<^sub>2) \<sqsubseteq> T\<^sub>x" and
      \<Gamma>x: "\<Gamma> x = Some T\<^sub>x" using TcastE by fastforce
    from \<Gamma>x have
      30: "x \<in> dom \<Gamma>" by auto
    from t 32 have
      "Some (D, k, n\<^sub>2) \<sqsubseteq> Some T\<^sub>x" by simp
    with \<Gamma>x have
      "Some (D, k, n\<^sub>2) \<sqsubseteq> \<Gamma> x" by simp
    moreover obtain T where
      T: "T = (D, k, n\<^sub>2)" by simp
    ultimately have
      "Some T \<sqsubseteq> \<Gamma> x" by simp
    with 32 \<Gamma>x have
      TTx: "T \<sqsubseteq> T\<^sub>x" by simp
    obtain C\<^sub>x k\<^sub>x n\<^sub>x where
      Tx: "T\<^sub>x = (C\<^sub>x, k\<^sub>x, n\<^sub>x)" using prod_cases3 by blast
    with cast.hyps t have
      V: "h \<turnstile> v : (D, n\<^sub>2)" by simp
    from cast.prems(2) 30 have
      "x \<in> dom \<sigma>" by simp
    with cast.hyps(1) have
      \<sigma>': "dom (\<sigma>(x := \<sigma> y)) = dom \<sigma>" by auto
    with cast.prems(2) have
      "dom (\<sigma>(x := \<sigma> y)) = dom \<Gamma>" by simp
    with cast.prems(2) have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma> (x := \<sigma> y)" unfolding c\<^sub>1_def by blast
    from cast.hyps(1) have
      yD: "y \<in> dom \<sigma>" using domI by blast
    from cast.prems(3) have
      10: "\<turnstile>\<^sub>2 h" .
    have
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3  \<sigma> (x := \<sigma> y)" unfolding c\<^sub>3_def
    proof (rule ballI, rule impI)
      fix z
      assume
        zD: "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        zN: "\<not> nullable (the (\<Gamma> z)) \<and> z \<in> \<Delta>'"
      show
        "the ((\<sigma>(x := \<sigma> y)) z) \<noteq> null"
      proof (cases "x = z")
        case True
        then show ?thesis
        proof (cases "nullable (the (\<Gamma> x))")
          case True
          with \<sigma>' zD zN "28" cast.prems(4) show ?thesis by auto
        next
          case False
          with TTx \<Gamma>x have
            N: "\<not> nullable T" using nullity_nullable_subtyping by fastforce
          with V T have
            "v \<noteq> null" by auto
          with True cast.hyps(1) show ?thesis by simp
        qed
      next
        case False
        with cast.prems(4) zD 28 \<sigma>' zN show ?thesis by auto
      qed
    qed
    have
      12: "\<forall> z \<in> dom (\<sigma>(x := \<sigma> y)).
           the ((\<sigma>(x := \<sigma> y)) z) \<noteq> null \<longrightarrow>
           h \<turnstile> the ((\<sigma>(x := \<sigma> y)) z) : type_as_t (the (\<Gamma> z))"
    proof (rule ballI, rule impI)
      fix z
      assume
        "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        zN: "the ((\<sigma>(x := \<sigma> y)) z) \<noteq> null"
      with \<sigma>' have
        zD: "z \<in> dom \<sigma>" by blast
      show "h \<turnstile> the ((\<sigma>(x := \<sigma> y)) z) : type_as_t (the (\<Gamma> z))"
      proof (cases "z = x")
        case True
        with T TTx \<Gamma>x cast.hyps(1) cast.hyps(2) show ?thesis
          using runtime_type_assignment_leq subtyping_as_simple_t t type_as_t
          by (metis fun_upd_same option.sel)
      next
        case False
        with zN have
          "the (\<sigma> z) \<noteq> null" by simp
        with cast.prems(5) zD have
          "h \<turnstile> the (\<sigma> z) : type_as_t (the (\<Gamma> z))" by blast
        moreover from False have
           "the ((\<sigma>(x := \<sigma> y)) z) = the (\<sigma> z)" by simp
        ultimately show ?thesis by simp
      qed
    qed
    from cast.prems(6) have
      cd: "\<forall> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<longrightarrow> deep_init\<^sub>v h (the (\<sigma> x))" using c\<^sub>5_def by blast
    from cast.prems(6) have
      fn: "\<forall> x \<in> dom \<sigma>. \<forall> y \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<longrightarrow>
        (free (the (\<Gamma> y)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (the (\<sigma> y)))" using c\<^sub>5_def by blast
    have
      13: "\<Gamma> \<turnstile>\<^sub>5 h, \<sigma> (x := \<sigma> y)" unfolding c\<^sub>5_def
    proof (rule ballI, rule ballI, rule impI)
      fix z w
      assume
        "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        cz: "committed (the (\<Gamma> z))"
      with \<sigma>' have
        zD:"z \<in> dom \<sigma>" by blast
      have
        "deep_init\<^sub>v h (the ((\<sigma>(x := \<sigma> y)) z))"
      proof (cases "z = x")
        case True
        with Tx \<Gamma>x cz 32 have
          "k = \<one>" using initialization_subtyping by fastforce
        with 31 yD have
          "committed (the (\<Gamma> y))" by auto
        with cd yD True show ?thesis by simp
      next
        case False
        with cd cz zD show ?thesis by simp
      qed
      moreover
      assume
        "w \<in> dom (\<sigma>(x := \<sigma> y))"
      with \<sigma>' have
        wD: "w \<in> dom \<sigma>" by simp
      {
        assume
          fw: "free (the (\<Gamma> w))"
        have
          "\<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (the ((\<sigma>(x := \<sigma> y)) w))"
        proof (cases "z = x")
          case True
          with 31 Tx \<Gamma>x fw cz 32 fn wD yD show ?thesis
          using initialization initialization_classified_subtyping lemma_2\<^sub>4'
          using k.distinct(5) by (metis fun_upd_apply option.sel)
        next
          case False
          with 31 Tx \<Gamma>x fw wD cz 32 fn yD zD show ?thesis
            using initialization initialization_leq initialization_subtyping
            using TvarE k.distinct(3)
            by (metis fun_upd_apply option.sel)
        qed
      }
      ultimately show
        "deep_init\<^sub>v h (the ((\<sigma>(x := \<sigma> y)) z)) \<and>
           (free (the (\<Gamma> w)) \<longrightarrow> \<not> reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (the ((\<sigma>(x := \<sigma> y)) w)))"
        by simp
    qed
    from cast.prems(7,8) have
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" by auto
    have
      14: "ok = ok" by simp
    from "9" cast.prems(2,11) have
      15: "(\<sigma>(x := \<sigma> y)) this = \<sigma> this \<and> dom (\<sigma>(x := \<sigma> y)) = dom \<sigma> \<and> h \<le> h" by simp
    from 29 have
      16: "\<forall> f \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
               the (\<chi>\<^sub>v h (ref (the (\<sigma> this)), f)) \<noteq> null" by simp
    have
      17: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           \<forall> f \<in> set (fields (the (cls h \<iota>))).
              \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
              the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null" by simp
    have
      18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h \<iota>" by simp
    have
      19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> z \<in> dom (\<sigma>(x := \<sigma> y)).
          reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
            (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" by auto
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> z \<in> dom (\<sigma>(x := \<sigma> y)).
          reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (non_null \<iota>) \<and>
          committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x := \<sigma> y)) z)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
            (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> z
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)" and
        zD: "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (non_null \<iota>) \<and>
          committed (the (\<Gamma> z)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the ((\<sigma>(x := \<sigma> y)) z)) \<in> dom (\<chi>\<^sub>c h)"
      then have
        rz: "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (non_null \<iota>)" and
        cz: "committed (the (\<Gamma> z))" and
        zD\<^sub>h: "ref (the ((\<sigma>(x := \<sigma> y)) z)) \<in> dom (\<chi>\<^sub>c h)" by simp_all
      show
        "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
      proof (cases "z = x")
        case True
        with cz have
          cx: "committed (the (\<Gamma> x))" by simp
        with T TTx \<Gamma>x have "k = \<one>" using initialization_subtyping by fastforce
        moreover from True rz have
          rx: "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)" by simp
        moreover note 31 yD
        ultimately show ?thesis using TvarE initialization by metis
      next
        case False
        with zD \<sigma>' cz rz show ?thesis by auto
      qed
    qed
    have
      g20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
             reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values (\<sigma>(x := \<sigma> y)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
             reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)"
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values (\<sigma>(x := \<sigma> y)) (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
      moreover
      then obtain z where
        "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        "reaches\<^sub>v (\<chi>\<^sub>v h) (the ((\<sigma>(x := \<sigma> y)) z)) (non_null \<iota>)" and
        "committed (the (\<Gamma> z))" by auto
      moreover
      with \<iota>D cast.prems(9)have
        "ref (the ((\<sigma>(x := \<sigma> y)) z)) \<in> dom (\<chi>\<^sub>c h)" using reaches\<^sub>v_source_dom by fastforce
      moreover note 20
      ultimately have
        "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)" by blast
      then show "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" by auto
    qed
    have
      g21': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values (\<sigma>(x := \<sigma> y)) (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)"
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values (\<sigma>(x := \<sigma> y)) (free \<circ> (the \<circ> \<Gamma>)))"
      then obtain z where
        "z \<in> dom (\<sigma>(x := \<sigma> y))" and
        fz: "free (the (\<Gamma> z))" and
        r': "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the ((\<sigma>(x := \<sigma> y)) z))" by auto
      with \<sigma>' have
        zD: "z \<in> dom \<sigma>" by blast
      have
        "\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y))"
      proof (cases "x = z")
        case True
        with fz have
          fx: "free (the (\<Gamma> x))" by simp
        with T TTx \<Gamma>x Tx have "k = \<zero>" using initialization_subtyping by fastforce
        moreover
        with True r' have
          "reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y))" by simp
        moreover note yD 31
        ultimately show ?thesis using TvarE initialization by metis
      next
        case False
        with fz r' zD show ?thesis by auto
      qed
      then show "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by auto
    qed
    have
      22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
          reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
          (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and>reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
          (\<exists> y \<in> dom \<sigma>. \<forall> z \<in>dom \<sigma>.
             free (the (\<Gamma> y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))" by simp
    from 29 have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by simp
    from cast.prems(9) have
      Wh': "wf_heap_dom h" .
    from cast.prems(10) have
      wi: "wf_heap_image h" .
    from cast.prems(12) have
      uh: "unbounded_heap h" .
    from 9 10 11 12 13 TN 14 15 16 17 18 19 g20' 20 g21' 22 gS Wh' wi uh show ?case by blast
  next
    case (castBad \<sigma> y v h t x e)
    from castBad.prems(1) have
      "False" by simp
    then show ?case by simp
  next
    case (seq h \<sigma> s\<^sub>1 h\<^sub>1 \<sigma>\<^sub>1 s\<^sub>2 h\<^sub>2 \<sigma>\<^sub>2 \<epsilon> \<Gamma> \<Delta> \<Delta>' \<Sigma>)
    from seq.prems(2) have
      p3\<^sub>1: "dom \<sigma> = dom \<Gamma>" and
      p3\<^sub>2: "this \<in> dom \<sigma>" by auto
    from seq.prems(8) obtain \<Delta>\<^sub>1 \<Sigma>\<^sub>1 \<Sigma>\<^sub>2 where
      96: "\<Sigma> = \<Sigma>\<^sub>1 \<union> \<Sigma>\<^sub>2" and
      97: "\<Gamma>, \<Delta> \<turnstile> s\<^sub>1 | \<Delta>\<^sub>1, \<Sigma>\<^sub>1" and
      98: "\<Gamma>, \<Delta>\<^sub>1 \<turnstile> s\<^sub>2 | \<Delta>', \<Sigma>\<^sub>2" by auto
    have
      "ok \<noteq> castExc" by simp
    moreover note seq.prems(2)
    moreover note seq.prems(3)
    moreover note seq.prems(4)
    moreover note seq.prems(5)
    moreover note seq.prems(6)
    moreover note seq.prems(7)
    moreover note 97
    moreover note seq.prems(9)
    moreover note seq.prems(10)
    moreover from seq.prems(11) have
      "wf_statement s\<^sub>1" by simp
    moreover note seq.prems(12)
    ultimately have
      "(\<Gamma> \<turnstile>\<^sub>1 \<sigma>\<^sub>1) \<and>
      (\<turnstile>\<^sub>2 h\<^sub>1) \<and>
      (\<Gamma>, \<Delta>\<^sub>1 \<turnstile>\<^sub>3 \<sigma>\<^sub>1) \<and>
      (\<forall>x\<in>dom \<sigma>\<^sub>1. the (\<sigma>\<^sub>1 x) \<noteq> null \<longrightarrow> h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : type_as_t (the (\<Gamma> x))) \<and>
      (\<Gamma> \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1) \<and>
      (\<Gamma>, \<Delta>\<^sub>1 \<turnstile>\<^sub>6) \<and>
      ok = ok \<and>
      (\<sigma>\<^sub>1 this = \<sigma> this \<and> dom \<sigma>\<^sub>1 = dom \<sigma> \<and> h \<le> h\<^sub>1) \<and>
      (\<forall> f \<in> \<Sigma>\<^sub>1. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
          the (\<chi>\<^sub>v h\<^sub>1 (ref (the (\<sigma> this)), f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))).
        \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
          the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null) \<and>
      (\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h\<^sub>1 \<iota>) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> x \<in> dom \<sigma>\<^sub>1.
        reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (value.non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
          (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (value.non_null \<iota>))) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
         reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
         reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> x \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>) \<and>
        committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>1 x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
          (\<exists>y\<in>dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))) \<and>
      (\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
        reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
        (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
        (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
           free (the (\<Gamma> y)) \<and>
           reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
           reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))) \<and>
      (\<forall> f \<in> \<Sigma>\<^sub>1. f \<in> set (fields (base (the (\<Gamma> this))))) \<and>
      wf_heap_dom h\<^sub>1 \<and>
      wf_heap_image h\<^sub>1 \<and>
      unbounded_heap h\<^sub>1" by (rule seq.IH(1))
    then have
      IH1_9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma>\<^sub>1" and
      IH1_10: "\<turnstile>\<^sub>2 h\<^sub>1" and
      IH1_11: "\<Gamma>, \<Delta>\<^sub>1 \<turnstile>\<^sub>3 \<sigma>\<^sub>1" and
      IH1_12: "\<forall> x \<in> dom \<sigma>\<^sub>1. the (\<sigma>\<^sub>1 x) \<noteq> null \<longrightarrow> h\<^sub>1 \<turnstile> the (\<sigma>\<^sub>1 x) : type_as_t (the (\<Gamma> x))" and
      IH1_13: "\<Gamma> \<turnstile>\<^sub>5 h\<^sub>1, \<sigma>\<^sub>1" and
      IH1_TN: "\<Gamma>, \<Delta>\<^sub>1 \<turnstile>\<^sub>6" and
      IH1_15\<^sub>1: "\<sigma>\<^sub>1 this = \<sigma> this" and
      IH1_15\<^sub>2: "dom \<sigma>\<^sub>1 = dom \<sigma>" and
      IH1_15\<^sub>3: "h \<le> h\<^sub>1" and
      IH1_16: "\<forall> f \<in> \<Sigma>\<^sub>1. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
              the (\<chi>\<^sub>v h\<^sub>1 (ref (the (\<sigma> this)), f)) \<noteq> null" and
      IH1_17:  "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))). \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and> the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow>
          the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null" and
      IH1_18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h\<^sub>1 \<iota>" and
      IH1_19: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1). \<forall>x\<in>dom \<sigma>\<^sub>1.
              reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
                (\<exists>y\<in>dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" and
      IH1_20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
                 reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
                 reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" and
      IH1_20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> x \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>) \<and>
                committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>1 x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
                  (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" and
      IH1_21': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" and
      IH1_22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
                reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
                (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
                (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
                   free (the (\<Gamma> y)) \<and>
                   reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
                   reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))" and
      IH1_S: "\<forall> f \<in> \<Sigma>\<^sub>1. f \<in> set (fields (base (the (\<Gamma> this))))" and
      IH1_Wh: "wf_heap_dom h\<^sub>1" and
      IH1_wi: "wf_heap_image h\<^sub>1" and
      IH1_uh: "unbounded_heap h\<^sub>1" by metis+ (* blast+ works, but much slower *)
    note seq.prems(1)
    moreover note IH1_9
    moreover note IH1_10
    moreover note IH1_11
    moreover from IH1_12 IH1_9 have
      "\<Gamma> \<turnstile>\<^sub>4 h\<^sub>1, \<sigma>\<^sub>1" by (simp add: c\<^sub>4I)
    moreover note IH1_13
    moreover note IH1_TN
    moreover note 98
    moreover note IH1_Wh
    moreover note IH1_wi
    moreover from seq.prems(11) have
      "wf_statement s\<^sub>2" by simp
    moreover note IH1_uh
    ultimately have
      "(\<Gamma> \<turnstile>\<^sub>1 \<sigma>\<^sub>2) \<and>
      (\<turnstile>\<^sub>2 h\<^sub>2) \<and>
      (\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>\<^sub>2) \<and>
      (\<forall>x\<in>dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h\<^sub>2 \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma> x))) \<and>
      (\<Gamma> \<turnstile>\<^sub>5 h\<^sub>2, \<sigma>\<^sub>2) \<and>
      (\<Gamma>, \<Delta>' \<turnstile>\<^sub>6) \<and>
      \<epsilon> = ok \<and>
      (\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this \<and> dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1 \<and> h\<^sub>1 \<le> h\<^sub>2) \<and>
      (\<forall> f \<in> \<Sigma>\<^sub>2. \<not> nullable (the (fType (the (cls h\<^sub>1 (ref (the (\<sigma>\<^sub>1 this)))), f))) \<longrightarrow>
         the (\<chi>\<^sub>v h\<^sub>2 (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> f \<in> set (fields (the (cls h\<^sub>1 \<iota>))).  \<not> nullable (the (fType (the (cls h\<^sub>1 \<iota>), f))) \<and>
         the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null \<longrightarrow> the (\<chi>\<^sub>v h\<^sub>2 (\<iota>, f)) \<noteq> null) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<iota> \<notin> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow> init h\<^sub>2 \<iota>) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2.
        reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
        (\<exists> y \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1).
        reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
        reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
        committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
          (\<exists> y \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))) \<and>
      (\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1).
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>2) (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
        reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>)))) \<and>
      (\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h\<^sub>1). reaches (\<chi>\<^sub>v h\<^sub>2) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
        reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
        (\<exists> x \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
        (\<exists> y \<in> dom \<sigma>\<^sub>1. \<exists> z \<in> dom \<sigma>\<^sub>1.
           free (the (\<Gamma> y)) \<and>
           reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
           reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))) \<and>
      (\<forall> f \<in> \<Sigma>\<^sub>2. f \<in> set (fields (base (the (\<Gamma> this))))) \<and>
      wf_heap_dom h\<^sub>2 \<and>
      wf_heap_image h\<^sub>2 \<and>
      unbounded_heap h\<^sub>2" by (rule seq.IH(2))
    then have
      9: "\<Gamma> \<turnstile>\<^sub>1 \<sigma>\<^sub>2" and
      10: "\<turnstile>\<^sub>2 h\<^sub>2" and
      11: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>3 \<sigma>\<^sub>2" and
      12: "\<forall> x \<in> dom \<sigma>\<^sub>2. the (\<sigma>\<^sub>2 x) \<noteq> null \<longrightarrow> h\<^sub>2 \<turnstile> the (\<sigma>\<^sub>2 x) : type_as_t (the (\<Gamma> x))" and
      13: "\<Gamma> \<turnstile>\<^sub>5 h\<^sub>2, \<sigma>\<^sub>2" and
      TN: "\<Gamma>, \<Delta>' \<turnstile>\<^sub>6" and
      14: "\<epsilon> = ok" and
      IH2_15\<^sub>1: "\<sigma>\<^sub>2 this = \<sigma>\<^sub>1 this" and
      IH2_15\<^sub>2: "dom \<sigma>\<^sub>2 = dom \<sigma>\<^sub>1" and
      IH2_15\<^sub>3: "h\<^sub>1 \<le> h\<^sub>2" and
      IH2_16: "\<forall> f \<in> \<Sigma>\<^sub>2. \<not> nullable (the (fType (the (cls h\<^sub>1 (ref (the (\<sigma>\<^sub>1 this)))), f))) \<longrightarrow>
         the (\<chi>\<^sub>v h\<^sub>2 (ref (the (\<sigma>\<^sub>1 this)), f)) \<noteq> null" and
      IH2_17: "\<forall>\<iota>\<in>dom (\<chi>\<^sub>c h\<^sub>1). \<forall> f \<in> set (fields (the (cls h\<^sub>1 \<iota>))). \<not> nullable (the (fType (the (cls h\<^sub>1 \<iota>), f))) \<and>
         the (\<chi>\<^sub>v h\<^sub>1 (\<iota>, f)) \<noteq> null \<longrightarrow> the (\<chi>\<^sub>v h\<^sub>2 (\<iota>, f)) \<noteq> null" and
      IH2_18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<iota> \<notin> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow> init h\<^sub>2 \<iota>" and
      IH2_19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2.
              reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
                (\<exists> y \<in> dom \<sigma>\<^sub>1. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH2_20': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1).
                reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
                reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" and
      IH2_20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
                committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h\<^sub>1) \<longrightarrow>
                (\<exists> y \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 y)) (non_null \<iota>))" and
      IH2_21': "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1).
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>2) (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
                reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>)))" and
      IH2_22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h\<^sub>1). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h\<^sub>1). reaches (\<chi>\<^sub>v h\<^sub>2) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
                reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
                (\<exists> x \<in> dom \<sigma>\<^sub>1. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
                (\<exists> y \<in> dom \<sigma>\<^sub>1. \<exists> z \<in> dom \<sigma>\<^sub>1.
                   free (the (\<Gamma> y)) \<and>
                   reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
                   reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))" and
      IH2_S: "\<forall> f \<in> \<Sigma>\<^sub>2. f \<in> set (fields (base (the (\<Gamma> this))))" and
      Wh': "wf_heap_dom h\<^sub>2" and
      wi: "wf_heap_image h\<^sub>2" and
      uh: "unbounded_heap h\<^sub>2"
      by metis+ (* blast+ works, but much slower *)
    from IH1_15\<^sub>1 IH2_15\<^sub>1 have
      "15\<^sub>1": "\<sigma>\<^sub>2 this = \<sigma> this" by simp
    moreover from seq.prems(2) 9 have
      "15\<^sub>2": "dom \<sigma>\<^sub>2 = dom \<sigma>" by simp
    moreover from IH1_15\<^sub>3 IH2_15\<^sub>3 have
      "15\<^sub>3": "h \<le> h\<^sub>2" by (rule predecessor_transitive) 
    ultimately have
      15: "\<sigma>\<^sub>2 this = \<sigma> this \<and> dom \<sigma>\<^sub>2 = dom \<sigma> \<and> h \<le> h\<^sub>2" by simp
    from IH1_17 IH2_17 IH1_15\<^sub>3 have
      17: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). \<forall> f \<in> set (fields (the (cls h \<iota>))). \<not> nullable (the (fType (the (cls h \<iota>), f))) \<and>
            the (\<chi>\<^sub>v h (\<iota>, f)) \<noteq> null \<longrightarrow> the (\<chi>\<^sub>v h\<^sub>2 (\<iota>, f)) \<noteq> null"
      unfolding predecessor_def by (simp add: subset_iff)
    from 96 IH1_S IH2_S have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by blast
    have
      16: "\<forall> f \<in> \<Sigma>. \<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f))) \<longrightarrow>
               the (\<chi>\<^sub>v h\<^sub>2 (ref (the (\<sigma> this)), f)) \<noteq> null"
    proof (rule ballI, rule impI)
      fix f
      assume
        F: "f \<in> \<Sigma>" and
        N: "\<not> nullable (the (fType (the (cls h (ref (the (\<sigma> this)))), f)))"
      from seq.prems(2) have
        "this \<in> dom \<sigma>" using c\<^sub>1_def by blast
      with seq.prems(4,5,7) have
        tD: "ref (the (\<sigma> this)) \<in> dom (\<chi>\<^sub>c h)" using runtime_type_assignment.cases value.sel
        using c\<^sub>3_def c\<^sub>4_def c\<^sub>6_def
        by (metis old.prod.exhaust)
      with IH1_15\<^sub>3 IH1_15\<^sub>1 have
        c1: "cls h\<^sub>1 (ref (the (\<sigma>\<^sub>1 this))) = cls h (ref (the (\<sigma> this)))" unfolding predecessor_def by simp
      from tD IH1_15\<^sub>1 "15\<^sub>3" have
        c2: "cls h\<^sub>2 (ref (the (\<sigma>\<^sub>1 this))) = cls h (ref (the (\<sigma> this)))" unfolding predecessor_def by simp
      then show "the (\<chi>\<^sub>v h\<^sub>2 (ref (the (\<sigma> this)), f)) \<noteq> null"
      proof (cases "f \<in> \<Sigma>\<^sub>2")
        case True
        with IH2_16 IH1_15\<^sub>1 N c1 show ?thesis by simp
      next
        case False
        with 96 F have
          "f \<in> \<Sigma>\<^sub>1" by blast
        moreover with IH1_16 N have
          "the (\<chi>\<^sub>v h\<^sub>1 (ref (the (\<sigma> this)), f)) \<noteq> null" by blast
        moreover from seq.prems(2,4,7) have
          "the (\<sigma> this) \<noteq> null" unfolding c\<^sub>1_def c\<^sub>3_def c\<^sub>6_def by blast
        moreover from IH1_15\<^sub>1 N c2 have
          "\<not> nullable (the (fType (the (cls h\<^sub>2 (ref (the (\<sigma> this)))), f)))" by auto
        moreover note IH1_12 IH1_15\<^sub>1 IH1_9 IH1_S IH2_17 c1 c2
        ultimately show ?thesis using monotonic_fields simple_base
            type_as_t_class value.sel runtime_type_assignment.simps unfolding c\<^sub>1_def
          by (metis (no_types) in_mono)
      qed
    qed
    have
      18: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<iota> \<notin> dom (\<chi>\<^sub>c h) \<longrightarrow> init h\<^sub>2 \<iota>"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        I: "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2)" and
        E: "\<iota> \<notin> dom (\<chi>\<^sub>c h)"
      show
        "init h\<^sub>2 \<iota>"
      proof (cases "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)")
        case True
        with E IH1_18 have
          "init h\<^sub>1 \<iota>" by blast
        with 17 IH2_15\<^sub>3 IH2_17 True show ?thesis unfolding init_def predecessor_def by simp
      next
        case False
        with I IH2_18 show ?thesis by blast
      qed
    qed
    from IH1_15\<^sub>3 IH1_19 IH2_19 have
      19: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2.
            reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))" 
      by (meson in_mono predecessor_dom)
    have
      "20'": "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>) \<longrightarrow>
           reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)"
      moreover
      with IH1_15\<^sub>3 have
        "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom by blast
      moreover
      assume
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)"
      moreover note IH1_20' IH2_20'
      ultimately show "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" by blast
    qed
    have
      20: "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2). \<forall> x \<in> dom \<sigma>\<^sub>2. reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and>
            committed (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
              (\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota> x
      assume
        \<iota>D\<^sub>2: "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>2)" and
        xD: "x \<in> dom \<sigma>\<^sub>2" and
        "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>) \<and> committed (the (\<Gamma> x)) \<and>
          \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> ref (the (\<sigma>\<^sub>2 x)) \<in> dom (\<chi>\<^sub>c h)"
      then have
        "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (the (\<sigma>\<^sub>2 x)) (non_null \<iota>)" and
        "committed (the (\<Gamma> x))" and
        \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" by simp_all
      with xD have
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>2) (values \<sigma>\<^sub>2 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" by auto
      with \<iota>D "20'" have
        "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>)" by blast
      then show "\<exists> y \<in> dom \<sigma>. committed (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> y)) (non_null \<iota>)"
        by auto
    qed
    have
      "21'": "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h).
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>2) (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>))) \<longrightarrow>
           reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))"
    proof (rule ballI, rule impI)
      fix \<iota>
      assume
        "\<iota> \<in> dom (\<chi>\<^sub>c h)"
      moreover
      with IH1_15\<^sub>3 have
        "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom by blast
      moreover assume
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>2) (non_null \<iota>) (values \<sigma>\<^sub>2 (free \<circ> (the \<circ> \<Gamma>)))"
      moreover note IH2_21' IH1_21'
      ultimately show
        "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by blast
    qed
    have
      22: "\<forall> \<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h). \<forall> \<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h). reaches (\<chi>\<^sub>v h\<^sub>2) \<iota>\<^sub>1 \<iota>\<^sub>2 \<longrightarrow>
            reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
            (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
            (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
               free (the (\<Gamma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
    proof (rule ballI, rule ballI, rule impI)
      fix \<iota>\<^sub>1 \<iota>\<^sub>2
      let ?thesis = "reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
            (\<exists> x \<in> dom \<sigma>. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) (non_null \<iota>\<^sub>2)) \<or>
            (\<exists> y \<in> dom \<sigma>. \<exists> z \<in> dom \<sigma>.
               free (the (\<Gamma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (the (\<sigma> y)) \<and>
               reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2))"
      assume
        \<iota>\<^sub>1D: "\<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h)" and
        \<iota>\<^sub>2D: "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h)"
      with IH1_15\<^sub>3 have
        \<iota>\<^sub>1D\<^sub>1: "\<iota>\<^sub>1 \<in> dom (\<chi>\<^sub>c h\<^sub>1)" and
        \<iota>\<^sub>2D\<^sub>1: "\<iota>\<^sub>2 \<in> dom (\<chi>\<^sub>c h\<^sub>1)" using predecessor_dom by blast+
      moreover
      assume
        "reaches (\<chi>\<^sub>v h\<^sub>2) \<iota>\<^sub>1 \<iota>\<^sub>2"
      moreover note IH2_22
      ultimately have
        "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2 \<or>
          (\<exists>x\<in>dom \<sigma>\<^sub>1. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)) \<or>
          (\<exists>y\<in>dom \<sigma>\<^sub>1. \<exists>z\<in>dom \<sigma>\<^sub>1.
             free (the (\<Gamma> y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2))" by blast
      moreover
      from IH1_22 \<iota>\<^sub>1D \<iota>\<^sub>2D have ?thesis if "reaches (\<chi>\<^sub>v h\<^sub>1) \<iota>\<^sub>1 \<iota>\<^sub>2" using that by simp
      moreover
      have ?thesis
        if "\<exists>x\<in>dom \<sigma>\<^sub>1. committed (the (\<Gamma> x)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)"
      proof -
        from that obtain x where
          "x\<in>dom \<sigma>\<^sub>1"
          "committed (the (\<Gamma> x))"
          "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 x)) (non_null \<iota>\<^sub>2)" by auto
        then have
          "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (values \<sigma>\<^sub>1 (committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>\<^sub>2)" by auto
        with \<iota>\<^sub>2D IH1_20' have
          "reaches\<^sub>V\<^sub>v (\<chi>\<^sub>v h) (values \<sigma> (Type.committed \<circ> (the \<circ> \<Gamma>))) (non_null \<iota>\<^sub>2)" by simp
        then show ?thesis by auto
      qed
      moreover
      have ?thesis if "\<exists>y\<in>dom \<sigma>\<^sub>1. \<exists>z\<in>dom \<sigma>\<^sub>1.
             free (the (\<Gamma> y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y)) \<and>
             reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2)"
      proof -
        from that obtain y z where
          "y\<in>dom \<sigma>\<^sub>1" and
          "free (the (\<Gamma> y))" and
          "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (the (\<sigma>\<^sub>1 y))" by auto
        then have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h\<^sub>1) (non_null \<iota>\<^sub>1) (values \<sigma>\<^sub>1 (free \<circ> (the \<circ> \<Gamma>)))" by auto
        with \<iota>\<^sub>1D IH1_21' have
          "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>\<^sub>1) (values \<sigma> (free \<circ> (the \<circ> \<Gamma>)))" by blast
        moreover from that obtain z where
          "z\<in>dom \<sigma>\<^sub>1" and
          rz: "reaches\<^sub>v (\<chi>\<^sub>v h\<^sub>1) (the (\<sigma>\<^sub>1 z)) (non_null \<iota>\<^sub>2)" by auto
        with IH1_19 \<iota>\<^sub>2D \<iota>\<^sub>2D\<^sub>1 have
          "\<exists> z \<in> dom \<sigma>. reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> z)) (non_null \<iota>\<^sub>2)" by blast
        ultimately show ?thesis by auto
      qed
      ultimately show ?thesis by blast
    qed
    from 96 IH1_S IH2_S have
      gS: "\<forall> f \<in> \<Sigma>. f \<in> set (fields (base (the (\<Gamma> this))))" by blast
    from 9 10 11 12 13 TN 14 15 16 17 18 19 "20'" 20 "21'" 22 gS Wh' wi uh show ?case by blast
  next
    case (seqBad h \<sigma> s\<^sub>1 h\<^sub>1 \<sigma>\<^sub>1 \<epsilon> s\<^sub>2)
    note seqBad.prems(1)
    moreover note seqBad.prems(2)
    moreover note seqBad.prems(3)
    moreover note seqBad.prems(4)
    moreover note seqBad.prems(5)
    moreover note seqBad.prems(6)
    moreover note seqBad.prems(7)
    moreover from seqBad.prems(8) obtain \<Delta>'' \<Sigma>'' where
      "\<Gamma>, \<Delta> \<turnstile> s\<^sub>1 | \<Delta>'', \<Sigma>''" by blast
    moreover note seqBad.prems(9)
    moreover note seqBad.prems(10)
    moreover from seqBad.prems(11) have
      "wf_statement s\<^sub>1" by simp
    moreover note seqBad.prems(12)
    moreover note seqBad.IH
    ultimately have
      "\<epsilon> = ok" by blast
    with seqBad.hyps(2) have
      "False" by simp
    then show ?case by simp
  qed
  then have
    9: "?g9" and
    10: "?g10" and
    11: "?g11" and
    12: "?g12" and
    13: "?g13" and
    TN: "?gTN" and
    14: "?g14" and
    15: "?g15" and
    16: "?g16" and
    17: "?g17" and
    18: "?g18" and
    19: "?g19" and
    "20'": "?g20'" and
    20: "?g20" and
    "21'": "?g21'" and
    22: "?g22" and
    gS: "?gS" and
    gWh': "?gWh'" and
    gwi: "?gwi" and
    guh: "?guh"
    by metis+ (* blast+ works too but slower *)
  note 9 10 11 
  moreover from 12 have
    "\<Gamma> \<turnstile>\<^sub>4 h', \<sigma>'" by auto
  moreover note 13 TN
  ultimately have
    "\<Gamma>, \<Delta>' \<turnstile> h', \<sigma>'" using good_configuration_def by blast 
  with 14 show "\<Gamma>, \<Delta>' \<turnstile> h', \<sigma>' \<and> \<epsilon> = ok" by blast
  from 15 show ?g15 .
  from 16 show ?g16 .
  from 17 show ?g17 .
  from 17 15 show "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h). init h \<iota> \<longrightarrow> init h' \<iota>"
    unfolding init_def predecessor_def by simp
  from 18 show ?g18 .
  from 19 show ?g19 .
  from 20 show ?g20 .
  show "\<forall> \<iota> \<in> dom (\<chi>\<^sub>c h'). \<forall> x \<in> dom \<sigma>'. reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the (\<sigma>' x)) \<and>
        free (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> (ref (the (\<sigma>' x))) \<in> dom (\<chi>\<^sub>c h) \<longrightarrow>
        (\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y)))"
  proof (rule ballI, rule ballI, rule impI)
    fix \<iota> x
    assume
      "x \<in> dom \<sigma>'" and
      "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the (\<sigma>' x)) \<and>
        free (the (\<Gamma> x)) \<and> \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> (ref (the (\<sigma>' x))) \<in> dom (\<chi>\<^sub>c h)"
    moreover
    then have
      "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) (the (\<sigma>' x))" and
      "free (the (\<Gamma> x))" and
      \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)" by simp_all
    ultimately have
      "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h') (non_null \<iota>) (values \<sigma>' (free o (the o \<Gamma>)))" by auto
    with "21'" \<iota>D have
      "reaches\<^sub>v\<^sub>V (\<chi>\<^sub>v h) (non_null \<iota>) (values \<sigma> (free o (the o \<Gamma>)))" by blast
    then show
      "\<exists> y \<in> dom \<sigma>. free (the (\<Gamma> y)) \<and> reaches\<^sub>v (\<chi>\<^sub>v h) (non_null \<iota>) (the (\<sigma> y))" by auto
  qed
  from 22 show "?g22" .
qed

subsubsection \<open>Theorem 1 (Preservation and Safety)\<close>

lemmas (in wf_runtime) theorem_1 = lemma_1(1)

end