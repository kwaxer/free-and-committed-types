(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 7 (Well-formed program)\<close>

theory WellFormedness imports Lookup Typing begin

(*
fun xS_upds :: "(x \<rightharpoonup> 'C T) \<Rightarrow> \<theta> \<Rightarrow> 'C xS\<^sub>i \<Rightarrow> (x \<rightharpoonup> 'C T)" ("\<upsilon>")
  where
    "xS_upds f \<theta> [] = f" |
    "xS_upds f \<theta> ((x, S) # xS\<^sub>i) = xS_upds (f (x := \<ss> \<theta> S)) \<theta> xS\<^sub>i"
*)

definition xS_upds :: "(x \<rightharpoonup> 'C T) \<Rightarrow> \<theta> \<Rightarrow> 'C xS\<^sub>i \<Rightarrow> (x \<rightharpoonup> 'C T)" ("\<upsilon>")
  where
    [iff]: "xS_upds f \<theta> xS\<^sub>i = f \<lparr>map fst xS\<^sub>i := map (\<ss> \<theta> o snd) xS\<^sub>i\<rparr>"

lemma instance_env_upd_dom:
  fixes
    xS\<^sub>i :: "'C xS\<^sub>i"
  assumes
    "instance \<theta> R (\<tau> xS\<^sub>i)"
  shows
    "dom (\<upsilon> f \<theta> xS\<^sub>i) = dom f \<union> set (\<eta> xS\<^sub>i)"
proof -
  from assms obtain T\<^sub>i where
    M: "map (\<ss> \<theta> \<circ> snd) xS\<^sub>i = map Some T\<^sub>i" using instance_arg_types variable_types_def
    by (metis map_map)
  then have
    "dom f \<lparr> map fst xS\<^sub>i := map (\<ss> \<theta> \<circ> snd) xS\<^sub>i \<rparr> = dom f \<lparr> map fst xS\<^sub>i := map Some T\<^sub>i \<rparr>" by simp
  from M have
    "map fst xS\<^sub>i = take (length T\<^sub>i) (map fst xS\<^sub>i)"
    by (metis length_map order_refl take_all)
  then have
    "dom f \<lparr> map fst xS\<^sub>i := map Some T\<^sub>i \<rparr> = dom f \<union> set (\<eta> xS\<^sub>i)" unfolding variable_names_def using fun_updr_some_dom
    by metis
  with M show ?thesis by simp
qed

lemma instance_sub_env_upd_dom:
  fixes
    xS\<^sub>i xS\<^sub>j:: "'C xS\<^sub>i"
  assumes
    "instance \<theta> R (\<tau> xS\<^sub>i)" and
    "vars (\<tau> xS\<^sub>j) \<subseteq> vars (\<tau> xS\<^sub>i)"
  shows
    "dom (\<upsilon> f \<theta> xS\<^sub>j) = dom f \<union> set (\<eta> xS\<^sub>j)"
proof -
  from assms obtain T\<^sub>j where
    M: "map (\<ss> \<theta> \<circ> snd) xS\<^sub>j = map Some T\<^sub>j" using substitution_instance_sub_types variable_types_def
    by (metis map_map)
  then have
    "dom f \<lparr> map fst xS\<^sub>i := map (\<ss> \<theta> \<circ> snd) xS\<^sub>j \<rparr> = dom f \<lparr> map fst xS\<^sub>i := map Some T\<^sub>j \<rparr>" by simp
  from M have
    "map fst xS\<^sub>j = take (length T\<^sub>j) (map fst xS\<^sub>j)"
    by (metis length_map order_refl take_all)
  then have
    "dom f \<lparr> map fst xS\<^sub>j := map Some T\<^sub>j \<rparr> = dom f \<union> set (\<eta> xS\<^sub>j)"
    unfolding variable_names_def using fun_updr_some_dom by metis
  with M show ?thesis by simp
qed

context lookup begin

inductive well_formed_method :: "'C \<Rightarrow> m \<Rightarrow> bool" ("\<turnstile>\<^sub>m _, _")
  where
    wfMeth: "\<lbrakk>
      mBody (C, m) = Some s;
      wf_statement s;
      mSig (C, m) = Some (\<gamma>, xS\<^sub>i, S, xS\<^sub>j);
      \<not> (nullable S) \<Longrightarrow> res \<in> \<Delta>;
      \<theta>\<^sub>l = instances S ((C, \<gamma>, !) # (\<tau> xS\<^sub>i));
      \<forall> \<theta> \<in> \<theta>\<^sub>l. (\<upsilon> ((\<upsilon> Map.empty \<theta> xS\<^sub>i) (this := Some (C, the (\<ee> \<theta> \<gamma>), !))) \<theta> xS\<^sub>j) (res := \<ss> \<theta> S), set (\<eta> xS\<^sub>i) \<union> {this} \<turnstile> s | \<Delta>, \<Sigma>
    \<rbrakk> \<Longrightarrow>
    \<turnstile>\<^sub>m C, m"
  for
    S :: "'C S" and xS\<^sub>i xS\<^sub>j :: "'C xS\<^sub>i" and \<gamma> :: \<gamma>

inductive_cases wfMethE[elim]: "\<turnstile>\<^sub>m C, m"

inductive well_formed_constructor :: "'C \<Rightarrow> bool" ("\<turnstile>\<^sub>C _")
  where
    wfCons: "\<lbrakk>
      cBody C = Some s;
      wf_statement s;
      cSig C = Some (xS\<^sub>i, xS\<^sub>j);
      {f. f \<in> set (fields C) \<and> \<not> (nullable (the (fType (C, f))))} \<subseteq> \<Sigma>;
      \<theta>\<^sub>l = instances (C, Inl \<zero>, !) (\<tau> xS\<^sub>i);
      \<forall> \<theta> \<in> \<theta>\<^sub>l. (\<upsilon> ((\<upsilon> Map.empty \<theta> xS\<^sub>i) (this := Some (C, \<zero>, !))) \<theta> xS\<^sub>j), set (\<eta> xS\<^sub>i) \<union> {this} \<turnstile> s | \<Delta>, \<Sigma>
    \<rbrakk> \<Longrightarrow>
    \<turnstile>\<^sub>C C"

inductive_cases wfConsE[elim]: "\<turnstile>\<^sub>C C"

end

subsubsection \<open>Well-formed program\<close>

\<comment> \<open>Definition 7 does not specify formally what a program is.
   The following locale extends the notion of well-formedness to all classes.\<close>

locale wf_lookup = lookup +
  assumes
    wf_constructor: "\<turnstile>\<^sub>C C" and
    wf_method: "\<lbrakk>m \<in> methods C\<rbrakk> \<Longrightarrow> \<turnstile>\<^sub>m C, m"

end