(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Preliminary definitions: names\<close>

theory Name imports Main begin

type_synonym fname = "char list" \<comment> \<open>Field name\<close>
type_synonym f = fname
type_synonym mname = "char list" \<comment> \<open>Method name\<close>
type_synonym m = mname
type_synonym vname = "char list" \<comment> \<open>Variable name (argument or local)\<close>
type_synonym x = vname
type_synonym ename = "char list" \<comment> \<open>Expectation variable name\<close>

definition this :: x
  where
    "this = ''this''"

definition res :: x
  where
    "res = ''res''"

lemma this_res [simp]: "this \<noteq> res"
  unfolding this_def res_def by simp

end