(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 1 (Classes and Types)\<close>

theory Type imports Common Name "HOL-Lattice.Orders" "HOL-Library.List_Lexorder" begin

(*no_notation "Orderings.ord_class.less_eq"  ("(_/ \<le> _)" [51, 51] 50)*)

locale classes =
  fixes
    is_subclass :: "'C:: {equal, partial_order} \<Rightarrow> 'C \<Rightarrow> bool" and
    "class" :: "'C set" and
    "fields" :: "'C \<Rightarrow> f list" and
    "methods" :: "'C \<Rightarrow> m set"
  assumes
    monotonic_fields: "A \<sqsubseteq> B \<Longrightarrow> set (fields B) \<subseteq> set (fields A)" and
    monotonic_methods: "A \<sqsubseteq> B \<Longrightarrow> methods B \<subseteq> methods A"

datatype n = unknown ("?") | non_null ("!")

datatype k = free ("\<zero>") | committed ("\<one>") | unclassified ("\<diamondop>")

type_synonym ('C, 'I) type = "'C \<times> 'I \<times> n"
type_synonym ('C, 'I) V = "('C, 'I) type"
type_synonym ('C, 'I) V\<^sub>i = "('C, 'I) V list"

type_synonym 'C T = "('C, k) V"
type_synonym 'C T\<^sub>i = "('C, k) V\<^sub>i"

definition base :: "('C, 'I) type \<Rightarrow> 'C"
  where
    [iff]: "base T \<equiv> fst T"

lemma base: "base (C, k, n) = C"
  by simp

definition initialization :: "('C, 'I) type \<Rightarrow> 'I"
  where
    "initialization T \<equiv> fst (snd T)"

lemma initialization [iff]: "initialization (C, k, n) = k"
  using initialization_def by fastforce

definition nullity :: "('C, 'I) type \<Rightarrow> n"
  where
    [iff]: "nullity T \<equiv> snd (snd T)"

lemma nullity: "nullity (C, k, n) = n"
  by simp

subsubsection "Expectation variables"

type_synonym \<alpha> = ename

subsubsection "Generic expectations"

type_synonym expectation = "k + \<alpha>"
type_synonym \<gamma> = expectation

subsubsection "Signature type"

type_synonym 'C S = "('C, \<gamma>) type"
type_synonym 'C S\<^sub>i = "'C S list"

definition is_type:: "'C S \<Rightarrow> bool"
  where
    [iff]: "is_type S \<equiv> (case initialization S of Inl _ \<Rightarrow> True | _ \<Rightarrow> False)"

definition are_types:: "'C S\<^sub>i \<Rightarrow> bool"
  where
    [iff]: "are_types S\<^sub>i \<equiv> fold (\<and>) (map is_type S\<^sub>i) True"

lemma are_types_set: "are_types S\<^sub>i = (\<forall> S \<in> set S\<^sub>i. is_type S)"
  by (simp add: fold_conj_map)

subsubsection "Simple type"

type_synonym 'C simple_type = "'C \<times> n"
type_synonym 'C t = "'C simple_type"

definition simple_base :: "'C t \<Rightarrow> 'C"
  where
    "simple_base t \<equiv> fst t"

lemma simple_base: "simple_base (C, n) = C"
  unfolding simple_base_def by simp

definition simple_nullity :: "'C t \<Rightarrow> n"
  where
    "simple_nullity t \<equiv> snd t"

lemma simple_nullity: "simple_nullity (C, n) = n"
  unfolding simple_nullity_def by simp

lemma simple_type [iff]: "(simple_base t, simple_nullity t) = t"
  by (simp add: simple_base_def simple_nullity_def)

definition type_as_t :: "('C, 'I) type \<Rightarrow> 'C t "
  where
    "type_as_t T = (base T, nullity T)"

lemma type_as_t_class: "base T = simple_base (type_as_t T)"
  unfolding simple_base_def type_as_t_def by simp

lemma type_as_t_nullity: "nullity T = simple_nullity (type_as_t T)"
  unfolding simple_nullity_def type_as_t_def by simp

lemma type_as_t [iff]: "type_as_t (C, k, n) = (C, n)"
  unfolding type_as_t_def base_def nullity_def by simp

subsection \<open>Definition 2 (Type Relations)\<close>

subsubsection \<open>Initialization expectation\<close>

instantiation k :: "{partial_order, top}"
begin

  definition
    initialization_leq [iff]: "k\<^sub>1 \<sqsubseteq> k\<^sub>2 \<equiv> k\<^sub>1 = k\<^sub>2 \<or> k\<^sub>2 = \<diamondop>"

  definition
    [iff]: "top = \<diamondop>"

  instance proof
  qed auto

end

fun specialization_list:: "'a::partial_order list \<Rightarrow> 'a list \<Rightarrow> bool"
  where
    "specialization_list [] [] \<longleftrightarrow> True" |
    "specialization_list (k\<^sub>1 # k\<^sub>1') (k\<^sub>2 # k\<^sub>2') \<longleftrightarrow> k\<^sub>1 \<sqsubseteq> k\<^sub>2 \<and> specialization_list k\<^sub>1' k\<^sub>2'" |
    "specialization_list _ _ \<longleftrightarrow> False"

lemma specialization_list_head:
  "specialization_list (x # xs) (y # ys) \<Longrightarrow> x \<sqsubseteq> y" by simp

lemma specialization_list_tail:
  "specialization_list (x # xs) (y # ys) \<Longrightarrow> specialization_list xs ys" by simp

lemma specialization_list_combine_head_tail:
  "\<lbrakk>x \<sqsubseteq> y; specialization_list xs ys\<rbrakk> \<Longrightarrow> specialization_list (x # xs) (y # ys)"
  by simp

lemma specialization_list_length:
  "specialization_list xs ys \<Longrightarrow> length xs = length ys"
  by (induction rule: specialization_list.induct) simp_all

lemma specialization_list_item:
  "\<lbrakk>i < length xs; specialization_list xs ys \<rbrakk> \<Longrightarrow> xs ! i \<sqsubseteq> ys ! i"
  by (induction arbitrary: i rule: specialization_list.induct)
    (simp_all add: nth_Cons')

lemma specialization_list_combine_item:
  "\<lbrakk>length xs = length ys; \<forall> i < length xs. xs ! i \<sqsubseteq> ys ! i\<rbrakk> \<Longrightarrow> specialization_list xs ys"
proof (induction rule: specialization_list.induct)
next
  case (2 k\<^sub>1 k\<^sub>1' k\<^sub>2 k\<^sub>2')
  moreover {
    fix i
    assume
      A: "i < length k\<^sub>1'"
    then have
      "Suc i < length (k\<^sub>1 # k\<^sub>1') \<Longrightarrow> (k\<^sub>1 # k\<^sub>1') ! i \<sqsubseteq> (k\<^sub>2 # k\<^sub>2') ! i"
      using "2.prems"(2) Suc_lessD by blast
    with A have
      "k\<^sub>1' ! i \<sqsubseteq> k\<^sub>2' ! i"
      using "2.prems"(2) by auto
  }
  then have "\<forall> i < length k\<^sub>1'. k\<^sub>1' ! i \<sqsubseteq> k\<^sub>2' ! i" by simp
  moreover from "2.prems" have
    "length k\<^sub>1' = length k\<^sub>2'" by simp
  ultimately show ?case by auto
qed simp_all

lemma specialization_list_classified:
  "\<forall> y \<in> set ys. y \<noteq> \<diamondop> \<Longrightarrow> specialization_list xs ys \<Longrightarrow> xs = ys"
  by (induction rule: specialization_list.induct) simp_all

instantiation list :: (partial_order) partial_order
begin

  definition
    list_leq_def: "(xs :: _ list) \<sqsubseteq> ys \<longleftrightarrow> specialization_list xs ys"
  
  instance proof
    fix x :: "_ list"
    show "x \<sqsubseteq> x" unfolding list_leq_def
      by (induction x) (simp_all add: leq_refl)
  next
    fix x y z:: "_ list"
    assume
      "x \<sqsubseteq> y"
      "y \<sqsubseteq> z"
    then have
      L: "length x = length z"
        by (simp add: list_leq_def specialization_list_length)
    from \<open>x \<sqsubseteq> y\<close> have "\<And> i. i < length x \<Longrightarrow> x ! i \<sqsubseteq> y ! i"
      by (simp add: list_leq_def specialization_list_item)
    moreover from \<open>y \<sqsubseteq> z\<close> have "\<And> i. i < length y \<Longrightarrow> y ! i \<sqsubseteq> z ! i"
      by (simp add: list_leq_def specialization_list_item)
    ultimately have "\<And> i. i < length x \<Longrightarrow> x ! i \<sqsubseteq> z ! i"
      by (metis \<open>x \<sqsubseteq> y\<close> leq_trans list_leq_def specialization_list_length)
    with L show "x \<sqsubseteq> z"
      by (simp add: list_leq_def specialization_list_combine_item)
  next
    fix x y:: "_ list"
    assume
      "x \<sqsubseteq> y"
      "y \<sqsubseteq> x"
    then show "x = y"
      by (induction rule: specialization_list.induct)
       (simp_all add: leq_antisym list_leq_def)
  qed

end

lemma list_leq_length:
  fixes
    x y :: "('a :: partial_order) list"
  assumes
    "x \<sqsubseteq> y"
  shows
    "length x = length y"
  using assms list_leq_def specialization_list_length by auto

lemma list_leq_cons:
  fixes
    xs ys :: "('a :: partial_order) list"
  shows
    "x \<sqsubseteq> y \<and> xs \<sqsubseteq> ys \<longleftrightarrow> x # xs \<sqsubseteq> y # ys"
  by (induction xs arbitrary: ys) (simp_all add: list_leq_def)

lemma list_classified:
  "\<forall> y \<in> set ys. y \<noteq> \<diamondop> \<Longrightarrow> xs \<sqsubseteq> ys \<Longrightarrow> xs = ys"
  by (simp add: list_leq_def specialization_list_classified)

definition partial_mono:: "('a :: partial_order \<Rightarrow> 'b :: partial_order) \<Rightarrow> bool"
  where
    "partial_mono f \<equiv> (\<forall> a b. a \<sqsubseteq> b \<longrightarrow> f a \<sqsubseteq> f b)"

lemma partial_monoD: "partial_mono f \<Longrightarrow> a \<sqsubseteq> b \<Longrightarrow> f a \<sqsubseteq> f b"
  using partial_mono_def by blast

lemma partial_monoI: "(\<forall> a b. a \<sqsubseteq> b \<longrightarrow> f a \<sqsubseteq> f b) \<Longrightarrow> partial_mono f"
  using partial_mono_def by blast

lemma partial_mono_map:
  assumes
    "partial_mono f"
    "xs \<sqsubseteq> ys"
  shows
    "map f xs \<sqsubseteq> map f ys"
proof -
  from assms(2) have
    "length xs = length ys" using list_leq_length by blast
  then obtain zs where
    "zs = zip xs ys" and
    "length zs = length xs" and
    "length zs = length ys" and
    "map fst zs = xs" and
    "map snd zs = ys" by simp
  moreover with assms(2) have
    "map fst zs \<sqsubseteq> map snd zs" by simp
  then have
    "map (f \<circ> fst) zs \<sqsubseteq> map (f \<circ> snd) zs"
  proof (induction zs)
    case Nil
    then show ?case using leq_refl by simp
  next
    case (Cons z zs)
    from Cons.prems have
      "fst z \<sqsubseteq> snd z" and
      "map fst zs \<sqsubseteq> map snd zs" using list_leq_cons by auto
    with assms(1) Cons.IH have
      "(f \<circ> fst) z \<sqsubseteq> (f \<circ> snd) z" and
      "map (f \<circ> fst) zs \<sqsubseteq> map (f \<circ> snd) zs" using partial_monoD by fastforce+
    then show ?case using list_leq_cons by (metis (no_types) list.simps(9))
  qed
  ultimately show ?thesis using map_map by metis
qed

instantiation option:: (order) order
begin

definition
  less_eq_option :: "'a option \<Rightarrow> 'a option \<Rightarrow> bool"
  where
    "less_eq_option x y \<equiv> (case x of
    Some a \<Rightarrow> (case y of
      Some b \<Rightarrow> a \<le> b |
      _ \<Rightarrow> False) |
    None \<Rightarrow> y = None)"

definition
  less_option :: "'a option \<Rightarrow> 'a option \<Rightarrow> bool"
  where
    "less_option x y \<equiv> x \<le> y \<and> \<not> y \<le> x"

instance proof
  fix x y :: "('a :: order) option"
  show "(x < y) = (x \<le> y \<and> \<not> y \<le> x)"
    by (cases x; cases y) (simp_all add: less_eq_option_def less_option_def)
  fix x :: "('a :: order) option"
  show "x \<le> x" by (cases x) (simp_all add: less_eq_option_def)
  fix x y z :: "('a :: order) option"
  show "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z"
    by (cases x; cases y; cases z) (simp_all add: less_eq_option_def)
  show "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = y"
    by (cases x; cases y) (simp_all add: less_eq_option_def)
qed
end

instantiation option:: (partial_order) partial_order
begin

definition
  option_leq [iff]: "leq (x::'a option) y = (case x of
    Some a \<Rightarrow> (case y of
      Some b \<Rightarrow> a \<sqsubseteq> b |
      _ \<Rightarrow> False) |
    None \<Rightarrow> y = None)"

instance proof
  fix
    x :: "('a::partial_order) option"
  show "x \<sqsubseteq> x" by (cases x, simp_all add: leq_refl)
next
  fix
    x y z :: "('a::partial_order) option"
  assume
    "x \<sqsubseteq> y"
    "y \<sqsubseteq> z"
  then show "x \<sqsubseteq> z"
    by (cases x, simp, cases y, simp, cases z, simp_all add: leq_trans)
next
  fix
    x y :: "('a::partial_order) option"
  assume
    "x \<sqsubseteq> y"
    "y \<sqsubseteq> x"
  then show "x = y"
    by (cases x, simp, cases y, simp_all add: leq_antisym)
qed

end

lemma partial_mono_option: "partial_mono Some"
  unfolding partial_mono_def using option_leq by simp

lemma mono_some: "mono Some"
  by (simp add: less_eq_option_def monoI)

lemma option_leq_dom: "Some x \<sqsubseteq> f y \<Longrightarrow> y \<in> dom f"
proof -
  assume
    "Some x \<sqsubseteq> f y"
  then have
    "case Some x of
      Some a \<Rightarrow> (case f y of
        Some b \<Rightarrow> a \<sqsubseteq> b |
        _ \<Rightarrow> False) |
      None \<Rightarrow> f y = None" by simp
  then show "y \<in> dom f" using domIff by fastforce
qed

instantiation sum :: (top, _) partial_order
begin

  definition
    generic_initialization_specialization [iff]: "\<gamma>\<^sub>1 \<sqsubseteq> \<gamma>\<^sub>2 \<equiv> \<gamma>\<^sub>1 = \<gamma>\<^sub>2 \<or> \<gamma>\<^sub>2 = Inl top"

  instance proof
  qed auto

end

lemma list_leq_iff:
  fixes
    \<gamma>\<^sub>i' \<gamma>\<^sub>i'' :: "('I::partial_order) list"
  shows
    "leq \<gamma>\<^sub>i' \<gamma>\<^sub>i'' \<longleftrightarrow> length \<gamma>\<^sub>i' = length \<gamma>\<^sub>i'' \<and> (\<forall> i. i < length \<gamma>\<^sub>i' \<longrightarrow> leq (\<gamma>\<^sub>i' ! i) (\<gamma>\<^sub>i'' ! i))"
  using list_leq_def specialization_list_combine_item specialization_list_item specialization_list_length by blast

lemma zip_leq_iff:
  fixes
    zs :: "('I \<times> 'I::partial_order) list"
  shows
    "leq (map fst zs) (map snd zs) \<longleftrightarrow> (\<forall> z \<in> set zs. leq (fst z) (snd z))"
  using list_leq_cons leq_refl by (induction zs) fastforce+

lemma zip_fun_leq_iff [simp]:
  "map (f \<circ> fst) zs \<sqsubseteq> map (g \<circ> snd) zs \<longleftrightarrow> (\<forall> z \<in> set zs. (f \<circ> fst) z \<sqsubseteq> (g \<circ> snd) z)"
proof -
  obtain xs where
    xs: "xs = zip (map (f \<circ> fst) zs) (map (g \<circ> snd) zs)" by simp
  then have
    L: "length zs = length zs" and
    X1: "map fst xs = map (f \<circ> fst) zs" and
    X2: "map snd xs = map (g \<circ> snd) zs" by simp_all
  have
    *: "map fst xs \<sqsubseteq> map snd xs \<longleftrightarrow> (\<forall> x \<in> set xs. fst x \<sqsubseteq> snd x)" using zip_leq_iff by blast
  show ?thesis
  proof (rule iffI)
    assume
      A: "map (f \<circ> fst) zs \<sqsubseteq> map (g \<circ> snd) zs"
    with X1 X2 * have
      S: "\<forall> x \<in> set xs. fst x \<sqsubseteq> snd x" by simp
    show "\<forall>z\<in>set zs. (f \<circ> fst) z \<sqsubseteq> (g \<circ> snd) z"
    proof (rule ballI)
      fix z
      assume
        "z \<in> set zs"
      then obtain i where
        "i < length zs" and
        "z = zs ! i" by (metis in_set_conv_nth)
      with L xs A show
        "(f \<circ> fst) z \<sqsubseteq> (g \<circ> snd) z" by (simp add: list_leq_iff)
    qed
  next
    assume
      A: "\<forall> z \<in> set zs. (f \<circ> fst) z \<sqsubseteq> (g \<circ> snd) z"
    have
      "\<forall> x \<in> set xs. fst x \<sqsubseteq> snd x"
    proof (rule ballI)
      fix x
      assume
        "x \<in> set xs"
      then obtain i where
        "i < length xs" and
        "x = xs ! i" by (metis in_set_conv_nth)
      with L xs A show
        "fst x \<sqsubseteq> snd x" by simp
    qed
    with X1 X2 * show
      "map (f \<circ> fst) zs \<sqsubseteq> map (g \<circ> snd) zs" by simp
  qed
qed

instantiation n :: "{order_bot, order_top, linorder, partial_order}"
begin

definition
  non_null_specialization [iff]: " n\<^sub>1 \<le> n\<^sub>2 \<longleftrightarrow> n\<^sub>1 = n\<^sub>2 \<or> n\<^sub>2 = ?"

definition
  non_null_specialization1 [iff]: " n\<^sub>1 \<sqsubseteq> n\<^sub>2 \<longleftrightarrow> n\<^sub>1 = n\<^sub>2 \<or> n\<^sub>2 = ?"

definition
  [simp]: "n\<^sub>1 < n\<^sub>2 \<longleftrightarrow> n\<^sub>1 = ! \<and> n\<^sub>2 = ?"

definition
  [simp]: "bot = !"

definition
  [simp]: "top = ?"

instance proof
qed (insert n.exhaust, auto)

end

definition subtyping:: "('C::partial_order, 'I::partial_order) type \<Rightarrow> ('C, 'I) type \<Rightarrow> bool"
  where
    [iff]: "subtyping T\<^sub>1 T\<^sub>2 \<equiv> T\<^sub>1 \<sqsubseteq> T\<^sub>2"

lemma base_subtyping:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> base T\<^sub>1 \<sqsubseteq> base T\<^sub>2" by (simp add: leq_prod_def)

lemma initialization_subtyping:
  "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> initialization T\<^sub>1 \<sqsubseteq> initialization T\<^sub>2"
  by (metis initialization_def leq_prodE)

lemma initializations_subtyping:
  "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> map initialization T\<^sub>1 \<sqsubseteq> map initialization T\<^sub>2"
  using initialization_subtyping partial_mono_def partial_mono_map by blast

lemma initialization_classified_subtyping:
  "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> initialization T\<^sub>2 \<noteq> \<diamondop> \<Longrightarrow> initialization T\<^sub>1 = initialization T\<^sub>2"
  using initialization_leq initialization_subtyping by blast

lemma initializations_classified_subtyping:
  "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> \<forall> T \<in> set T\<^sub>2. initialization T \<noteq> \<diamondop> \<Longrightarrow> map initialization T\<^sub>1 = map initialization T\<^sub>2"
  by (simp add: initializations_subtyping list_classified)

lemma nullity_subtyping:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> nullity T\<^sub>1 \<le> nullity T\<^sub>2" by (simp add: leq_prod_def)

lemma nullity_partial_subtyping:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> nullity T\<^sub>1 \<sqsubseteq> nullity T\<^sub>2" by (simp add: leq_prod_def)

lemma nullity_nullable_subtyping:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> nullity T\<^sub>1 = ? \<Longrightarrow> nullity T\<^sub>2 = ?"
  using nullity_subtyping by fastforce

lemma nullity_notnullable_subtyping:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> nullity T\<^sub>2 = ! \<Longrightarrow> nullity T\<^sub>1 = !"
  using nullity_subtyping by fastforce

lemma subtyping:
  assumes
    "base T\<^sub>1 \<sqsubseteq> base T\<^sub>2"
    "initialization T\<^sub>1 \<sqsubseteq> initialization T\<^sub>2"
    "nullity T\<^sub>1 \<sqsubseteq> nullity T\<^sub>2"
  shows
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2"
  using assms unfolding base_def initialization_def nullity_def by (simp add: leq_prodI)

definition simple_subtyping:: "'C::partial_order t \<Rightarrow> 'C t \<Rightarrow> bool"
  where
    [iff]: "simple_subtyping t\<^sub>1 t\<^sub>2 \<equiv> t\<^sub>1 \<sqsubseteq> t\<^sub>2"

lemma simple_base_subtyping:
    "t\<^sub>1 \<sqsubseteq> t\<^sub>2 \<Longrightarrow> simple_base t\<^sub>1 \<sqsubseteq> simple_base t\<^sub>2"
  by (simp add: leq_prod_def simple_base_def)

lemma simple_nullity_subtyping:
    "t\<^sub>1 \<sqsubseteq> t\<^sub>2 \<Longrightarrow> simple_nullity t\<^sub>1 \<le> simple_nullity t\<^sub>2"
  by (simp add: leq_prod_def simple_nullity_def)

lemma simple_subtypingI:
  "simple_base t\<^sub>1 \<sqsubseteq> simple_base t\<^sub>2 \<Longrightarrow> simple_nullity t\<^sub>1 \<le> simple_nullity t\<^sub>2 \<Longrightarrow> t\<^sub>1 \<sqsubseteq> t\<^sub>2"
  unfolding simple_base_def simple_nullity_def using leq_prodI by blast

lemma subtyping_as_simple_t:
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> type_as_t T\<^sub>1 \<sqsubseteq> type_as_t T\<^sub>2"
  using base_subtyping nullity_subtyping simple_subtypingI type_as_t_class type_as_t_nullity
  by metis

lemma subtyping_as_simple_p:
    "(C\<^sub>1 :: 'C::partial_order, k\<^sub>1 :: 'I::partial_order, n\<^sub>1 :: n) \<sqsubseteq> (C\<^sub>2, k\<^sub>2, n\<^sub>2) \<Longrightarrow> (C\<^sub>1, n\<^sub>1) \<sqsubseteq> (C\<^sub>2, n\<^sub>2)"
  using subtyping_as_simple_t type_as_t by metis

lemma simple_nullity_subtyping_null:
    "(C, ?) \<sqsubseteq> t\<^sub>2 \<Longrightarrow> simple_nullity t\<^sub>2 = ?"
  unfolding simple_nullity_def using non_null_specialization1 leq_prodE
  by (cases "snd t\<^sub>2") (simp, fastforce)

lemma simple_nullity_subtyping_non_null:
    "t\<^sub>1 \<sqsubseteq> (C, !) \<Longrightarrow> simple_nullity t\<^sub>1 = !"
  unfolding simple_nullity_def using non_null_specialization1 leq_prodE
  by (cases "snd t\<^sub>1") (simp, fastforce)

abbreviation nullable\<^sub>T :: "('C, 'I) type \<Rightarrow> bool"
  where "nullable\<^sub>T T \<equiv> nullity T = ?"
notation nullable\<^sub>T ("nullable")

abbreviation nullable\<^sub>t :: "'C t \<Rightarrow> bool"
  where
    "nullable\<^sub>t T \<equiv> simple_nullity T = ?"
notation nullable\<^sub>t ("nullable")

abbreviation committed where "committed T \<equiv> initialization T = \<one>"

definition committed_all :: "'C T\<^sub>i \<Rightarrow> bool"
  where
    "committed_all Ts \<longleftrightarrow> fold conj (map committed Ts) True"

lemma committed_all [iff]: "committed_all T\<^sub>i \<longleftrightarrow> (\<forall> T \<in> set T\<^sub>i. committed T)"
  by (simp add: committed_all_def fold_conj_map)

abbreviation free where "free T \<equiv> initialization T = \<zero>"

abbreviation unclassified where "unclassified T \<equiv> initialization T = \<diamondop>"

lemma committed_subtyping: "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> committed T\<^sub>2 \<Longrightarrow> committed T\<^sub>1"
  by (simp add: initialization_classified_subtyping)

lemma free_subtyping: "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> free T\<^sub>2 \<Longrightarrow> free T\<^sub>1"
  by (simp add: initialization_classified_subtyping)

lemma unclassified_subtyping: "T\<^sub>1 \<sqsubseteq> T\<^sub>2 \<Longrightarrow> unclassified T\<^sub>1 \<Longrightarrow> unclassified T\<^sub>2"
  using initialization_classified_subtyping by fastforce

lemma map_some_partial_order [simp]: "map Some (X :: ('a :: partial_order, 'b :: partial_order) type list) \<sqsubseteq> map Some (Y :: ('a :: partial_order, 'b :: partial_order) type list) \<Longrightarrow> X \<sqsubseteq> Y"
proof -
  assume
    M: "map Some X \<sqsubseteq> map Some Y"
  then have
    L: "length X = length Y" unfolding list_leq_def using specialization_list_length by fastforce
  then obtain Z where
    Z: "Z = zip X Y" by simp
  with L M have
    "map Some (map fst Z) \<sqsubseteq> map Some (map snd Z)" by simp
  then have
    "map (Some o fst) Z \<sqsubseteq> map (Some o snd) Z" by simp
  then have "map fst Z \<sqsubseteq> map snd Z"
  proof (induction Z)
    case Nil
    then show ?case by (simp add: leq_refl)
  next
    case (Cons z Z)
    then have
      "map (Some \<circ> fst) Z \<sqsubseteq> map (Some \<circ> snd) Z" unfolding list_leq_def by simp
    with Cons.IH have
      "map fst Z \<sqsubseteq> map snd Z" by blast 
    moreover
    from Cons.prems(1) have
      "(Some \<circ> fst) z \<sqsubseteq> (Some \<circ> snd) z" unfolding list_leq_def by simp
    then have
      "fst z \<sqsubseteq> snd z" by simp
    ultimately show ?case unfolding list_leq_def by simp
  qed
  with L Z show ?thesis by simp
qed

lemma partial_mono_type_as_t: "partial_mono type_as_t"
  unfolding type_as_t_def using partial_mono_def subtyping_as_simple_t by fastforce

lemma map_some_structure:
  assumes
    "map Some X \<sqsubseteq> Y"
  obtains
    Z
  where
    "Y = map Some Z"
proof -
  from assms have
    L: "length X = length Y" using list_leq_length by fastforce
  obtain P where
    "P = zip X Y" by simp
  with L have
    "X = map fst P" and
    Y: "Y = map snd P" by simp_all
  with assms have
    "map (Some o fst) P \<sqsubseteq> map snd P" by auto
  then have
    "\<exists> Z. map snd P = map Some Z"
  proof (induction P)
    case Nil
    then show ?case by simp
  next
    case (Cons p P)
    from Cons.prems have
      "map (Some \<circ> fst) P \<sqsubseteq> map snd P" unfolding list_leq_def by simp
    with Cons.IH obtain Z where
      "map snd P = map Some Z" by blast
    moreover
    from Cons.prems have
      "(Some \<circ> fst) p \<sqsubseteq> snd p" unfolding list_leq_def by simp
    from \<open>(Some \<circ> fst) p \<sqsubseteq> snd p\<close> obtain z where
      "snd p = Some z" unfolding option_leq by fastforce
    ultimately show ?case by (metis list.simps(9)) 
  qed
  with Y show ?thesis using that by blast
qed

subsubsection "Subtyping lifting"

lemma types_initialization_leq:
  fixes
    T\<^sub>i T\<^sub>j :: "('C::partial_order, 'I::partial_order) type list"
  assumes
    "T\<^sub>i \<sqsubseteq> T\<^sub>j"
  shows
    "(map initialization T\<^sub>i) \<sqsubseteq> (map initialization T\<^sub>j)"
  using assms
proof (induction "length T\<^sub>i" arbitrary: T\<^sub>i T\<^sub>j)
  case 0
  then show ?case using leq_refl list_leq_def specialization_list_length by fastforce
next
  case (Suc n)
  then have
    N: "Suc n = length T\<^sub>j"
      by (simp add: list_leq_def specialization_list_length)
  from Suc.hyps Suc.prems N have
    "hd T\<^sub>i \<sqsubseteq> hd T\<^sub>j" using specialization_list_item
    by (metis hd_conv_nth length_greater_0_conv list_leq_def zero_less_Suc)
  then have "initialization (hd T\<^sub>i) \<sqsubseteq> initialization (hd T\<^sub>j)"
    using initialization_subtyping by blast
  moreover from Suc.hyps Suc.prems N have
    "tl T\<^sub>i \<sqsubseteq> tl T\<^sub>j" using specialization_list.simps(2)
    by (metis Zero_not_Suc length_0_conv list.collapse list_leq_def)
  with Suc.hyps have "map initialization (tl T\<^sub>i) \<sqsubseteq> map initialization (tl T\<^sub>j)" by simp
  moreover from Suc.hyps have
    "map initialization T\<^sub>i = initialization (hd T\<^sub>i) # map initialization (tl T\<^sub>i)"
    by (metis Zero_not_Suc length_0_conv list.collapse list.simps(9))
  moreover from N have
    "map initialization T\<^sub>j = initialization (hd T\<^sub>j) # map initialization (tl T\<^sub>j)"
    by (metis Zero_not_Suc length_0_conv list.collapse list.simps(9))
  ultimately show ?case by (simp add: list_leq_def)
qed

lemma subtyping_same_count:
  fixes T\<^sub>i T\<^sub>j :: "('C::partial_order, 'I::partial_order) type list"
  assumes
    "T\<^sub>i \<sqsubseteq> T\<^sub>j"
  shows "length T\<^sub>i = length T\<^sub>j"
using assms
  using list_leq_def specialization_list_length by auto

lemma subtyping_option_same_count:
  fixes T\<^sub>i T\<^sub>j :: "('C::partial_order, 'I::partial_order) type option list"
  assumes
    "T\<^sub>i \<sqsubseteq> T\<^sub>j"
  shows "length T\<^sub>i = length T\<^sub>j"
using assms
  using list_leq_def specialization_list_length by auto

end