(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 4 (Generic Expectation Instances)\<close>

theory Expectation imports Type begin

type_synonym expectation_substitution = "\<alpha> \<rightharpoonup> k"
type_synonym \<theta> = expectation_substitution

lemma initialization_substitution_value:
  assumes
    "\<alpha> \<in> dom \<theta>"
  obtains
    k
  where
    "\<theta> \<alpha> = Some k"
  using assms by blast

definition initialization_extension :: "\<theta> \<Rightarrow> \<gamma> \<rightharpoonup> k"
  where
    "initialization_extension \<theta> \<gamma> \<equiv> (case \<gamma> of Inl k \<Rightarrow> Some k | Inr \<alpha> \<Rightarrow> \<theta> \<alpha>)"
declare initialization_extension_def
notation initialization_extension ("\<ee>")

definition signature_expectation_extension :: "\<theta> \<Rightarrow> 'C S \<rightharpoonup> k"
  where
    "signature_expectation_extension \<theta> S \<equiv> \<ee> \<theta> (initialization S)"
declare signature_expectation_extension_def [iff]
notation signature_expectation_extension ("\<ee>")

definition var :: "\<gamma> \<Rightarrow> \<alpha> set"
  where
    "var \<gamma> \<equiv> (case \<gamma> of Inr \<alpha> \<Rightarrow> {\<alpha>} | _ \<Rightarrow> {})"

lemma var0: "var (Inl k) = {}"
  by (simp add: var_def)

lemma var1: "var (Inr \<alpha>) = {\<alpha>}"
  by (simp add: var_def)

lemma varI0:
  assumes
    "var \<gamma> = {}"
  obtains
    k
  where
    "\<gamma> = Inl k"
  using assms by (cases \<gamma>) (simp_all add: var_def)

lemma varI1:
  assumes
    "var \<gamma> = {\<alpha>}"
  shows
    "\<gamma> = Inr \<alpha>"
  using assms by (cases \<gamma>) (simp_all add: var_def)

lemma var_le:
  fixes
    \<gamma>' \<gamma>'' :: \<gamma>
  assumes
    "\<gamma>' \<sqsubseteq> \<gamma>''"
  shows
    "var \<gamma>'' \<subseteq> var \<gamma>'"
  using assms var_def
  by (cases \<gamma>', (cases \<gamma>'', simp, simp)+)

lemma initialization_extension_value:
  fixes
    \<theta> :: \<theta> and \<gamma> :: \<gamma>
  assumes
    "var \<gamma> \<subseteq> dom \<theta>"
  obtains
    k
  where
    "(\<ee> \<theta>) \<gamma> = Some k" using initialization_extension_def
  using assms by (cases \<gamma>) (auto simp add: var_def)

lemma initialization_extension_none:
  fixes
    \<theta> :: \<theta> and \<gamma> :: \<gamma>
  assumes
    "\<ee> \<theta> \<gamma> = None"
  shows
    "\<not> var \<gamma> \<subseteq> dom \<theta>"
  by (metis assms initialization_extension_value option.discI)

lemma initialization_extension_var_iff:
  fixes
    \<theta> :: \<theta> and \<gamma> :: \<gamma>
  shows
    "\<ee> \<theta> \<gamma> \<noteq> None \<longleftrightarrow> var \<gamma> \<subseteq> dom \<theta>"
  by (rule, cases "\<gamma>", insert initialization_extension_none initialization_extension_def)
    (fastforce simp add: var0 var1)+

lemma initialization_extension_restriction:
  fixes
    \<theta> :: \<theta> and \<gamma> :: \<gamma>
  assumes
    "var \<gamma> \<subseteq> A"
  shows
    "\<ee> (\<theta> |` A) \<gamma> = \<ee> \<theta> \<gamma>" using initialization_extension_def
  using assms by (cases \<gamma>) (simp_all add: var1)

definition var1 :: "'C S \<Rightarrow> \<alpha> set"
  where
    "var1 S = var (initialization S)"

lemma initialization_var:
  "initialization T = Inr \<alpha> \<longleftrightarrow> var1 T = {\<alpha>}"
  by rule (simp_all add: var1 var1_def varI1)

definition vars :: "'C S\<^sub>i \<Rightarrow> \<alpha> set"
  where
    "vars S\<^sub>i = foldr (\<union>) (map var1 S\<^sub>i) {}"

lemma vars_constant: "vars [(C, Inl k, n)] = {}"
  by (simp add: vars_def var1_def var_def)

lemma vars_variable: "vars [(C, Inr \<alpha>, n)] = {\<alpha>}"
  by (simp add: vars_def var1_def var_def)

lemma vars_value: "vars [S] = var1 S"
  by (simp add: vars_def)

lemma vars_value': "vars [S] = var (initialization S)"
  by (simp add: vars_def var1_def)

lemma vars_any: "vars [S] = {} \<or> (\<exists> \<alpha>. vars [S] = {\<alpha>})"
  by (cases "initialization S", simp_all add: vars_def var1_def var_def vars_constant vars_variable)

lemma vars_empty: "vars [] = {}"
  by (simp add: vars_def)

lemma vars_cons1: "vars (S # S\<^sub>i) = var1 S \<union> vars S\<^sub>i"
  by (simp add: vars_def)

lemma vars_cons: "vars (S # S\<^sub>i) = vars [S] \<union> vars S\<^sub>i"
  by (simp add: vars_def)

lemma vars_append: "vars (S\<^sub>i' @ S\<^sub>i'') = vars S\<^sub>i' \<union> vars S\<^sub>i''"
  by (induction S\<^sub>i') (simp_all add: vars_empty sup_assoc vars_cons1)

lemma vars_set:
  assumes
    "S \<in> set S\<^sub>i"
  shows
    "vars [S] \<subseteq> vars S\<^sub>i"
  using assms by (induction S\<^sub>i, auto simp add: vars_def)

lemma vars_item: "S \<in> set S\<^sub>i \<Longrightarrow> var1 S \<subseteq> vars S\<^sub>i"
  using vars_set vars_value by blast

lemma initialization_expectation_other: "\<alpha> \<notin> var \<gamma> \<Longrightarrow> \<ee> (\<theta> (\<alpha> := k)) \<gamma> = \<ee> \<theta> \<gamma>"
  unfolding initialization_extension_def by (cases \<gamma>) (simp_all add: var_def)

lemma (in classes) var1_le:
  fixes
    S' S'' :: "'C S"
  assumes
    "S' \<sqsubseteq> S''"
  shows
    "var1 S'' \<subseteq> var1 S'"
  using assms initialization_subtyping var1_def var_le by blast

lemma (in classes) vars1_le:
  fixes
    S' S'' :: "'C S"
  assumes
    "S' \<sqsubseteq> S''"
  shows
    "vars [S''] \<subseteq> vars [S']"
  using assms var1_le vars_value by blast

lemma (in classes) initialization_vars_le:
  fixes
    S\<^sub>i S\<^sub>j :: "'C S\<^sub>i"
  assumes
    "map initialization S\<^sub>i \<sqsubseteq> map initialization S\<^sub>j"
  shows
    "vars S\<^sub>j \<subseteq> vars S\<^sub>i"
  using assms
  apply (induction rule: specialization_list.induct)
  apply simp
  apply (metis (no_types) list.simps(9) list_leq_cons sup.mono var_le vars_cons1 vars_value vars_value')
  by (simp_all add: list_leq_def)

lemma (in classes) vars_le:
  fixes
    S\<^sub>i S\<^sub>j :: "'C S\<^sub>i"
  assumes
    "S\<^sub>i \<sqsubseteq> S\<^sub>j"
  shows
    "vars S\<^sub>j \<subseteq> vars S\<^sub>i"
  using assms initialization_vars_le types_initialization_leq by blast

definition signature_extension :: "\<theta> \<Rightarrow> 'C S \<rightharpoonup> 'C T"
  where
    "signature_extension \<theta> S = (case initialization S of
      Inl k \<Rightarrow> Some (base S, k, nullity S) |
      Inr \<alpha> \<Rightarrow> (case \<theta> \<alpha> of
        Some k \<Rightarrow> Some (base S, k, nullity S) |
        _ \<Rightarrow> None))"
notation "signature_extension" ("\<ss>")

lemma signature_extension:
  "var1 S \<subseteq> dom \<theta> \<Longrightarrow> \<ss> \<theta> S = Some (base S, the (\<ee> \<theta> (initialization S)), nullity S)"
  unfolding signature_extension_def initialization_extension_def var1_def using var1
  by (cases "initialization S") auto

lemma signature_extension_map:
  "vars S\<^sub>i \<subseteq> dom \<theta> \<Longrightarrow>
  map (\<ss> \<theta>) S\<^sub>i = map Some (zip (map base S\<^sub>i) (zip (map (the o (signature_expectation_extension \<theta>)) S\<^sub>i) (map nullity S\<^sub>i)))"
  by (induction S\<^sub>i) (simp_all add: signature_extension vars_cons1)

lemma signature_initialization:
  "var1 S \<subseteq> dom \<theta> \<Longrightarrow> initialization (the (\<ss> \<theta> S)) = the (\<ee> \<theta> (initialization S))"
  by (simp add: signature_extension)

lemma signature_expectation_other:
  "\<alpha> \<notin> var1 S \<Longrightarrow> var1 S \<subseteq> dom \<theta> \<Longrightarrow> \<ss> (\<theta> (\<alpha> := k)) S = \<ss> \<theta> S"
  using initialization_expectation_other signature_extension var1_def
  using initialization_extension_var_iff by metis

lemma signature_expectation_map_other:
  "\<alpha> \<notin> vars S\<^sub>i \<Longrightarrow> vars S\<^sub>i \<subseteq> dom \<theta> \<Longrightarrow> map (\<ss> (\<theta> (\<alpha> := k))) S\<^sub>i = map (\<ss> \<theta>) S\<^sub>i"
  using signature_expectation_other vars_item by fastforce

lemma some_expectation_substitution:
  "\<ss> \<theta> S \<noteq> None \<longleftrightarrow> \<ee> \<theta> S \<noteq> None"
  unfolding signature_extension_def signature_expectation_extension_def initialization_extension_def
  by (simp add: option.case_eq_if sum.case_eq_if)

lemma substitution_base: "\<ss> \<theta> S \<noteq> None \<Longrightarrow> base (the (\<ss> \<theta> S)) = base S"
  unfolding signature_extension_def using base initialization_extension_var_iff
  using signature_expectation_extension_def signature_extension signature_extension_def
  using some_expectation_substitution var1_def by (metis option.sel)

lemma substitution_base': "\<ss> \<theta> S \<noteq> None \<Longrightarrow> base ((the \<circ> (\<ss> \<theta>)) S) = base S"
  using substitution_base by fastforce

lemma substitution_base'': "\<ss> \<theta> S \<noteq> None \<Longrightarrow> (base \<circ> the \<circ> (\<ss> \<theta>)) S = base S"
  using substitution_base by fastforce

lemma substitution_base''': "\<ss> \<theta> S = Some x \<Longrightarrow> (base \<circ> the \<circ> (\<ss> \<theta>)) S = base S"
  using substitution_base'' by fastforce

lemma substitution_initialization:
  "\<ee> \<theta> S \<noteq> None \<Longrightarrow> initialization (the (\<ss> \<theta> S)) = the (\<ee> \<theta> S)"
  using signature_extension initialization_extension_var_iff
  by (simp add: signature_extension var1_def)

lemma substitution_nullity: "\<ss> \<theta> S \<noteq> None \<Longrightarrow> nullity (the (\<ss> \<theta> S)) = nullity S"
  unfolding signature_extension_def
  by (cases "initialization S") (simp, metis nullity old.sum.simps(6) option.case_eq_if option.sel)

lemma substitution_t: "\<ss> \<theta> S \<noteq> None \<Longrightarrow> type_as_t (the (\<ss> \<theta> S)) = type_as_t S"
  using substitution_base substitution_nullity type_as_t_def by metis

lemma signature_extension_value:
  assumes
    "var1 S \<subseteq> dom \<theta>"
  obtains
    T
  where
    "\<ss> \<theta> S = Some T"
  by (simp add: assms signature_extension)

lemma expectation_substitutions_signature_extension_value:
  assumes
    "vars [S] \<subseteq> dom \<theta>"
  obtains
    T
  where
    "\<ss> \<theta> S = Some T"
  using assms signature_extension vars_value by blast

lemma expectation_substitutions_signature_extension_none:
  assumes
    "(\<ss> \<theta>) S = None"
  shows
    "\<not> vars [S] \<subseteq> dom \<theta>"
  using assms expectation_substitutions_signature_extension_value by (metis not_Some_eq)

lemma substitution_restriction:
  assumes
    "var1 S \<subseteq> A"
  shows
    "\<ss> (\<theta> |` A) S = \<ss> \<theta> S"
  using assms var1 unfolding var1_def signature_extension_def
  by (cases "initialization S") simp_all


(*
definition signature_substitution :: "\<theta> \<Rightarrow> 'C S \<Rightarrow> 'C T + 'C S"
  where
    [iff]: "signature_substitution \<theta> S = (case initialization S of
      Inl k \<Rightarrow> Inl (base S, k, nullity S) |
      Inr \<alpha> \<Rightarrow>(case \<theta> \<alpha> of
        Some k \<Rightarrow> Inl (base S, k, nullity S) |
        _ \<Rightarrow> Inr S))"
notation signature_substitution ("\<Theta>")

lemma signature_substitution_right:
  assumes
    "\<Theta> \<theta> S' = Inr S''"
  shows
    "S' = S''"
proof -
  obtain C \<gamma> n where
    S: "S' = (C, \<gamma>, n)" using prod_cases3 by blast
  show ?thesis
  proof (cases "\<gamma>")
    case (Inl k)
    with S assms show ?thesis by simp
  next
    case (Inr \<alpha>)
    with S assms show ?thesis by (cases "\<theta> \<alpha>", auto)
  qed
qed

lemma signature_substitution_left_base:
  assumes
    "\<Theta> \<theta> S = Inl T"
  shows
    "base S = base T"
proof -
  obtain C \<gamma> n where
    S: "S = (C, \<gamma>, n)" using prod_cases3 by blast
  show ?thesis
  proof (cases "\<gamma>")
    case (Inl k)
    with S assms show ?thesis unfolding signature_substitution_def initialization_def by auto
  next
    case (Inr \<alpha>)
    with S assms show ?thesis unfolding signature_substitution_def initialization_def
      by (cases "\<theta> \<alpha>") auto
  qed
qed

lemma signature_substitution_left_nullity:
  assumes
    "\<Theta> \<theta> S = Inl T"
  shows
    "nullity S = nullity T"
proof -
  obtain C \<gamma> n where
    S: "S = (C, \<gamma>, n)" using prod_cases3 by blast
  show ?thesis
  proof (cases "\<gamma>")
    case (Inl k)
    with S assms show ?thesis unfolding signature_substitution_def initialization_def by auto
  next
    case (Inr \<alpha>)
    with S assms show ?thesis unfolding signature_substitution_def initialization_def
      by (cases "\<theta> \<alpha>") auto
  qed
qed

lemma signature_substitution_left:
  assumes
    "vars [S] \<subseteq> A" and
    "\<Theta> \<theta> S = Inl T"
  shows
    "\<Theta> (\<theta> |` A) S = Inl T"
proof -
  obtain C \<gamma> n where
    S: "S = (C, \<gamma>, n)" using prod_cases3 by blast
  show ?thesis
  proof (cases "\<gamma>")
    case (Inl k)
    with S assms show ?thesis by simp
  next
    case (Inr \<alpha>)
    with S assms show ?thesis by (cases "\<theta> \<alpha>", auto simp add: vars_def var1_def var_def)
  qed
qed

lemma signature_substitutions_left:
  assumes
    "vars S\<^sub>i \<subseteq> A" and
    "map (\<Theta> \<theta>) S\<^sub>i = map Inl T\<^sub>i"
  shows
    "map (\<Theta> (\<theta> |` A)) S\<^sub>i = map Inl T\<^sub>i"
  using assms
proof (induction S\<^sub>i arbitrary: T\<^sub>i)
  case Nil
  then show ?case
    by simp
next
  case (Cons S S\<^sub>i)
  moreover then obtain T T\<^sub>i' where
    "map (\<Theta> \<theta>) S\<^sub>i = map Inl T\<^sub>i'" and
    T: "T\<^sub>i = T # T\<^sub>i'"
    by (metis (no_types) list.simps(9) map_eq_Cons_D)
  moreover from Cons.prems have "vars S\<^sub>i \<subseteq> A" by (simp add: vars_def)
  ultimately have "map (\<Theta> (\<theta> |` A)) S\<^sub>i = map Inl T\<^sub>i'" by blast
  with Cons.prems T show ?case using signature_substitution_left vars_set
    by (metis (no_types) list.inject list.set_intros(1) list.simps(9) order_trans)
qed

lemma vars_subst:
  assumes
    "vars [S] \<subseteq> dom (\<theta>::\<theta>)"
  shows
    "\<exists> T. \<Theta> \<theta> S = Inl T"
proof (cases "initialization S")
  case (Inl k)
  then obtain C n where "S = (C, Inl k, n)"
    by (metis initialization_def surjective_pairing)
  then show ?thesis by simp
next
  case (Inr \<alpha>)
  then obtain C n where
    S: "S = (C, Inr \<alpha>, n)" by (metis initialization_def surjective_pairing)
  then have "vars [S] = {\<alpha>}" by (simp add: vars_def var1_def var_def)
  with assms obtain k where "\<theta> \<alpha> = Some k" by auto
  with S show ?thesis by simp
qed

lemma vars_subst':
  assumes
    "vars [S] \<subseteq> dom (\<theta>::\<theta>)"
  obtains
    T
  where
    "\<Theta> \<theta> S = Inl T"
  using assms vars_subst by blast 

lemma vars_subst_none:
  assumes
    "vars [S] = {\<alpha>}" and
    "\<alpha> \<notin> dom \<theta>"
  shows
    "\<Theta> \<theta> S = Inr S"
proof (cases "initialization S")
  case (Inl k)
  with assms show ?thesis by (simp add: var0 vars_value')
next
  case (Inr \<alpha>')
  moreover with assms have "\<alpha> = \<alpha>'" by (simp add: var1_def vars_value var_def)
  ultimately show ?thesis unfolding signature_substitution_def using assms(2)
    using option.simps(4) by fastforce
qed

lemma vars_substs:
  assumes
    "vars S\<^sub>i \<subseteq> dom (\<theta>::\<theta>)"
  shows
    "\<exists> T\<^sub>i. map (\<Theta> \<theta>) S\<^sub>i = map Inl T\<^sub>i"
  using assms
proof (induction S\<^sub>i)
  case Nil
  then show "vars [] \<subseteq> dom \<theta> \<Longrightarrow> \<exists>T\<^sub>i. map (\<Theta> \<theta>) [] = map Inl T\<^sub>i"
    by simp
next
  case (Cons S S\<^sub>i)
  assume
    IH: "vars S\<^sub>i \<subseteq> dom \<theta> \<Longrightarrow> \<exists>T\<^sub>i. map (\<Theta> \<theta>) S\<^sub>i = map Inl T\<^sub>i" and
    A: "vars (S # S\<^sub>i) \<subseteq> dom \<theta>"
  then obtain T where "\<Theta> \<theta> S = Inl T" using vars_subst vars_cons
    by (metis Un_subset_iff)
  with A IH show "\<exists>T\<^sub>i. map (\<Theta> \<theta>) (S # S\<^sub>i) = map Inl T\<^sub>i" using vars_cons
    by (metis Un_subset_iff list.simps(9))
qed

lemma vars_substs':
  assumes
    "vars S\<^sub>i \<subseteq> dom (\<theta>::\<theta>)"
  obtains
    T\<^sub>i
  where
    "map (\<Theta> \<theta>) S\<^sub>i = map Inl T\<^sub>i"
  using assms vars_substs by blast
*)

\<comment> \<open>The report defines an instance as having a single argument
corresponding to a sequence of signature types. However, authors argue
that a generic identify function has concrete versions, and
the restriction is needed only for target and argument types.
Therefore, the definition below has an additional argument for result
that is not used in restriction checks for duplicate occurrences.\<close>

definition "instance" :: "\<theta> \<Rightarrow> 'C S \<Rightarrow> 'C S\<^sub>i \<Rightarrow> bool"
  where
    "instance \<theta> R S\<^sub>i \<equiv>
    var1 R \<subseteq> dom \<theta> \<and>
    vars S\<^sub>i \<subseteq> dom \<theta> \<and>
    (\<forall> i j \<alpha>.
      i < length S\<^sub>i \<and>
      j < length S\<^sub>i \<and>
      i \<noteq> j \<and>
      initialization (nth S\<^sub>i i) = Inr \<alpha> \<and>
      initialization (nth S\<^sub>i j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>)"

lemma instanceD:
  assumes
    "instance \<theta> R S\<^sub>i"
  shows
    "var1 R \<subseteq> dom \<theta>" and
    "vars S\<^sub>i \<subseteq> dom \<theta>" and
    "\<forall> i j \<alpha>.
      i < length S\<^sub>i \<and>
      j < length S\<^sub>i \<and>
      i \<noteq> j \<and>
      initialization (nth S\<^sub>i i) = Inr \<alpha> \<and>
      initialization (nth S\<^sub>i j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>"
  using assms by (simp_all add: instance_def)

lemma instanceI:
    "var1 R \<subseteq> dom \<theta> \<Longrightarrow>
    vars S\<^sub>i \<subseteq> dom \<theta> \<Longrightarrow>
    \<forall> i j \<alpha>.
      i < length S\<^sub>i \<and>
      j < length S\<^sub>i \<and>
      i \<noteq> j \<and>
      initialization (nth S\<^sub>i i) = Inr \<alpha> \<and>
      initialization (nth S\<^sub>i j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop> \<Longrightarrow>
    instance \<theta> R S\<^sub>i"
  by (simp add: instance_def)

lemma instance_res_type:
  assumes
    "instance \<theta> R S\<^sub>i"
  obtains
    T
  where
    "(\<ss> \<theta>) R = Some T"
  using assms instanceD(1) signature_extension by blast

lemma instance_arg_types:
  assumes
    "instance \<theta> R S\<^sub>i"
  obtains
    T\<^sub>i
  where
    "map (\<ss> \<theta>) S\<^sub>i = map Some T\<^sub>i"
proof -
  from assms have
    "\<forall> S \<in> set S\<^sub>i. \<exists> T. \<ss> \<theta> S = Some T" using vars_item instanceD(2) signature_extension
    by (metis (no_types) subset_trans)
  with that show ?thesis
    by (induction S\<^sub>i) (simp, metis list.set_intros(1) list.set_intros(2) list.simps(9))
qed

lemma (in classes) instance_arg_subtyping:
  fixes
    X\<^sub>i Y\<^sub>i :: "'C S\<^sub>i"
  assumes
    "var1 B \<subseteq> dom \<theta>"
    "X\<^sub>i \<sqsubseteq> Y\<^sub>i"
    "instance \<theta> A X\<^sub>i"
  shows
    "instance \<theta> B Y\<^sub>i"
proof -
  have
    "\<forall>i j \<alpha>.
        i < length Y\<^sub>i \<and>
        j < length Y\<^sub>i \<and>
        i \<noteq> j \<and>
        initialization (Y\<^sub>i ! i) = Inr \<alpha> \<and>
        initialization (Y\<^sub>i ! j) = Inr \<alpha> \<longrightarrow>
        \<theta> \<alpha> \<noteq> Some \<diamondop>"
  proof (rule allI, rule allI, rule allI, rule impI)
    fix i j \<alpha>
    assume
      "i < length Y\<^sub>i \<and>
       j < length Y\<^sub>i \<and>
       i \<noteq> j \<and>
       initialization (Y\<^sub>i ! i) = Inr \<alpha> \<and>
       initialization (Y\<^sub>i ! j) = Inr \<alpha>"
    then have
      "i < length Y\<^sub>i" and
      "j < length Y\<^sub>i" and
      "i \<noteq> j" and
      ii: "initialization (Y\<^sub>i ! i) = Inr \<alpha>" and
      ij: "initialization (Y\<^sub>i ! j) = Inr \<alpha>" by simp_all
    with assms(2,3) have
      "i < length X\<^sub>i"
      "j < length X\<^sub>i"
      "i \<noteq> j"
      using instanceD(3) list_leq_length by fastforce+
    moreover with assms(2) have
      "initialization (X\<^sub>i ! i) \<sqsubseteq> initialization (Y\<^sub>i ! i)" and
      "initialization (X\<^sub>i ! j) \<sqsubseteq> initialization (Y\<^sub>i ! j)"
      using initialization_subtyping list_leq_iff by blast+
    with ii ij have
      "initialization (X\<^sub>i ! i) = Inr \<alpha>"
      "initialization (X\<^sub>i ! j) = Inr \<alpha>" by simp_all
    moreover note assms(3)
    ultimately show "\<theta> \<alpha> \<noteq> Some \<diamondop>" using instanceD(3) by blast
  qed
  moreover note assms(1)
  moreover from assms(2,3) have
    "vars Y\<^sub>i \<subseteq> dom \<theta>" using vars_le instanceD(2) by blast
  ultimately show ?thesis by (simp add: instanceI)
qed

(*
lemma instance_arg_sub_types:
  assumes
    "instance \<theta> R X\<^sub>i"
    "vars Y\<^sub>j \<subseteq> vars X\<^sub>i"
  obtains
    T\<^sub>j
  where
    "map (\<ss> \<theta>) Y\<^sub>j = map Some T\<^sub>j"
  using assms instance_arg_dom
  using vars_substs by (metis1 subset_trans)
*)

lemma instance_cons:
  "instance \<theta> R (S # S\<^sub>i) \<Longrightarrow> instance \<theta> R S\<^sub>i"
proof (induction S\<^sub>i)
  case Nil
  then show ?case unfolding instance_def using vars_empty by auto
next
  case (Cons S' S\<^sub>i)
  assume
    A: "instance \<theta> R (S # S' # S\<^sub>i)"
  then have
    "var1 R \<subseteq> dom \<theta>" using instanceD(1) by blast 
  moreover from A have
    "vars (S # S' # S\<^sub>i) \<subseteq> dom \<theta>" and
    N: "\<forall> i j \<alpha>.
      i < length (S # S' # S\<^sub>i) \<and>
      j < length (S # S' # S\<^sub>i) \<and>
      i \<noteq> j \<and>
      initialization (nth (S # S' # S\<^sub>i) i) = Inr \<alpha> \<and>
      initialization (nth (S # S' # S\<^sub>i) j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>" unfolding instance_def by simp_all
  then have
    "vars (S' # S\<^sub>i) \<subseteq> dom \<theta>" using vars_cons1 by blast
  moreover have
    "\<forall> i j \<alpha>.
      i < length (S' # S\<^sub>i) \<and>
      j < length (S' # S\<^sub>i) \<and>
      i \<noteq> j \<and>
      initialization (nth (S' # S\<^sub>i) i) = Inr \<alpha> \<and>
      initialization (nth (S' # S\<^sub>i) j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>" 
  proof (rule allI, rule allI, rule allI, rule impI)
    fix i j \<alpha>
    assume
      "i < length (S' # S\<^sub>i) \<and>
      j < length (S' # S\<^sub>i) \<and>
      i \<noteq> j \<and>
      initialization (nth (S' # S\<^sub>i) i) = Inr \<alpha> \<and>
      initialization (nth (S' # S\<^sub>i) j) = Inr \<alpha>"
    then have
      "Suc i < length (S # S' # S\<^sub>i)"
      "Suc j < length (S # S' # S\<^sub>i)"
      "Suc i \<noteq> Suc j"
      "initialization (nth (S # S' # S\<^sub>i) (Suc i)) = Inr \<alpha>"
      "initialization (nth (S # S' # S\<^sub>i) (Suc j)) = Inr \<alpha>" by simp_all
    with N show "\<theta> \<alpha> \<noteq> Some \<diamondop>" by blast
  qed
  ultimately show ?case using instanceI by blast
qed

definition "instances" :: "'C S \<Rightarrow> 'C S\<^sub>i \<Rightarrow> \<theta> set"
  where
    "instances R S\<^sub>i \<equiv> {\<theta>. dom \<theta> = var1 R \<union> vars S\<^sub>i \<and> instance \<theta> R S\<^sub>i}"

lemma instances_instance: "\<theta> \<in> instances R S\<^sub>i \<Longrightarrow> instance \<theta> R S\<^sub>i"
  by (simp add: instances_def)

lemma instancesI: "dom \<theta> = var1 R \<union> vars S\<^sub>i \<Longrightarrow> instance \<theta> R S\<^sub>i \<Longrightarrow> \<theta> \<in> instances R S\<^sub>i"
  by (simp add: instances_def)

lemma (in classes) instance_restriction:
  fixes
    S\<^sub>i S\<^sub>j :: "'C S\<^sub>i"
  assumes
    "var1 B \<subseteq> dom \<theta>"
    "instance \<theta> A S\<^sub>i"
    "map initialization S\<^sub>i \<sqsubseteq> map initialization S\<^sub>j"
  shows
    "instance \<theta> B S\<^sub>j"
proof -
  from assms(3) have
    "vars S\<^sub>j \<subseteq> vars S\<^sub>i" using initialization_vars_le by simp
  with assms(2) have
    "vars S\<^sub>j \<subseteq> dom \<theta>" and
    "\<forall> i j \<alpha>.
      i < length S\<^sub>i \<and>
      j < length S\<^sub>i \<and>
      i \<noteq> j \<and>
      initialization (nth S\<^sub>i i) = Inr \<alpha> \<and>
      initialization (nth S\<^sub>i j) = Inr \<alpha> \<longrightarrow>
      \<theta> \<alpha> \<noteq> Some \<diamondop>" using instance_def by blast+
  moreover from assms(3) have
    S: "\<forall> i. i < length S\<^sub>i \<longrightarrow> initialization (nth S\<^sub>i i) \<sqsubseteq> initialization (nth S\<^sub>j i)"
    unfolding list_leq_def using specialization_list_item specialization_list_length
    by (metis (no_types) length_map nth_map)
  have
    "\<forall>i j \<alpha>.
        i < length S\<^sub>j \<and>
        j < length S\<^sub>j \<and>
        i \<noteq> j \<and>
        initialization (S\<^sub>j ! i) = Inr \<alpha> \<and>
        initialization (S\<^sub>j ! j) = Inr \<alpha> \<longrightarrow>
        \<theta> \<alpha> \<noteq> Some \<diamondop>"
  proof (rule allI, rule allI, rule allI, rule impI)
    fix i j \<alpha>
    assume
      "i < length S\<^sub>j \<and>
       j < length S\<^sub>j \<and>
       i \<noteq> j \<and>
       initialization (S\<^sub>j ! i) = Inr \<alpha> \<and>
       initialization (S\<^sub>j ! j) = Inr \<alpha>"
    then have
      iL: "i < length S\<^sub>j" and
      jL: "j < length S\<^sub>j" and
      ij: "i \<noteq> j" and
      iV:"initialization (S\<^sub>j ! i) = Inr \<alpha>" and
      jV: "initialization (S\<^sub>j ! j) = Inr \<alpha>" by simp_all
    with S assms(3) have
      "initialization (nth S\<^sub>i i) = Inr \<alpha> \<or> initialization (nth S\<^sub>i i) = Inl \<diamondop>"
      "initialization (nth S\<^sub>i j) = Inr \<alpha> \<or> initialization (nth S\<^sub>i j) = Inl \<diamondop>"
      using list_leq_length by fastforce+
    with S assms(3) iL iV jL jV ij show "\<theta> \<alpha> \<noteq> Some \<diamondop>"
      apply (cases "initialization (nth S\<^sub>i i)"; cases "initialization (nth S\<^sub>i j)")
      using list_leq_length apply force
      using list_leq_length apply force
      using  list_leq_length apply force
      by (simp add: calculation(2) list_leq_iff)
  qed
  moreover note assms(1)
  ultimately show ?thesis by (simp add: instanceI)
qed

subsubsection "Expectation lifting"

lemma substitutions_value:
  assumes
    "set xs \<subseteq> dom \<theta>"
  obtains
    ks
  where
    "map \<theta> xs = map Some ks"
using assms by (induction xs) (simp_all, metis (no_types) domD map_eq_Cons_conv)

lemma substitutions_restriction:
  assumes
    "set xs \<subseteq> A"
  shows
    "map (\<theta> |` A) xs = map \<theta> xs"
  using assms by auto

definition left :: "'a + 'b \<rightharpoonup> 'a"
  where
    "left x = (case x of Inl a \<Rightarrow> Some a | _ \<Rightarrow> None)"

lemma left_inl: "left (Inl a) = Some a"
  by (simp add: left_def)

lemma left_inr: "left (Inr b) = None"
  by (simp add: left_def)

lemma left_some: "left x = Some a \<Longrightarrow> x = Inl a"
  by (cases x) (simp_all add: left_def)

lemma left_none: "left x = None \<Longrightarrow> \<exists> b. x = Inr b"
  by (cases x) (simp_all add: left_def)

lemma left_eq: "x = Inl z \<Longrightarrow> left x = left y \<Longrightarrow> x = y"
  by (metis left_inl left_some)

lemma map_left_some:
  assumes
    "map left xs = map Some ys"
  obtains
    zs
  where
    "xs = map Inl zs"
  using assms
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons x xs ys')
  from Cons obtain y ys where
    "ys' = y # ys" by auto
  with Cons.prems(2) have
    "map left xs = map Some ys"
    "left x = Some y" by simp_all
  with Cons.IH Cons.prems(1) show ?case using Cons_eq_map_conv left_some by fastforce
qed

lemma map_left_map_inl: "\<forall> zs. \<exists> ys. map left (map Inl zs) = map Some ys"
  unfolding left_def by (simp add: case_sum_o_inj(1))

lemma instance_types_base:
  assumes
    "instance \<theta> R S\<^sub>i"
  shows
    "map (base o the o (\<ss> \<theta>)) S\<^sub>i = map base S\<^sub>i"
  using assms
proof (induction S\<^sub>i)
  case Nil
  then show ?case by simp
next
  case (Cons S S\<^sub>i)
  then have
    "instance \<theta> R S\<^sub>i" using instance_cons by blast
  with Cons.IH have
    "map (base \<circ> the \<circ> \<ss> \<theta>) S\<^sub>i = map base S\<^sub>i" by simp
  moreover
  from Cons.prems have
    "var1 S \<subseteq> dom \<theta>" using instanceD(2) vars_cons1 by blast
  then have
    "\<ss> \<theta> S \<noteq> None"
    using expectation_substitutions_signature_extension_none vars_value by blast
  then have
    "base (the (\<ss> \<theta> S)) = base S" using substitution_base by blast
  ultimately show ?case by simp
qed

lemma instance_types_nullity:
  assumes
    "instance \<theta> R S\<^sub>i"
  shows
    "map (nullity \<circ> the \<circ> (\<ss> \<theta>)) S\<^sub>i = map nullity S\<^sub>i"
  using assms
proof (induction S\<^sub>i)
  case Nil
  then show ?case by simp
next
  case (Cons S S\<^sub>i)
  then have
    "instance \<theta> R S\<^sub>i" using instance_cons by blast
  with Cons.IH have
    "map (nullity \<circ> the \<circ> \<ss> \<theta>) S\<^sub>i = map nullity S\<^sub>i" by simp
  moreover
  from Cons.prems have
    "var1 S \<subseteq> dom \<theta>" using instanceD(2) vars_cons1 by blast
  then have
    "\<ss> \<theta> S \<noteq> None"
    using expectation_substitutions_signature_extension_none vars_value by blast
  then have
    "nullity (the (\<ss> \<theta> S)) = nullity S" using substitution_nullity by blast
  ultimately show ?case by simp
qed

lemma instance_types_t:
  assumes
    "instance \<theta> R S\<^sub>i"
  shows
    "map (type_as_t \<circ> the \<circ> (\<ss> \<theta>)) S\<^sub>i = map type_as_t S\<^sub>i"
  using assms
proof (induction S\<^sub>i)
  case Nil
  then show ?case by simp
next
  case (Cons S S\<^sub>i)
  then have
    "instance \<theta> R S\<^sub>i" using instance_cons by blast
  with Cons.IH have
    "map (type_as_t \<circ> the \<circ> \<ss> \<theta>) S\<^sub>i = map type_as_t S\<^sub>i" by simp
  moreover
  from Cons.prems have
    "var1 S \<subseteq> dom \<theta>" using instanceD(2) vars_cons1 by blast
  then have
    "\<ss> \<theta> S \<noteq> None"
    using expectation_substitutions_signature_extension_none vars_value by blast
  then have
    "type_as_t (the (\<ss> \<theta> S)) = type_as_t S" using substitution_t by blast
  ultimately show ?case by simp
qed

lemma substitution_instance_signature:
  assumes
    "instance \<theta> R X\<^sub>i"
    "var1 Y \<subseteq> vars X\<^sub>i"
  obtains
    T
  where
    "\<ss> \<theta> Y = Some T"
  using assms signature_extension_value instanceD(2) by (metis subset_trans)

lemma substitution_instance_sub_types:
  assumes
    "instance \<theta> R X\<^sub>i"
    "vars Y\<^sub>j \<subseteq> vars X\<^sub>i"
  obtains
    T\<^sub>j
  where
    "map (\<ss> \<theta>) Y\<^sub>j = map Some T\<^sub>j"
proof -
  from assms have
    "\<forall> i < length Y\<^sub>j. \<exists> T. \<ss> \<theta> (Y\<^sub>j ! i) = Some T" using substitution_instance_signature vars_item
    by (metis nth_mem order.trans)
  moreover obtain T\<^sub>j where
    "T\<^sub>j = map (the o (\<ss> \<theta>)) Y\<^sub>j" by simp
  ultimately have
    "map (\<ss> \<theta>) Y\<^sub>j = map Some T\<^sub>j" by (simp add: list_eq_iff_nth_eq)
  then show ?thesis using that by simp
qed

lemma substitution_nullity_subtyping: "Some T \<sqsubseteq> \<ss> \<theta> X \<Longrightarrow> nullity T \<sqsubseteq> nullity X"
  using substitution_nullity nullity_partial_subtyping
  by (metis (no_types, lifting) case_optionE option.discI option.sel option_leq)

lemma initialization_substitution_subtyping: "var x \<subseteq> dom \<theta> \<Longrightarrow> var y \<subseteq> dom \<theta> \<Longrightarrow> x \<sqsubseteq> y \<Longrightarrow> \<ee> \<theta> x \<sqsubseteq> \<ee> \<theta> y"
proof (cases x; cases y)
  fix \<alpha> b
  assume
    "var x \<subseteq> dom \<theta>"
    "x = Inr \<alpha>"
    "x \<sqsubseteq> y"
    "y = Inl b"
  then have
    "\<alpha> \<in> dom \<theta>"
    "\<ee> \<theta> x = \<theta> \<alpha>" and
    "\<ee> \<theta> y = Some \<diamondop>" unfolding initialization_extension_def
    using generic_initialization_specialization var1 by simp_all
  then show
    "\<ee> \<theta> x \<sqsubseteq> \<ee> \<theta> y" by auto
qed (insert leq_refl, auto simp add: initialization_extension_def)

lemma substitution_subtyping: "var1 X \<subseteq> dom \<theta> \<Longrightarrow> var1 Y \<subseteq> dom \<theta> \<Longrightarrow> X \<sqsubseteq> Y \<Longrightarrow> \<ss> \<theta> X \<sqsubseteq> \<ss> \<theta> Y"
proof -
  assume
    XD: "var1 X \<subseteq> dom \<theta>" and 
    YD: "var1 Y \<subseteq> dom \<theta>" and
    "X \<sqsubseteq> Y"
  then have
    "var (initialization X) \<subseteq> dom \<theta>"
    "var (initialization Y) \<subseteq> dom \<theta>"
    "base X \<sqsubseteq> base Y"
    "initialization X \<sqsubseteq> initialization Y"
    "nullity X \<sqsubseteq> nullity Y" unfolding var1_def
    by (fastforce dest: base_subtyping initialization_subtyping nullity_partial_subtyping)+
  moreover then have
    XN: "\<ss> \<theta> X \<noteq> None" and
    YN: "\<ss> \<theta> Y \<noteq> None" and
    "\<ee> \<theta> (initialization X) \<sqsubseteq> \<ee> \<theta> (initialization Y)" using signature_extension unfolding var1_def
    using initialization_substitution_subtyping by blast+
  with XD YD calculation(1) have
    "initialization (the (\<ss> \<theta> X)) \<sqsubseteq> initialization (the (\<ss> \<theta> Y))"
    using initialization_extension_var_iff signature_initialization option_leq
    by (metis option.case_eq_if)
  moreover from XN YN calculation(3,5) have
    "base (the (\<ss> \<theta> X)) \<sqsubseteq> base (the (\<ss> \<theta> Y))"
    "nullity (the (\<ss> \<theta> X)) \<sqsubseteq> nullity (the (\<ss> \<theta> Y))"
    using substitution_base substitution_nullity by metis+
  ultimately have
    "the (\<ss> \<theta> X) \<sqsubseteq> the (\<ss> \<theta> Y)" using subtyping by blast
  with XN YN show ?thesis by auto
qed

lemma substitutions_subtyping:
  assumes
    "vars X\<^sub>i \<subseteq> dom \<theta>"
    "vars Y\<^sub>i \<subseteq> dom \<theta>"
    "X\<^sub>i \<sqsubseteq> Y\<^sub>i"
  shows
    "map (\<ss> \<theta>) X\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) Y\<^sub>i"
proof -
  from assms(3) obtain Z\<^sub>i where
    L: "length X\<^sub>i = length Y\<^sub>i" and
    Z: "Z\<^sub>i = zip X\<^sub>i Y\<^sub>i" and
    "length Z\<^sub>i = length X\<^sub>i"
    "length Z\<^sub>i = length Y\<^sub>i" using subtyping_same_count by auto
  moreover with assms have
    "vars (map fst Z\<^sub>i) \<subseteq> dom \<theta>"
    "vars (map snd Z\<^sub>i) \<subseteq> dom \<theta>"
    "map fst Z\<^sub>i \<sqsubseteq> map snd Z\<^sub>i" by simp_all
  then have
    "map (\<ss> \<theta> o fst) Z\<^sub>i \<sqsubseteq> map (\<ss> \<theta> o snd) Z\<^sub>i"
  proof (induction Z\<^sub>i)
    case Nil
    then show ?case by (simp add: leq_refl)
  next
    case (Cons Z Z\<^sub>i)
    then have
      "var1 (fst Z) \<subseteq> dom \<theta>"
      "var1 (snd Z) \<subseteq> dom \<theta>" unfolding vars_def by simp_all
    moreover from Cons.prems(3) have
      "fst Z \<sqsubseteq> snd Z" using list_leq_cons by fastforce
    ultimately have
      "(\<ss> \<theta> \<circ> fst) Z \<sqsubseteq> (\<ss> \<theta> \<circ> snd) Z" using substitution_subtyping by (metis comp_apply)
    moreover from Cons.prems have
      "vars (map fst Z\<^sub>i) \<subseteq> dom \<theta>"
      "vars (map snd Z\<^sub>i) \<subseteq> dom \<theta>"
      "map fst Z\<^sub>i \<sqsubseteq> map snd Z\<^sub>i" unfolding vars_def using list_leq_cons by auto
    with Cons.IH have
      "map (\<ss> \<theta> \<circ> fst) Z\<^sub>i \<sqsubseteq> map (\<ss> \<theta> \<circ> snd) Z\<^sub>i" by blast
    ultimately show ?case using list_leq_cons by (metis (no_types) list.simps(9))
  qed
  ultimately show ?thesis by (metis (no_types) map_map zip_eq_conv)
qed

lemma substitutions_nullity_subtyping: "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) X\<^sub>i \<Longrightarrow> map nullity T\<^sub>i \<sqsubseteq> map nullity X\<^sub>i"
proof -
  assume
    M: "map Some T\<^sub>i \<sqsubseteq> map (\<ss> \<theta>) X\<^sub>i"
  moreover
  then obtain Y\<^sub>i where
    "map (\<ss> \<theta>) X\<^sub>i = map Some Y\<^sub>i" using map_some_structure by auto
  moreover
  then have
    "map nullity X\<^sub>i = map nullity Y\<^sub>i"
  proof -
    from calculation(2) have
      L: "length X\<^sub>i = length Y\<^sub>i" using  map_eq_imp_length_eq by auto
    obtain Z where
      "Z = zip X\<^sub>i Y\<^sub>i" by simp
    with L have
      X: "X\<^sub>i = map fst Z" and
      Y: "Y\<^sub>i = map snd Z" by simp_all
    with calculation(2) have
      "map ((\<ss> \<theta>) o fst) Z = map (Some o snd) Z" by simp
    then have
      "map (nullity o fst) Z = map (nullity o snd) Z"
    proof (induction Z)
      case Nil
      then show ?case by simp
    next
      case (Cons z Z)
      then have
        "map (nullity \<circ> fst) Z = map (nullity \<circ> snd) Z" by simp
      moreover
      from Cons.prems have
        "(\<ss> \<theta> \<circ> fst) z = (Some \<circ> snd) z" by simp
      then have
        "(nullity \<circ> fst) z = (nullity \<circ> snd) z" using substitution_nullity
        by (metis comp_def option.distinct(1) option.sel)
      ultimately show ?case by simp
    qed
    with X Y show ?thesis by simp
  qed
  ultimately show ?thesis
    using map_some_partial_order nullity_partial_subtyping partial_mono_def partial_mono_map
    by metis
qed

(*
lemma map_expectation_dom:
  fixes
    S\<^sub>i:: "'C S\<^sub>i"
  assumes
    "vars S\<^sub>i \<subseteq> dom \<theta>"
  obtains
    T\<^sub>i:: "'C T\<^sub>i"
  where
    "map (\<ss> \<theta>) S\<^sub>i = map Some T\<^sub>i"
  using assms vars_substs expectation_substitutions_inl_some_iff by blast
*)

lemma expectation_restriction:
  fixes
    S\<^sub>i:: "'C S\<^sub>i"
  assumes
    "vars S\<^sub>i \<subseteq> A"
  shows
    "map (\<ss> (\<theta> |` A)) S\<^sub>i = map (\<ss> \<theta>) S\<^sub>i"
  using assms
proof (induction S\<^sub>i arbitrary: A)
  case (Cons S S\<^sub>i)
  then have
    "var1 S \<subseteq> A" using vars_cons1 by blast
  then have
    "\<ss> (\<theta> |` A) S = \<ss> \<theta> S" using substitution_restriction by blast
  moreover from Cons.prems have
    "vars S\<^sub>i \<subseteq> A" using vars_cons1 by blast
  with Cons.IH have
    "map (\<ss> (\<theta> |` A)) S\<^sub>i = map (\<ss> \<theta>) S\<^sub>i" .
  ultimately show ?case by simp
qed simp

end