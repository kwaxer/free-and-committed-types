(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 5 (Expressions)\<close>

theory Expression imports Name begin

type_synonym x = vname
type_synonym x\<^sub>i = "x list"

datatype expression =
  Variable x |
  Field_access x f |
  Null

notation Variable ("\<V> _")
notation Field_access ("_ \<^bsub>\<bullet>\<^esub> _")

type_synonym e = expression

end