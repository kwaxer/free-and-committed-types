(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>General facts used in the proofs\<close>

text \<open>Lemmas in this section are useful for any kind of reasoning in Isabelle/HOL and
are potential candidates for adding to some library.\<close>

theory Common imports Main begin

lemma distinct_index: "distinct xs \<Longrightarrow> x \<in> set xs \<Longrightarrow> \<exists>! i. i < size xs \<and> xs ! i = x"
  by (induction xs) (simp, metis distinct_Ex1)

lemma fold_conj_map: "fold (\<and>) (map f xs) True = (\<forall> x\<in> set xs. f x)"
proof (induction xs)
  case (Cons x xs)
  moreover have "fold (\<and>) (map f (x # xs)) True = ((fold (\<and>) (map f xs) True) \<and> f x)"
    by (cases "f x", simp, induction xs, simp_all)
  ultimately show ?case by auto
qed simp

lemma foldr_conj_map: "foldr (\<lambda> x. (\<and>) (f x)) xs s \<Longrightarrow> s \<and> (\<forall> x\<in> set xs. f x)"
  by (induction xs) simp_all

lemma nth_map_eq:
  "length xs = length ys \<and> (\<forall> i < length (xs). f (xs ! i) = g (ys ! i)) \<longleftrightarrow> map f xs = map g ys"
proof -
  have "length xs = length ys \<and> (\<forall> i < length (xs). f (xs ! i) = g (ys ! i)) \<Longrightarrow> map f xs = map g ys"
  proof -
    obtain zs where
      Z: "zs = zip xs ys" by simp
    obtain n where
      N: "n = length zs" by simp
    assume
      "length xs = length ys \<and> (\<forall> i < length (xs). f (xs ! i) = g (ys ! i))"
    then have
      L: "length xs = length ys" and
      I: "\<forall> i < length (xs). f (xs ! i) = g (ys ! i)" by simp_all
    from L Z N have "length xs = n" by simp
    with L I Z N show "map f xs = map g ys"
      by (induction zs) (simp, simp add: L list_eq_iff_nth_eq)
  qed
  moreover have
    "map f xs = map g ys \<Longrightarrow> length xs = length ys \<and> (\<forall> i < length (xs). f (xs ! i) = g (ys ! i))"
  proof
    assume
      M: "map f xs = map g ys"
    then show "length xs = length ys" using map_eq_imp_length_eq by blast
    with M show "\<forall> i < length (xs). f (xs ! i) = g (ys ! i)" by (metis nth_map)
  qed
  ultimately show ?thesis by blast
qed

lemma those_map_some: "those (map f xs) = Some ys \<Longrightarrow> map f xs = map Some ys"
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  then obtain y where
    Y: "f a = Some y" by fastforce
  then have "those (map f (a # xs)) = map_option (Cons y) (those (map f xs))"
    by simp
  moreover then obtain ys' where
    Ys: "those (map f xs) = Some ys'"
      using Cons.prems by auto
  ultimately have "ys = y # ys'"
    using Cons.prems by auto
  then show ?case using Cons.IH Y Ys by auto
qed

lemma map_some_those: "map f xs = map Some ys \<Longrightarrow> those (map f xs) = Some ys"
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  then obtain y where
    Y: "f a = Some y" by fastforce
  then have "those (map f (a # xs)) = map_option (Cons y) (those (map f xs))"
    by simp
  moreover then obtain ys' where
    Ys: "those (map f xs) = Some ys'" using Cons by auto
  ultimately have "ys = y # ys'"
    using Cons by auto
  then show ?case using Cons.IH Y Ys by auto
qed

subsubsection \<open>Transitive closure properties\<close>

lemma rtranclp_to_trace:
  assumes
    "P\<^sup>*\<^sup>* x y"
  shows
    "\<exists> xs. length xs > 0 \<and> xs ! 0 = x \<and> xs ! (length xs - 1) = y \<and> (\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i))"
using assms
proof (induction rule:rtranclp_induct)
  case base
  then show ?case
    by (metis Skolem_list_nth diff_Suc_1 length_Cons less_nat_zero_code nth_Cons_0 zero_less_Suc)
next
  case (step y z)
  moreover then obtain xs' where
    L': "length xs' > 0" and
    X': "xs' ! 0 = x" and
    Y': "xs' ! (length xs' - 1) = y" and
    P': "\<forall> i < length xs' - 1. P (xs' ! i) (xs' ! Suc i)" by blast
  obtain xs where
    Z: "xs = xs' @ [z]" by simp
  moreover with L' have
    "length xs > Suc 0" by simp
  moreover from L' Y' Z have
    "xs ! (length xs - 2) = y" by (simp add: nth_append)
  moreover with L' Z step.hyps have
    "P (xs ! (length xs - 2)) (xs ! (length xs - 1))" by simp
  moreover from L' P' Z have
    "\<forall> i < length xs - 2. P (xs ! i) (xs ! Suc i)"
    by (metis One_nat_def Suc_1 Suc_mono Suc_pred diff_Suc_Suc length_append_singleton less_SucI nth_append)
  ultimately have "\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)"
    by (metis Suc_1 Suc_diff_Suc less_SucE numeral_1_eq_Suc_0 numerals(1)) 
  moreover from L' X' Z have
    "xs ! 0 = x" by (simp add: nth_append)
  moreover note Z
  ultimately show ?case by auto
qed

lemma rtranclp_to_trace_distinct:
  assumes
    "P\<^sup>*\<^sup>* x y"
  shows
    "\<exists> xs. size xs > 0 \<and> xs ! 0 = x \<and> xs ! (size xs - 1) = y \<and> distinct xs \<and> (\<forall> i < size xs - 1. P (xs ! i) (xs ! Suc i))"
using assms
proof (induction rule:rtranclp_induct)
  case base
  then show ?case
    by (metis One_nat_def cancel_comm_monoid_add_class.diff_cancel distinct.simps(2) distinct_conv_nth in_set_replicate length_replicate less_numeral_extra(1) not_less_zero nth_Cons_0 replicate_Suc)
next
  case (step y z)
  then obtain xs' where
    L': "length xs' > 0" and
    X': "xs' ! 0 = x" and
    Y': "xs' ! (size xs' - 1) = y" and
    D': "distinct xs'" and
    P': "\<forall> i < size xs' - 1. P (xs' ! i) (xs' ! Suc i)" by blast
  have ?case if "z \<notin> set xs'" using that
  proof -
    obtain xs where
      Z: "xs = xs' @ [z]" by simp
    moreover with L' have
      "size xs > Suc 0" by simp
    moreover from L' Y' Z have
      "xs ! (size xs - 2) = y" by (simp add: nth_append)
    moreover with L' Z step.hyps have
      "P (xs ! (size xs - 2)) (xs ! (size xs - 1))" by simp
    moreover from L' P' Z have
      "\<forall> i < size xs - 2. P (xs ! i) (xs ! Suc i)"
      by (metis One_nat_def Suc_1 Suc_mono Suc_pred diff_Suc_Suc length_append_singleton less_SucI nth_append)
    ultimately have "\<forall> i < size xs - 1. P (xs ! i) (xs ! Suc i)"
      by (metis Suc_1 Suc_diff_Suc less_SucE numeral_1_eq_Suc_0 numerals(1)) 
    moreover from Z have
      "0 < size xs" by simp
    moreover from L' X' Z have
      "xs ! 0 = x" by (simp add: nth_append)
    moreover from Z have
      "xs ! (size xs - 1) = z" by simp
    moreover from D' Z have
      "distinct xs" by (simp add: that)
    ultimately show ?thesis by auto
  qed
  moreover have ?case if "z \<in> set xs'" using that
  proof -
    assume
      A: "z \<in> set xs'"
    then obtain j where
      J: "j < size xs'" and
      Z: "xs' ! j = z" using in_set_conv_nth by metis
    then obtain xs where
      S: "xs = take (Suc j) xs'" by simp
    with J L' have
      L: "Suc j = size xs" by (induction xs' arbitrary: j xs) simp_all
    from S L' have
      "0 < size xs" by simp
    moreover from S X' have
      "xs ! 0 = x" by simp
    moreover from J S Z have
      "xs ! (size xs - 1) = z"
      by (metis Suc_leI butlast_take diff_Suc_1 length_butlast nth_append_length take_Suc_conv_app_nth)
    moreover from D' S have
      "distinct xs" by simp
    moreover from S have
      "\<forall> i < size xs. xs ! i = xs' ! i" by simp
    with P' L J have
      "\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)" by simp
    ultimately show ?thesis by blast
  qed
  ultimately show ?case by blast
qed

lemma rtranclp_from_trace':
  assumes
    "\<forall> i < length xs. P ((xs @ [y]) ! i) ((xs @ [y]) ! Suc i)"
  shows
    "P\<^sup>*\<^sup>* (hd (xs @ [y])) y"
  using assms
proof (induction xs)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  from Cons.prems have
    "\<forall> i < length xs. P ((xs @ [y]) ! i) ((xs @ [y]) ! Suc i)" by auto
  with Cons.IH have
    "P\<^sup>*\<^sup>* (hd (xs @ [y])) y" by simp
  moreover from Cons.prems have
    "P (((a # xs) @ [y]) ! length xs) (((a # xs) @ [y]) ! Suc (length xs))" by auto
  moreover have
    "Suc (length xs) = length (a # xs)" by simp
  ultimately show ?case
    by (metis Cons.prems append_Cons append_is_Nil_conv converse_rtranclp_into_rtranclp hd_conv_nth not_Cons_self2 nth_Cons_Suc zero_less_Suc)
qed

lemma rtranclp_from_trace'':
  assumes
    L: "0 < length xs" and
    X: "xs ! 0 = x" and
    Y: "xs ! (length xs - 1) = y" and
    P: "\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)"
  shows
    "P\<^sup>*\<^sup>* x y"
proof -
  from L Y obtain zs where
    Z: "zs @ [y] = xs" by (simp add: last_conv_nth snoc_eq_iff_butlast)
  moreover from L X have
    "hd (xs @ [y]) = x" by (simp add: hd_append hd_conv_nth)
  moreover from Z have
    "length zs = length xs - 1" by auto
  moreover note L P
  ultimately show ?thesis using rtranclp_from_trace'
    by (metis hd_append length_greater_0_conv) 
qed

lemma rtranclp_trace:
  "P\<^sup>*\<^sup>* x y \<longleftrightarrow>
    (\<exists> xs. 0 < length xs \<and> xs ! 0 = x \<and> xs ! (length xs - 1) = y \<and> (\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)))"
  by (metis rtranclp_from_trace'' rtranclp_to_trace)

lemma rtranclp_intrace:
  "P\<^sup>*\<^sup>* x y \<longleftrightarrow>
    (\<exists> xs.
      0 < length xs \<and>
      xs ! 0 = x \<and>
      y \<in> set xs \<and>
      (\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)))"
proof (rule iffI)
  assume
    "P\<^sup>*\<^sup>* x y"
  then show "\<exists> xs.
      0 < length xs \<and>
      xs ! 0 = x \<and>
      y \<in> set xs \<and>
      (\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i))" using rtranclp_trace
    by (metis diff_less less_numeral_extra(1) nth_mem)
next
  assume
    "\<exists> xs.
      0 < length xs \<and>
      xs ! 0 = x \<and>
      y \<in> set xs \<and>
      (\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i))"
  then obtain xs where
    x1: "0 < length xs" and
    x2: "xs ! 0 = x" and
    x3: "y \<in> set xs" and
    x4: "\<forall> i < length xs - 1. P (xs ! i) (xs ! Suc i)" by blast
  then obtain i where
    il: "i < length xs" and
    y: "xs ! i = y" by (meson in_set_conv_nth) 
  moreover obtain ys where
    ys: "ys = take (Suc i) xs" by simp
  ultimately have
    ysL: "length ys = Suc i" by simp
  with x1 x2 x4 y ys have
    "0 < length ys" and
    "ys ! 0 = x" and
    "ys ! (length ys - 1) = y" and
    "\<forall> i < length ys - 1. P (ys ! i) (ys ! Suc i)" by simp_all
  then show "P\<^sup>*\<^sup>* x y" using rtranclp_trace by metis
qed

lemma rtranclp_fun_upd: "\<not> P\<^sup>*\<^sup>* x y \<Longrightarrow> (P (y := v))\<^sup>*\<^sup>* x z = P\<^sup>*\<^sup>* x z"
  by (rule; erule rtranclp_induct, simp) (metis fun_upd_apply rtranclp.simps)+

lemma rtranclp_rel_upd_left:
  "\<not> P\<^sup>*\<^sup>* x y \<Longrightarrow> (\<lambda> a. if a = y then R a else P a)\<^sup>*\<^sup>* x z = P\<^sup>*\<^sup>* x z"
proof (rule; erule rtranclp_induct)
  assume
    "\<not> P\<^sup>*\<^sup>* x y"
  show "P\<^sup>*\<^sup>* x x" by simp
next
  fix
    t z
  assume
    "\<not> P\<^sup>*\<^sup>* x y"
    "(\<lambda>a. if a = y then R a else P a)\<^sup>*\<^sup>* x t"
    "(if t = y then R t else P t) z"
    "P\<^sup>*\<^sup>* x t"
  then show "P\<^sup>*\<^sup>* x z"
    by (metis (full_types) reflclp_tranclp rtranclp_into_tranclp1 sup2CI)
next
  assume
    "\<not> P\<^sup>*\<^sup>* x y"
  show "(\<lambda>a. if a = y then R a else P a)\<^sup>*\<^sup>* x x" by simp
next
  fix
    t z
  assume
    "\<not> P\<^sup>*\<^sup>* x y"
    "P\<^sup>*\<^sup>* x t"
    "P t z"
    "(\<lambda>a. if a = y then R a else P a)\<^sup>*\<^sup>* x t"
  then have
    "\<exists>a. (\<lambda>a. if a = y then R a else P a)\<^sup>*\<^sup>* x a \<and> (if a = y then R a else P a) z" by auto
  then show "(\<lambda>a. if a = y then R a else P a)\<^sup>*\<^sup>* x z" by (meson rtranclp.simps)
qed

subsubsection \<open>Function update list\<close>

text \<open>There is a standard update function that applies to a single element.
      This section describes updates to a function presented by a list of elements to be updated.\<close>

no_syntax "_record_update" :: "'a => field_updates => 'b"("_/(3\<lparr>_\<rparr>)" [900, 0] 900)

text \<open>The order of function updates is important. Here, it is using @{term foldr},
      i.e. first update element in the list is applied last.\<close>

definition fun_updr :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b list \<Rightarrow> ('a \<Rightarrow> 'b)" ("_ \<lparr> _ := _ \<rparr>")
  where
    [iff, simp]: "f \<lparr>xs := ys\<rparr> \<equiv> foldr (\<lambda> p f. f (fst p := snd p)) (zip xs ys) f"

lemma fun_updr_map_zip: "f \<lparr>xs := ys\<rparr> = f \<lparr>map fst (zip xs ys) := map snd (zip xs ys)\<rparr>"
  unfolding fun_updr_def by (simp add: zip_map_fst_snd)

lemma fun_updr_cons: "f \<lparr>x # xs := y # ys\<rparr> = f \<lparr>xs := ys\<rparr> (x := y)"
  unfolding fun_updr_def by simp

lemma fun_updr_append: "f \<lparr>(map fst zs) @ xs := (map snd zs) @ ys\<rparr> =
  (f \<lparr>xs := ys\<rparr>) \<lparr>map fst zs := map snd zs\<rparr>"
  unfolding fun_updr_def by (induction zs) auto

lemma fun_updr_le: "length xs \<le> length vs \<Longrightarrow> f \<lparr>xs := vs\<rparr> = f \<lparr>xs := take (length xs) vs\<rparr>"
  unfolding fun_updr_def
  by (induction xs arbitrary: vs)
   (simp, metis (lifting) map_fst_zip_take map_snd_zip map_snd_zip_take min_def zip_map_fst_snd)

lemma fun_updr_ge: "length xs \<ge> length ys \<Longrightarrow> f \<lparr>xs := ys\<rparr> = f \<lparr>take (length ys) xs := ys\<rparr>"
  unfolding fun_updr_def
  by (induction xs arbitrary: ys)
    (simp, metis (lifting) map_fst_zip_take map_snd_zip map_snd_zip_take min_def zip_map_fst_snd zip_commute)

lemma fun_updr_take_length: "f \<lparr>xs := ys\<rparr> = f \<lparr>take (length ys) xs := take (length xs) ys\<rparr>"
  using fun_updr_le fun_updr_ge
  by (metis le_cases take_all)

lemma fun_updr_min_length: "n = min (length xs) (length ys) \<Longrightarrow>
  f \<lparr>xs := ys\<rparr> = f \<lparr>take n xs := take n ys\<rparr>"
  using fun_updr_take_length
  by (metis (no_types) length_take min.idem take_take)

lemma fun_updr_some_le_dom: "length xs \<le> length ys \<Longrightarrow>
  dom (f \<lparr>xs := map Some ys\<rparr>) = dom f \<union> set xs"
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons x xs)
  from Cons.prems(1) obtain y ys' where
    vs: "ys = y # ys'" by (metis Suc_le_length_iff length_Cons)
  moreover
  with Cons.prems have
    "length xs \<le> length ys'" by simp
  moreover
  with Cons.IH have
    "dom f \<lparr>xs := map Some ys'\<rparr> = dom f \<union> set xs" by blast
  ultimately have "dom (f \<lparr>x # xs := map Some ys\<rparr>) = dom f \<union> set xs \<union> {x}" by simp
  then show ?case by simp
qed

lemma fun_updr_some_dom:
  "dom (f \<lparr>xs := map Some ys\<rparr>) = dom f \<union> set (take (length ys) xs)"
  by (cases "length xs \<le> length ys")
    (metis fun_updr_some_le_dom take_all,
    metis (no_types) fun_updr_ge fun_updr_some_le_dom length_map length_take min_def nat_le_linear)

lemma fun_updr_other:
  assumes
    "x \<notin> set xs"
  shows
    "f \<lparr>xs := ys\<rparr> x = f x"
proof -
  obtain zs where
    "zs = zip xs ys" by simp
  moreover
  with assms have
    "x \<notin> set (map fst zs)" by (metis in_set_takeD map_fst_zip_take)
  then have
    "f \<lparr>map fst zs := map snd zs\<rparr> x = f x" by (induction zs) simp_all
  ultimately show ?thesis using fun_updr_map_zip by metis
qed

lemma fun_updr_others:
  assumes
    "set zs \<inter> set xs = {}"
  shows
    "map (f \<lparr>xs := ys\<rparr>) zs = map f zs"
  using assms by (meson disjoint_iff_not_equal fun_updr_other map_ext)

lemma fun_updr_same: "distinct xs \<Longrightarrow> length xs \<le> length ys \<Longrightarrow> map (f \<lparr>xs := ys\<rparr>) xs = take (length xs) ys"
proof -
  obtain zs where
    "zs = zip xs ys" by blast
  moreover assume
    L: "length xs \<le> length ys"
  ultimately have
    x: "map fst zs = xs" and
    "map snd zs = take (length xs) ys" by (simp_all add: map_fst_zip_take map_snd_zip_take min_def)
  moreover assume
    "distinct xs"
  with x have
    "distinct (map fst zs)" by simp
  then have
    "map (f \<lparr>map fst zs := map snd zs\<rparr>) (map fst zs) = map snd zs"
  proof (induction zs)
    case (Cons z zs)
    from Cons.prems(1) have
      "fst z \<notin> set (map fst zs)" by simp
    moreover have
      "map (f \<lparr>map fst (z # zs) := map snd (z # zs)\<rparr>) (map fst (z # zs)) =
      map (f \<lparr>map fst zs := map snd zs\<rparr> (fst z := snd z)) (map fst (z # zs))" by simp
    ultimately have
      "\<dots> = snd z # (map (f \<lparr> map fst zs := map snd zs \<rparr>) (map fst zs))"
      unfolding fun_updr_def
      by (metis (no_types, lifting) fun_upd_same list.simps(9) map_fun_upd)
    with Cons.IH Cons.prems show ?case by simp
  qed simp
  moreover note L
  ultimately show ?thesis using fun_updr_le by metis
qed

end