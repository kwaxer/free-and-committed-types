(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Lemma 5 (Heap Update Properties)\<close>

theory Update imports Evaluation Reachability begin

lemma (in heap) lemma_5\<^sub>1:
  assumes
    E: "\<lfloor>x \<^bsub>\<bullet>\<^esub> f\<rfloor>\<^bsub>h,\<sigma>\<^esub> = Inl v" and
    W: "wf_heap_dom h" and
    R: "reaches\<^sub>v (\<chi>\<^sub>v h) v v'"
  shows
    "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) v'"
proof (cases v)
  case (non_null \<iota>\<^sub>v)
  from E have
    C: "(case the (\<sigma> x) of
         non_null \<iota> \<Rightarrow>
            if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))
            then Inl (the (\<chi>\<^sub>v h (\<iota>, f)))
            else Inr derefExc |
         _ \<Rightarrow> Inr derefExc) = Inl v" by simp
  then obtain \<iota> where
    N: "the (\<sigma> x) = non_null \<iota>" by (cases "the (\<sigma> x)") simp_all
  with C have
    I: "(if \<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))
        then Inl (the (\<chi>\<^sub>v h (\<iota>, f)))
        else Inr derefExc) = Inl v" by simp
  then have
    T: "\<iota> \<in> dom (\<chi>\<^sub>c h) \<and> f \<in> set (fields (the (cls h \<iota>)))" by (meson Inr_Inl_False)
  with W have
    "(\<iota>, f) \<in> dom (\<chi>\<^sub>v h)" using access_dom by fastforce
  with I T have
    "\<chi>\<^sub>v h (\<iota>, f) = Some v" by auto
  then have
    "\<exists>f. \<chi>\<^sub>v h (\<iota>, f) = Some v" by blast
  with non_null have
    "the (\<sigma> x) = value.non_null \<iota> \<Longrightarrow>
        case the (\<sigma> x) of
          non_null \<iota>\<^sub>1 \<Rightarrow> case v of
            non_null \<iota>\<^sub>2 \<Rightarrow> reaches (\<chi>\<^sub>v h) \<iota>\<^sub>1 \<iota>\<^sub>2 |
            null \<Rightarrow> False |
          null \<Rightarrow> False" unfolding reaches\<^sub>1_def reaches_def reaches_def
      by (simp add: r_into_rtranclp)
    with N have "reaches\<^sub>v (\<chi>\<^sub>v h) (the (\<sigma> x)) v " unfolding reaches\<^sub>v_def by simp
    with R show ?thesis using reaches\<^sub>v_trans by blast
next
  case null
  with R show ?thesis using reaches\<^sub>v_left_null by blast
qed

lemma (in heap) lemma_5\<^sub>2:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> \<iota>' \<iota>'':: "'a \<iota>"
  assumes
    S: "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)" and
    I: "reaches h\<^sub>v \<iota>' \<iota>" and
    R: "reaches h\<^sub>v' \<iota>' \<iota>''"
  shows
    "reaches\<^sub>v h\<^sub>v v (non_null \<iota>'') \<or> reaches h\<^sub>v \<iota>' \<iota>''"
proof -
  from S I have
    R': "reaches h\<^sub>v \<iota>' \<iota>" by simp 
  from R S have
    R: "(reaches\<^sub>1 (h\<^sub>v ((\<iota>, f) := Some v)))\<^sup>*\<^sup>* \<iota>' \<iota>''" by simp
  then obtain ps :: "('a \<iota> \<times> f) list" where
    L: "0 < length ps" and
    X: "fst (ps ! 0) = \<iota>'" and
    Y: "fst (ps ! (length ps - 1)) = \<iota>''" and
    D: "distinct ps" and
    P: "\<forall> i < length ps - 1. (h\<^sub>v ((\<iota>, f) := Some v)) (ps ! i) = Some (non_null (fst (ps ! Suc i)))"
    using reaches\<^sub>_to_trace by force
  then show ?thesis
  proof (cases "(\<iota>, f) \<in> set ps")
    case False
    then have
      "\<forall> i < length ps. ps ! i \<noteq> (\<iota>, f)" using nth_mem by fastforce
    with P L have
      "\<forall> i < length ps - 1. h\<^sub>v (ps ! i) = Some (non_null (fst (ps ! Suc i)))" by simp
    with L X Y have "reaches h\<^sub>v \<iota>' \<iota>''" using reaches\<^sub>_trace by blast
    then show ?thesis by simp
  next
    case True
    then obtain j where
      JL: "j < length ps" and
      J: "ps ! j = (\<iota>, f)" using in_set_conv_nth by metis
    then obtain ps' where
      PS': "ps' = drop j ps" by simp
    with D have
      D': "distinct ps'" by simp
    from PS' JL have
      JL': "length ps = length ps' + j" by (simp add: less_imp_le_nat)
    from PS' have
      A: "\<forall> i < length ps'. ps' ! i = ps ! (i + j)" by (simp add: add.commute)
    then have
      A1: "\<And> i. i < length ps' \<Longrightarrow> ps' ! i = ps ! (i + j)" by simp
    from J JL' PS' have
      I: "fst (ps' ! 0) = \<iota>" by fastforce 
    from JL JL' have
      PL: "length ps' - 1 + j = length ps - 1" by linarith
    with Y PS' have
      X': "fst (ps' ! (length ps' - 1)) = \<iota>''" by (simp add: add.commute) 
    moreover have "reaches\<^sub>v h\<^sub>v v (non_null \<iota>'') \<or> reaches h\<^sub>v \<iota>' \<iota>''" if "(\<iota>, f) \<in> set ps" using that
    proof (cases v)
      case null
      with R' A J JL P PL PS' X' have "reaches h\<^sub>v  \<iota>' \<iota>''" unfolding reaches_def reaches\<^sub>1_def
        using value.distinct(1)
        by (metis (no_types, lifting) add.left_neutral fst_conv fun_upd_same length_drop less_add_same_cancel2 not_gr_zero option.inject  zero_less_diff)
      then show ?thesis by simp
    next
      case (non_null \<iota>''')
      with D' J JL PS' have
        I:"(\<iota>, f) \<notin> set (drop 1 ps')" using that
        by (metis Cons_nth_drop_Suc distinct.simps(2) drop_drop plus_1_eq_Suc)
      with A D' J have
        N: "\<And> i. 0 < i \<Longrightarrow> i < length ps' - 1 \<Longrightarrow> ps' ! i \<noteq> (\<iota>, f)"
        by (metis add.left_neutral add_lessD1 distinct_conv_nth less_diff_conv neq0_conv)
      from I A1 have
        DR: "\<And> i. i < length ps' - 1 \<Longrightarrow> (drop 1 ps') ! i = ps' ! Suc i" by simp
      from A1 JL' P have
        B: "\<forall> i < length ps' - 1. (h\<^sub>v((\<iota>, f) \<mapsto> v)) (ps' ! i) = Some (value.non_null (fst (ps' ! Suc i)))"
        using less_diff_conv by simp
      with N P have
        "\<And> i. 0 < i \<Longrightarrow> i < length ps' - 1 \<Longrightarrow> h\<^sub>v (ps' ! i) = Some (non_null (fst (ps' ! Suc i)))"
        by auto
      with DR have
        N: "\<forall> i < length (drop 1 ps') - 1. h\<^sub>v ((drop 1 ps') ! i) = Some (non_null (fst ((drop 1 ps') ! Suc i)))"
        by auto
      moreover from non_null have
        F: "(h\<^sub>v ((\<iota>, f) := Some v)) (\<iota>, f) = Some (non_null \<iota>''')" by simp
      from A1 J JL JL' have
        Z: "ps' ! 0 = (\<iota>, f)" by simp
      show ?thesis
      proof (cases "size ps' > 1")
        case True
        with B have
          "(h\<^sub>v ((\<iota>, f) := Some v)) (ps' ! 0) = Some (non_null (fst (ps' ! Suc 0)))" by simp
        with F Z True have
          "fst ((drop 1 ps') ! 0) = \<iota>'''" by simp
        moreover from X' DR True have
          "fst ((drop 1 ps') ! (size (drop 1 ps') - 1)) = \<iota>''"
          by (metis Suc_diff_1 length_drop lessI zero_less_diff)
        moreover from True have "0 < length (drop 1 ps')" by simp
        moreover note N
        ultimately have "reaches h\<^sub>v \<iota>''' \<iota>''" using reaches\<^sub>_trace by blast
        with non_null show ?thesis using reaches\<^sub>v_reaches by simp
      next
        case False
        with I X' Z assms(2) show ?thesis by simp
      qed
    qed
    ultimately show ?thesis using True by simp
  qed
qed

lemma (in heap) lemma_5\<^sub>2\<^sub>v:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> :: "'a \<iota>" and v v' v'':: "'a v"
  assumes
    "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)"
    "reaches\<^sub>v h\<^sub>v v' (non_null \<iota>)"
    "reaches\<^sub>v h\<^sub>v' v' v''"
  shows
    "reaches\<^sub>v h\<^sub>v v v''\<or> reaches\<^sub>v h\<^sub>v v' v''"
  using assms lemma_5\<^sub>2 reaches\<^sub>v_reaches by simp

lemma (in heap) lemma_5\<^sub>3:
  fixes
    h\<^sub>v h\<^sub>v':: "'a h\<^sub>v" and \<iota> \<iota>':: "'a \<iota>"
  assumes
    S: "h\<^sub>v' = h\<^sub>v ((\<iota>, f) := Some v)" and
    I: "\<not> reaches h\<^sub>v \<iota>' \<iota>"
  shows
    "reaches h\<^sub>v' \<iota>' = reaches h\<^sub>v \<iota>'"
by rule (insert I S reaches_upd_other, simp)

lemma (in heap) lemma_5\<^sub>4:
  fixes
    h h':: "('a, 'C) h" and \<iota>':: "'a \<iota>"
  assumes
    "unbounded_heap h"
    "(h', \<iota>') = alloc h C" and
    "wf_heap_dom_weak h" and
    "wf_heap_image h"
  shows
    "\<forall> \<iota>. \<iota> \<in> dom (\<chi>\<^sub>c h) \<longrightarrow> \<not> reaches (\<chi>\<^sub>v h') \<iota> \<iota>' \<and> \<not> reaches (\<chi>\<^sub>v h') \<iota>' \<iota>"
proof (rule allI, rule impI, rule conjI)
  fix \<iota>:: "'a \<iota>"
  from assms(1,2,3) have
    "\<forall> f. (\<iota>', f) \<in> dom (\<chi>\<^sub>v h') \<longrightarrow> \<chi>\<^sub>v h' (\<iota>', f) = Some null" using alloc_null by simp
  then have
    "\<forall> \<iota>. \<not> reaches\<^sub>1 (\<chi>\<^sub>v h') \<iota>' \<iota>" using reaches\<^sub>1_def by auto
  moreover assume
    \<iota>D: "\<iota> \<in> dom (\<chi>\<^sub>c h)"
  moreover note assms(1,2)
  ultimately show "\<not> reaches (\<chi>\<^sub>v h') \<iota>' \<iota>" using alloc converse_reachesE by metis
  from assms \<iota>D show "\<not> reaches (\<chi>\<^sub>v h') \<iota> \<iota>'" using alloc reaches_alloc_to_new by metis
qed

end