(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>A list of all theories\<close>

theory Free_and_Committed_Types imports
	Common
	Name
	Type
	Lookup
	Expectation
	Expression
	Statement
	Typing
	WellFormedness
	Heap
	Evaluation
	Semantics
	Initialization
	Initialization_Set
	Reachability
	Reachable_Set
	Configuration
	Safety
	Generation
	Instantiation
	Assignment
	Update
	Update_Set
begin

end