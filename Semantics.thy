(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 10 (Operational Semantics)\<close>
                                     
theory Semantics imports Evaluation Heap Lookup Statement begin

subsubsection \<open>Exception States\<close>

datatype "\<epsilon>" = ok | derefExc | castExc

subsubsection \<open>Runtime Type Assignment\<close>

inductive (in classes) runtime_type_assignment:: "('a, 'C) h \<Rightarrow> 'a value \<Rightarrow> 'C t \<Rightarrow> bool" ("_ \<turnstile> _ : _")
  where
    RNull: "h \<turnstile> null : (C, ?)" |
    RAddr: "\<lbrakk>\<iota> \<in> dom (\<chi>\<^sub>c h); the (cls h \<iota>) \<sqsubseteq> C\<rbrakk> \<Longrightarrow> h \<turnstile> (non_null \<iota>) : (C, n)"

declare (in classes) runtime_type_assignment.intros[intro!]

lemmas (in classes) runtime_type_assignment_induct = runtime_type_assignment.induct[split_format(complete)]
inductive_cases (in classes) RNullE[elim!]: "h \<turnstile> null : (C, n)"
inductive_cases (in classes) RAddrE[elim!]: "h \<turnstile> (non_null \<iota>) : (C, n)"

lemma (in classes) runtime_type_assignment_predecessor:
  fixes
    h\<^sub>1 h\<^sub>2 :: "('a, 'C) h"
  assumes
    "h\<^sub>1 \<le> h\<^sub>2"
    "h\<^sub>1 \<turnstile> v : T"
  shows
    "h\<^sub>2 \<turnstile> v : T"
proof (cases v)
  case (non_null \<iota>)
  with assms(2) obtain C n where
    "T = (C, n)" and
    "\<iota> \<in> dom (\<chi>\<^sub>c h\<^sub>1)" and
    "the (cls h\<^sub>1 \<iota>) \<sqsubseteq> C" using RAddrE runtime_type_assignment.cases value.distinct(1) value.sel
    by metis
  with non_null assms(1) show ?thesis using RAddr predecessor_def by (metis in_mono)
next
  case null
  with assms(2) obtain C where
    "T = (C, ?)" using RNullE simple_type by metis
  then show ?thesis using null by blast
qed

lemma (in classes) runtime_type_assignment_leq:
  assumes
    "T\<^sub>1 \<sqsubseteq> T\<^sub>2"
    "h \<turnstile> v : T\<^sub>1"
  shows
    "h \<turnstile> v : T\<^sub>2"
proof -
  obtain C\<^sub>1 n\<^sub>1 C\<^sub>2 n\<^sub>2 where
    T1: "T\<^sub>1 = (C\<^sub>1, n\<^sub>1)" and
    T2: "T\<^sub>2 = (C\<^sub>2, n\<^sub>2)" by fastforce
  with assms(1) have
    C: "C\<^sub>1 \<sqsubseteq> C\<^sub>2" and
    n: "n\<^sub>1 \<le> n\<^sub>2" using simple_base_subtyping simple_nullity_subtyping
    unfolding simple_base_def simple_nullity_def by fastforce+
  from assms(2) T1 have
    "h \<turnstile> v : (C\<^sub>1, n\<^sub>1)" by simp
  with C n have
    "h \<turnstile> v : (C\<^sub>2, n\<^sub>2)"
  proof (cases v)
    case (non_null x)
    with T1 assms(2) have
      "the (cls h x) \<sqsubseteq> C\<^sub>1" by auto
    moreover with C have
      "the (cls h x) \<sqsubseteq> C\<^sub>2" using leq_trans by blast
    moreover from T1 assms(2) non_null have "x \<in> dom (\<chi>\<^sub>c h)" by auto
    ultimately show ?thesis using non_null by blast
  next
    case null
    with T1 assms(2) n show ?thesis by blast
  qed
  with T2 show ?thesis by simp
qed

definition (in classes) runtime_type_assignments:: "('a, 'C) h \<Rightarrow> 'a value list \<Rightarrow> 'C t list \<Rightarrow> bool" ("_ \<turnstile> _ : _")
  where
    "h \<turnstile> v\<^sub>i : T\<^sub>i \<equiv> foldr (\<lambda> p b. h \<turnstile> fst p : snd p \<and> b) (zip v\<^sub>i T\<^sub>i) (length v\<^sub>i = length T\<^sub>i)"

lemma (in classes) runtime_type_assignments_map:
  "h \<turnstile> (map fst P\<^sub>i) : (map snd P\<^sub>i) \<longleftrightarrow> foldr (\<lambda> p b. h \<turnstile> fst p : snd p \<and> b) P\<^sub>i True"
  unfolding runtime_type_assignments_def by (simp add: zip_map_fst_snd) 

lemma (in classes) runtime_type_assignments_length: "h \<turnstile> v\<^sub>i : T\<^sub>i \<Longrightarrow> length v\<^sub>i = length T\<^sub>i"
  unfolding runtime_type_assignments_def using foldr_conj_map by fastforce

lemma (in classes) runtime_type_assignments_head: "h \<turnstile> v # v\<^sub>i : T # T\<^sub>i \<Longrightarrow> h \<turnstile> v : T"
  unfolding runtime_type_assignments_def by simp

lemma (in classes) runtime_type_assignments_tail: "h \<turnstile> v # v\<^sub>i : T # T\<^sub>i \<Longrightarrow> h \<turnstile> v\<^sub>i : T\<^sub>i"
  unfolding runtime_type_assignments_def by simp

lemma (in classes) runtime_type_assignments_cons:
  fixes
    h :: "('a, 'C) h" and v :: "'a value" and v\<^sub>i :: "'a value list" and T :: "'C t" and T\<^sub>i :: "'C t list"
  assumes
    "h \<turnstile> v : T" and
    "h \<turnstile> v\<^sub>i : T\<^sub>i"
  shows
    "h \<turnstile> v # v\<^sub>i : T # T\<^sub>i"
  using assms unfolding runtime_type_assignments_def using runtime_type_assignments_length by simp

lemma (in classes) runtime_type_assignments_nth:
  fixes
    h :: "('a, 'C) h" and v\<^sub>i :: "'a value list" and T\<^sub>i :: "'C t list"
  assumes
    "h \<turnstile> v\<^sub>i : T\<^sub>i"
    "i < length v\<^sub>i"
  shows
    "h \<turnstile> nth v\<^sub>i i : nth T\<^sub>i i"
proof -
  obtain ps where
    P: "ps = zip v\<^sub>i T\<^sub>i" by simp
  moreover from assms(1) have
    L: "length v\<^sub>i = length T\<^sub>i" by (rule runtime_type_assignments_length)
  moreover note assms(1)
  ultimately have
    "h \<turnstile> map fst ps: map snd ps" by simp
  then have
    "\<forall> p \<in> set ps. h \<turnstile> fst p: snd p" using runtime_type_assignments_map foldr_conj_map by fastforce
  moreover from L P assms(2) have
    "i < length ps" by simp
  ultimately have
    "h \<turnstile> fst (nth ps i) : snd (nth ps i)" by simp
  with L P assms(2) show ?thesis by simp
qed

subsubsection \<open>Operational Semantics\<close>

locale "runtime" = lookup + heap +
  constrains
    is_subclass :: "'C:: {equal, partial_order} \<Rightarrow> 'C \<Rightarrow> bool" and
    "fields" :: "'C \<Rightarrow> f list" and
    address :: "'a address" and
    fType :: "'C \<times> f \<rightharpoonup> 'C t" and
    mSig :: "'C \<times> m \<rightharpoonup> ('C, \<gamma>) method_signature" and
    cSig :: "'C \<rightharpoonup> ('C, \<gamma>) constructor_signature" and
    mBody :: "'C \<times> m \<rightharpoonup> 'C s" and
    cBody :: "'C \<rightharpoonup> 'C s"

inductive (in runtime) big_step_report:: "\<epsilon> \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> 'C s \<Rightarrow> ('a, 'C) h \<Rightarrow> 'a \<sigma> \<Rightarrow> \<epsilon> \<Rightarrow> bool" ("_, _, _, _ \<leadsto> _, _, _")
  where
    varAss: "\<lfloor>e\<rfloor>\<^bsub>h, \<sigma>\<^esub> = Inl v \<Longrightarrow> ok, h, \<sigma>, x \<Colon>= e \<leadsto> h, \<sigma> (x := Some v), ok" |
    varAssBad: "\<lfloor>e\<rfloor>\<^bsub>h, \<sigma>\<^esub> = Inr derefExc.derefExc \<Longrightarrow> ok, h, \<sigma>, x \<Colon>= e \<leadsto> h, \<sigma>, derefExc" |
    fldAss: "\<sigma> x = Some (non_null \<iota>) \<Longrightarrow> ok, h, \<sigma>, x \<^bsub>\<bullet>\<^esub> f \<Colon>= y \<leadsto> ((\<chi>\<^sub>v h) ((\<iota>, f) := \<sigma> y), \<chi>\<^sub>c h), \<sigma>, ok" |
    fldAssBad: "\<sigma> x = Some null \<Longrightarrow> ok, h, \<sigma>, x \<^bsub>\<bullet>\<^esub> f \<Colon>= y \<leadsto> h, \<sigma>, derefExc" |
    call: "\<lbrakk>
        \<sigma> y = Some (non_null \<iota>);
        cls h \<iota> = Some C;
        mSig (C, m) = Some (\<gamma>, X\<^sub>i, S, Y\<^sub>j);
        x\<^sub>i = \<eta> X\<^sub>i;
        y\<^sub>j = \<eta> Y\<^sub>j;
        \<sigma>\<^sub>1 = (\<sigma>\<^sub>\<emptyset> (this := Some (non_null \<iota>)) \<lparr>x\<^sub>i := map \<sigma> z\<^sub>i\<rparr> (res := Some null) \<lparr>y\<^sub>j := map (\<lambda> x. Some null) y\<^sub>j\<rparr>);
        mBody (C, m) = Some s;
        ok, h, \<sigma>\<^sub>1, s \<leadsto> h', \<sigma>', \<epsilon>
      \<rbrakk> \<Longrightarrow> ok, h, \<sigma>, x := y\<^bsub>\<bullet>\<^esub>m (z\<^sub>i) \<leadsto> h', \<sigma> (x := \<sigma>' res), \<epsilon>" |
    callBad: "\<sigma> y = Some null \<Longrightarrow> ok, h, \<sigma>, x := y\<^bsub>\<bullet>\<^esub>m (z\<^sub>i) \<leadsto> h, \<sigma>, derefExc" |
    create: "\<lbrakk>
        cSig C = Some (X\<^sub>i, Y\<^sub>j);
        x\<^sub>i = \<eta> X\<^sub>i;
        y\<^sub>j = \<eta> Y\<^sub>j;
        alloc h C = (h\<^sub>1, \<iota>\<^sub>1);
        \<sigma>\<^sub>1 = (\<sigma>\<^sub>\<emptyset> (this := Some (non_null \<iota>\<^sub>1)) \<lparr>x\<^sub>i := map \<sigma> z\<^sub>i\<rparr> \<lparr>y\<^sub>j := map (\<lambda> x. Some null) y\<^sub>j\<rparr>);
        cBody C = Some s;
        ok, h\<^sub>1, \<sigma>\<^sub>1, s \<leadsto> h', \<sigma>\<^sub>2, \<epsilon>
      \<rbrakk> \<Longrightarrow> ok, h, \<sigma>, x \<Colon>= new C (z\<^sub>i) \<leadsto> h', \<sigma> (x := Some (non_null \<iota>\<^sub>1)), \<epsilon>" |
    cast: "\<lbrakk>\<sigma> y = Some v; h \<turnstile> v : t\<rbrakk> \<Longrightarrow> ok, h, \<sigma>, (x \<Colon>= \<lparr>t\<rparr> y) \<leadsto> h, \<sigma> (x := \<sigma> y), ok" |
    castBad: "\<lbrakk>\<sigma> y = Some v; \<not> (h \<turnstile> v : t)\<rbrakk> \<Longrightarrow> ok, h, \<sigma>, (x \<Colon>= \<lparr>t\<rparr> e) \<leadsto> h, \<sigma>, castExc" |
    seq: "\<lbrakk>ok, h, \<sigma>, s\<^sub>1 \<leadsto> h\<^sub>1, \<sigma>\<^sub>1, ok; ok, h\<^sub>1, \<sigma>\<^sub>1, s\<^sub>2 \<leadsto> h\<^sub>2, \<sigma>\<^sub>2, \<epsilon>\<rbrakk> \<Longrightarrow> ok, h, \<sigma>, s\<^sub>1 ;; s\<^sub>2 \<leadsto> h\<^sub>2, \<sigma>\<^sub>2, \<epsilon>" |
    seqBad: "\<lbrakk>ok, h, \<sigma>, s\<^sub>1 \<leadsto> h\<^sub>1, \<sigma>\<^sub>1, \<epsilon>; \<epsilon> \<noteq> ok\<rbrakk> \<Longrightarrow> ok, h, \<sigma>, s\<^sub>1 ;; s\<^sub>2 \<leadsto> h\<^sub>1, \<sigma>\<^sub>1, \<epsilon>"

lemmas (in runtime) big_step_report_induct = big_step_report.inducts[split_format(complete)]
inductive_cases (in runtime) varAssE[elim!]: "\<epsilon>, h, \<sigma>, x \<Colon>= e \<leadsto> h', \<sigma>', \<epsilon>'"
inductive_cases (in runtime) fldAssE[elim!]: "\<epsilon>, h, \<sigma>, x \<^bsub>\<bullet>\<^esub> f \<Colon>= y \<leadsto> h', \<sigma>', \<epsilon>'"
inductive_cases (in runtime) callE[elim!]: "\<epsilon>, h, \<sigma>, x := y\<^bsub>\<bullet>\<^esub>m (z\<^sub>i) \<leadsto> h', \<sigma>', \<epsilon>'"
inductive_cases (in runtime) createE[elim!]: "\<epsilon>, h, \<sigma>, x \<Colon>= new C (z\<^sub>i) \<leadsto> h', \<sigma>', \<epsilon>'"
inductive_cases (in runtime) castE[elim!]: "\<epsilon>, h, \<sigma>, (x \<Colon>= \<lparr>t\<rparr> e) \<leadsto> h', \<sigma>', \<epsilon>'"
inductive_cases (in runtime) seqE[elim!]: "\<epsilon>, h, \<sigma>, s\<^sub>1 ;; s\<^sub>2 \<leadsto> h', \<sigma>', \<epsilon>'"

end