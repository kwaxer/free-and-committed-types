(* Copyright (C) 2020 Alexander Kogtenkov
   This work is licensed under a "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International" License. *)

subsection \<open>Definition 11 (Reachability)\<close>

theory Reachability imports Heap begin

definition (in heap) reaches\<^sub>1 :: "'a h\<^sub>v \<Rightarrow> 'a \<iota> \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
    "reaches\<^sub>1 h\<^sub>v \<equiv> \<lambda> \<iota>\<^sub>1 \<iota>\<^sub>2. \<exists> f. h\<^sub>v (\<iota>\<^sub>1, f) = Some (non_null \<iota>\<^sub>2)"

lemma (in heap) reaches\<^sub>1_upd_other:
  "\<iota>'' \<noteq> \<iota> \<Longrightarrow> reaches\<^sub>1 (h\<^sub>v((\<iota>'', f) := v)) \<iota> \<iota>' \<longleftrightarrow> reaches\<^sub>1 h\<^sub>v \<iota> \<iota>'"
  unfolding reaches\<^sub>1_def by simp

lemma (in heap) reaches\<^sub>1_upd_same:
  "reaches\<^sub>1 (h\<^sub>v((\<iota>, f) := Some (non_null \<iota>'))) \<iota> \<iota>'"
  unfolding reaches\<^sub>1_def by auto

lemma (in heap) reaches\<^sub>1_source_dom\<^sub>v: "reaches\<^sub>1 h\<^sub>v \<iota> \<iota>' \<Longrightarrow> \<exists> f. (\<iota>, f) \<in> dom h\<^sub>v"
  using reaches\<^sub>1_def by auto

lemma (in heap) reaches\<^sub>1_source_dom:
  assumes
    "reaches\<^sub>1 (\<chi>\<^sub>v h) \<iota> \<iota>'"
    "wf_heap_dom h"
  shows
    "\<iota> \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>1_def wf_heap_dom_def by blast

lemma (in heap) reaches\<^sub>1_target_dom:
  assumes
    "reaches\<^sub>1 (\<chi>\<^sub>v h) \<iota> \<iota>'"
    "wf_heap_image h"
  shows
    "\<iota>' \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>1_def wf_heap_image_def by fastforce

lemma (in heap) reaches\<^sub>1_alloc_from_new:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
  shows
    "\<not> reaches\<^sub>1 (\<chi>\<^sub>v h') \<iota> \<iota>'"
  using assms unfolding reaches\<^sub>1_def using alloc_null value.discI
  by (metis domI option.inject)

lemma (in heap) reaches\<^sub>1_alloc_from_old:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
    "reaches\<^sub>1 (\<chi>\<^sub>v h') s t"
  shows
    "s \<noteq> \<iota>"
  using assms reaches\<^sub>1_alloc_from_new by blast

lemma (in heap) reaches\<^sub>1_alloc_to_new:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
    "wf_heap_image h"
  shows
    "\<not> reaches\<^sub>1 (\<chi>\<^sub>v h') \<iota>' \<iota>"
  using assms unfolding reaches\<^sub>1_def using alloc alloc_value
  using reaches\<^sub>1_alloc_from_new reaches\<^sub>1_target_dom reaches\<^sub>1_upd_same
  by (metis fun_upd_triv)

lemma (in heap) reaches\<^sub>1_alloc_to_old:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
    "wf_heap_image h"
    "reaches\<^sub>1 (\<chi>\<^sub>v h') s t"
  shows
    "t \<noteq> \<iota>"
  using assms reaches\<^sub>1_alloc_to_new by blast

definition (in heap) reaches :: "'a h\<^sub>v \<Rightarrow> 'a \<iota> \<Rightarrow> 'a \<iota> \<Rightarrow> bool"
  where
  [iff]: "reaches h\<^sub>v \<equiv> (reaches\<^sub>1 h\<^sub>v)\<^sup>*\<^sup>*"

lemma (in heap) converse_reachesE:
  assumes
    "reaches h\<^sub>v x z"
    "x = z \<Longrightarrow> P"
    "\<And>y. reaches\<^sub>1 h\<^sub>v x y \<Longrightarrow> reaches h\<^sub>v y z \<Longrightarrow> P"
  shows P
  using assms unfolding reaches_def by (metis converse_rtranclpE)

lemma (in heap) reaches_source_dom\<^sub>v:
  assumes
    "reaches h\<^sub>v s t"
    "s \<noteq> t"
  obtains f
  where
    "(s, f) \<in> dom h\<^sub>v"
  using assms reaches\<^sub>1_source_dom\<^sub>v converse_reachesE by meson

lemma (in heap) reaches_source_dom:
  assumes
    "reaches (\<chi>\<^sub>v h) s t"
    "s \<noteq> t"
    "wf_heap_dom h"
  shows
    "s \<in> dom (\<chi>\<^sub>c h)"
  using assms converse_reachesE reaches\<^sub>1_source_dom by blast

lemma (in heap) reaches_target_dom:
  assumes
    "reaches (\<chi>\<^sub>v h) s t"
    "s \<in> dom (\<chi>\<^sub>c h)"
    "wf_heap_image h"
  shows
    "t \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches_def using reaches\<^sub>1_target_dom by (metis rtranclp.cases)

lemma (in heap) reaches_alloc_from_new:
  assumes
    "reaches (\<chi>\<^sub>v h') \<iota> \<iota>'"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
  shows
    "\<iota> = \<iota>'"
  using assms unfolding reaches_def using reaches\<^sub>1_alloc_from_new converse_rtranclpE by metis

lemma (in heap) reaches_alloc_to_new:
  assumes
    "reaches (\<chi>\<^sub>v h') \<iota> \<iota>'"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>')"
    "wf_heap_dom_weak h"
    "wf_heap_image h"
  shows
    "\<iota> = \<iota>'"
  using assms unfolding reaches_def using reaches\<^sub>1_alloc_to_new by (metis rtranclp.cases)

lemma (in heap) reaches\<^sub>_trace:
  "reaches h\<^sub>v x y \<longleftrightarrow>
    (\<exists> ps.
      0 < length ps \<and>
      fst (ps ! 0) = x \<and>
      fst (ps ! (length ps - 1)) = y \<and>
      (\<forall> i < length ps - 1. h\<^sub>v (ps ! i) = Some (non_null (fst (ps ! Suc i)))))"
  unfolding reaches_def
proof (rule iffI)
  assume
    "(reaches\<^sub>1 h\<^sub>v)\<^sup>*\<^sup>* x y"
  then obtain xs' :: "'a \<iota> list" where
    "0 < length xs'" and
    X: "xs' ! 0 = x" and
    Y: "xs' ! (length xs' - 1) = y" and
    S': "(\<forall> i < length xs' - 1. (reaches\<^sub>1 h\<^sub>v) (xs' ! i) (xs' ! Suc i))" using rtranclp_to_trace by force
  then obtain xs where
    X': "xs' = xs @ [y]" by (metis append_butlast_last_id last_conv_nth length_greater_0_conv)
  with S' have
    "\<forall> i < length xs. (reaches\<^sub>1 h\<^sub>v) ((xs @ [y]) ! i) ((xs @ [y]) ! Suc i)" by simp
  then have
    "\<forall> i < length (xs @ [y]) - 1. \<exists> f. h\<^sub>v ((xs @ [y]) ! i, f) = Some (non_null ((xs @ [y]) ! Suc i))" unfolding reaches\<^sub>1_def by simp
  then have
    "\<exists> fs :: f list.
      length fs = length (xs @ [y]) \<and>
      (\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs ! i) = Some (non_null ((xs @ [y]) ! Suc i)))"
  proof (induction xs)
    case Nil
    then show ?case by (simp add: Ex_list_of_length)
  next
    case (Cons a xs)
    from Cons.prems have
      "\<forall> i < length (xs @ [y]) - 1. \<exists>f. h\<^sub>v ((xs @ [y]) ! i, f) = Some (value.non_null ((xs @ [y]) ! Suc i))"
      by fastforce
    with Cons.IH obtain fs' :: "f list" where
      L': "length fs' = length (xs @ [y])" and
      S: "\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs' ! i) = Some (value.non_null ((xs @ [y]) ! Suc i))" by metis
    have "0 < length (a # (xs @ [y])) - 1" by simp
    from Cons.prems obtain f where
      F: "h\<^sub>v ((a # xs @ [y]) ! 0, f) = Some (value.non_null ((a # xs @ [y]) ! Suc 0))" by fastforce
    then obtain fs where
      C: "fs = f # fs'" by simp
    with F S have
      "\<forall> i < length (a # xs @ [y]) - 1.
       h\<^sub>v ((a # xs @ [y]) ! i, fs ! i) =
        Some (value.non_null ((a # xs @ [y]) ! Suc i))" by (simp add: nth_Cons')
    with C L' show "\<exists>fs :: f list. length fs = length ((a # xs) @ [y]) \<and> (\<forall>i<length ((a # xs) @ [y]) - 1.
                   h\<^sub>v (((a # xs) @ [y]) ! i, fs ! i) =
                   Some (value.non_null (((a # xs) @ [y]) ! Suc i)))" by force
  qed
  then obtain fs :: "f list" where
    L: "length fs = length (xs @ [y])" and
    S: "\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs ! i) = Some (non_null ((xs @ [y]) ! Suc i))"
    by auto
  then obtain ps where
    P: "ps = zip (xs @ [y]) fs" by simp
  with L have "length ps = length (xs @ [y])" by simp
  then have "0 < length ps" by simp 
  moreover from X X' P L have "fst (ps ! 0) = x" by simp
  moreover from Y X' P L have "fst (ps ! (length ps - 1)) = y" by simp
  moreover from S X' P L have "\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i)))" by simp
  ultimately show
    "\<exists>ps. 0 < length ps \<and>
         fst (ps ! 0) = x \<and>
         fst (ps ! (length ps - 1)) = y \<and>
         (\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i))))" by blast
next
  assume
    "\<exists>ps. 0 < length ps \<and>
         fst (ps ! 0) = x \<and>
         fst (ps ! (length ps - 1)) = y \<and>
         (\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i))))"
  then obtain ps :: "('a \<iota> \<times> f) list" where
    L: "0 < length ps" and
    X: "fst (ps ! 0) = x" and
    Y: "fst (ps ! (length ps - 1)) = y" and
    S: "\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i)))" by auto
  obtain xs fs where
    M: "xs = map fst ps" and
    F: "fs = map snd ps" by simp
  from L M X Y have
    L': "0 < length xs" and
    X': "xs ! 0 = x" and
    Y': "xs ! (length xs - 1) = y" by simp_all
  from F M S have
    "\<forall> i < length xs - 1. h\<^sub>v (xs ! i, fs ! i) = Some (non_null (xs ! Suc i))" by simp
  then have
    "\<forall> i < length xs - 1. (reaches\<^sub>1 h\<^sub>v) (xs ! i) (xs ! Suc i)" unfolding reaches\<^sub>1_def by blast
  with L' X' Y' show "(reaches\<^sub>1 h\<^sub>v)\<^sup>*\<^sup>* x y" by (simp add: rtranclp_from_trace'')
qed

lemma (in heap) reaches_alloc:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom h"
    "wf_heap_image h"
  shows
    "reaches (\<chi>\<^sub>v h) s t = reaches (\<chi>\<^sub>v h') s t"
proof (cases "s = t")
  case True
  then show ?thesis unfolding reaches_def by simp
next
  case False
  assume
    st: "s \<noteq> t"
  then show ?thesis
  proof (cases "s = \<iota>")
    case True
    moreover
    from assms(1,2,3) have
      "reaches (\<chi>\<^sub>v h) \<iota> t \<longleftrightarrow> t = \<iota>" using alloc reaches_def reaches_source_dom
      by (metis rtranclp.rtrancl_refl) 
    moreover
    from assms(1,2,3) have
      "reaches (\<chi>\<^sub>v h') \<iota> t \<longleftrightarrow> t = \<iota>" using reaches_alloc_from_new wf_heap_dom_weak by fastforce
    ultimately show ?thesis by simp
  next
    case False
    assume
      s: "s \<noteq> \<iota>"
    then show ?thesis
    proof (cases "t = \<iota>")
      case True
      moreover
      from assms(1,2,3,4) False have
        "reaches (\<chi>\<^sub>v h) s \<iota> \<longleftrightarrow> s = \<iota>" using alloc reaches_source_dom reaches_target_dom by blast 
      moreover
      from assms(1,2,3,4) have
        "(reaches\<^sub>1 (\<chi>\<^sub>v h'))\<^sup>*\<^sup>* s \<iota> \<longleftrightarrow> s = \<iota>" using reaches_alloc_to_new wf_heap_dom_weak by fastforce
      ultimately show ?thesis by simp
    next
      case False
      assume
        t: "t \<noteq> \<iota>"
      then show ?thesis
      proof (cases "reaches (\<chi>\<^sub>v h) s t")
        case True
        with assms(2,3,4) st have
          sD: "s \<in> dom (\<chi>\<^sub>c h)" and
          tD: "t \<in> dom (\<chi>\<^sub>c h)"
          using reaches_source_dom reaches_target_dom by blast+
        from True obtain ps where
          p1: "0 < length ps" and
          p2: "fst (nth ps 0) = s" and 
          p3: "fst (ps ! (length ps - 1)) = t" and
          p4: "\<forall> i < length ps - 1. (\<chi>\<^sub>v h) (nth ps i) = Some (non_null (fst (ps ! Suc i)))"
          using reaches\<^sub>_trace by auto
        from p4 have
          "\<forall> i < length ps - 1. nth ps i \<in> dom (\<chi>\<^sub>v h)" by blast
        with assms(3) have
          "\<forall> i < length ps - 1. fst (nth ps i) \<in> dom (\<chi>\<^sub>c h)" unfolding wf_heap_dom_def by auto
        with assms(1,2) have
          "\<forall> i < length ps - 1. fst (nth ps i) \<noteq> \<iota>" using alloc by blast
        with p4 assms(1,2) have
          "\<forall> i < length ps - 1. (\<chi>\<^sub>v h') (nth ps i) = Some (non_null (fst (ps ! Suc i)))"
          using alloc_value by (metis surjective_pairing)
        with p1 p2 p3 True show ?thesis using reaches\<^sub>_trace by blast
      next
        case False
        from assms have
          w1: "wf_heap_dom h'" and
          w2: "wf_heap_image h'"
          using alloc_dom alloc_image wf_heap_dom_weak by blast+
        assume
          rh: "\<not> reaches (\<chi>\<^sub>v h) s t"
        then show ?thesis
        proof (cases "reaches (\<chi>\<^sub>v h') s t")
          case True
          from w1 w2 st True have
            sD: "s \<in> dom (\<chi>\<^sub>c h')" and
            tD: "t \<in> dom (\<chi>\<^sub>c h')"
            using reaches_source_dom reaches_target_dom by blast+
          from True obtain ps where
            p1: "0 < length ps" and
            p2: "fst (nth ps 0) = s" and 
            p3: "fst (ps ! (length ps - 1)) = t" and
            p4: "\<forall> i < length ps - 1. (\<chi>\<^sub>v h') (nth ps i) = Some (non_null (fst (ps ! Suc i)))"
            using reaches\<^sub>_trace by auto
          from p4 have
            "\<forall> i < length ps - 1. nth ps i \<in> dom (\<chi>\<^sub>v h')" by blast
          with w1 have
            "\<forall> i < length ps - 1. fst (nth ps i) \<in> dom (\<chi>\<^sub>c h')" unfolding wf_heap_dom_def by auto
          have
            "\<forall> i < length ps - 1. fst (nth ps i) \<noteq> \<iota>"
          proof (rule allI, rule impI)
            fix i
            assume
              "i < length ps - 1"
            with p4 have
              "\<chi>\<^sub>v h' (ps ! i) = Some (value.non_null (fst (ps ! Suc i)))" by simp
            then obtain x where
              "reaches\<^sub>1 (\<chi>\<^sub>v h') (fst (ps ! i)) x" unfolding reaches\<^sub>1_def
              by (metis eq_fst_iff)
            with assms(1,2,3) show
              "fst (ps ! i) \<noteq> \<iota>" using reaches\<^sub>1_alloc_from_new wf_heap_dom_weak by blast
          qed
          with p4 assms(1,2) have
            "\<forall> i < length ps - 1. (\<chi>\<^sub>v h) (nth ps i) = Some (non_null (fst (ps ! Suc i)))"
            using alloc_value by (metis surjective_pairing)
          with p1 p2 p3 True show ?thesis using reaches\<^sub>_trace by blast
        next
          case False
          with rh show ?thesis by simp
        qed
      qed
    qed
  qed
qed

lemma (in heap) reaches\<^sub>_trace\<^sub>v:
  "reaches h\<^sub>v x y \<longleftrightarrow>
    (\<exists> ps.
      0 < length ps \<and>
      fst (nth ps 0) = (non_null x) \<and>
      fst (nth ps (length ps - 1)) = (non_null y) \<and>
      (\<forall> i < length ps. fst (nth ps i) \<noteq> null) \<and>
      (\<forall> i < length ps - 1. h\<^sub>v (ref (fst (nth ps i)), snd (nth ps i)) = Some (fst (nth ps (Suc i)))))"
proof (rule iffI)
  fix h\<^sub>v and x y :: "'a \<iota>"
  assume
    "reaches h\<^sub>v x y"
  then obtain ps :: "('a \<iota> \<times> f) list" where
    pL: "0 < length ps" and
    pX: "fst (ps ! 0) = x" and
    pY: "fst (ps ! (length ps - 1)) = y" and
    pS: "\<forall> i < length ps - 1. h\<^sub>v (ps ! i) = Some (non_null (fst (ps ! Suc i)))"
    using reaches\<^sub>_trace by auto
  moreover
  then obtain ps\<^sub>v where
    Z: "ps\<^sub>v = zip (map non_null ((map fst) ps)) (map snd ps)" by simp
  moreover
  then have
    [simp]: "length ps\<^sub>v = length ps" by simp
  ultimately have
    "0 < length ps\<^sub>v \<and>
     fst (ps\<^sub>v ! 0) = non_null x \<and>
     fst (ps\<^sub>v ! (length ps\<^sub>v - 1)) = non_null y \<and>
     (\<forall>i<length ps\<^sub>v. fst (ps\<^sub>v ! i) \<noteq> null) \<and>
     (\<forall>i<length ps\<^sub>v - 1. h\<^sub>v (ref (fst (ps\<^sub>v ! i)), snd (ps\<^sub>v ! i)) = Some (fst (ps\<^sub>v ! Suc i)))"
    by simp
  then show
    "\<exists>ps. 0 < length ps \<and>
       fst (ps ! 0) = non_null x \<and>
       fst (ps ! (length ps - 1)) = non_null y \<and>
       (\<forall>i<length ps. fst (ps ! i) \<noteq> null) \<and>
       (\<forall>i<length ps - 1. h\<^sub>v (ref (fst (ps ! i)), snd (ps ! i)) = Some (fst (ps ! Suc i)))" by blast
next
  assume
    "\<exists>ps. 0 < length ps \<and>
         fst (ps ! 0) = non_null x \<and>
         fst (ps ! (length ps - 1)) = non_null y \<and>
         (\<forall>i<length ps. fst (ps ! i) \<noteq> null) \<and>
         (\<forall>i<length ps - 1. h\<^sub>v (ref (fst (ps ! i)), snd (ps ! i)) = Some (fst (ps ! Suc i)))"
  then obtain ps\<^sub>v where
    "0 < length ps\<^sub>v \<and>
     fst (ps\<^sub>v ! 0) = non_null x \<and>
     fst (ps\<^sub>v ! (length ps\<^sub>v - 1)) = non_null y \<and>
     (\<forall>i<length ps\<^sub>v. fst (ps\<^sub>v ! i) \<noteq> null) \<and>
     (\<forall>i<length ps\<^sub>v - 1. h\<^sub>v (ref (fst (ps\<^sub>v ! i)), snd (ps\<^sub>v ! i)) = Some (fst (ps\<^sub>v ! Suc i)))" by blast
  moreover then obtain ps where
    "ps = zip (map ref (map fst ps\<^sub>v)) (map snd ps\<^sub>v)" by simp
  moreover
  then have
    [simp] : "length ps = length ps\<^sub>v" by simp
  ultimately have
    "0 < length ps \<and>
     fst (ps ! 0) = x \<and>
     fst (ps ! (length ps - 1)) = y \<and>
     (\<forall> i < length ps - 1. h\<^sub>v (ps ! i) = Some (non_null (fst (ps ! Suc i))))" by simp
  then show "reaches h\<^sub>v x y" using reaches\<^sub>_trace by blast
qed

lemma (in heap) reaches\<^sub>_to_trace:
  assumes
    "reaches h\<^sub>v x y"
  shows
    "\<exists> ps.
      0 < length ps \<and>
      fst (ps ! 0) = x \<and>
      fst (ps ! (length ps - 1)) = y \<and>
      distinct ps \<and>
      (\<forall> i < length ps - 1. h\<^sub>v (ps ! i) = Some (non_null (fst (ps ! Suc i))))"
  using assms unfolding reaches_def
proof -
  assume
    "(reaches\<^sub>1 h\<^sub>v)\<^sup>*\<^sup>* x y"
  then obtain xs' :: "'a \<iota> list" where
    "0 < length xs'" and
    X: "xs' ! 0 = x" and
    Y: "xs' ! (length xs' - 1) = y" and
    D: "distinct xs'" and
    S': "(\<forall> i < length xs' - 1. (reaches\<^sub>1 h\<^sub>v) (xs' ! i) (xs' ! Suc i))" using rtranclp_to_trace_distinct by force
  then obtain xs where
    X': "xs' = xs @ [y]" by (metis append_butlast_last_id last_conv_nth length_greater_0_conv)
  with S' have
    "\<forall> i < length xs. (reaches\<^sub>1 h\<^sub>v) ((xs @ [y]) ! i) ((xs @ [y]) ! Suc i)" by simp
  then have
    "\<forall> i < length (xs @ [y]) - 1. \<exists> f. h\<^sub>v ((xs @ [y]) ! i, f) = Some (non_null ((xs @ [y]) ! Suc i))" unfolding reaches\<^sub>1_def by simp
  then have
    "\<exists> fs :: f list.
      length fs = length (xs @ [y]) \<and>
      (\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs ! i) = Some (non_null ((xs @ [y]) ! Suc i)))"
  proof (induction xs)
    case Nil
    then show ?case by (simp add: Ex_list_of_length)
  next
    case (Cons a xs)
    from Cons.prems have
      "\<forall> i < length (xs @ [y]) - 1. \<exists>f. h\<^sub>v ((xs @ [y]) ! i, f) = Some (value.non_null ((xs @ [y]) ! Suc i))"
      by fastforce
    with Cons.IH obtain fs' :: "f list" where
      L': "length fs' = length (xs @ [y])" and
      S: "\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs' ! i) = Some (value.non_null ((xs @ [y]) ! Suc i))" by metis
    have "0 < length (a # (xs @ [y])) - 1" by simp
    from Cons.prems obtain f where
      F: "h\<^sub>v ((a # xs @ [y]) ! 0, f) = Some (value.non_null ((a # xs @ [y]) ! Suc 0))" by auto
    then obtain fs where
      C: "fs = f # fs'" by simp
    with F S have
      "\<forall> i < length (a # xs @ [y]) - 1.
       h\<^sub>v ((a # xs @ [y]) ! i, fs ! i) =
        Some (value.non_null ((a # xs @ [y]) ! Suc i))" by (simp add: nth_Cons')
    with C L' show "\<exists>fs :: f list. length fs = length ((a # xs) @ [y]) \<and> (\<forall>i<length ((a # xs) @ [y]) - 1.
                   h\<^sub>v (((a # xs) @ [y]) ! i, fs ! i) =
                   Some (value.non_null (((a # xs) @ [y]) ! Suc i)))" by force
  qed
  then obtain fs :: "f list" where
    L: "length fs = length (xs @ [y])" and
    S: "\<forall> i < length (xs @ [y]) - 1. h\<^sub>v ((xs @ [y]) ! i, fs ! i) = Some (non_null ((xs @ [y]) ! Suc i))"
    by auto
  then obtain ps where
    P: "ps = zip (xs @ [y]) fs" by simp
  with L have "length ps = length (xs @ [y])" by simp
  then have "0 < length ps" by simp 
  moreover from X X' P L have "fst (ps ! 0) = x" by simp
  moreover from Y X' P L have "fst (ps ! (length ps - 1)) = y" by simp
  moreover from D X' P have "distinct ps" by (simp add: distinct_zipI1)
  moreover from S X' P L have "\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i)))" by simp
  ultimately show
    "\<exists>ps. 0 < length ps \<and>
         fst (ps ! 0) = x \<and>
         fst (ps ! (length ps - 1)) = y \<and>
         distinct ps \<and>
         (\<forall>i<length ps - 1. h\<^sub>v (ps ! i) = Some (value.non_null (fst (ps ! Suc i))))" by blast
qed

lemma (in heap) reaches_upd_other:
  assumes
    "\<not> reaches h \<iota> \<iota>'"
  shows
    "reaches (h ((\<iota>', f) := v)) \<iota> \<iota>'' = reaches h \<iota> \<iota>''"
  unfolding reaches_def
  by rule (induction rule:rtranclp_induct, simp, metis (no_types) assms reaches\<^sub>1_upd_other reaches_def rtranclp.simps)+

definition (in heap) reaches\<^sub>v :: "'a h\<^sub>v \<Rightarrow> 'a v \<Rightarrow> 'a v \<Rightarrow> bool"
  where
    "reaches\<^sub>v h v\<^sub>1 v\<^sub>2 \<equiv> case v\<^sub>1 of
      null \<Rightarrow> False |
      non_null \<iota>\<^sub>1 \<Rightarrow> case v\<^sub>2 of
        null \<Rightarrow> False |
        non_null \<iota>\<^sub>2 \<Rightarrow> reaches h \<iota>\<^sub>1 \<iota>\<^sub>2"

lemma (in heap) reaches\<^sub>v_reaches:
  "reaches\<^sub>v h\<^sub>v v v' \<longleftrightarrow> v \<noteq> null \<and> v' \<noteq> null \<and> reaches h\<^sub>v (ref v) (ref v')"
  using reaches\<^sub>v_def by (simp add: value.case_eq_if)

lemma (in heap) reaches\<^sub>v_left_null: "\<not> reaches\<^sub>v h null v"
  using reaches\<^sub>v_def by simp

lemma (in heap) reaches\<^sub>v_right_null: "\<not> reaches\<^sub>v h v null"
  unfolding reaches\<^sub>v_def by (simp add: value.case_eq_if)

lemma (in heap) reaches\<^sub>v_upd_other:
  assumes
    "\<not> reaches\<^sub>v h v (non_null \<iota>)"
  shows
    "reaches\<^sub>v (h ((\<iota>, f) := v'')) v v' = reaches\<^sub>v h v v'"
  using assms reaches_upd_other reaches\<^sub>v_def by (cases v, cases v') simp_all

lemma (in heap) reaches\<^sub>v_source_dom\<^sub>v:
  assumes
    "reaches\<^sub>v h\<^sub>v v v'"
    "v \<noteq> v'"
  obtains f
  where
    "(ref v, f) \<in> dom h\<^sub>v"
  using assms unfolding reaches\<^sub>v_def using reaches_source_dom\<^sub>v
  by (metis value.case_eq_if value.expand)

lemma (in heap) reaches\<^sub>v_source_dom:
  assumes
    "reaches\<^sub>v (\<chi>\<^sub>v h) v v'"
    "v \<noteq> v'"
    "wf_heap_dom h"
  shows
    "ref v \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>v_def using reaches_source_dom by (cases v, cases v') simp_all

lemma (in heap) reaches\<^sub>v_target_dom:
  assumes
    "reaches\<^sub>v (\<chi>\<^sub>v h) v v'"
    "ref v \<in> dom (\<chi>\<^sub>c h)"
    "wf_heap_image h"
  shows
    "ref v' \<in> dom (\<chi>\<^sub>c h)"
  using assms unfolding reaches\<^sub>v_def using reaches_target_dom by (cases v, cases v') simp_all

lemma (in heap) reaches\<^sub>v_trans:
  assumes
    "reaches\<^sub>v h\<^sub>v v v'"
    "reaches\<^sub>v h\<^sub>v v' v''"
  shows
    "reaches\<^sub>v h\<^sub>v v v''"
  using assms unfolding reaches\<^sub>v_def reaches_def by (metis rtranclp_trans value.case_eq_if)

lemma (in heap) reaches\<^sub>v_alloc_from_new:
  assumes
    "reaches\<^sub>v (\<chi>\<^sub>v h') (non_null \<iota>) v'"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom_weak h"
  shows
    "non_null \<iota> = v'"
  using assms using reaches\<^sub>v_reaches reaches_alloc_from_new by (cases v') simp_all

lemma (in heap) reaches\<^sub>v_alloc_to_new:
  assumes
    "reaches\<^sub>v (\<chi>\<^sub>v h') v (non_null \<iota>')"
    "unbounded_heap h"
    "alloc h C = (h', \<iota>')"
    "wf_heap_dom_weak h"
    "wf_heap_image h"
  shows
    "v = non_null \<iota>'"
  using assms using reaches\<^sub>v_reaches reaches_alloc_to_new by (cases v) simp_all

lemma (in heap) reaches\<^sub>v\<^sub>_trace:
  assumes
    "x \<noteq> null"
    "y \<noteq> null"
  shows
    "reaches\<^sub>v h\<^sub>v x y \<longleftrightarrow>
      (\<exists> ps.
        0 < length ps \<and>
        fst (nth ps 0) = ref x \<and>
        fst (nth ps (length ps - 1)) = ref y \<and>
        (\<forall> i < length ps - 1. h\<^sub>v (nth ps i) = Some (non_null (fst (nth ps (Suc i))))))"
  using assms reaches\<^sub>_trace reaches\<^sub>v_reaches by auto

lemma (in heap) reaches\<^sub>v_alloc:
  assumes
    "unbounded_heap h"
    "alloc h C = (h', \<iota>)"
    "wf_heap_dom h"
    "wf_heap_image h"
  shows
    "reaches\<^sub>v (\<chi>\<^sub>v h) s t = reaches\<^sub>v (\<chi>\<^sub>v h') s t"
  using assms reaches\<^sub>v_reaches reaches_alloc by auto

lemma (in heap) reaches\<^sub>v\<^sub>_trace\<^sub>v:
  shows
    "reaches\<^sub>v h\<^sub>v x y \<longleftrightarrow>
      (\<exists> ps.
        0 < length ps \<and>
        fst (nth ps 0) = x \<and>
        fst (nth ps (length ps - 1)) = y \<and>
        (\<forall> i < length ps. fst (nth ps i) \<noteq> null) \<and>
        (\<forall> i < length ps - 1. h\<^sub>v (ref (fst (nth ps i)), snd (nth ps i)) = Some (fst (nth ps (Suc i)))))"
  by rule (insert reaches\<^sub>_trace\<^sub>v reaches\<^sub>v_reaches, auto)

definition (in heap) "reaches\<^sub>V\<^sub>v" :: "'a h\<^sub>v \<Rightarrow> 'a v set \<Rightarrow> 'a v \<Rightarrow> bool"
  where
    [simp]: "reaches\<^sub>V\<^sub>v h\<^sub>v V v \<equiv> \<exists> v' \<in> V. reaches\<^sub>v h\<^sub>v v' v"

lemma (in heap) reaches\<^sub>V\<^sub>v_I:  "v \<in> V \<Longrightarrow> reaches\<^sub>v h\<^sub>v v v' \<Longrightarrow> reaches\<^sub>V\<^sub>v h\<^sub>v V v'"
  by auto

lemma (in heap) reaches\<^sub>V\<^sub>v_empty: "\<not> reaches\<^sub>V\<^sub>v h\<^sub>v {} v"
  by simp

lemma (in heap) reaches\<^sub>V\<^sub>v_right_null: "\<not> reaches\<^sub>V\<^sub>v h\<^sub>v V null"
  using reaches\<^sub>v_right_null by simp

definition (in heap) "reaches\<^sub>v\<^sub>V" :: "'a h\<^sub>v \<Rightarrow> 'a v \<Rightarrow> 'a v set \<Rightarrow> bool"
  where
    [simp]: "reaches\<^sub>v\<^sub>V h\<^sub>v v V \<equiv> \<exists> v' \<in> V. reaches\<^sub>v h\<^sub>v v v'"

lemma (in heap) reaches\<^sub>v\<^sub>V_I:  "v \<in> V \<Longrightarrow> reaches\<^sub>v h\<^sub>v v' v \<Longrightarrow> reaches\<^sub>v\<^sub>V h\<^sub>v v' V"
  by auto

lemma (in heap) reaches\<^sub>v\<^sub>V_empty: "\<not> reaches\<^sub>v\<^sub>V h\<^sub>v v {}"
  by simp

lemma (in heap) reaches\<^sub>v\<^sub>V_left_null: "\<not> reaches\<^sub>v\<^sub>V h\<^sub>v null V"
  using reaches\<^sub>v_left_null by simp

(*
lemma (in heap) "wf_heap_dom h \<Longrightarrow> wf_heap_image h \<Longrightarrow> \<iota> \<in> dom (\<chi>\<^sub>c h) \<Longrightarrow> \<iota>' \<in> reachable (\<chi>\<^sub>v h) \<iota> \<Longrightarrow> \<iota>' \<in> dom (\<chi>\<^sub>c h)"
  unfolding \<chi>\<^sub>c_def \<chi>\<^sub>v_def reachable_def reaches_def reaches\<^sub>1_def wf_heap_dom_def wf_heap_image_def2
*)

end